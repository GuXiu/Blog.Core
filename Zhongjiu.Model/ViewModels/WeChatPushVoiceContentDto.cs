﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zhongjiu.Model
{
    public class WeChatPushVoiceContentDto
    {
        /// <summary>
        /// 语音mediaID
        /// </summary>
        public string voiceMediaID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zhongjiu.Model
{
    public class WeChatPushPictureContentDto
    {
        /// <summary>
        /// 图片mediaID
        /// </summary>
        public string pictureMediaID { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Zhongjiu.Model
{
  /// <summary>
  /// 微信OpenID列表Dto
  /// </summary>
  public class WeChatOpenIDsDto
    {
        public List<string> openid { get; set; } = new List<string>();
    }
}

﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///经营类别申请类目详细表（弃用）
    ///</summary>

    [SugarTable("Himall_BusinessCategoriesApplyDetail","Zhongjiu")]
    public partial class BusinessCategoriesApplyDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分佣比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisRate {get;set;}

           /// <summary>
           /// Desc:类目ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:申请Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ApplyId {get;set;}

    }
}
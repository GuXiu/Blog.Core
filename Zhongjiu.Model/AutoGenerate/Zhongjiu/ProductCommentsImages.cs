﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品评论图片表
    ///</summary>

    [SugarTable("Himall_ProductCommentsImages","Zhongjiu")]
    public partial class ProductCommentsImages
    {
           /// <summary>
           /// Desc:自增物理主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:评论图片
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CommentImage {get;set;}

           /// <summary>
           /// Desc:评论ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CommentId {get;set;}

           /// <summary>
           /// Desc:评论类型（首次评论/追加评论）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CommentType {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///管理员表
    ///</summary>

    [SugarTable("Himall_Managers","Zhongjiu")]
    public partial class Managers
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:角色ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long RoleId {get;set;}

           /// <summary>
           /// Desc:用户名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Password {get;set;}

           /// <summary>
           /// Desc:密码加盐
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PasswordSalt {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:真实名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RealName {get;set;}

           /// <summary>
           /// Desc:部门ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:账套
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:金蝶ERP对接，销售员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EasKingdeeSalePerson {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:是否添加为导购员
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsAddedTradeAssistant {get;set;}

           /// <summary>
           /// Desc:关联配送中心
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionShopId {get;set;}

    }
}
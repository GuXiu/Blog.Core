﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品名索引表
    ///</summary>

    [SugarTable("Himall_IndexName","Zhongjiu")]
    public partial class IndexName
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string Word {get;set;}

           /// <summary>
           /// Desc:
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? Count {get;set;}

           /// <summary>
           /// Desc:0,标题，1分类，2品牌
           /// Default:0
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public int Type {get;set;}

           /// <summary>
           /// Desc:权重
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Weights {get;set;}

           /// <summary>
           /// Desc:拼音
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PinYinAll {get;set;}

           /// <summary>
           /// Desc:拼音首字母
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PinYinFirst {get;set;}

           /// <summary>
           /// Desc:是否是手动维护数据 0 不是 ；1 是
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsCustomName {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单表
    ///</summary>

    [SugarTable("Himall_Orders","Zhongjiu")]
    public partial class Orders
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单状态 [Description('待付款')]WaitPay = 1,[Description('待发货')]WaitDelivery,[Description('待收货')]WaitReceiving,[Description('已关闭')]Close,[Description('已完成')]Finish
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderStatus {get;set;}

           /// <summary>
           /// Desc:订单创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderDate {get;set;}

           /// <summary>
           /// Desc:关闭原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CloseReason {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:商家电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SellerPhone {get;set;}

           /// <summary>
           /// Desc:商家发货地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SellerAddress {get;set;}

           /// <summary>
           /// Desc:商家说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SellerRemark {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:会员名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:会员留言
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserRemark {get;set;}

           /// <summary>
           /// Desc:收货人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShipTo {get;set;}

           /// <summary>
           /// Desc:收货人电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CellPhone {get;set;}

           /// <summary>
           /// Desc:收货人地址省份ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TopRegionId {get;set;}

           /// <summary>
           /// Desc:收货人区域ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RegionId {get;set;}

           /// <summary>
           /// Desc:全名的收货地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RegionFullName {get;set;}

           /// <summary>
           /// Desc:收货具体街道信息
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:快递公司
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ExpressCompanyName {get;set;}

           /// <summary>
           /// Desc:运费
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Freight {get;set;}

           /// <summary>
           /// Desc:运费来源  [0:未知,1:快递,2:自配送]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int FreightSource {get;set;}

           /// <summary>
           /// Desc:物流订单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShipOrderNumber {get;set;}

           /// <summary>
           /// Desc:发货日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ShippingDate {get;set;}

           /// <summary>
           /// Desc:是否打印快递单
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsPrinted {get;set;}

           /// <summary>
           /// Desc:付款类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentTypeName {get;set;}

           /// <summary>
           /// Desc:付款类型使用 插件名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentTypeGateway {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PaymentType {get;set;}

           /// <summary>
           /// Desc:支付接口返回的ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GatewayOrderId {get;set;}

           /// <summary>
           /// Desc:付款注释
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayRemark {get;set;}

           /// <summary>
           /// Desc:付款日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PayDate {get;set;}

           /// <summary>
           /// Desc:发票类型  1.纸质普通发票(个人)，2.电子普通发票(个人)，3.纸质普通发票(公司)，4.电子普通发票(公司)，5.纸质专用发票(公司)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int InvoiceType {get;set;}

           /// <summary>
           /// Desc:发票抬头
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoiceTitle {get;set;}

           /// <summary>
           /// Desc:税钱，但是未使用
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

           /// <summary>
           /// Desc:完成订单日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? FinishDate {get;set;}

           /// <summary>
           /// Desc:商品总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotalAmount {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundTotalAmount {get;set;}

           /// <summary>
           /// Desc:佣金总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisTotalAmount {get;set;}

           /// <summary>
           /// Desc:退还佣金总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundCommisAmount {get;set;}

           /// <summary>
           /// Desc:未使用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActiveType {get;set;}

           /// <summary>
           /// Desc:来自哪个终端的订单
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Platform {get;set;}

           /// <summary>
           /// Desc:针对该订单的优惠金额（用于优惠券）
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DiscountAmount {get;set;}

           /// <summary>
           /// Desc:积分优惠金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal IntegralDiscount {get;set;}

           /// <summary>
           /// Desc:发票明细
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoiceContext {get;set;}

           /// <summary>
           /// Desc:订单类型 [1:组合购][2:限时购][3:拼团][4:礼券]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? OrderType {get;set;}

           /// <summary>
           /// Desc:分销员Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShareUserId {get;set;}

           /// <summary>
           /// Desc:佣金结算类型：1平台结算，2店铺手动结算
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DistributorPayType {get;set;}

           /// <summary>
           /// Desc:订单备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrderRemarks {get;set;}

           /// <summary>
           /// Desc:最后操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastModifyTime {get;set;}

           /// <summary>
           /// Desc:营销优惠金额（商品满减、订单满减）
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? ReduceMoney {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrdersCode {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Sources {get;set;}

           /// <summary>
           /// Desc:标记状态，默认0
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TagStatus {get;set;}

           /// <summary>
           /// Desc:标记的内容或描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TagContent {get;set;}

           /// <summary>
           /// Desc:积分兑换总数量（积分优惠数量）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IntegralDiscountNumber {get;set;}

           /// <summary>
           /// Desc:实收金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ReceiveAmount {get;set;}

           /// <summary>
           /// Desc:找零
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal RefundChange {get;set;}

           /// <summary>
           /// Desc:会员卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardNumber {get;set;}

           /// <summary>
           /// Desc:会员卡优惠金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipCardFavorable {get;set;}

           /// <summary>
           /// Desc:中酒币抵金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CoinFavorable {get;set;}

           /// <summary>
           /// Desc:加减价，改价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DisPrice {get;set;}

           /// <summary>
           /// Desc:订单渠道:0线上、1线下
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int OrderChannel {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Creater {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:商品数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ProductCount {get;set;}

           /// <summary>
           /// Desc:体系积分抵扣金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AccountSetIntegralReduceAmount {get;set;}

           /// <summary>
           /// Desc:体系积分抵扣数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AccountSetIntegralReduceCount {get;set;}

           /// <summary>
           /// Desc:体系积分兑换数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AccountSetIntegralExchangeCount {get;set;}

           /// <summary>
           /// Desc:体系积分兑换金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AccountSetIntegralExchangeAmount {get;set;}

           /// <summary>
           /// Desc:金蝶ERP对接，自定义订单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EasKingdeeOrderNumber {get;set;}

           /// <summary>
           /// Desc:推送时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EasKingdeePushTime {get;set;}

           /// <summary>
           /// Desc:销售类型[0:默认,零售订单][1:团购订单][2:批发订单]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SalesType {get;set;}

           /// <summary>
           /// Desc:下单类型[0:默认,零售订单][1:临单(挂单)][2:赊账]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int PlaceType {get;set;}

           /// <summary>
           /// Desc:营业员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TradeAssistantId {get;set;}

           /// <summary>
           /// Desc:配送方式[0:未选择配送方式,1:送货,2自提,3现取]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DistributionType {get;set;}

           /// <summary>
           /// Desc:配送店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionShopId {get;set;}

           /// <summary>
           /// Desc:配送单Id(如果是配送中心发货)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionDeliveryId {get;set;}

           /// <summary>
           /// Desc:发货方式[0:未设置发货方式][1:快递][2:配送]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SendGoodType {get;set;}

           /// <summary>
           /// Desc:最迟付款时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PayDateAtLatest {get;set;}

           /// <summary>
           /// Desc:订单会员折扣率
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public float? VipDiscountRate {get;set;}

           /// <summary>
           /// Desc:配送方式[0:未选择配送方式,1:送货,2自提,3现取](废弃)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte DeliveryType {get;set;}

           /// <summary>
           /// Desc:打包费
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PackagingAmount {get;set;}

           /// <summary>
           /// Desc:配送员Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? DeliveryId {get;set;}

           /// <summary>
           /// Desc:预计送达时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EstimatedDeliveryTime {get;set;}

    }
}
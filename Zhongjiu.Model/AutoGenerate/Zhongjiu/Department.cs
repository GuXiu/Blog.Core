﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///部门表
    ///</summary>

    [SugarTable("Himall_Department","Zhongjiu")]
    public partial class Department
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string Id {get;set;}

           /// <summary>
           /// Desc:部门名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:父节点
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ParentId {get;set;}

           /// <summary>
           /// Desc:瀑布流
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CascadeId {get;set;}

           /// <summary>
           /// Desc:可用性
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:状态(预留)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:账套
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:体系id（附近店铺）
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? SystemId {get;set;}

           /// <summary>
           /// Desc:体系Logo
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentLogo {get;set;}

           /// <summary>
           /// Desc:是否是测试体系
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsTest {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sort {get;set;}

    }
}
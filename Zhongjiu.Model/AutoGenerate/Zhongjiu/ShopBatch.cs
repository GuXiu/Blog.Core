﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺自动获取批次
    ///</summary>

    [SugarTable("ZJ_ShopBatch","Zhongjiu")]
    public partial class ShopBatch
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:所属体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSetId {get;set;}

           /// <summary>
           /// Desc:云码批次
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public string Batch {get;set;}

           /// <summary>
           /// Desc:是否自动获取
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsAutomaticAcquisition {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

    }
}
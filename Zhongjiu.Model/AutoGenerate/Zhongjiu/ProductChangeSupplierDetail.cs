﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品供应商变更详情表
    ///</summary>

    [SugarTable("ZJ_ProductChangeSupplierDetail","Zhongjiu")]
    public partial class ProductChangeSupplierDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Pid {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:含税进价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? JinJia1 {get;set;}

           /// <summary>
           /// Desc:不含税进价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? JinJia2 {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? JinJia3 {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
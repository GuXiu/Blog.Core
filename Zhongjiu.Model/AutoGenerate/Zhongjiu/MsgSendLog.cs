﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///公众号消息发送记录表
    ///</summary>

    [SugarTable("ZJ_MsgSendLog","Zhongjiu")]
    public partial class MsgSendLog
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:消息模板Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TemplateId {get;set;}

           /// <summary>
           /// Desc:消息类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MsgType {get;set;}

           /// <summary>
           /// Desc:跳转页面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:消息接收者Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Receiver {get;set;}

           /// <summary>
           /// Desc:消息点击次数
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ClickCount {get;set;}

           /// <summary>
           /// Desc:url携带的唯一标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Sign {get;set;}

           /// <summary>
           /// Desc:消息发送时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? SendDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UniqueNo {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///活动奖项
    ///</summary>

    [SugarTable("ZJ_LuckDrawActivityPrize","Zhongjiu")]
    public partial class LuckDrawActivityPrize
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:奖项名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PrizeName {get;set;}

           /// <summary>
           /// Desc:奖项数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PrizeNumber {get;set;}

           /// <summary>
           /// Desc:中奖率
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal WinningRate {get;set;}

           /// <summary>
           /// Desc:优先级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PrizePriority {get;set;}

           /// <summary>
           /// Desc:中奖提示文字
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PrizeTips {get;set;}

           /// <summary>
           /// Desc:活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ActivityId {get;set;}

           /// <summary>
           /// Desc:添加人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Cerator {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Modified {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime ModifyTime {get;set;}

           /// <summary>
           /// Desc:奖项状态(1正常 0删除)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:奖项顺序
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int PrizeItem {get;set;}

    }
}
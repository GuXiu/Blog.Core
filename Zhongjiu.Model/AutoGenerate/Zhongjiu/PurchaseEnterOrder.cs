﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购入库订单
    ///</summary>

    [SugarTable("ZJ_PurchaseEnterOrder","Zhongjiu")]
    public partial class PurchaseEnterOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PurchaseOrderCode {get;set;}

           /// <summary>
           /// Desc:门店Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:配送中心ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:供应商Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SupplierId {get;set;}

           /// <summary>
           /// Desc:含税总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:创建人(制单人)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:审核状态（0：未审核 1：已审核 2：已生效 3：已终止）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditState {get;set;}

           /// <summary>
           /// Desc:是否已结算（0：未结算  1：已结算）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsSettlement {get;set;}

           /// <summary>
           /// Desc:采购员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyerName {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间，默认和创建时间相同
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺等级表
    ///</summary>

    [SugarTable("Himall_ShopGrades","Zhongjiu")]
    public partial class ShopGrades
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺等级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:最大上传商品数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ProductLimit {get;set;}

           /// <summary>
           /// Desc:最大图片可使用空间数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ImageLimit {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TemplateLimit {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ChargeStandard {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:佣金
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Commission {get;set;}

    }
}
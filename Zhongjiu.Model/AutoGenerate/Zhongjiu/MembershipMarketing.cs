﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销表
    ///</summary>

    [SugarTable("ZJ_MembershipMarketing","Zhongjiu")]
    public partial class MembershipMarketing
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动标题名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:店铺Id集合(,逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:触发方式[0:不指定,>1:指定事件 会员生日=11,会员到店=12,注册周年=13,创建订单=14,订单付款=15,订单发货=16,订单派送=17,订单签收=18]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TriggerMode {get;set;}

           /// <summary>
           /// Desc:触发操作[1:短信,2:邮件,3:优惠券,4:积分,5:发微信图文] 多选逗号分隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TriggerOperation {get;set;}

           /// <summary>
           /// Desc:短信发送 内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShortMessageContent {get;set;}

           /// <summary>
           /// Desc:邮件标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MailTitle {get;set;}

           /// <summary>
           /// Desc:邮件内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MailContent {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id集合(,逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CouponIds {get;set;}

           /// <summary>
           /// Desc:积分 数量
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IntegralContent {get;set;}

           /// <summary>
           /// Desc:发微信图文类型，0:图文消息；1:文本消息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? WXMsgType {get;set;}

           /// <summary>
           /// Desc:微信图文素材Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinMediaId {get;set;}

           /// <summary>
           /// Desc:微信图文内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SendContent {get;set;}

           /// <summary>
           /// Desc:执行方式-循环 的开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CirculateBeginTime {get;set;}

           /// <summary>
           /// Desc:执行方式-循环 的结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CirculateEndTime {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:状态[1:即将开始,2:正在进行,3:已关闭,4:已结束]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Creater {get;set;}

           /// <summary>
           /// Desc:营销人群[1:店铺会员][2:会员人群]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CrowdMode {get;set;}

           /// <summary>
           /// Desc:执行方式[0:单次,1:循环]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ExecutionMode {get;set;}

           /// <summary>
           /// Desc:发送时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ExecutionTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///同步数据记录表
    ///</summary>

    [SugarTable("zj_synchronousdatarecording","Zhongjiu")]
    public partial class synchronousdatarecording
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? createdate {get;set;}

           /// <summary>
           /// Desc:更新截止时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? Deadline {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string remark {get;set;}

           /// <summary>
           /// Desc:0：失败；1：成功
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? upstate {get;set;}

    }
}
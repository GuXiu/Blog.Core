﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下订单App扫码支付表
    ///</summary>

    [SugarTable("OffLine_PaymentRecord","Zhongjiu")]
    public partial class OffLine_PaymentRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:线下商铺Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? OffLineShopId {get;set;}

           /// <summary>
           /// Desc:线下订单编号
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public string OffLineOrderCode {get;set;}

           /// <summary>
           /// Desc:支付码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayCode {get;set;}

           /// <summary>
           /// Desc:用户Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:时间戳
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TimeSpan {get;set;}

           /// <summary>
           /// Desc:唯一标示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OnlySign {get;set;}

           /// <summary>
           /// Desc:订单金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? Amount {get;set;}

           /// <summary>
           /// Desc:状态:5支付码过期，6支付码用户不存在，7支付码用户不可用，1余额不足
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下订单退货表
    ///</summary>

    [SugarTable("himall_OffLineOrderRefunds","Zhongjiu")]
    public partial class OffLineOrderRefunds
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ReturnQuantity {get;set;}

           /// <summary>
           /// Desc:退货总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:退款方式（1：现金 2：原路返回）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RefundType {get;set;}

           /// <summary>
           /// Desc:平台佣金退还总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ReturnPlatCommission {get;set;}

           /// <summary>
           /// Desc:退货单编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ReturnCode {get;set;}

           /// <summary>
           /// Desc:支付平台交易号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TansactionNum {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Creater {get;set;}

    }
}
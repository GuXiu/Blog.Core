﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///售后原因
    ///</summary>

    [SugarTable("Himall_RefundReason","Zhongjiu")]
    public partial class RefundReason
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:售后原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AfterSalesText {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:100
           /// Nullable:True
           /// </summary>           
           public int? Sequence {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员的收货地址表
    ///</summary>

    [SugarTable("Himall_ShippingAddresses","Zhongjiu")]
    public partial class ShippingAddresses
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:区域ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RegionId {get;set;}

           /// <summary>
           /// Desc:收货人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShipTo {get;set;}

           /// <summary>
           /// Desc:收货具体街道信息
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:收货人电话
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:是否为默认
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsDefault {get;set;}

           /// <summary>
           /// Desc:是否为轻松购地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsQuick {get;set;}

    }
}
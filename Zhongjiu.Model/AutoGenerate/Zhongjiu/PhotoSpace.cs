﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///图库表
    ///</summary>

    [SugarTable("Himall_PhotoSpace","Zhongjiu")]
    public partial class PhotoSpace
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:图片分组ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PhotoCategoryId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:图片名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PhotoName {get;set;}

           /// <summary>
           /// Desc:图片路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PhotoPath {get;set;}

           /// <summary>
           /// Desc:图片大小
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? FileSize {get;set;}

           /// <summary>
           /// Desc:图片上传时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UploadTime {get;set;}

           /// <summary>
           /// Desc:图片最后更新时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastUpdateTime {get;set;}

    }
}
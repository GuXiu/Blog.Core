﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品出入库表库存表
    ///</summary>

    [SugarTable("Himall_OffLineInventory","Zhongjiu")]
    public partial class OffLineInventory
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:单号(入库单入库，调拨入库，出库单出库，调拨出库)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderNumber {get;set;}

           /// <summary>
           /// Desc:调拨单号(调拨出入库 使用，其他为空)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SchedulingOrderNumber {get;set;}

           /// <summary>
           /// Desc:出货仓库(选择下拉框)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OutWarehouseId {get;set;}

           /// <summary>
           /// Desc:入货仓库(选择下拉框)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long InWarehouseId {get;set;}

           /// <summary>
           /// Desc:时间(入库单入库，调拨入库，出库单出库，调拨出库)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime Time {get;set;}

           /// <summary>
           /// Desc:[1:采购入库][2:调拨入库][4:调拨出库][5:订单出库][6:盘盈入库][7:盘亏出库][8:退单入库][9:配送出库][10:配送入库][11:配退][12:配退入库][13:配送差异入库][14:采购退货][15:所有出库单][16:所有入库单]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int InventoryType {get;set;}

           /// <summary>
           /// Desc:调拨状态(0进行中，1已完成，2未完成)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:出/入库员(创建人姓名)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CreaterPeople {get;set;}

           /// <summary>
           /// Desc:商品总数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotals {get;set;}

           /// <summary>
           /// Desc:商品总金额
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotalsAmount {get;set;}

           /// <summary>
           /// Desc:备注(可选)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remarks {get;set;}

           /// <summary>
           /// Desc:快递单号(可选)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CourierNumber {get;set;}

           /// <summary>
           /// Desc:支付方式(可选)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentMethod {get;set;}

           /// <summary>
           /// Desc:是否删除
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

           /// <summary>
           /// Desc:调拨入库操作人员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InCreaterPeople {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统类型表
    ///</summary>

    [SugarTable("Himall_Types","Zhongjiu")]
    public partial class Types
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:类型名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:是否支持颜色规格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsSupportColor {get;set;}

           /// <summary>
           /// Desc:是否支持尺寸规格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsSupportSize {get;set;}

           /// <summary>
           /// Desc:是否支持版本规格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsSupportVersion {get;set;}

           /// <summary>
           /// Desc:最后一次更新时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastAsyncDateTime {get;set;}

    }
}
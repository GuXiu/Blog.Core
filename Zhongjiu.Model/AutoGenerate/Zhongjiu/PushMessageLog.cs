﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///App消息推送记录表
    ///</summary>

    [SugarTable("ZJ_PushMessageLog","Zhongjiu")]
    public partial class PushMessageLog
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:推送的类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PushType {get;set;}

           /// <summary>
           /// Desc:会员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:推送的设备Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DeviceId {get;set;}

           /// <summary>
           /// Desc:推送的内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PushMessage {get;set;}

           /// <summary>
           /// Desc:入库时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:订单号，标识些订单已经推送过了
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:优惠券号，多个
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CouponOns {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题答案表
    ///</summary>

    [SugarTable("ZJ_CouponShareAnswer","Zhongjiu")]
    public partial class CouponShareAnswer
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:问题ID
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProblemId {get;set;}

           /// <summary>
           /// Desc:答案选项
           /// Default:null
           /// Nullable:True
           /// </summary>           
           public string Answer {get;set;}

           /// <summary>
           /// Desc:答案序号
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? SerialNumber {get;set;}

    }
}
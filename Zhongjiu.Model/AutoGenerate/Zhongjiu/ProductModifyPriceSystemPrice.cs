﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品调价单会员等级价调价明细
    ///</summary>

    [SugarTable("ZJ_ProductModifyPriceSystemPrice","Zhongjiu")]
    public partial class ProductModifyPriceSystemPrice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系会员等级Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MGradeId {get;set;}

           /// <summary>
           /// Desc:新会员等级价格
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MemberPrice {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:门店Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:调价单主键
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? TJId {get;set;}

           /// <summary>
           /// Desc:原会员等级价格
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? OldMPrice {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录
    ///</summary>

    [SugarTable("ZJ_MembershipMarketingRecord","Zhongjiu")]
    public partial class MembershipMarketingRecord
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员营销表Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MarketingId {get;set;}

           /// <summary>
           /// Desc:消息记录类型[1:短信,2:邮件,3:优惠券,4:积分,5:微信图文]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RecordType {get;set;}

           /// <summary>
           /// Desc:会员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:会员手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:邮件标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

           /// <summary>
           /// Desc:发送时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:发送状态[0:发送中,1:失败,2:成功]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:优惠券Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponRecordId {get;set;}

           /// <summary>
           /// Desc:要发送的，邮箱
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Email {get;set;}

           /// <summary>
           /// Desc:要发送的，手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:短信发送后的 回执编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PhoneSerialNumber {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Guid {get;set;}

    }
}
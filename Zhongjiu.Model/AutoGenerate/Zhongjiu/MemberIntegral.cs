﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员积分表
    ///</summary>

    [SugarTable("Himall_MemberIntegral","Zhongjiu")]
    public partial class MemberIntegral
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? MemberId {get;set;}

           /// <summary>
           /// Desc:用户名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:用户历史积分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int HistoryIntegrals {get;set;}

           /// <summary>
           /// Desc:用户可用积分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AvailableIntegrals {get;set;}

           /// <summary>
           /// Desc:体系ID 
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:是否允许升级
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? AllowHistoryAdd {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ExternalIntegrals {get;set;}

    }
}
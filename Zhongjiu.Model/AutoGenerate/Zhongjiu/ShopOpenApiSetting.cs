﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺OpenAPI秘钥
    ///</summary>

    [SugarTable("Himall_ShopOpenApiSetting","Zhongjiu")]
    public partial class ShopOpenApiSetting
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:app_key
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppKey {get;set;}

           /// <summary>
           /// Desc:app_secreat
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppSecreat {get;set;}

           /// <summary>
           /// Desc:增加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddDate {get;set;}

           /// <summary>
           /// Desc:最后重置时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastEditDate {get;set;}

           /// <summary>
           /// Desc:是否开启
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsEnable {get;set;}

           /// <summary>
           /// Desc:是否己注册
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsRegistered {get;set;}

           /// <summary>
           /// Desc:第三方名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AName {get;set;}

           /// <summary>
           /// Desc:体系
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

    }
}
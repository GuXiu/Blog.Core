﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置店铺信息
    ///</summary>

    [SugarTable("Himall_ShopEasKingdee","Zhongjiu")]
    public partial class ShopEasKingdee
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,销售组织
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleOrgUnit {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,销售组
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleGroup {get;set;}

           /// <summary>
           /// Desc:销售方库存组织(json 主录)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string StorageOrgUnitHost {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,发货组织(json 子录)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string StorageOrgUnit {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,送货客户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DeliveryCustomer {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,收款客户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentCustomer {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,应收客户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveCustomer {get;set;}

           /// <summary>
           /// Desc:订货客户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrderCustomer {get;set;}

           /// <summary>
           /// Desc:12月，每个月的扎帐结束日期，逗号分隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TieOffTime {get;set;}

           /// <summary>
           /// Desc:销售方仓库(仓库发货专用)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Warehouse {get;set;}

           /// <summary>
           /// Desc:供货组织(仓库发货专用)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProStorageOrgUnit {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销板块开关设置
    ///</summary>

    [SugarTable("Himall_DistributorSetting","Zhongjiu")]
    public partial class DistributorSetting
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:模块开关
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:商家规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SellerRule {get;set;}

           /// <summary>
           /// Desc:推广员规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PromoterRule {get;set;}

           /// <summary>
           /// Desc:分销市场banner
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DisBanner {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///费用设置表
    ///</summary>

    [SugarTable("ZJ_ExpenseSet","Zhongjiu")]
    public partial class ExpenseSet
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:区域中心ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:费用编码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ExpenseNo {get;set;}

           /// <summary>
           /// Desc:费用名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:创建人ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:父体系创建的费用设置
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

    }
}
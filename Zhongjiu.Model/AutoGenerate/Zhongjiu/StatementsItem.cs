﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///结算单明细
    ///</summary>

    [SugarTable("ZJ_StatementsItem","Zhongjiu")]
    public partial class StatementsItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:结算单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long StatementsId {get;set;}

           /// <summary>
           /// Desc:原始单号ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderNo {get;set;}

           /// <summary>
           /// Desc:单据类型（1：供应商费用单，2：供应商预付款单，3：供应商对账单，4：加盟门店费用单，5：加盟门店预付款单，6：加盟门店对账单，7：直营店费用单，8：直营店其它收入，9：直营店营业成本，10：直营店营业收入）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderType {get;set;}

           /// <summary>
           /// Desc:业务单金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

    }
}
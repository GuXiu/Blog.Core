﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单卡券记录表
    ///</summary>

    [SugarTable("ZJ_CardCode","Zhongjiu")]
    public partial class CardCode
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:卡券ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardId {get;set;}

           /// <summary>
           /// Desc:二维码Ticket
           /// Default:
           /// Nullable:True
           /// </summary>           
           [SugarColumn(ColumnName="CardCode")]
           public string CardCode1 {get;set;}

           /// <summary>
           /// Desc:生成二维码时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? GenerateTime {get;set;}

           /// <summary>
           /// Desc:使用状态（0未使用,1已使用,2已退货）
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? State {get;set;}

           /// <summary>
           /// Desc:OpenId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺首页的楼层模板商品表
    ///</summary>

    [SugarTable("Himall_ShopHomeModuleProducts","Zhongjiu")]
    public partial class ShopHomeModuleProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long HomeModuleId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int DisplaySequence {get;set;}

           /// <summary>
           /// Desc:选项卡ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TabId {get;set;}

    }
}
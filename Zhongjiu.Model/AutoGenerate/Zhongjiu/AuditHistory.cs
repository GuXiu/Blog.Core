﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///单据审核记录表
    ///</summary>

    [SugarTable("ZJ_AuditHistory","Zhongjiu")]
    public partial class AuditHistory
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:是否审核通过
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsPassed {get;set;}

           /// <summary>
           /// Desc:审核说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Auditer {get;set;}

           /// <summary>
           /// Desc:表单类型(1:要货申请单，2:配送单，3:配送入库单，4:配退单，5:配退收货单，6:采购单，7:采购入库单，8:采退单)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AuditTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销商品表
    ///</summary>

    [SugarTable("Himall_ProductBrokerage","Zhongjiu")]
    public partial class ProductBrokerage
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:百分比
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal rate {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:成交数 *是卖出的数量，还是成交订单数，退货时是否需要维护
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SaleNum {get;set;}

           /// <summary>
           /// Desc:代理数 *清退的时候是否需要维护此字段
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? AgentNum {get;set;}

           /// <summary>
           /// Desc:转发数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ForwardNum {get;set;}

           /// <summary>
           /// Desc:状态 上架、下架、移除
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Sort {get;set;}

           /// <summary>
           /// Desc:销售额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal saleAmount {get;set;}

           /// <summary>
           /// Desc:商品已结佣金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal BrokerageAmount {get;set;}

           /// <summary>
           /// Desc:商品佣金(包含已结未结)
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal BrokerageTotal {get;set;}

    }
}
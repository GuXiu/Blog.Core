﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员OpenID备份表
    ///</summary>

    [SugarTable("Himall_MemberOpenIds_qqbak","Zhongjiu")]
    public partial class MemberOpenIds_qqbak
    {
           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:微信OpenID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:开发平台Openid
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UnionOpenId {get;set;}

           /// <summary>
           /// Desc:开发平台Unionid
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UnionId {get;set;}

           /// <summary>
           /// Desc:插件名称（Himall.Plugin.OAuth.WeiXin）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ServiceProvider {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AppIdType {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime AddDate {get;set;}

    }
}
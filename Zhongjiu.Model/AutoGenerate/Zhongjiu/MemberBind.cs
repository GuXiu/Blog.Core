﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///第三方平台与商城用户绑定关系表
    ///</summary>

    [SugarTable("Himall_MemberBind","Zhongjiu")]
    public partial class MemberBind
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:平台用户昵称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string NickName {get;set;}

           /// <summary>
           /// Desc:平台Id。1.淘宝
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PlatId {get;set;}

           /// <summary>
           /// Desc:绑定时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateDate {get;set;}

           /// <summary>
           /// Desc:是否可用
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? Enable {get;set;}

    }
}
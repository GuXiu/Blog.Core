﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///群发分享 领取记录
    ///</summary>

    [SugarTable("ZJ_CouponShareBatchRecord","Zhongjiu")]
    public partial class CouponShareBatchRecord
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:ZJ_CouponShareBatchGroup表 Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CsbgId {get;set;}

           /// <summary>
           /// Desc:ZJ_CouponShareBatchDetail表 Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CsbdId {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:优惠券 券码Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponRecordId {get;set;}

           /// <summary>
           /// Desc:用户微信昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveNickName {get;set;}

           /// <summary>
           /// Desc:头像图片地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveHeadImgUrl {get;set;}

           /// <summary>
           /// Desc:先获取 微信信息，检测注册的 直接发到账号，未注册的 需要注册后发到账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveOpenId {get;set;}

           /// <summary>
           /// Desc:会员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ReceiveUserId {get;set;}

           /// <summary>
           /// Desc:用户手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceivePhone {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:领取时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ReceiveTime {get;set;}

           /// <summary>
           /// Desc:[0:未领取][1:已领取][2:已收回]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceiveState {get;set;}

           /// <summary>
           /// Desc:当与订单有关联时，保存下此订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:当与订单有关联时，保存下此订单 item 详情表 Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderItemId {get;set;}

    }
}
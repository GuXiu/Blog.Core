﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///推广员海报
    ///</summary>

    [SugarTable("ZJ_PromoterPoster","Zhongjiu")]
    public partial class PromoterPoster
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long id {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopID {get;set;}

           /// <summary>
           /// Desc:是否显示营销活动
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? ShowMarketingActivities {get;set;}

           /// <summary>
           /// Desc:是否显示营销活动商品
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? ShowActivitiesProducts {get;set;}

           /// <summary>
           /// Desc:是否显示店铺地址
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? ShowShopAddress {get;set;}

           /// <summary>
           /// Desc:是否显示店铺联系方式
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? ShowShopContact {get;set;}

           /// <summary>
           /// Desc:广告词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AdvertisingWords {get;set;}

    }
}
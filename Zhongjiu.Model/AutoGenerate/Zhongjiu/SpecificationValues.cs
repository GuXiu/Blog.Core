﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///规格值表
    ///</summary>

    [SugarTable("Himall_SpecificationValues","Zhongjiu")]
    public partial class SpecificationValues
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:规格名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Specification {get;set;}

           /// <summary>
           /// Desc:类型ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TypeId {get;set;}

           /// <summary>
           /// Desc:规格值
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Value {get;set;}

    }
}
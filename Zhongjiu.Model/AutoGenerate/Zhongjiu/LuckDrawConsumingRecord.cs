﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///抽奖记录表
    ///</summary>

    [SugarTable("ZJ_LuckDrawConsumingRecord","Zhongjiu")]
    public partial class LuckDrawConsumingRecord
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ActivityId {get;set;}

           /// <summary>
           /// Desc:奖项ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PrizeId {get;set;}

           /// <summary>
           /// Desc:奖品明细ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AwardsItemId {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:奖品内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Content {get;set;}

           /// <summary>
           /// Desc:每次抽奖唯一标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CodeId {get;set;}

    }
}
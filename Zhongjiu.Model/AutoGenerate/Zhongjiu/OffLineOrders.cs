﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下订单表
    ///</summary>

    [SugarTable("himall_OffLineOrders","Zhongjiu")]
    public partial class OffLineOrders
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ProductCount {get;set;}

           /// <summary>
           /// Desc:商品总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotalAmount {get;set;}

           /// <summary>
           /// Desc:实收金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ReceiveAmount {get;set;}

           /// <summary>
           /// Desc:找零
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundChange {get;set;}

           /// <summary>
           /// Desc:佣金总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisTotalAmount {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundTotalAmount {get;set;}

           /// <summary>
           /// Desc:退还佣金总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundCommisAmount {get;set;}

           /// <summary>
           /// Desc:支付方式(1=现金支付 2=微信支付 3=支付宝支付)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PayType {get;set;}

           /// <summary>
           /// Desc:订单创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderDate {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Creater {get;set;}

           /// <summary>
           /// Desc:支付平台交易号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TansactionNum {get;set;}

           /// <summary>
           /// Desc:订单状态 [Description('待付款')]WaitPay = 1,[Description('待发货')]WaitDelivery,[Description('待收货')]WaitReceiving,[Description('已关闭')]Close,[Description('已完成')]Finish
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderStatus {get;set;}

           /// <summary>
           /// Desc:标记状态，默认0
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TagStatus {get;set;}

           /// <summary>
           /// Desc:标记的内容或描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TagContent {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long VipId {get;set;}

           /// <summary>
           /// Desc:会员卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardNumber {get;set;}

           /// <summary>
           /// Desc:订单满额优惠金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? OrderDiscount {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? VipCardFavorable {get;set;}

           /// <summary>
           /// Desc:中酒币抵金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CoinFavorable {get;set;}

           /// <summary>
           /// Desc:线上余额支付用户Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MemberId {get;set;}

           /// <summary>
           /// Desc:加减价，改价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DisPrice {get;set;}

           /// <summary>
           /// Desc:活动ID，多个活动逗号分隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ActivicyIds {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
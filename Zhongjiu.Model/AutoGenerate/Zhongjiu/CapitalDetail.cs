﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员资金流水表
    ///</summary>

    [SugarTable("Himall_CapitalDetail","Zhongjiu")]
    public partial class CapitalDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:资产主表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CapitalID {get;set;}

           /// <summary>
           /// Desc:资产类型:1领取红包，2充值，3提现，4消费，5退款，6推广佣金收入，7拼团返现收入
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SourceType {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:来源数据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SourceData {get;set;}

           /// <summary>
           /// Desc:交易时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:操作后会员余额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CurrentBalance {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

    }
}
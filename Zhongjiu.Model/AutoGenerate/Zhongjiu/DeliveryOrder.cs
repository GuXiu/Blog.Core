﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配送单
    ///</summary>

    [SugarTable("ZJ_DeliveryOrder","Zhongjiu")]
    public partial class DeliveryOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:配送单号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:要货单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductApplyCode {get;set;}

           /// <summary>
           /// Desc:收货店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:配送中心ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:含税总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:配送状态（0：未发货，1：已发货，2：已收货）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceivingState {get;set;}

           /// <summary>
           /// Desc:审核状态（0：未审核 1：已审核 2：已生效 3：已终止 4：已完成）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditState {get;set;}

           /// <summary>
           /// Desc:配送单类型（0：配送单，1：配送出库单）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:配送出库单对应的配送单ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间，默认和创建时间相同
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:是否生成出库单
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsAddOuterOrder {get;set;}

           /// <summary>
           /// Desc:是否生成入库单
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsAddEnterOrder {get;set;}

           /// <summary>
           /// Desc:收货人(存用户名)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Receiver {get;set;}

           /// <summary>
           /// Desc:收货地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:是否打印过
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? IsPrint {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信公众号资料表
    ///</summary>

    [SugarTable("ZJ_WechatManager","Zhongjiu")]
    public partial class WechatManager
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:网站地址，唯一标识
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RouteName {get;set;}

           /// <summary>
           /// Desc:微信公众号OpenId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:开发者ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppId {get;set;}

           /// <summary>
           /// Desc:开发者密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppSecret {get;set;}

           /// <summary>
           /// Desc:令牌(Token)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Token {get;set;}

           /// <summary>
           /// Desc:消息加解密密钥
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EncodingAESKey {get;set;}

           /// <summary>
           /// Desc:公众号的全局唯一接口调用凭据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccessToken {get;set;}

           /// <summary>
           /// Desc:AccessToken 过期日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AccessTokenOutTime {get;set;}

           /// <summary>
           /// Desc:微信Ticket
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JsapiTicket {get;set;}

           /// <summary>
           /// Desc:Ticket 过期日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? JsapiTicketOutTime {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除,false正常，true删除
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
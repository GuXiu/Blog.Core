﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户步数兑换记录
    ///</summary>

    [SugarTable("ZJ_MemberStepsExchangeRecord","Zhongjiu")]
    public partial class MemberStepsExchangeRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? WeChatUserId {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? MemberId {get;set;}

           /// <summary>
           /// Desc:兑换时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ExchangeTime {get;set;}

           /// <summary>
           /// Desc:兑换步数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ExchangeSteps {get;set;}

           /// <summary>
           /// Desc:兑换获得的积分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ExchangeIntegral {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单金额小数操作记录（删除）
    ///</summary>

    [SugarTable("Himall_OrderAmountFractional","Zhongjiu")]
    public partial class OrderAmountFractional
    {
           /// <summary>
           /// Desc:自增ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopId {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrderId {get;set;}

           /// <summary>
           /// Desc:订单明细ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrderItemId {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:类型：1订单金额小数改价
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

    }
}
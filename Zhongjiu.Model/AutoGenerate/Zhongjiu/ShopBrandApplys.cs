﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商家品牌申请表
    ///</summary>

    [SugarTable("Himall_ShopBrandApplys","Zhongjiu")]
    public partial class ShopBrandApplys
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:商家Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:品牌Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? BrandId {get;set;}

           /// <summary>
           /// Desc:品牌名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BrandName {get;set;}

           /// <summary>
           /// Desc:品牌Logo
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Logo {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:品牌授权证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AuthCertificate {get;set;}

           /// <summary>
           /// Desc:申请类型 枚举 BrandApplyMode
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ApplyMode {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:审核状态 枚举 BrandAuditStatus
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:操作时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ApplyTime {get;set;}

           /// <summary>
           /// Desc:品牌Banner
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Banner {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///我要推广设置表
    ///</summary>

    [SugarTable("Himall_InviteRule","Zhongjiu")]
    public partial class InviteRule
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:邀请能获得的积分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? InviteIntegral {get;set;}

           /// <summary>
           /// Desc:被邀请能获得的积分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? RegIntegral {get;set;}

           /// <summary>
           /// Desc:分享标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareTitle {get;set;}

           /// <summary>
           /// Desc:分享详细
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareDesc {get;set;}

           /// <summary>
           /// Desc:分享图标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareIcon {get;set;}

           /// <summary>
           /// Desc:分享规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareRule {get;set;}

           /// <summary>
           /// Desc:分享背景图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareBackgroundImg {get;set;}

    }
}
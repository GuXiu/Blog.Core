﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券领取区域
    ///</summary>

    [SugarTable("Himall_CouponArea","Zhongjiu")]
    public partial class CouponArea
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:区域Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long AreaId {get;set;}

           /// <summary>
           /// Desc:上级Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long AreaPId {get;set;}

           /// <summary>
           /// Desc:区域名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AreaName {get;set;}

           /// <summary>
           /// Desc:类型[0:领取区域][1:收货地址区域]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AreaType {get;set;}

    }
}
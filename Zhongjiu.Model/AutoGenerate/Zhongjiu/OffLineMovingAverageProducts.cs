﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品调移动平均价表
    ///</summary>

    [SugarTable("Himall_OffLineMovingAverageProducts","Zhongjiu")]
    public partial class OffLineMovingAverageProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MovingId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:先前移动平均价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PrePrice {get;set;}

           /// <summary>
           /// Desc:当前移动平均价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CurrentPrice {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销申请设置表
    ///</summary>

    [SugarTable("Himall_RecruitSetting","Zhongjiu")]
    public partial class RecruitSetting
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:招募开关
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:手机是否必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte MustMobile {get;set;}

           /// <summary>
           /// Desc:地址必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte MustAddress {get;set;}

           /// <summary>
           /// Desc:姓名必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte MustRealName {get;set;}

    }
}
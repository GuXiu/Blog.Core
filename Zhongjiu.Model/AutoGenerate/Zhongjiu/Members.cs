﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员表
    ///</summary>

    [SugarTable("Himall_Members","Zhongjiu")]
    public partial class Members
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Password {get;set;}

           /// <summary>
           /// Desc:密码加盐
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PasswordSalt {get;set;}

           /// <summary>
           /// Desc:昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Nick {get;set;}

           /// <summary>
           /// Desc:微信昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WxNick {get;set;}

           /// <summary>
           /// Desc:性别  0 未知；1 男；2 女
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Sex {get;set;}

           /// <summary>
           /// Desc:邮件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Email {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateDate {get;set;}

           /// <summary>
           /// Desc:省份ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TopRegionId {get;set;}

           /// <summary>
           /// Desc:省市区ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RegionId {get;set;}

           /// <summary>
           /// Desc:真实姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RealName {get;set;}

           /// <summary>
           /// Desc:电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CellPhone {get;set;}

           /// <summary>
           /// Desc:QQ
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QQ {get;set;}

           /// <summary>
           /// Desc:街道地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:是否禁用
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte Disabled {get;set;}

           /// <summary>
           /// Desc:最后登录日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime LastLoginDate {get;set;}

           /// <summary>
           /// Desc:下单次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderNumber {get;set;}

           /// <summary>
           /// Desc:未使用
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Expenditure {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Points {get;set;}

           /// <summary>
           /// Desc:头像
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Photo {get;set;}

           /// <summary>
           /// Desc:商家父账号ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentSellerId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:支付密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayPwd {get;set;}

           /// <summary>
           /// Desc:支付密码加密字符
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayPwdSalt {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? InviteUserId {get;set;}

           /// <summary>
           /// Desc:分销员Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShareUserId {get;set;}

           /// <summary>
           /// Desc:是否绑定手机号
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsMobile {get;set;}

           /// <summary>
           /// Desc:注册平台(1.网站 2.弹窗 3.网站联合登录 4.弹窗联合登录 5.wap 6.wap联合登录 7.ios 8.ios联合登录 9.android 10.android联合登录 11.微信小程序)；100：pc短信注册； 101：h5短信注册；102 pc收银台注册；103：收银app注册 104：openapi领优惠券注册用户  105：中酒app注册用户 106 openapi添加积分注册  107 阿里智慧门店注册  108 机构注册门店   109.webform3导入会员；110 pc页面注册；111一步之邀注册  112门店新增收银员 、113 M站领优惠券 默认注册、114 商家新增会员 、115 平台新增会员、116云码绑定分销员时分销员不是用户 、117 商家新增分销员、118 平台新增分销员、119 商家-推广员绑定会员、120 商家-推广员导入绑定会员、121 平台-推广员绑定会员、122 平台-推广员导入绑定会员、123 商家-导购员绑定会员、124 商家-导购员导入绑定会员、125 平台-导购员绑定会员、126 平台-导购员导入绑定会员、129 m站分销员新增会员、130  微信小程序分销员绑定会员、131 支付宝小程序分销员绑定会员、132 支付宝小程序注册
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RegisterPlat {get;set;}

           /// <summary>
           /// Desc:出生日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? Birthday {get;set;}

           /// <summary>
           /// Desc:身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IdCard {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CityId {get;set;}

           /// <summary>
           /// Desc:是否是短信验证码注册
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsRegister {get;set;}

           /// <summary>
           /// Desc:最后更改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyDate {get;set;}

           /// <summary>
           /// Desc:会员来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SourceType {get;set;}

    }
}
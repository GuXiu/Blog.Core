﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入明细
    ///</summary>

    [SugarTable("ZJ_DirectSaleStoreIncomeItem","Zhongjiu")]
    public partial class DirectSaleStoreIncomeItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:直营门店其他收入表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long IncomeId {get;set;}

           /// <summary>
           /// Desc:付款方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayType {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Pay {get;set;}

    }
}
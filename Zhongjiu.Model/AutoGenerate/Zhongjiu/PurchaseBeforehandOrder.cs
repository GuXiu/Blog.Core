﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购预采订单
    ///</summary>

    [SugarTable("ZJ_PurchaseBeforehandOrder","Zhongjiu")]
    public partial class PurchaseBeforehandOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:要货单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductApplyCode {get;set;}

           /// <summary>
           /// Desc:门店Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:配送中心ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:供应商Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SupplierId {get;set;}

           /// <summary>
           /// Desc:创建人(制单人)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:处理状态（0：未处理 1：已处理 2：已过期）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ProcessingState {get;set;}

           /// <summary>
           /// Desc:到期时间(有效期限)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ExpirationDate {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间，默认和创建时间相同
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
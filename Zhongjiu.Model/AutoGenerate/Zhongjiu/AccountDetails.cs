﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///结算详细表
    ///</summary>

    [SugarTable("Himall_AccountDetails","Zhongjiu")]
    public partial class AccountDetails
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:结算记录外键
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AccountId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:完成日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime Date {get;set;}

           /// <summary>
           /// Desc:订单下单日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderFinshDate {get;set;}

           /// <summary>
           /// Desc:枚举 完成订单1，退订单0
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderType {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:订单金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderAmount {get;set;}

           /// <summary>
           /// Desc:商品实付总额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ProductActualPaidAmount {get;set;}

           /// <summary>
           /// Desc:运费金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal FreightAmount {get;set;}

           /// <summary>
           /// Desc:佣金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommissionAmount {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundTotalAmount {get;set;}

           /// <summary>
           /// Desc:退还佣金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundCommisAmount {get;set;}

           /// <summary>
           /// Desc:退单的日期集合以;分隔
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderRefundsDates {get;set;}

           /// <summary>
           /// Desc:分销佣金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal BrokerageAmount {get;set;}

           /// <summary>
           /// Desc:退分销佣金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ReturnBrokerageAmount {get;set;}

           /// <summary>
           /// Desc:结算金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SettlementAmount {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentTypeName {get;set;}

           /// <summary>
           /// Desc:订单来源 [1:线上订单,2:线下订单]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte OrderSource {get;set;}

           /// <summary>
           /// Desc:推送订单 [0:默认,1:是,2:否]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte OrderPush {get;set;}

           /// <summary>
           /// Desc:订单类型[DF=0,PC=1,微信=2,安卓APP=3,苹果APP=4,移动端wap=5,门店=6,系统推送=7]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte OrderSourceType {get;set;}

           /// <summary>
           /// Desc:商家改价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ChangePrices {get;set;}

           /// <summary>
           /// Desc:订单实付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderTotalAmount {get;set;}

           /// <summary>
           /// Desc:平台优惠券
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PlatformCoupon {get;set;}

           /// <summary>
           /// Desc:商家优惠券
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal MerchantCoupon {get;set;}

           /// <summary>
           /// Desc:商家会员优惠 (线下：会员折扣 和 中酒币 优惠的总额)
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal BusinessMembershipDiscount {get;set;}

           /// <summary>
           /// Desc:会员卡支付
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal MembershipCardPayment {get;set;}

           /// <summary>
           /// Desc:积分代支付
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IntegralGenerationPayment {get;set;}

           /// <summary>
           /// Desc:配送费
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DeliveryFee {get;set;}

           /// <summary>
           /// Desc:积分成本
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IntegralCost {get;set;}

           /// <summary>
           /// Desc:实际结算金额 = 结算金额-积分成本-平台佣金(扣点)	
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SettlementTotalAmount {get;set;}

           /// <summary>
           /// Desc:商品总价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotalAmount {get;set;}

    }
}
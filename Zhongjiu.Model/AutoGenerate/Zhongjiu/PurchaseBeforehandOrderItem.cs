﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购预采订单明细
    ///</summary>

    [SugarTable("ZJ_PurchaseBeforehandOrderItem","Zhongjiu")]
    public partial class PurchaseBeforehandOrderItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:采购预采订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long PurchaseBeforehandId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:采购数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:已经处理的数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal DealNumber {get;set;}

           /// <summary>
           /// Desc:即时库存数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Stock {get;set;}

    }
}
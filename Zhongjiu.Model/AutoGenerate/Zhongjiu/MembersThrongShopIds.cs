﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员人群 店铺Id关系表
    ///</summary>

    [SugarTable("ZJ_MembersThrongShopIds","Zhongjiu")]
    public partial class MembersThrongShopIds
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员人群Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MembersThrongId {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
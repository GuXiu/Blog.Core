﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信小程序群聊配置
    ///</summary>

    [SugarTable("ZJ_WxGroupChats","Zhongjiu")]
    public partial class WxGroupChats
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:群聊名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:加群方式：1按钮，2二维码
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int JoinType {get;set;}

           /// <summary>
           /// Desc:企业群Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CorpId {get;set;}

           /// <summary>
           /// Desc:群链接、企业群二维码图片地址、客服配置Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WxChatKey {get;set;}

           /// <summary>
           /// Desc:客服Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string KefuId {get;set;}

           /// <summary>
           /// Desc:是否在店铺首页开启群聊
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsShowInShopHome {get;set;}

           /// <summary>
           /// Desc:是否在商品详情页开启群聊
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsShowInProductDetail {get;set;}

           /// <summary>
           /// Desc:是否在下单成功开启群聊
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsShowAfterSubmitOrder {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:状态：0不可用,1可用，
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:更新时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:配置类型：0企业群聊，1联系客服
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int GroupType {get;set;}

           /// <summary>
           /// Desc:适用类型：0店铺，1秒杀，2优惠券
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SuitType {get;set;}

           /// <summary>
           /// Desc:限时秒杀活动id，SuitType=1时生效
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long LimitTimeBuyId {get;set;}

           /// <summary>
           /// Desc:是否是任意群、任意客服
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsAnyChat {get;set;}

    }
}
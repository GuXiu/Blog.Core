﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///群发分享 优惠券活动关联表
    ///</summary>

    [SugarTable("ZJ_CouponShareBatchDetail","Zhongjiu")]
    public partial class CouponShareBatchDetail
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:ZJ_CouponShareBatchGroup表 Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CsbgId {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:发送张数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Number {get;set;}

           /// <summary>
           /// Desc:领取数量
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? GetNumber {get;set;}

           /// <summary>
           /// Desc:已领取数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceivedQuantity {get;set;}

    }
}
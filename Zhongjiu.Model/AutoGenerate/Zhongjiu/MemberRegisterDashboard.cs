﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户注册数统计表 统计逻辑将每日新增的会员按店铺分组录入，为0则不录入
    ///</summary>

    [SugarTable("ZJ_MemberRegisterDashboard","Zhongjiu")]
    public partial class MemberRegisterDashboard
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:新增会员数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long NumOfNewMember {get;set;}

           /// <summary>
           /// Desc:统计时间 统计日期按yyyy-MM-dd 00:00:00记录
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CountingTime {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:体系id	
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:部门Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品主表
    ///</summary>

    [SugarTable("Himall_Products","Zhongjiu")]
    public partial class Products
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:ERP产品Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ProductId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:分类ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:分类路径
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryPath {get;set;}

           /// <summary>
           /// Desc:类型ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TypeId {get;set;}

           /// <summary>
           /// Desc:品牌ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductCode {get;set;}

           /// <summary>
           /// Desc:广告词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShortDescription {get;set;}

           /// <summary>
           /// Desc:销售状态(1:销售中,4:已删除)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SaleStatus {get;set;}

           /// <summary>
           /// Desc:审核状态(1:待审核，2：已审核，4违规下架)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:添加日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AddedDate {get;set;}

           /// <summary>
           /// Desc:显示顺序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:存放图片的目录
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ImagePath {get;set;}

           /// <summary>
           /// Desc:市场价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal MarketPrice {get;set;}

           /// <summary>
           /// Desc:最小销售价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal MinSalePrice {get;set;}

           /// <summary>
           /// Desc:是否有SKU
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte HasSKU {get;set;}

           /// <summary>
           /// Desc:浏览次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long VistiCounts {get;set;}

           /// <summary>
           /// Desc:销售量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleCounts {get;set;}

           /// <summary>
           /// Desc:运费模板ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FreightTemplateId {get;set;}

           /// <summary>
           /// Desc:重量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Weight {get;set;}

           /// <summary>
           /// Desc:体积
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Volume {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Quantity {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MeasureUnit {get;set;}

           /// <summary>
           /// Desc:修改状态 0 正常 1己修改 2待审核 3 己修改并待审核
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int EditStatus {get;set;}

           /// <summary>
           /// Desc:是否同步 0 已同步 1 需要同步
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsAsync {get;set;}

           /// <summary>
           /// Desc:线下商品ID
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? OffLineProductId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MonthSaleCount {get;set;}

           /// <summary>
           /// Desc:是否支持货到付款 默认 0 支持 1 不支持
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? SupportCashOnDelivery {get;set;}

           /// <summary>
           /// Desc:生产厂家
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Factory {get;set;}

           /// <summary>
           /// Desc:销售渠道 0 全部 1 线上 2 线下
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Channel {get;set;}

           /// <summary>
           /// Desc:平台是否审核 1 未审核 0 审核
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? PlatAuditing {get;set;}

           /// <summary>
           /// Desc:商品所属类别 0 酒类 1 非酒类
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ProductType {get;set;}

           /// <summary>
           /// Desc:是否开启库存小数点
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? IsRatioStock {get;set;}

           /// <summary>
           /// Desc:审批流审批状态0待审核1审核中2审核成功
           /// Default:2
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:来源 0 区域管理员新增，1 区域管理员基库添加，2 门店新增，3 门店基库添加，4 平台导入，5 收银台新增，6 云码新增，7 云码基库添加到门店，8 收银台基库添加到门店，9 配送中心商品从基库添加到门店
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? InsertType {get;set;}

           /// <summary>
           /// Desc:来源基库Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? BaseProductId {get;set;}

           /// <summary>
           /// Desc:商品名称首字母拼音
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductNameSpell {get;set;}

           /// <summary>
           /// Desc:是否支付交易赠送体系积分
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? IsSupportDealGiveTiXiInteral {get;set;}

           /// <summary>
           /// Desc:最后修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastModifyTime {get;set;}

           /// <summary>
           /// Desc:特殊商品不显示价格
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsHidePrice {get;set;}

    }
}
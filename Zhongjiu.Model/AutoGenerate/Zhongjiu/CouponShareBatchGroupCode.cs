﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券分享单问题表
    ///</summary>

    [SugarTable("ZJ_CouponShareBatchGroupCode","Zhongjiu")]
    public partial class CouponShareBatchGroupCode
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券组合Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CsbgId {get;set;}

           /// <summary>
           /// Desc:是否已经使用[0:未使用][1:已使用][2:已领取]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsUse {get;set;}

           /// <summary>
           /// Desc:答案1
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code1 {get;set;}

           /// <summary>
           /// Desc:答案2
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code2 {get;set;}

           /// <summary>
           /// Desc:答案3
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code3 {get;set;}

           /// <summary>
           /// Desc:答案4
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code4 {get;set;}

           /// <summary>
           /// Desc:答案5
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code5 {get;set;}

           /// <summary>
           /// Desc:领取人的OPENID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserOpenId {get;set;}

    }
}
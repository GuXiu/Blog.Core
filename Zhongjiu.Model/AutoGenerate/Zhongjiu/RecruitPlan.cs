﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销招募计划表
    ///</summary>

    [SugarTable("Himall_RecruitPlan","Zhongjiu")]
    public partial class RecruitPlan
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:招募标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:招募内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

    }
}
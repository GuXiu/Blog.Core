﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///注册送优惠券关联优惠券
    ///</summary>

    [SugarTable("Himall_CouponSendByRegisterDetailed","Zhongjiu")]
    public partial class CouponSendByRegisterDetailed
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:注册活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CouponRegisterId {get;set;}

           /// <summary>
           /// Desc:优惠券ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

    }
}
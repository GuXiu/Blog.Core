﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺投诉表
    ///</summary>

    [SugarTable("ZJ_ShopReports","Zhongjiu")]
    public partial class ShopReports
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopID {get;set;}

           /// <summary>
           /// Desc:店铺名字
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:会员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:投诉标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Mobile {get;set;}

           /// <summary>
           /// Desc:投诉内容描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Centent {get;set;}

           /// <summary>
           /// Desc:入库时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:是否已经处理
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsEnable {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分仓发货
    ///</summary>

    [SugarTable("ZJ_WarehouseDelivery","Zhongjiu")]
    public partial class WarehouseDelivery
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:标题名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:规则(-1:不限制,0:指定收货地址,1:指定商品,2:指定收货地址和商品)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Rule {get;set;}

           /// <summary>
           /// Desc:规则数据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RuleData {get;set;}

           /// <summary>
           /// Desc:配送店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DistributionShopId {get;set;}

           /// <summary>
           /// Desc:无库存是否继续分配(1:无库存,将自动增加配送仓的商品库存,2:无库存,不能选择此配送仓库)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int NoStockContinue {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:正常,1:删除]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsDel {get;set;}

    }
}
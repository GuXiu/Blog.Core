﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下操作日志表
    ///</summary>

    [SugarTable("Himall_OffLineOperationLog","Zhongjiu")]
    public partial class OffLineOperationLog
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:分类[默认:0]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CategoryId {get;set;}

           /// <summary>
           /// Desc:操作人Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreaterId {get;set;}

           /// <summary>
           /// Desc:操作内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已经删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDelete {get;set;}

    }
}
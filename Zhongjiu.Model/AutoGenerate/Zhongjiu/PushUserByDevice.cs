﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户App设备ID绑定关系表
    ///</summary>

    [SugarTable("ZJ_PushUserByDevice","Zhongjiu")]
    public partial class PushUserByDevice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:设备Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DeviceId {get;set;}

           /// <summary>
           /// Desc:0全部，1 android,2 ios
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? SysType {get;set;}

           /// <summary>
           /// Desc:最后一次修改时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? LastEditTime {get;set;}

           /// <summary>
           /// Desc:入库时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

    }
}
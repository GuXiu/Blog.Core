﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统商品属性表（店铺商品、基库商品共用）
    ///</summary>

    [SugarTable("Himall_Attributes","Zhongjiu")]
    public partial class Attributes
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TypeId {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:是否为必选
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsMust {get;set;}

           /// <summary>
           /// Desc:是否可多选
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsMulti {get;set;}

    }
}
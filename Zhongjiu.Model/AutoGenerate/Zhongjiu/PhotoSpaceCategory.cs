﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///图库类别表
    ///</summary>

    [SugarTable("Himall_PhotoSpaceCategory","Zhongjiu")]
    public partial class PhotoSpaceCategory
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:图片空间分类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PhotoSpaceCatrgoryName {get;set;}

           /// <summary>
           /// Desc:显示顺序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? DisplaySequence {get;set;}

    }
}
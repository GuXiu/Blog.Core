﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///礼品卡表
    ///</summary>

    [SugarTable("Himall_GiftCards","Zhongjiu")]
    public partial class GiftCards
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CardNumber {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PassWord {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:在此店铺使用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:是否使用[0:未使用][1:已使用]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsUse {get;set;}

           /// <summary>
           /// Desc:使用时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UseTime {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DepartmentId {get;set;}

    }
}
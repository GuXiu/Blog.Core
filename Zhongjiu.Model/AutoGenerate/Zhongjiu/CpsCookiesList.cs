﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单CPS记录表
    ///</summary>

    [SugarTable("Himall_CpsCookiesList","Zhongjiu")]
    public partial class CpsCookiesList
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrdersId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CookieStr {get;set;}

    }
}
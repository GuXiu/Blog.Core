﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///提成计算记录 商品详情表
    ///</summary>

    [SugarTable("ZJ_SalesCommissionCalculationItem","Zhongjiu")]
    public partial class SalesCommissionCalculationItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:提成计算记录Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SccId {get;set;}

           /// <summary>
           /// Desc:订单详情Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderItemId {get;set;}

           /// <summary>
           /// Desc:提成方式[1:商品提成(比例),2:商品提成(金额)][3:毛利提成(比例)]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte ModeType {get;set;}

           /// <summary>
           /// Desc:提成方式对应的规则值[商品提成(比例值),商品提成(金额值)][毛利提成(比例值)]
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ModeValue {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

    }
}
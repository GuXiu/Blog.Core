﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统基库SKU表
    ///</summary>

    [SugarTable("Himall_BaseSKUs","Zhongjiu")]
    public partial class BaseSKUs
    {
           /// <summary>
           /// Desc:商品ID_颜色规格ID_颜色规格ID_尺寸规格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Id {get;set;}

           /// <summary>
           /// Desc:自增主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long AutoId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:颜色规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Color {get;set;}

           /// <summary>
           /// Desc:尺寸规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Size {get;set;}

           /// <summary>
           /// Desc:版本规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Version {get;set;}

           /// <summary>
           /// Desc:SKU
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Sku {get;set;}

           /// <summary>
           /// Desc:库存
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Stock {get;set;}

           /// <summary>
           /// Desc:成本价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CostPrice {get;set;}

           /// <summary>
           /// Desc:销售价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SalePrice {get;set;}

           /// <summary>
           /// Desc:显示图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShowPic {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductItemId {get;set;}

           /// <summary>
           /// Desc:活动类型，1是特价
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public short ActivityType {get;set;}

           /// <summary>
           /// Desc:是否禁用优惠券默认为0（不禁用），1为禁用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte DisabledCoupon {get;set;}

           /// <summary>
           /// Desc:规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BottleCapacity {get;set;}

           /// <summary>
           /// Desc:平均价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? AvgPrice {get;set;}

           /// <summary>
           /// Desc:是否参与折扣 默认1 支持 2 不支持
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? IsDiscounted {get;set;}

           /// <summary>
           /// Desc:箱容量 箱/瓶
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BoxCapacity {get;set;}

           /// <summary>
           /// Desc:是否支持体系积分兑换 0 不支持（默认）;1 支持
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsSystemExchangeIntegral {get;set;}

           /// <summary>
           /// Desc:体系积分兑换数量
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? SystemExchangeIntegralAmount {get;set;}

           /// <summary>
           /// Desc:垛规
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string StackGauge {get;set;}

           /// <summary>
           /// Desc:盒规
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BoxGauge {get;set;}

    }
}
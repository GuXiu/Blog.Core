﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///角色表
    ///</summary>

    [SugarTable("Himall_Roles","Zhongjiu")]
    public partial class Roles
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:角色名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RoleName {get;set;}

           /// <summary>
           /// Desc:说明
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:部门ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:账套
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:是否是管理员
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsAdmin {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateDate {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销数据统计 统计逻辑记录每日分销员的关联订单数量和金额，首次成为分销员当日及有订单时记录
    ///</summary>

    [SugarTable("ZJ_DistributionDashboard","Zhongjiu")]
    public partial class DistributionDashboard
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:推广员id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? PromoterId {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:是否为新推广员 1是 0否
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? NewPromoter {get;set;}

           /// <summary>
           /// Desc:统计时间 统计日期按yyyy-MM-dd 00:00:00记录
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CountingTime {get;set;}

           /// <summary>
           /// Desc:订单笔数
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? OrderCounts {get;set;}

           /// <summary>
           /// Desc:订单总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? OrderValue {get;set;}

    }
}
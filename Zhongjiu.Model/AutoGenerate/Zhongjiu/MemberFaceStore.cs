﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///人脸信息库
    ///</summary>

    [SugarTable("ZJ_MemberFaceStore","Zhongjiu")]
    public partial class MemberFaceStore
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:用户Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MemberId {get;set;}

           /// <summary>
           /// Desc:用户即时拍照路径
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FacePicture {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime DateTime {get;set;}

           /// <summary>
           /// Desc:人脸类型 0 注册；1 登录
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int FaceType {get;set;}

           /// <summary>
           /// Desc:所属店铺
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
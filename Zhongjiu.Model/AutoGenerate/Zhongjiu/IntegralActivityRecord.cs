﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///积分翻倍活动Id
    ///</summary>

    [SugarTable("ZJ_IntegralActivityRecord","Zhongjiu")]
    public partial class IntegralActivityRecord
    {
           /// <summary>
           /// Desc:IntegralActivityId
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:积分活动id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? IntegralActivityId {get;set;}

           /// <summary>
           /// Desc:订单Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:订单获得积分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? OrderIntegral {get;set;}

           /// <summary>
           /// Desc:积分记录Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? IntegralRecordId {get;set;}

           /// <summary>
           /// Desc:活动获得的积分
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ActivityIntegral {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:参加活动的商品id 如1,2,3 空表示全部参加
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductIds {get;set;}

           /// <summary>
           /// Desc:积分活动备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Accountset {get;set;}

    }
}
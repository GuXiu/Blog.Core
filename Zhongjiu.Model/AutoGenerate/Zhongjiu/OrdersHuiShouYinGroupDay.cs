﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///小精灵支付，按天统计(订单记录信息)
    ///</summary>

    [SugarTable("ZJ_OrdersHuiShouYinGroupDay","Zhongjiu")]
    public partial class OrdersHuiShouYinGroupDay
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:订单数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int OrderQuantity {get;set;}

           /// <summary>
           /// Desc:订单金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderAmount {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal RefundAmount {get;set;}

           /// <summary>
           /// Desc:手续费金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal FeesAmt {get;set;}

           /// <summary>
           /// Desc:到账金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AccountAmount {get;set;}

           /// <summary>
           /// Desc:交易金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal TransactionAmount {get;set;}

           /// <summary>
           /// Desc:结算日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime SettlementDate {get;set;}

           /// <summary>
           /// Desc:运费
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public decimal Freight {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///平台资金流水表
    ///</summary>

    [SugarTable("Himall_PlatAccountItem","Zhongjiu")]
    public partial class PlatAccountItem
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:交易流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountNo {get;set;}

           /// <summary>
           /// Desc:关联资金编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AccoutID {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:帐户剩余
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Balance {get;set;}

           /// <summary>
           /// Desc:交易类型
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TradeType {get;set;}

           /// <summary>
           /// Desc:是否收入
           /// Default:
           /// Nullable:False
           /// </summary>           
           public bool IsIncome {get;set;}

           /// <summary>
           /// Desc:交易备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReMark {get;set;}

           /// <summary>
           /// Desc:详情ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DetailId {get;set;}

    }
}
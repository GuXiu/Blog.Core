﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购退货订单明细
    ///</summary>

    [SugarTable("ZJ_PurchaseReturnOrderItem","Zhongjiu")]
    public partial class PurchaseReturnOrderItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:采购退货单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long PurchaseReturnId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:退货数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员等级表
    ///</summary>

    [SugarTable("Himall_MemberGrade","Zhongjiu")]
    public partial class MemberGrade
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员等级名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GradeName {get;set;}

           /// <summary>
           /// Desc:该等级所需积分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Integral {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:部门ID 0是平台会员等级， 其它值是对应体系会员等级
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:会员等级折扣 默认全额不打折
           /// Default:100
           /// Nullable:False
           /// </summary>           
           public int Discount {get;set;}

           /// <summary>
           /// Desc:会员类型 默认为零售
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Type {get;set;}

    }
}
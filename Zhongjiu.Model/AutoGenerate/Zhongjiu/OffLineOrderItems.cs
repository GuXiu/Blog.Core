﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下订单明细表
    ///</summary>

    [SugarTable("himall_OffLineOrderItems","Zhongjiu")]
    public partial class OffLineOrderItems
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品条码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductModel {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:商品单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Price {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Quantity {get;set;}

           /// <summary>
           /// Desc:佣金比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisRate {get;set;}

           /// <summary>
           /// Desc:加减单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DisPrice {get;set;}

           /// <summary>
           /// Desc:入库价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? EnterPrice {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AvgPrice {get;set;}

           /// <summary>
           /// Desc:订单满额优惠金额(活动满减均摊金额) 满减均摊金额 = 商品金额/商品总金额*订单总满减金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderDiscountAverage {get;set;}

           /// <summary>
           /// Desc:会员优惠金额(会员卡打折的均摊金额) 均摊打折金额 = 商品金额/商品总金额*订单总折扣金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipCardFavorableAverage {get;set;}

           /// <summary>
           /// Desc:中酒币均摊金额                     均摊中酒币金额 = 商品金额/商品总金额*订单总中酒币金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CoinFavorableAverage {get;set;}

           /// <summary>
           /// Desc:加减价，改价均摊金额               改价均摊金额 = 商品金额/商品总金额*订单总改价金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DisPriceAverage {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺分销设置表
    ///</summary>

    [SugarTable("Himall_ShopDistributorSetting","Zhongjiu")]
    public partial class ShopDistributorSetting
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:分销默认分佣比
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal DistributorDefaultRate {get;set;}

           /// <summary>
           /// Desc:分销分享名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DistributorShareName {get;set;}

           /// <summary>
           /// Desc:分销分享说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DistributorShareContent {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DistributorShareLogo {get;set;}

           /// <summary>
           /// Desc:结算方式 1 平台结算；2 商家手动结算
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int SettlementType {get;set;}

           /// <summary>
           /// Desc:是否有收取佣金
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? HasReceiveCommissions {get;set;}

           /// <summary>
           /// Desc:佣金税额承担 0 店铺 1推广员
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? CommissionTaxLiabilityType {get;set;}

    }
}
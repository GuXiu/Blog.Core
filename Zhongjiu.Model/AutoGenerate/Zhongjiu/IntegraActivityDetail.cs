﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///积分活动商品表
    ///</summary>

    [SugarTable("ZJ_IntegraActivityDetail","Zhongjiu")]
    public partial class IntegraActivityDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:特价Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long IntegraActivityId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:特价金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺浏览量记录表
    ///</summary>

    [SugarTable("Himall_ShopVistis","Zhongjiu")]
    public partial class ShopVistis
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime Date {get;set;}

           /// <summary>
           /// Desc:浏览量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long VistiCounts {get;set;}

           /// <summary>
           /// Desc:销售量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleCounts {get;set;}

           /// <summary>
           /// Desc:销售额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleAmounts {get;set;}

    }
}
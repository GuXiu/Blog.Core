﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品分组表
    ///</summary>

    [SugarTable("ZJ_ProductsGroup","Zhongjiu")]
    public partial class ProductsGroup
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品群组Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int GroupId {get;set;}

           /// <summary>
           /// Desc:商品简称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductAcronym {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///发票表
    ///</summary>

    [SugarTable("Himall_Invoice","Zhongjiu")]
    public partial class Invoice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:公司名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyName {get;set;}

           /// <summary>
           /// Desc:公司注册地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RegisteredAddress {get;set;}

           /// <summary>
           /// Desc:公司开户行
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyBank {get;set;}

           /// <summary>
           /// Desc:公司银行账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankAccount {get;set;}

           /// <summary>
           /// Desc:公司税号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyEin {get;set;}

           /// <summary>
           /// Desc:公司电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyPhone {get;set;}

           /// <summary>
           /// Desc:发票类型  1.纸质普通发票(个人)，2.电子普通发票(个人)，3.纸质普通发票(公司)，4.电子普通发票(公司)，5.纸质专用发票(公司)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? InvoiceType {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddDataTime {get;set;}

           /// <summary>
           /// Desc:发票类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? InvoiceNature {get;set;}

    }
}
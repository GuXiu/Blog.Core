﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///企业微信群列表
    ///</summary>

    [SugarTable("ZJ_WxGroupChatList","Zhongjiu")]
    public partial class WxGroupChatList
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:企业微信ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CorpId {get;set;}

           /// <summary>
           /// Desc:群ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ChatId {get;set;}

           /// <summary>
           /// Desc:群名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChatName {get;set;}

           /// <summary>
           /// Desc:群创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ChatCreateTime {get;set;}

           /// <summary>
           /// Desc:群主
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Owner {get;set;}

           /// <summary>
           /// Desc:群公告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Notice {get;set;}

           /// <summary>
           /// Desc:拉取数据操作人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Puller {get;set;}

           /// <summary>
           /// Desc:拉取数据时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime PullTime {get;set;}

    }
}
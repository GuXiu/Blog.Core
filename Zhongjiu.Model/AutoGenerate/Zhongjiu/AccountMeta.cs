﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///营销服务费结算明细表
    ///</summary>

    [SugarTable("Himall_AccountMeta","Zhongjiu")]
    public partial class AccountMeta
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AccountId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MetaKey {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MetaValue {get;set;}

           /// <summary>
           /// Desc:营销服务开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ServiceStartTime {get;set;}

           /// <summary>
           /// Desc:营销服务结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ServiceEndTime {get;set;}

    }
}
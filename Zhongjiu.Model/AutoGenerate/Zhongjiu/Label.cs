﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员标签表
    ///</summary>

    [SugarTable("Himall_Label","Zhongjiu")]
    public partial class Label
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:标签名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LabelName {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LabelDesc {get;set;}

    }
}
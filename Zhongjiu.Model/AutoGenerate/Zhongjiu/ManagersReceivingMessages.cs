﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///接收店铺有关消息的用户表
    ///</summary>

    [SugarTable("ZJ_ManagersReceivingMessages","Zhongjiu")]
    public partial class ManagersReceivingMessages
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:微信原始Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXOriginalId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string UnionId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Openid {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:微信昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinNickname {get;set;}

           /// <summary>
           /// Desc:微信头像
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Headimgurl {get;set;}

           /// <summary>
           /// Desc:扫码时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreationTime {get;set;}

           /// <summary>
           /// Desc:是否接收线上新订单通知
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool NewOrdersOnline {get;set;}

           /// <summary>
           /// Desc:是否接收订单有退款通知
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool OrderRefund {get;set;}

           /// <summary>
           /// Desc:是否接收老会员进店提醒
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool ShopReminder {get;set;}

           /// <summary>
           /// Desc:是否接收订单签收提醒
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool OrderSigning {get;set;}

           /// <summary>
           /// Desc:是否接收库存不足提醒
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool NotEnoughStock {get;set;}

           /// <summary>
           /// Desc:是否接收店铺报表提醒
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool BusinessReport {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///拼团活动
    ///</summary>

    [SugarTable("Himall_FightGroupActive","Zhongjiu")]
    public partial class FightGroupActive
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IconUrl {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? StartTime {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EndTime {get;set;}

           /// <summary>
           /// Desc:参团人数限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LimitedNumber {get;set;}

           /// <summary>
           /// Desc:成团时限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? LimitedHour {get;set;}

           /// <summary>
           /// Desc:数量限制
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? LimitQuantity {get;set;}

           /// <summary>
           /// Desc:成团数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? GroupCount {get;set;}

           /// <summary>
           /// Desc:成功成团数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? OkGroupCount {get;set;}

           /// <summary>
           /// Desc:活动添加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:平台操作状态
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ManageAuditStatus {get;set;}

           /// <summary>
           /// Desc:平台操作说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ManageRemark {get;set;}

           /// <summary>
           /// Desc:平台操作时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ManageDate {get;set;}

           /// <summary>
           /// Desc:平台操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ManagerId {get;set;}

           /// <summary>
           /// Desc:活动当前状态
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ActiveTimeStatus {get;set;}

           /// <summary>
           /// Desc:团购方式(0全员参加 1 仅新用户参加)
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? GroupType {get;set;}

           /// <summary>
           /// Desc:创建人级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:省市管理员发布活动，关联店铺Id集合，逗号分隔字符串
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:配送方式[0:未选择配送方式,1:送货,2自提,3现取]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int DistributionType {get;set;}

           /// <summary>
           /// Desc:是否在M站首页显示
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int ShowType {get;set;}

           /// <summary>
           /// Desc:是否自动成团 0 否，1 是
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte AutoGroup {get;set;}

           /// <summary>
           /// Desc:是否显示倒计时
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte EnableCountDown {get;set;}

           /// <summary>
           /// Desc:倒计时时间 单位：分钟
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CountDownMins {get;set;}

           /// <summary>
           /// Desc:是否设置活动后立即显示倒计时
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsNowEnableCountDown {get;set;}

    }
}
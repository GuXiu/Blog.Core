﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销用户与店铺关联表
    ///</summary>

    [SugarTable("Himall_DistributionUserLink","Zhongjiu")]
    public partial class DistributionUserLink
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:销售员
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PartnerId {get;set;}

           /// <summary>
           /// Desc:店铺
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LinkTime {get;set;}

           /// <summary>
           /// Desc:买家
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long BuyUserId {get;set;}

    }
}
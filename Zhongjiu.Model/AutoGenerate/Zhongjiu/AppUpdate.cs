﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///App版本更新
    ///</summary>

    [SugarTable("ZJ_AppUpdate","Zhongjiu")]
    public partial class AppUpdate
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:App名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppName {get;set;}

           /// <summary>
           /// Desc:App版本名字,如4.0
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Version {get;set;}

           /// <summary>
           /// Desc:App版本号，如150
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? VersionCode {get;set;}

           /// <summary>
           /// Desc:1 android  2ios
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? AppSite {get;set;}

           /// <summary>
           /// Desc:下载地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DownUrl {get;set;}

           /// <summary>
           /// Desc:文件的MD5值，热更新时使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FileMd5 {get;set;}

           /// <summary>
           /// Desc:更新的文件名，热更新时使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Bundle {get;set;}

           /// <summary>
           /// Desc:更新的文件版本
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string bundleVersion {get;set;}

           /// <summary>
           /// Desc:更新描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:是否为热更新，0否，1是
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsHotUpdate {get;set;}

           /// <summary>
           /// Desc:是否强制更新0否1是
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsUpdate {get;set;}

           /// <summary>
           /// Desc:是否启用，0否1是
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Enable {get;set;}

           /// <summary>
           /// Desc:入库时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? createtime {get;set;}

           /// <summary>
           /// Desc:是否静默更新
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? Silent {get;set;}

           /// <summary>
           /// Desc:是否为测试
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? IsLoadTest {get;set;}

           /// <summary>
           /// Desc:js包的适用最低版本，默认=Version
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MinSuitVersion {get;set;}

           /// <summary>
           /// Desc:附加下载链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppendUrl {get;set;}

           /// <summary>
           /// Desc:App类型（0中酒网，1收银APP）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AppType {get;set;}

    }
}
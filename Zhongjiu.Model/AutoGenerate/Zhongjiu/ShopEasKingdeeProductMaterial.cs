﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置物料信息
    ///</summary>

    [SugarTable("Himall_ShopEasKingdeeProductMaterial","Zhongjiu")]
    public partial class ShopEasKingdeeProductMaterial
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:金蝶ERP,物料编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Material {get;set;}

           /// <summary>
           /// Desc:中酒商品条码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Sku {get;set;}

           /// <summary>
           /// Desc:中酒商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:含税单价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal TaxPrice {get;set;}

    }
}
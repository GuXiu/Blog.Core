﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员订单发放红包表
    ///</summary>

    [SugarTable("Himall_ShopBonusGrant","Zhongjiu")]
    public partial class ShopBonusGrant
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:红包Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopBonusId {get;set;}

           /// <summary>
           /// Desc:发放人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BonusQR {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页推荐表
    ///</summary>

    [SugarTable("Himall_HomeRecommends","Zhongjiu")]
    public partial class HomeRecommends
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:选项卡名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TabName {get;set;}

           /// <summary>
           /// Desc:更多推荐跳转地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string MoreUrl {get;set;}

    }
}
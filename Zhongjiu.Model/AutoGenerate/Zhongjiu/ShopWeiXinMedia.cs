﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺微信素材表
    ///</summary>

    [SugarTable("ZJ_ShopWeiXinMedia","Zhongjiu")]
    public partial class ShopWeiXinMedia
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:素材类型 1 图文素材；2 图文（外链） ；3 小程序卡片
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? WXType {get;set;}

           /// <summary>
           /// Desc:微信素材Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinMediaId {get;set;}

           /// <summary>
           /// Desc:微信原始Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinOriginalId {get;set;}

           /// <summary>
           /// Desc:(指定页面设置)跳转链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChainingLink {get;set;}

           /// <summary>
           /// Desc:(指定页面设置)标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:(指定页面设置)主图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MasterGraph {get;set;}

           /// <summary>
           /// Desc:(指定页面设置)摘要
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Abstract {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:是否禁用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Disable {get;set;}

           /// <summary>
           /// Desc:是否为店铺关注推送消息
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Default {get;set;}

    }
}
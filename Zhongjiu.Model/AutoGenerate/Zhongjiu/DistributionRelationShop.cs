﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配送中心店铺关联表
    ///</summary>

    [SugarTable("ZJ_DistributionRelationShop","Zhongjiu")]
    public partial class DistributionRelationShop
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:配送中心ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:关联状态[0解除关联,1保持关联]
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RelationStatus {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Operator {get;set;}

    }
}
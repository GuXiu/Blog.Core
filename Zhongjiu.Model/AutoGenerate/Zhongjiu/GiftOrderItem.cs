﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///礼品订单明细
    ///</summary>

    [SugarTable("Himall_GiftOrderItem","Zhongjiu")]
    public partial class GiftOrderItem
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:礼品编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long GiftId {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Quantity {get;set;}

           /// <summary>
           /// Desc:积分单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SaleIntegral {get;set;}

           /// <summary>
           /// Desc:礼品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GiftName {get;set;}

           /// <summary>
           /// Desc:礼品价值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? GiftValue {get;set;}

           /// <summary>
           /// Desc:图片存放地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ImagePath {get;set;}

    }
}
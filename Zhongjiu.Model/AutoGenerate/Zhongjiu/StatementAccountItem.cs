﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///对账单明细
    ///</summary>

    [SugarTable("ZJ_StatementAccountItem","Zhongjiu")]
    public partial class StatementAccountItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:对账单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long StatementAccountId {get;set;}

           /// <summary>
           /// Desc:业务单号ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderNo {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:加盟店价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:加盟店数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:加盟店金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:差异金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DifferAmount {get;set;}

           /// <summary>
           /// Desc:单据类型（1：采购入库， 2 采退单，3：配送入库，4：配退单）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderType {get;set;}

    }
}
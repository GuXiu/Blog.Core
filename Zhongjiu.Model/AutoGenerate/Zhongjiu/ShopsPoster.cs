﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///海报素材
    ///</summary>

    [SugarTable("ZJ_ShopsPoster","Zhongjiu")]
    public partial class ShopsPoster
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:海报素材标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:海报素材图片
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PosterPhoto {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreatTime {get;set;}

           /// <summary>
           /// Desc:所属文件夹
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? FolderId {get;set;}

           /// <summary>
           /// Desc:所属体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSetId {get;set;}

    }
}
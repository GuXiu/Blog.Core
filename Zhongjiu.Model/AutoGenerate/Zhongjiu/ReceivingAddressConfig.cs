﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///区域支付方式配置
    ///</summary>

    [SugarTable("Himall_ReceivingAddressConfig","Zhongjiu")]
    public partial class ReceivingAddressConfig
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分类ID[0:货到付款][1:在线支付]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CategoryId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AddressId_City {get;set;}

           /// <summary>
           /// Desc:逗号分隔
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AddressId {get;set;}

           /// <summary>
           /// Desc:预留字段，防止将来其他商家一并支持货到付款
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
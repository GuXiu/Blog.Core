﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///抽奖活动
    ///</summary>

    [SugarTable("ZJ_LuckDrawActivity","Zhongjiu")]
    public partial class LuckDrawActivity
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ActivityName {get;set;}

           /// <summary>
           /// Desc:奖项总量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Prize {get;set;}

           /// <summary>
           /// Desc:活动开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BeginTime {get;set;}

           /// <summary>
           /// Desc:活动结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:活动状态[0:未开始或已开始][1:结束][2:已关闭][3:已删除]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActivityStatus {get;set;}

           /// <summary>
           /// Desc:添加人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creator {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Modified {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime ModifyTime {get;set;}

           /// <summary>
           /// Desc:抽奖问题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CodeContent {get;set;}

           /// <summary>
           /// Desc:提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TipContent {get;set;}

           /// <summary>
           /// Desc:活动规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RuleContent {get;set;}

           /// <summary>
           /// Desc:每人，限制抽奖总次数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TotalNumber {get;set;}

           /// <summary>
           /// Desc:每人每天，限制抽奖次数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DailyNumber {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:banner图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Banner {get;set;}

           /// <summary>
           /// Desc:背景图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Background {get;set;}

           /// <summary>
           /// Desc:banner图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Banner1 {get;set;}

           /// <summary>
           /// Desc:背景图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Background1 {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///群聊设置选择的企业群
    ///</summary>

    [SugarTable("ZJ_WxGroupChatItems","Zhongjiu")]
    public partial class WxGroupChatItems
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:群聊设置id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long GroupChatId {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:群Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ChatId {get;set;}

           /// <summary>
           /// Desc:群名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChatName {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Owner {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime ChatCreateTime {get;set;}

    }
}
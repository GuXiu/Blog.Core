﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///预付款明细
    ///</summary>

    [SugarTable("ZJ_AdvancesItem","Zhongjiu")]
    public partial class AdvancesItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:预付款表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AdvancesId {get;set;}

           /// <summary>
           /// Desc:付款方式
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PayType {get;set;}

           /// <summary>
           /// Desc:预付金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Expense {get;set;}

    }
}
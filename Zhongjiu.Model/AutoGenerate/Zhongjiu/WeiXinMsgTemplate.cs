﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信消息模板表
    ///</summary>

    [SugarTable("Himall_WeiXinMsgTemplate","Zhongjiu")]
    public partial class WeiXinMsgTemplate
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:消息类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MessageType {get;set;}

           /// <summary>
           /// Desc:消息模板编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TemplateNum {get;set;}

           /// <summary>
           /// Desc:消息模板ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TemplateId {get;set;}

           /// <summary>
           /// Desc:更新日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UpdateDate {get;set;}

           /// <summary>
           /// Desc:是否启用
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsOpen {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinAppId {get;set;}

    }
}
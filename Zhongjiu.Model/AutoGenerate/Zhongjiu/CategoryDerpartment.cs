﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系分类分佣比例表
    ///</summary>

    [SugarTable("Himall_CategoryDerpartment","Zhongjiu")]
    public partial class CategoryDerpartment
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分类Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? CategoryId {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:分佣比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? CommisRate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Operator {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? OperatorId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系消息发送设置表
    ///</summary>

    [SugarTable("Himall_InformationSetting","Zhongjiu")]
    public partial class InformationSetting
    {
           /// <summary>
           /// Desc:id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:部门ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:短信AppKey
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppKey {get;set;}

           /// <summary>
           /// Desc:短信AppSecret
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppSecret {get;set;}

           /// <summary>
           /// Desc:SMTP服务器
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTPServer {get;set;}

           /// <summary>
           /// Desc:SMTP服务器端口
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTPServerPort {get;set;}

           /// <summary>
           /// Desc:SMTP用户名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTPUserName {get;set;}

           /// <summary>
           /// Desc:SMTP用户密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTPUserPassword {get;set;}

           /// <summary>
           /// Desc:SMTP邮箱
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SMTPEmail {get;set;}

           /// <summary>
           /// Desc:显示名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShowName {get;set;}

           /// <summary>
           /// Desc:微信AppId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinAppId {get;set;}

           /// <summary>
           /// Desc:微信AppSecret
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinAppSecret {get;set;}

    }
}
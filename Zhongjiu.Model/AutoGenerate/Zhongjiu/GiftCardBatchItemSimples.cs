﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///礼品卡简易批次明细
    ///</summary>

    [SugarTable("ZJ_GiftCardBatchItemSimples","Zhongjiu")]
    public partial class GiftCardBatchItemSimples
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:批次Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BatchId {get;set;}

           /// <summary>
           /// Desc:面值
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Count {get;set;}

           /// <summary>
           /// Desc:排序 asc
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sort {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺首页模板设置表
    ///</summary>

    [SugarTable("Himall_TemplateVisualizationSettings","Zhongjiu")]
    public partial class TemplateVisualizationSettings
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:当前使用的模板的名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CurrentTemplateName {get;set;}

           /// <summary>
           /// Desc:店铺Id（平台为0）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:模板类型（微信、APP...）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:模板内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

           /// <summary>
           /// Desc:模版数据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string JsonData {get;set;}

           /// <summary>
           /// Desc:app专题模版
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppTopicContent {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime AddTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信端首页的商品配置表
    ///</summary>

    [SugarTable("Himall_MobileHomeProducts","Zhongjiu")]
    public partial class MobileHomeProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:终端类型(微信、WAP）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PlatFormType {get;set;}

           /// <summary>
           /// Desc:顺序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short Sequence {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统商品属性值表（店铺商品、基库商品共用）
    ///</summary>

    [SugarTable("Himall_AttributeValues","Zhongjiu")]
    public partial class AttributeValues
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:属性ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AttributeId {get;set;}

           /// <summary>
           /// Desc:属性值
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Value {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

    }
}
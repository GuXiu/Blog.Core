﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///小精灵支付，订单记录信息
    ///</summary>

    [SugarTable("ZJ_OrdersHuiShouYin","Zhongjiu")]
    public partial class OrdersHuiShouYin
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:费用
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal FeesAmt {get;set;}

           /// <summary>
           /// Desc:费率
           /// Default:0.000
           /// Nullable:True
           /// </summary>           
           public decimal? FeesRate {get;set;}

           /// <summary>
           /// Desc:订单总金额
           /// Default:0.000
           /// Nullable:True
           /// </summary>           
           public decimal? TotalAmt {get;set;}

    }
}
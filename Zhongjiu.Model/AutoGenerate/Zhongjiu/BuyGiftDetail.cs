﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///买赠商品详情表
    ///</summary>

    [SugarTable("himall_BuyGiftDetail","Zhongjiu")]
    public partial class BuyGiftDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:买赠ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BuyGiftId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:类型（0=买；1=赠）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Type {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单佣金推送天津电票记录
    ///</summary>

    [SugarTable("ZJ_ElectronicInvoicePushOrderLog","Zhongjiu")]
    public partial class ElectronicInvoicePushOrderLog
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:订单佣金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Brokerage {get;set;}

           /// <summary>
           /// Desc:推广员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:订单状态[-1结果验签失败,0推送失败,1推送成功,2领取成功]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:备注说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:推送订单
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PushOrder {get;set;}

    }
}
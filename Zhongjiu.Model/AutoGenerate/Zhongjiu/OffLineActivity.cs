﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下活动表（弃用）
    ///</summary>

    [SugarTable("himall_OffLineActivity","Zhongjiu")]
    public partial class OffLineActivity
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ActivityName {get;set;}

           /// <summary>
           /// Desc:活动开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:活动结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:活动说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Note {get;set;}

           /// <summary>
           /// Desc:活动类型 （1、特价 2、满增 3、满减 4、买一送一）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ActivityType {get;set;}

           /// <summary>
           /// Desc:参加活动店铺ID，多个店铺以逗号分隔
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:活动审核状态[1:未审核][2:审核通过][3:审核拒绝][4:终止]
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:权威标识，[0:普通区域管理发布][1:网站超级管理员发布]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuthorityStatus {get;set;}

           /// <summary>
           /// Desc:用户进行中的活动，进行了修改需要再次申请，存放进行中活动的ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long AgainReapplyId {get;set;}

    }
}
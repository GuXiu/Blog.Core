﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺客显广告表
    ///</summary>

    [SugarTable("Himall_Advertisement","Zhongjiu")]
    public partial class Advertisement
    {
           /// <summary>
           /// Desc:ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:广告位类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ResourceType {get;set;}

           /// <summary>
           /// Desc:1.图片，2.视频,3顶图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DataType {get;set;}

           /// <summary>
           /// Desc:资源路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FilePath {get;set;}

           /// <summary>
           /// Desc:图片轮播间隔(s)
           /// Default:3
           /// Nullable:True
           /// </summary>           
           public int? IntervalTime {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateDate {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Creator {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyDate {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Modifier {get;set;}

           /// <summary>
           /// Desc:是否可用, 0 不可用 1 可用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Enables {get;set;}

           /// <summary>
           /// Desc:图片的排序
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Sort {get;set;}

           /// <summary>
           /// Desc:体系用户名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SystemName {get;set;}

    }
}
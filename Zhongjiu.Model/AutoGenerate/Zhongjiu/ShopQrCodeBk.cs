﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///门店注册二维码、接收消息二维码按照微信公众号原始Id备份，以备门店变换绑定公众号从此表中直接取，无需再调用微信接口生成数量有限的永久带参数的二维码
    ///</summary>

    [SugarTable("ZJ_ShopQrCodeBk","Zhongjiu")]
    public partial class ShopQrCodeBk
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:微信公众号原始Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXOriginalId {get;set;}

           /// <summary>
           /// Desc:门店注册二维码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RegisterQrCode {get;set;}

           /// <summary>
           /// Desc:门店接收消息二维码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveMsgQrCode {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime AddDate {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺新订单提醒设置
    ///</summary>

    [SugarTable("ZJ_NewOrderSetting","Zhongjiu")]
    public partial class NewOrderSetting
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:语音播报状态：0关闭，1打开
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool AudioStatus {get;set;}

           /// <summary>
           /// Desc:音量，0-100
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Volume {get;set;}

           /// <summary>
           /// Desc:播放次数：1，2，3
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int PlayCount {get;set;}

           /// <summary>
           /// Desc:自动打印小票：0关闭，1打开
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool PrintNoteStatus {get;set;}

           /// <summary>
           /// Desc:线上订单开启：0false,1true
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsOnlineOrder {get;set;}

           /// <summary>
           /// Desc:京东到家订单开启：0false,1true
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsJd {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreateId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime ModifyTime {get;set;}

           /// <summary>
           /// Desc:是否指定收银员打印小票:0不指定 1指定
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsAppointManager {get;set;}

           /// <summary>
           /// Desc:指定收银员ID,多个ID之间用逗号分隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppointManager {get;set;}

    }
}
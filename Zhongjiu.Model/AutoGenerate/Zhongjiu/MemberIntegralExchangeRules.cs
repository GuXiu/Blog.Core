﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户积分兑换规则表
    ///</summary>

    [SugarTable("Himall_MemberIntegralExchangeRules","Zhongjiu")]
    public partial class MemberIntegralExchangeRules
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:一块钱对应多少积分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IntegralPerMoney {get;set;}

           /// <summary>
           /// Desc:一个积分对应多少钱
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int MoneyPerIntegral {get;set;}

           /// <summary>
           /// Desc:部门ID 0是平台积分规则， 其它值是对应体系规则
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Type {get;set;}

    }
}
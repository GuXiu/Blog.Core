﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///拼团活动项
    ///</summary>

    [SugarTable("Himall_FightGroupActiveItem","Zhongjiu")]
    public partial class FightGroupActiveItem
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:所属活动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ActiveId {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:商品SKU
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SkuId {get;set;}

           /// <summary>
           /// Desc:活动售价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ActivePrice {get;set;}

           /// <summary>
           /// Desc:活动库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ActiveStock {get;set;}

           /// <summary>
           /// Desc:己售
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BuyCount {get;set;}

    }
}
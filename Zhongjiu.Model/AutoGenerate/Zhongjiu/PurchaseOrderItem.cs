﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购订单明细
    ///</summary>

    [SugarTable("ZJ_PurchaseOrderItem","Zhongjiu")]
    public partial class PurchaseOrderItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:采购订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long PurchaseId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:采购数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:已经入库的数据
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal EnterNumber {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

           /// <summary>
           /// Desc:含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:即时库存数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Stock {get;set;}

           /// <summary>
           /// Desc:含税总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? VATinclusiveTotalPrice {get;set;}

           /// <summary>
           /// Desc:不含税总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? NVATinclusiveTotalPrice {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///平台分类表
    ///</summary>

    [SugarTable("Himall_Categories","Zhongjiu")]
    public partial class Categories
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:分类图标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Icon {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ParentCategoryId {get;set;}

           /// <summary>
           /// Desc:分类的深度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Depth {get;set;}

           /// <summary>
           /// Desc:分类的路径（以|分离）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Path {get;set;}

           /// <summary>
           /// Desc:分类名称的路径（以|分离）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PathName {get;set;}

           /// <summary>
           /// Desc:未使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RewriteName {get;set;}

           /// <summary>
           /// Desc:是否有子分类
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte HasChildren {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TypeId {get;set;}

           /// <summary>
           /// Desc:分佣比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisRate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Description {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Keywords {get;set;}

           /// <summary>
           /// Desc:是否推荐 0 不推荐 1 推荐
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? IsRecommend {get;set;}

           /// <summary>
           /// Desc:最后一次更新时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastAsyncDateTime {get;set;}

    }
}
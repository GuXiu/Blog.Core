﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///库存不足提醒记录表
    ///</summary>

    [SugarTable("ZJ_StockNotEnoughMsgLog","Zhongjiu")]
    public partial class StockNotEnoughMsgLog
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:提醒时库存
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ThenStock {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? StockThresholdMin {get;set;}

           /// <summary>
           /// Desc:提醒库存阈值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? StockThresholdMax {get;set;}

           /// <summary>
           /// Desc:阈值来源 0店铺设置 1商品设置
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ThresholdSource {get;set;}

           /// <summary>
           /// Desc:状态0未送1已发送
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? State {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:productId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:sku表ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string skuId {get;set;}

    }
}
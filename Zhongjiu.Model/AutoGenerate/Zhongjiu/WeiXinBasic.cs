﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信基础信息表
    ///</summary>

    [SugarTable("Himall_WeiXinBasic","Zhongjiu")]
    public partial class WeiXinBasic
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:微信Ticket
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Ticket {get;set;}

           /// <summary>
           /// Desc:微信Ticket过期日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? TicketOutTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccessToken {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺品牌表
    ///</summary>

    [SugarTable("Himall_ShopBrands","Zhongjiu")]
    public partial class ShopBrands
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商家Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:品牌Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

    }
}
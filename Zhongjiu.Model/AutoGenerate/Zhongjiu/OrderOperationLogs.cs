﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单操作日志表
    ///</summary>

    [SugarTable("Himall_OrderOperationLogs","Zhongjiu")]
    public partial class OrderOperationLogs
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:操作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Operator {get;set;}

           /// <summary>
           /// Desc:操作日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OperateDate {get;set;}

           /// <summary>
           /// Desc:操作内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OperateContent {get;set;}

           /// <summary>
           /// Desc:操作人Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ManagerId {get;set;}

           /// <summary>
           /// Desc:操作人Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:身份类型[0:系统][1:用户][2:店铺管理员][3:平台管理员]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IdentityType {get;set;}

           /// <summary>
           /// Desc:订单状态 [Description('待付款')]WaitPay = 1,[Description('待发货')]WaitDelivery,[Description('待收货')]WaitReceiving,[Description('已关闭')]Close,[Description('已完成')]Finish
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int OrderStatus {get;set;}

           /// <summary>
           /// Desc:操作类型[0:固定程序执行,1:自定义手动输入]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int OperationType {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品视频表
    ///</summary>

    [SugarTable("ZJ_ProductVideos","Zhongjiu")]
    public partial class ProductVideos
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:绑定店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:视频类型，1 主图，2 商品详情
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int VideoType {get;set;}

           /// <summary>
           /// Desc:视频名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string VideoName {get;set;}

           /// <summary>
           /// Desc:视频描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoDesc {get;set;}

           /// <summary>
           /// Desc:商品资源服务器路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VideoUrl {get;set;}

           /// <summary>
           /// Desc:视频封面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Poster {get;set;}

           /// <summary>
           /// Desc:是否启用，0 不启用，1 启用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:审核状态，0 待审核，1 驳回，2 审核通过
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime LastModityTime {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Operator {get;set;}

    }
}
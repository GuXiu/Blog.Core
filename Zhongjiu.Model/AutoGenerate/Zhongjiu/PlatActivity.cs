﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///平台活动
    ///</summary>

    [SugarTable("ZJ_PlatActivity","Zhongjiu")]
    public partial class PlatActivity
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ActivityName {get;set;}

           /// <summary>
           /// Desc:活动状态 根据活动开始时间和结束时间判断活动状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ActivityStatus {get;set;}

           /// <summary>
           /// Desc:提报门店
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ReportShopIds {get;set;}

           /// <summary>
           /// Desc:活动开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:活动结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:商品提报截止时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ReportEndTime {get;set;}

           /// <summary>
           /// Desc:参与规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Rules {get;set;}

           /// <summary>
           /// Desc:推广资源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Resources {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AddTime {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:装修页面Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TemplateId {get;set;}

    }
}
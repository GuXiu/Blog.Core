﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页、店铺首页广告图片表
    ///</summary>

    [SugarTable("Himall_ImageAds","Zhongjiu")]
    public partial class ImageAds
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:图片的存放URL
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ImageUrl {get;set;}

           /// <summary>
           /// Desc:图片的调整地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:是否是横向长广告
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsTransverseAD {get;set;}

           /// <summary>
           /// Desc:类别
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TypeId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///卡券识别记录表
    ///</summary>

    [SugarTable("ZJ_CardCodeRecord","Zhongjiu")]
    public partial class CardCodeRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户openID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:CardCode二维码唯一标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserCardCode {get;set;}

           /// <summary>
           /// Desc:卡券Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardId {get;set;}

           /// <summary>
           /// Desc:扫码时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:CardCode表的Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? CardCodeId {get;set;}

    }
}
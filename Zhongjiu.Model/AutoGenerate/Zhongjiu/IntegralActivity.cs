﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///积分活动表
    ///</summary>

    [SugarTable("ZJ_IntegralActivity","Zhongjiu")]
    public partial class IntegralActivity
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:状态1正常 0下架
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Auditor {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AuditTime {get;set;}

           /// <summary>
           /// Desc:取消原因
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CancelReson {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Creator {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:活动适用商品范围[1:所有商品][0:指定商品]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int RangeOfActivity {get;set;}

           /// <summary>
           /// Desc:积分赠送方式0倍数 1直接赠送
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IntegralGrantType {get;set;}

           /// <summary>
           /// Desc:积分数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IntegralNumber {get;set;}

           /// <summary>
           /// Desc:会员等级id(1,2)形式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SuitableLevel {get;set;}

           /// <summary>
           /// Desc:活动时间规则 0不限1周期2生日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? TimeRulesType {get;set;}

           /// <summary>
           /// Desc:时间周期规律0每年1每月2每周
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CycleRulesType {get;set;}

           /// <summary>
           /// Desc:周期规则日期值从1开始
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? CycleRulesValue {get;set;}

           /// <summary>
           /// Desc:活动下架时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CancelTime {get;set;}

    }
}
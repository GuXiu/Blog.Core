﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分类品牌关联文章表
    ///</summary>

    [SugarTable("Himall_BrandArticleClassify","Zhongjiu")]
    public partial class BrandArticleClassify
    {
           /// <summary>
           /// Desc:id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long id {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:类型id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long typeId {get;set;}

           /// <summary>
           /// Desc:品牌类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string brandType {get;set;}

           /// <summary>
           /// Desc:品牌id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long brandid {get;set;}

           /// <summary>
           /// Desc:品牌名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string brand {get;set;}

           /// <summary>
           /// Desc:内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string content {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime creatime {get;set;}

           /// <summary>
           /// Desc:是否显示
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? isShow {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string keywords {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string description {get;set;}

           /// <summary>
           /// Desc:品牌文章来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Source {get;set;}

    }
}
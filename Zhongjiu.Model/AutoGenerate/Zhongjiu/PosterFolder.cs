﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///海报素材文件夹
    ///</summary>

    [SugarTable("ZJ_PosterFolder","Zhongjiu")]
    public partial class PosterFolder
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:父级Id，顶级为0
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:层级Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CascadeId {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:正序排序
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sort {get;set;}

           /// <summary>
           /// Desc:可用
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool Enable {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
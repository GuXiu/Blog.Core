﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员等级变更记录表
    ///</summary>

    [SugarTable("ZJ_MemeberGradeChangeRecored","Zhongjiu")]
    public partial class MemeberGradeChangeRecored
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SourceIntegral {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? TargetIntegral {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? SourceGrade {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? TargetGrade {get;set;}

           /// <summary>
           /// Desc:会员id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MemberId {get;set;}

           /// <summary>
           /// Desc:体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:会员类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? TypeId {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

    }
}
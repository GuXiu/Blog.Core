﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>

    [SugarTable("ZJ_WechatGroupChat","Zhongjiu")]
    public partial class WechatGroupChat
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:所属区域
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long AreaId {get;set;}

           /// <summary>
           /// Desc:微信群聊名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GroupChatName {get;set;}

           /// <summary>
           /// Desc:微信群聊链接
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string GroupChatUrl {get;set;}

           /// <summary>
           /// Desc:是否启用 0 禁用。1 启用
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品表
    ///</summary>

    [SugarTable("himall_OffLineProducts","Zhongjiu")]
    public partial class OffLineProducts
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:库存
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Stock {get;set;}

           /// <summary>
           /// Desc:库存总量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal StockTotal {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:商品条码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductModel {get;set;}

           /// <summary>
           /// Desc:价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:状态（1=在售 ;0=停售）
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool ProductStates {get;set;}

           /// <summary>
           /// Desc:生产厂家
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Factory {get;set;}

           /// <summary>
           /// Desc:是否线上销售
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsOnline {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:线上商品审核状态 1=待审核 2= 已通过（已受理） 3=已拒绝 4 =已完成 5=创建失败
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:是否删除
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDelete {get;set;}

           /// <summary>
           /// Desc:入库价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal EnterPrice {get;set;}

           /// <summary>
           /// Desc:分类ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:商品分类 路径(一级分类ID|二级分类ID|三级分类ID)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryPath {get;set;}

           /// <summary>
           /// Desc:品牌ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

           /// <summary>
           /// Desc:创建人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CreaterName {get;set;}

           /// <summary>
           /// Desc:规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BottleCapacity {get;set;}

           /// <summary>
           /// Desc:箱容量 箱/瓶
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BoxCapacity {get;set;}

           /// <summary>
           /// Desc:同步自那个商品的Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SynchroProductId {get;set;}

           /// <summary>
           /// Desc:是否支持打折（会员打折），[默认=1,支持][2,不支持]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsDiscounted {get;set;}

    }
}
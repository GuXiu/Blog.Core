﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>

    [SugarTable("ZJ_InformationcentreMemberInfo","Zhongjiu")]
    public partial class InformationcentreMemberInfo
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:开放平台用户id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:信息中心用户id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long InformationcentreMemberId {get;set;}

           /// <summary>
           /// Desc:手机号码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Cellphone {get;set;}

           /// <summary>
           /// Desc:所属体系
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? InfoCenterId {get;set;}

    }
}
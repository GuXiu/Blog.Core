﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺红包领取记录表
    ///</summary>

    [SugarTable("Himall_ShopBonusReceive","Zhongjiu")]
    public partial class ShopBonusReceive
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:红包Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BonusGrantId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:面额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Price {get;set;}

           /// <summary>
           /// Desc:1.未使用  2.已使用  3.已过期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int State {get;set;}

           /// <summary>
           /// Desc:领取时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReceiveTime {get;set;}

           /// <summary>
           /// Desc:使用时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UsedTime {get;set;}

           /// <summary>
           /// Desc:UserID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:使用的订单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UsedOrderId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXHead {get;set;}

    }
}
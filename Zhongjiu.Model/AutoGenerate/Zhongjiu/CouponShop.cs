﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺　关联　支持跨店使用　的优惠券　
    ///</summary>

    [SugarTable("Himall_CouponShop","Zhongjiu")]
    public partial class CouponShop
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:关联店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员资金余额表
    ///</summary>

    [SugarTable("Himall_Capital","Zhongjiu")]
    public partial class Capital
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemId {get;set;}

           /// <summary>
           /// Desc:可用余额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? Balance {get;set;}

           /// <summary>
           /// Desc:冻结资金
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? FreezeAmount {get;set;}

           /// <summary>
           /// Desc:累计充值总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? ChargeAmount {get;set;}

           /// <summary>
           /// Desc:体系ID
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public string AccontSet {get;set;}

           /// <summary>
           /// Desc:佣金金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? BrokerageAmount {get;set;}

           /// <summary>
           /// Desc:冻结佣金金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? FreezeBrokerageAmount {get;set;}

           /// <summary>
           /// Desc:拼团返现金额
           /// Default:0.0000
           /// Nullable:False
           /// </summary>           
           public decimal FightBrokerageAmount {get;set;}

           /// <summary>
           /// Desc:拼团返现冻结金额
           /// Default:0.0000
           /// Nullable:False
           /// </summary>           
           public decimal FreezeFightBrokerageAmount {get;set;}

    }
}
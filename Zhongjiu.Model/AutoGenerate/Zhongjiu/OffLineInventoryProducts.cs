﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品出入库明细表
    ///</summary>

    [SugarTable("Himall_OffLineInventoryProducts","Zhongjiu")]
    public partial class OffLineInventoryProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:库存Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long InventoryId {get;set;}

           /// <summary>
           /// Desc:线下商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:入库/出库价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:入库/出库数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remarks {get;set;}

           /// <summary>
           /// Desc:商品合并表备份原来商品ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? BackProductId {get;set;}

           /// <summary>
           /// Desc:订单号
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:调拨入库数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? InNumber {get;set;}

           /// <summary>
           /// Desc:调拨入库时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime InDate {get;set;}

           /// <summary>
           /// Desc:盘亏出库价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal InventoryLossOutPrice {get;set;}

           /// <summary>
           /// Desc:盘亏金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal InventoryLossPrice {get;set;}

    }
}
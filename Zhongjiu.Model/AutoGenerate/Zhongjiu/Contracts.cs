﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///合同信息表
    ///</summary>

    [SugarTable("ZJ_Contracts","Zhongjiu")]
    public partial class Contracts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string No {get;set;}

           /// <summary>
           /// Desc:供货范围
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GHArea {get;set;}

           /// <summary>
           /// Desc:签订时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? QDTime {get;set;}

           /// <summary>
           /// Desc:原始合同号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OldContractNo {get;set;}

           /// <summary>
           /// Desc:签订人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QDPerson {get;set;}

           /// <summary>
           /// Desc:合同有效期 开始日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UsedStartTime {get;set;}

           /// <summary>
           /// Desc:合同有效期 结束日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UsedEndTime {get;set;}

           /// <summary>
           /// Desc:采购员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Buyer {get;set;}

           /// <summary>
           /// Desc:经营方式 0 购销 ；1 代销
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? JYType {get;set;}

           /// <summary>
           /// Desc:合同附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContractOptions {get;set;}

           /// <summary>
           /// Desc:甲方 公司名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ACompanyName {get;set;}

           /// <summary>
           /// Desc:甲方 法人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ALegal {get;set;}

           /// <summary>
           /// Desc:甲方 营业执照
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ABusinessLicense {get;set;}

           /// <summary>
           /// Desc:甲方 地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AAddress {get;set;}

           /// <summary>
           /// Desc:甲方 联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ALinkPerson {get;set;}

           /// <summary>
           /// Desc:甲方 电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ATel {get;set;}

           /// <summary>
           /// Desc:乙方 公司名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BCompanyName {get;set;}

           /// <summary>
           /// Desc:乙方 法人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLegal {get;set;}

           /// <summary>
           /// Desc:乙方 营业执照
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BBusinessLicense {get;set;}

           /// <summary>
           /// Desc:乙方 地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BAddress {get;set;}

           /// <summary>
           /// Desc:乙方 联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BLinkPerson {get;set;}

           /// <summary>
           /// Desc:乙方 电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BTel {get;set;}

           /// <summary>
           /// Desc:商品类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductType {get;set;}

           /// <summary>
           /// Desc:品牌
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Brand {get;set;}

           /// <summary>
           /// Desc:商品税额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Tax {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:商标证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string A {get;set;}

           /// <summary>
           /// Desc:酒类生产许可证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string B {get;set;}

           /// <summary>
           /// Desc:酒类流通许可证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string C {get;set;}

           /// <summary>
           /// Desc:授权书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string D {get;set;}

           /// <summary>
           /// Desc:商品检测报告
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string E {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Creator {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:审批日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AuditedDate {get;set;}

           /// <summary>
           /// Desc:审批状态 0 待审核；1 已审核；2 已生效；3已失效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? AuditState {get;set;}

           /// <summary>
           /// Desc:门店配送中心
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

    }
}
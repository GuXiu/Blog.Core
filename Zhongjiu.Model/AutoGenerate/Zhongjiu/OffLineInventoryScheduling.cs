﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品调拨表
    ///</summary>

    [SugarTable("Himall_OffLineInventoryScheduling","Zhongjiu")]
    public partial class OffLineInventoryScheduling
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:调拨单号(自动生成)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SchedulingOrderNumber {get;set;}

           /// <summary>
           /// Desc:调拨日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime SchedulingTime {get;set;}

           /// <summary>
           /// Desc:出货仓库(选择下拉框)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OutWarehouseId {get;set;}

           /// <summary>
           /// Desc:入货仓库(选择下拉框)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long InWarehouseId {get;set;}

           /// <summary>
           /// Desc:调拨状态(0进行中，1已完成，2未完成)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:申请人(姓名)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ApplyPeople {get;set;}

           /// <summary>
           /// Desc:申请日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ApplyTime {get;set;}

           /// <summary>
           /// Desc:审核人(姓名)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AuditPeople {get;set;}

           /// <summary>
           /// Desc:审核日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AuditTime {get;set;}

           /// <summary>
           /// Desc:审核状态(0进行中，1已完成，2未完成)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:商品总数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ProductTotals {get;set;}

           /// <summary>
           /// Desc:是否删除
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分享给某人记录表
    ///</summary>

    [SugarTable("ZJ_CouponShareSomebodyRecord","Zhongjiu")]
    public partial class CouponShareSomebodyRecord
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券 券码Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponRecordId {get;set;}

           /// <summary>
           /// Desc:分享会员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShareUserId {get;set;}

           /// <summary>
           /// Desc:先获取 微信信息，检测注册的 直接发到账号，未注册的 需要注册后发到账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceiveOpenId {get;set;}

           /// <summary>
           /// Desc:用户手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceivePhone {get;set;}

           /// <summary>
           /// Desc:领取会员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ReceiveUserId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:领取时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ReceiveTime {get;set;}

           /// <summary>
           /// Desc:[0:未领取][1:已领取][2:已收回]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceiveState {get;set;}

    }
}
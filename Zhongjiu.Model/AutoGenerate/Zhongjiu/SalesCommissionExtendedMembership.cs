﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///拓展会员提成表
    ///</summary>

    [SugarTable("ZJ_SalesCommissionExtendedMembership","Zhongjiu")]
    public partial class SalesCommissionExtendedMembership
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:[金额元为单位]
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ModeValue {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:默认未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
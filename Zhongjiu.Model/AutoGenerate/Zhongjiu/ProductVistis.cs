﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品浏览记录表
    ///</summary>

    [SugarTable("Himall_ProductVistis","Zhongjiu")]
    public partial class ProductVistis
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime Date {get;set;}

           /// <summary>
           /// Desc:浏览次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long VistiCounts {get;set;}

           /// <summary>
           /// Desc:销售量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleCounts {get;set;}

           /// <summary>
           /// Desc:销售额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleAmounts {get;set;}

           /// <summary>
           /// Desc:订单总数
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? OrderCounts {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销员表
    ///</summary>

    [SugarTable("Himall_Promoter","Zhongjiu")]
    public partial class Promoter
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:推销员状态 0审核 1通过 2已拒绝 3已清退
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ApplyTime {get;set;}

           /// <summary>
           /// Desc:通过时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PassTime {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:锁定开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LockStart {get;set;}

           /// <summary>
           /// Desc:锁定结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LockEnd {get;set;}

           /// <summary>
           /// Desc:锁定期类型（0：非永久，1：永久）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Forever {get;set;}

           /// <summary>
           /// Desc:分销员二维码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QRCode {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺扩展表，存放一些不常用信息
    ///</summary>

    [SugarTable("ZJ_ShopsExpansion","Zhongjiu")]
    public partial class ShopsExpansion
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:扫码购 微信小程序下单付款后，取货提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MiniProgramOrderWord {get;set;}

           /// <summary>
           /// Desc:云柜 微信小程序下单付款后，取货提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MiniCabinetOrderWord {get;set;}

           /// <summary>
           /// Desc:云柜 微信小程序下单付款后，联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MiniCabinetOrderPhone {get;set;}

           /// <summary>
           /// Desc:打印云码所需最小订单金额
           /// Default:0.0000
           /// Nullable:False
           /// </summary>           
           public decimal MinAmountToShowCloudCode {get;set;}

           /// <summary>
           /// Desc:是否同步门店商品名称 0 否；1 是
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsAsyncPName {get;set;}

           /// <summary>
           /// Desc:图文素材ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PictureTextMaterialID {get;set;}

           /// <summary>
           /// Desc:会员积分抵现百分比
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? OffsetSwithPercentage {get;set;}

           /// <summary>
           /// Desc:商品单价保留小数位数类型：0四舍五入，1向下取整，2向上取整
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? UnitPriceFractionalType {get;set;}

           /// <summary>
           /// Desc:商品单价保留小数位数
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? UnitPriceFractional {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TagId {get;set;}

           /// <summary>
           /// Desc:[0:未开启，1:开启]是否启用 ZJ_ProductSupplements 的 FixedSalePrice 固定销售价格
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsFixedSalePrice {get;set;}

           /// <summary>
           /// Desc:店铺二维码（设置接收消息推送）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopQRcodeForReceiveMessages {get;set;}

           /// <summary>
           /// Desc:默认库存不足提醒阈值
           /// Default:0.0000
           /// Nullable:True
           /// </summary>           
           public decimal? StockDefalutThreshold {get;set;}

           /// <summary>
           /// Desc:一品多商 是否读取门店配置 0 读取平台体系配置；1 读取门店配置
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte YPDSIsReadShopConfig {get;set;}

           /// <summary>
           /// Desc:一品多商 体系范围 1 所有体系；2 本体系
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int YPDSAccountSetArea {get;set;}

           /// <summary>
           /// Desc:一品多商 距离范围 1 不限；2 同省；3 同城；4 距离
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int YPDSDistanceArea {get;set;}

           /// <summary>
           /// Desc:一品多商 距离
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal YPDSDistances {get;set;}

           /// <summary>
           /// Desc:承诺书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LetterOfCommitment {get;set;}

           /// <summary>
           /// Desc:是否同意入驻协议
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsReadSettleAgreement {get;set;}

           /// <summary>
           /// Desc:配送单打印比例
           /// Default:100.000
           /// Nullable:False
           /// </summary>           
           public decimal DeliveryOrderPrintScale {get;set;}

           /// <summary>
           /// Desc:开启扫码切换分销员绑定关系
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte EnableSwitchDistribuitorBindRelationship {get;set;}

           /// <summary>
           /// Desc:是否支持交易赠送体系积分(0不支持 1支持)
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? IsSupportGiveSystemIntegral {get;set;}

           /// <summary>
           /// Desc:是否提醒生日会员
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool IsTipVipBirthday {get;set;}

    }
}
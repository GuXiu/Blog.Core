﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微店配置表
    ///</summary>

    [SugarTable("Himall_WXshop","Zhongjiu")]
    public partial class WXshop
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:公众号的APPID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppId {get;set;}

           /// <summary>
           /// Desc:公众号的AppSecret
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AppSecret {get;set;}

           /// <summary>
           /// Desc:公众号原始Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OriginalId {get;set;}

           /// <summary>
           /// Desc:公众号的Token
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Token {get;set;}

           /// <summary>
           /// Desc:跳转的URL
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FollowUrl {get;set;}

    }
}
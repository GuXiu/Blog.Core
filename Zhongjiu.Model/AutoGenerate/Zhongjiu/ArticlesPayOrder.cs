﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///文章支付表
    ///</summary>

    [SugarTable("ZJ_ArticlesPayOrder","Zhongjiu")]
    public partial class ArticlesPayOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:文章id 逻辑外键  Himall_BrandArticleClassify 的 Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ArticleId {get;set;}

           /// <summary>
           /// Desc:文章标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ArticleTitle {get;set;}

           /// <summary>
           /// Desc:总额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Total {get;set;}

           /// <summary>
           /// Desc:文章订单创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:支付状态 0：待支付，1：支付成功，2：支付异常，3：关闭
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int PayState {get;set;}

           /// <summary>
           /// Desc:支付方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayType {get;set;}

           /// <summary>
           /// Desc:支付方式名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayTypeName {get;set;}

           /// <summary>
           /// Desc:交易流水单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayOrderId {get;set;}

           /// <summary>
           /// Desc:支付时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PayTime {get;set;}

    }
}
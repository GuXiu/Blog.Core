﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下活动明细表（弃用）
    ///</summary>

    [SugarTable("himall_OffLineActivityItems","Zhongjiu")]
    public partial class OffLineActivityItems
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ActivityId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:商品条码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductModel {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:[ActivityType(1,特价)活动价格][(2,满赠)满赠金额][(3,满减)满减金额][(4,买一送一)赠送商品id逗号分隔]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContainerOne {get;set;}

           /// <summary>
           /// Desc:[ActivityType(1,特价)商品价格][(2,满赠)赠送商品id逗号分隔][(3,满减)减满金额][(4,买赠)条码及数量]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContainerTwo {get;set;}

    }
}
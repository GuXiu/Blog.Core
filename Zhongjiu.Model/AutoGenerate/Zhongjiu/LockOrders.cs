﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单锁定设置
    ///</summary>

    [SugarTable("ZJ_LockOrders","Zhongjiu")]
    public partial class LockOrders
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:开启
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsOpen {get;set;}

           /// <summary>
           /// Desc:退款限制
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool IsStopRefund {get;set;}

           /// <summary>
           /// Desc:规则
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Rules {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

    }
}
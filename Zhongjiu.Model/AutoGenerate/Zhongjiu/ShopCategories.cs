﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺分类表
    ///</summary>

    [SugarTable("Himall_ShopCategories","Zhongjiu")]
    public partial class ShopCategories
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:上级分类ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ParentCategoryId {get;set;}

           /// <summary>
           /// Desc:分类名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:分类的深度
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Depth {get;set;}

           /// <summary>
           /// Desc:分类的路径（以|分离）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Path {get;set;}

           /// <summary>
           /// Desc:分类名称的路径（以|分离）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PathName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:是否显示
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsShow {get;set;}

           /// <summary>
           /// Desc:分类图标
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Icon {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///基库商品和商品属性值关系表
    ///</summary>

    [SugarTable("Himall_BaseProductAttributes","Zhongjiu")]
    public partial class BaseProductAttributes
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:属性ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AttributeId {get;set;}

           /// <summary>
           /// Desc:属性值ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ValueId {get;set;}

    }
}
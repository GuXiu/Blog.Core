﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信用户群组表
    ///</summary>

    [SugarTable("ZJ_WeChatUserGroup","Zhongjiu")]
    public partial class WeChatUserGroup
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:群组Id 
           /// Default:Default
           /// Nullable:True
           /// </summary>           
           public string GroupId {get;set;}

           /// <summary>
           /// Desc:群组名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GroupName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? WeChatUserId {get;set;}

           /// <summary>
           /// Desc:用户总步数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserSteps {get;set;}

    }
}
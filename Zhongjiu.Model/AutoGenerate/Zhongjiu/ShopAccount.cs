﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺资金表
    ///</summary>

    [SugarTable("Himall_ShopAccount","Zhongjiu")]
    public partial class ShopAccount
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:帐户余额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Balance {get;set;}

           /// <summary>
           /// Desc:待结算
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PendingSettlement {get;set;}

           /// <summary>
           /// Desc:已结算
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Settled {get;set;}

    }
}
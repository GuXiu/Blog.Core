﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///拆零商品关系表
    ///</summary>

    [SugarTable("ZJ_ProductChaiLingRelation_copy","Zhongjiu")]
    public partial class ProductChaiLingRelation_copy
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:大包装商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BigProductId {get;set;}

           /// <summary>
           /// Desc:小包装商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long SmallProductId {get;set;}

           /// <summary>
           /// Desc:拆零数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ProductNumber {get;set;}

           /// <summary>
           /// Desc:是否删除
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsDel {get;set;}

           /// <summary>
           /// Desc:添加人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Modifier {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:更新时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///电子价签商品二维码扫码记录表
    ///</summary>

    [SugarTable("ZJ_ProductDetailScanFrequency","Zhongjiu")]
    public partial class ProductDetailScanFrequency
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:扫描频次
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Frequency {get;set;}

           /// <summary>
           /// Desc:扫描时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? ScanTime {get;set;}

    }
}
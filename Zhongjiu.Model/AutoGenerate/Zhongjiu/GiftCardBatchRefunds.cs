﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///礼品卡退款
    ///</summary>

    [SugarTable("ZJ_GiftCardBatchRefunds","Zhongjiu")]
    public partial class GiftCardBatchRefunds
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:批次Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BatchId {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundAmount {get;set;}

           /// <summary>
           /// Desc:礼品卡明细Ids，逗号分隔
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GiftItemIds {get;set;}

           /// <summary>
           /// Desc:支付流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TradNo {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:状态：-1支付失败；0已发起支付，未收到异步通知；1支付成功；
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:更新时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

    }
}
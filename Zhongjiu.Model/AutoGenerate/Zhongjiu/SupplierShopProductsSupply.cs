﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///供应商门店商品供货关系表
    ///</summary>

    [SugarTable("ZJ_SupplierShopProductsSupply","Zhongjiu")]
    public partial class SupplierShopProductsSupply
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:供应商Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SupplierId {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:经营商品状态 0 不授权； 1 授权
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页楼层表
    ///</summary>

    [SugarTable("Himall_HomeFloors","Zhongjiu")]
    public partial class HomeFloors
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:楼层名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FloorName {get;set;}

           /// <summary>
           /// Desc:楼层小标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SubName {get;set;}

           /// <summary>
           /// Desc:显示顺序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:是否显示的首页
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsShow {get;set;}

           /// <summary>
           /// Desc:楼层所属样式（目前支持2套）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int StyleLevel {get;set;}

           /// <summary>
           /// Desc:楼层的默认tab标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DefaultTabName {get;set;}

           /// <summary>
           /// Desc:更多链接地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MoreUrl {get;set;}

    }
}
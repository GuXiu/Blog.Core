﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///发票内容表
    ///</summary>

    [SugarTable("Himall_InvoiceContext","Zhongjiu")]
    public partial class InvoiceContext
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:发票名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

    }
}
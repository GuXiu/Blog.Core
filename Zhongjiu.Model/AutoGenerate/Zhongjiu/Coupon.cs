﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城优惠券表
    ///</summary>

    [SugarTable("Himall_Coupon","Zhongjiu")]
    public partial class Coupon
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:最大可领取张数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PerMax {get;set;}

           /// <summary>
           /// Desc:订单金额（满足多少钱才能使用）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal OrderAmount {get;set;}

           /// <summary>
           /// Desc:发行张数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Num {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:优惠券名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CouponName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:[0：默认，店铺首页][1:积分兑换][2:主动发放][3:对外领取][4:发给新用户(注册自动领取)]，5：券码/海报领取，6：云码领取
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceiveType {get;set;}

           /// <summary>
           /// Desc:所需积分
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int NeedIntegral {get;set;}

           /// <summary>
           /// Desc:兑换截止时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EndIntegralExchange {get;set;}

           /// <summary>
           /// Desc:积分商城封面
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IntegralCover {get;set;}

           /// <summary>
           /// Desc:是否同步到微信
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsSyncWeiXin {get;set;}

           /// <summary>
           /// Desc:微信状态
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int WXAuditStatus {get;set;}

           /// <summary>
           /// Desc:微信卡券记录号 与微信卡券记录关联
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? CardLogId {get;set;}

           /// <summary>
           /// Desc:适用场景  0 全店 1 品类 2 品牌 3 商品
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ApplyScene {get;set;}

           /// <summary>
           /// Desc:品类适用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? CategoryApplyId {get;set;}

           /// <summary>
           /// Desc:品牌使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? BrandApplyId {get;set;}

           /// <summary>
           /// Desc:商品适用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductApplyId {get;set;}

           /// <summary>
           /// Desc:允许领取时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReceiveStartTime {get;set;}

           /// <summary>
           /// Desc:允许领取时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReceiveEndTime {get;set;}

           /// <summary>
           /// Desc:是否在首页显示0不显示，1显示
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsShowHome {get;set;}

           /// <summary>
           /// Desc:优惠券链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Link {get;set;}

           /// <summary>
           /// Desc:上级Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:省市管理员发布活动，关联店铺Id集合，逗号分隔字符串
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:是否支持跨店使用[0:默认不支持][1:支持]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsStrideOver {get;set;}

           /// <summary>
           /// Desc:优惠类型，[0:默认，优惠券价格][1:折扣，折扣的数值包含0和100(如果打9折，请输入90)][2:指定价格]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int PriceType {get;set;}

           /// <summary>
           /// Desc:是否包含邮费，[0:默认不包含][1:包含]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsContainPostage {get;set;}

           /// <summary>
           /// Desc:使用对象，多选逗号分隔[Himall_MemberGrade表的Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UseRange {get;set;}

           /// <summary>
           /// Desc:商品数量限制，指定数量内(包含该数量)可享受 面值、折扣、制定价格，超出数量 按原价走
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ProductApplyLimitNumber {get;set;}

           /// <summary>
           /// Desc:是否支持分发和转让[0:不支持][1:支持]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsDistributionTransfer {get;set;}

           /// <summary>
           /// Desc:生效类型[0:默认 绝对时间 固定 开始结束时间][1:相对时间 领取后 第X天—第Y天内有效][2:相对时间 领取后 第X月—第Y月内有效]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TakeEffectType {get;set;}

           /// <summary>
           /// Desc:生效 开始 第Number天
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TakeEffectBeginNumber {get;set;}

           /// <summary>
           /// Desc:生效 结束 第Number天
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TakeEffectEndNumber {get;set;}

           /// <summary>
           /// Desc:多商品折扣，最大可用商品种类数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MaxSKUNumber {get;set;}

           /// <summary>
           /// Desc:商品适用，用法 【0、（商品数量语义为：最大可优惠商品数量）；1、（商品数量语义为：最小加入购买数量才能使用优惠券）；】
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ProductApplyUsage {get;set;}

           /// <summary>
           /// Desc:标签
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CouponTag {get;set;}

           /// <summary>
           /// Desc:领取区域类型[0:不限制][1:指定区域可领][2:指定区域不可领]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceiveAreaType {get;set;}

           /// <summary>
           /// Desc:使用区域类型[0:不限制][1:指定区域可用][2:指定区域不可用]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int UseAreaType {get;set;}

           /// <summary>
           /// Desc:参与条件：-1不限制，0加入群聊，1添加客服
           /// Default:-1
           /// Nullable:False
           /// </summary>           
           public int JoinWxGroupChatType {get;set;}

           /// <summary>
           /// Desc:是否已激活 ApplyScene类型为券码时使用
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Actived {get;set;}

    }
}
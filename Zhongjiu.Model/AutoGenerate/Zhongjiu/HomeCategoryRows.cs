﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页分类所属行表
    ///</summary>

    [SugarTable("Himall_HomeCategoryRows","Zhongjiu")]
    public partial class HomeCategoryRows
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:行ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RowId {get;set;}

           /// <summary>
           /// Desc:所属行推荐图片1
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Image1 {get;set;}

           /// <summary>
           /// Desc:所属行推荐图片1的URL
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url1 {get;set;}

           /// <summary>
           /// Desc:所属行推荐图片2
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Image2 {get;set;}

           /// <summary>
           /// Desc:所属行推荐图片2的URL
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url2 {get;set;}

    }
}
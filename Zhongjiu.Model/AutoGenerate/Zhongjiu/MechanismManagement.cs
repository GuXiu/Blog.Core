﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///机构档案表
    ///</summary>

    [SugarTable("ZJ_MechanismManagement","Zhongjiu")]
    public partial class MechanismManagement
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:机构编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MNo {get;set;}

           /// <summary>
           /// Desc:机构名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MName {get;set;}

           /// <summary>
           /// Desc:管理员名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MUserName {get;set;}

           /// <summary>
           /// Desc:机构类型 0 区域中心；1  门店；2 配送中心
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? MType {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:省
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MProvince {get;set;}

           /// <summary>
           /// Desc:市
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MCity {get;set;}

           /// <summary>
           /// Desc:区
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MArea {get;set;}

           /// <summary>
           /// Desc:详细地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MAddress {get;set;}

           /// <summary>
           /// Desc:手写机构地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MHandWriteAddress {get;set;}

           /// <summary>
           /// Desc:纬度
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MLatitude {get;set;}

           /// <summary>
           /// Desc:经度
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MLongitude {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Operator {get;set;}

           /// <summary>
           /// Desc:关联体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:关联顶级体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MPost {get;set;}

           /// <summary>
           /// Desc:门店Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:上级机构
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MDepartmentId {get;set;}

           /// <summary>
           /// Desc:主配送中心
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MFirstDistributionCenter {get;set;}

           /// <summary>
           /// Desc:会员积分抵扣开关 默认关闭
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? OffsetSwith {get;set;}

           /// <summary>
           /// Desc:会员等级折扣开关 默认关闭
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? DiscountSwith {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系短信模板表
    ///</summary>

    [SugarTable("Himall_Messages","Zhongjiu")]
    public partial class Messages
    {
           /// <summary>
           /// Desc:id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:短信类型 （1.用户注册）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? TypeId {get;set;}

           /// <summary>
           /// Desc:短信内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Content {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModificationTime {get;set;}

    }
}
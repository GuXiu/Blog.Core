﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系公众号自动回复消息
    ///</summary>

    [SugarTable("ZJ_ShopAutomaticReplyMessages","Zhongjiu")]
    public partial class ShopAutomaticReplyMessages
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:体系公众号原始Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXOriginalId {get;set;}

           /// <summary>
           /// Desc:消息详情
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MessageDetails {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContentsInfo {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddDate {get;set;}

    }
}
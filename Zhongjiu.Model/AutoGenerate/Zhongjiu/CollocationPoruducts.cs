﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///组合购商品表
    ///</summary>

    [SugarTable("Himall_CollocationPoruducts","Zhongjiu")]
    public partial class CollocationPoruducts
    {
           /// <summary>
           /// Desc:ID自增
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:组合购ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ColloId {get;set;}

           /// <summary>
           /// Desc:是否主商品
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsMain {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? DisplaySequence {get;set;}

    }
}
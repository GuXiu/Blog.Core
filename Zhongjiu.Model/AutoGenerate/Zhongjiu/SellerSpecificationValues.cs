﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商家规格快照
    ///</summary>

    [SugarTable("Himall_SellerSpecificationValues","Zhongjiu")]
    public partial class SellerSpecificationValues
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:规格值ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ValueId {get;set;}

           /// <summary>
           /// Desc:规格（颜色、尺寸、版本）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Specification {get;set;}

           /// <summary>
           /// Desc:类型ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TypeId {get;set;}

           /// <summary>
           /// Desc:商家的规格值
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Value {get;set;}

    }
}
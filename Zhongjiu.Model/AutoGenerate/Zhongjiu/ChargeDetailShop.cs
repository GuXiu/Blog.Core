﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺充值流水表
    ///</summary>

    [SugarTable("Himall_ChargeDetailShop","Zhongjiu")]
    public partial class ChargeDetailShop
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:充值时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ChargeTime {get;set;}

           /// <summary>
           /// Desc:充值金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ChargeAmount {get;set;}

           /// <summary>
           /// Desc:充值方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChargeWay {get;set;}

           /// <summary>
           /// Desc:充值状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ChargeStatus {get;set;}

           /// <summary>
           /// Desc:提交充值时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

    }
}
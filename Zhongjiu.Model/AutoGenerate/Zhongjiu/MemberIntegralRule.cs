﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///积分规则表
    ///</summary>

    [SugarTable("Himall_MemberIntegralRule","Zhongjiu")]
    public partial class MemberIntegralRule
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:积分规则类型ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TypeId {get;set;}

           /// <summary>
           /// Desc:规则对应的积分数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Integral {get;set;}

           /// <summary>
           /// Desc:获得积分规则所属体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:用户类型0零售1团购2批发3员工
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MemberType {get;set;}

    }
}
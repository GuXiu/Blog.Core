﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺对应的快递网点配置
    ///</summary>

    [SugarTable("ZJ_ExpressPoint","Zhongjiu")]
    public partial class ExpressPoint
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:快递名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ExpressName {get;set;}

           /// <summary>
           /// Desc:快递编码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ExpressCode {get;set;}

           /// <summary>
           /// Desc:网点客户号、客户代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerName {get;set;}

           /// <summary>
           /// Desc:网点客户密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerPwd {get;set;}

           /// <summary>
           /// Desc:月结账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MonthCode {get;set;}

           /// <summary>
           /// Desc:网点编码、仓库id、归属网点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SendSite {get;set;}

           /// <summary>
           /// Desc:收件快递员（网点提供）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SendStaff {get;set;}

           /// <summary>
           /// Desc:邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int PayType {get;set;}

           /// <summary>
           /// Desc:接口生成打印模板尺寸规则，eg：德邦传值130表示76mm*130mm
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Size {get;set;}

           /// <summary>
           /// Desc:打印模板尺寸，高*宽：15*10，13*7.5，（单位mm）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PrintSize {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sort {get;set;}

           /// <summary>
           /// Desc:是否删除
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
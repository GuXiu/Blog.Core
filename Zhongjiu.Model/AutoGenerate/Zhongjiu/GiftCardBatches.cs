﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///礼品卡批次
    ///</summary>

    [SugarTable("ZJ_GiftCardBatches","Zhongjiu")]
    public partial class GiftCardBatches
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:批次号：Tyd0001
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BatchNo {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSetId {get;set;}

           /// <summary>
           /// Desc:是否限定叠加数量
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsLimitSuperposedCount {get;set;}

           /// <summary>
           /// Desc:叠加上限
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SupserposedCount {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BeginDate {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndDate {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:批次状态：0待支付；1已支付；2已取消；3已完成
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:支付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PayAmount {get;set;}

           /// <summary>
           /// Desc:支付单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayNo {get;set;}

           /// <summary>
           /// Desc:支付信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayMsg {get;set;}

           /// <summary>
           /// Desc:取消原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CancelReason {get;set;}

           /// <summary>
           /// Desc:取消时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CancelTime {get;set;}

           /// <summary>
           /// Desc:退款方式：0未退款；1原路返回；
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RefundType {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal RefundAmount {get;set;}

           /// <summary>
           /// Desc:支付流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TradNo {get;set;}

    }
}
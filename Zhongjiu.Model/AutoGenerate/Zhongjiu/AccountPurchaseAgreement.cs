﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///结算预付款表
    ///</summary>

    [SugarTable("Himall_AccountPurchaseAgreement","Zhongjiu")]
    public partial class AccountPurchaseAgreement
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? AccountId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime Date {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PurchaseAgreementId {get;set;}

           /// <summary>
           /// Desc:预付款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal AdvancePayment {get;set;}

           /// <summary>
           /// Desc:平台审核时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime FinishDate {get;set;}

           /// <summary>
           /// Desc:申请
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ApplyDate {get;set;}

    }
}
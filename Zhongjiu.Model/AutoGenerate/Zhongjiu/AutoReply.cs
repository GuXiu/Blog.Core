﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///自动回复设置表
    ///</summary>

    [SugarTable("Himall_AutoReply","Zhongjiu")]
    public partial class AutoReply
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:规则名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RuleName {get;set;}

           /// <summary>
           /// Desc:关键词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Keyword {get;set;}

           /// <summary>
           /// Desc:匹配方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MatchType {get;set;}

           /// <summary>
           /// Desc:文字回复内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TextReply {get;set;}

           /// <summary>
           /// Desc:是否开启
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? IsOpen {get;set;}

           /// <summary>
           /// Desc:消息回复类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ReplyType {get;set;}

    }
}
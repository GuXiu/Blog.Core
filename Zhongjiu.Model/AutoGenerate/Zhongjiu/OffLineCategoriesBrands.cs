﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品分类品牌关联表
    ///</summary>

    [SugarTable("Himall_OffLineCategoriesBrands","Zhongjiu")]
    public partial class OffLineCategoriesBrands
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品(二级)分类Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:品牌Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

    }
}
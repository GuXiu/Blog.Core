﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///运费模板详细表
    ///</summary>

    [SugarTable("Himall_FreightAreaContent","Zhongjiu")]
    public partial class FreightAreaContent
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:运费模板ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FreightTemplateId {get;set;}

           /// <summary>
           /// Desc:地区选择
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AreaContent {get;set;}

           /// <summary>
           /// Desc:首笔单元计量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? FirstUnit {get;set;}

           /// <summary>
           /// Desc:首笔单元费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public float? FirstUnitMonry {get;set;}

           /// <summary>
           /// Desc:递增单元计量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? AccumulationUnit {get;set;}

           /// <summary>
           /// Desc:递增单元费用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public float? AccumulationUnitMoney {get;set;}

           /// <summary>
           /// Desc:是否为默认
           /// Default:
           /// Nullable:True
           /// </summary>           
           public byte? IsDefault {get;set;}

    }
}
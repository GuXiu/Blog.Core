﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///采购入库订单明细
    ///</summary>

    [SugarTable("ZJ_PurchaseEnterOrderItem","Zhongjiu")]
    public partial class PurchaseEnterOrderItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:采购入库单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long PurchaseEnterId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:入库数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

           /// <summary>
           /// Desc:即时库存数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Stock {get;set;}

           /// <summary>
           /// Desc:含税总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? VATinclusiveTotalPrice {get;set;}

           /// <summary>
           /// Desc:不含税总金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? NVATinclusiveTotalPrice {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券组合，群发分享，红包群发形式 领取
    ///</summary>

    [SugarTable("ZJ_CouponShareBatchGroup","Zhongjiu")]
    public partial class CouponShareBatchGroup
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id[店铺分享为店铺Id][会员分享为0]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc: 会员Id[会员分享为会员Id][店铺管理员分享为0]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShareUserId {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:限制领取数量，一次性领取张数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RestrictNumber {get;set;}

           /// <summary>
           /// Desc:微信分享标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareTitle {get;set;}

           /// <summary>
           /// Desc:微信分享副标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareSubtitle {get;set;}

           /// <summary>
           /// Desc:微信分享图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShareImgUrl {get;set;}

           /// <summary>
           /// Desc:分享方式[0:默认未知分享方式，禁止使用][1:群发抢红包形式][2:直接分发到某人手机账号中]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SharingMode {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:状态[0:正常][1:全部领取][2:已收回]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:部门Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:口令类型[0:默认无口令][1:口令被验证后不能再次被使用][2:口令被验证后可反复使用 3 单选或问答]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CodeType {get;set;}

           /// <summary>
           /// Desc:口令内容[CodeType=0 则 CodeContent=NULL][CodeType=1 则 CodeContent=question1,q2,null,q4,q5(不需要提问的 可以用null字符串形式补位)][CodeType=2 则 CodeContent=口令信息，长度不超过15个字]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CodeContent {get;set;}

           /// <summary>
           /// Desc:领取优惠券是否随机[0:不随机][1:随机]
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsRandom {get;set;}

           /// <summary>
           /// Desc:提示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Prompt {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

           /// <summary>
           /// Desc:微信分享背景图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BackgroundImgUrl {get;set;}

    }
}
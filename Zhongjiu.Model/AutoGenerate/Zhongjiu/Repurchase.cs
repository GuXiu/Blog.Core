﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///换购活动
    ///</summary>

    [SugarTable("ZJ_Repurchase","Zhongjiu")]
    public partial class Repurchase
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:商品范围：0所有商品，1适用商品，2不适用商品
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SuitProductType {get;set;}

           /// <summary>
           /// Desc:单位：0元，1件
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Unit {get;set;}

           /// <summary>
           /// Desc:满多少元适用
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal MinAmount {get;set;}

           /// <summary>
           /// Desc:满多少个适用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MinCount {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:状态：1启用，0下架
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:活动范围:0全部，1线上，2线下
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Channel {get;set;}

           /// <summary>
           /// Desc:适用会员等级
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SuitMemberGrade {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

    }
}
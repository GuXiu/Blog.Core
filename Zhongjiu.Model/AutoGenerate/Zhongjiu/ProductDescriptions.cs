﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品详情表
    ///</summary>

    [SugarTable("Himall_ProductDescriptions","Zhongjiu")]
    public partial class ProductDescriptions
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Id {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:审核原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AuditReason {get;set;}

           /// <summary>
           /// Desc:详情
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:关联版式
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DescriptionPrefixId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DescriptiondSuffixId {get;set;}

           /// <summary>
           /// Desc:SEO
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Description {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Keywords {get;set;}

           /// <summary>
           /// Desc:移动端描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MobileDescription {get;set;}

    }
}
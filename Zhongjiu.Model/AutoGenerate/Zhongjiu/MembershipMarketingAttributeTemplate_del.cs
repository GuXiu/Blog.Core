﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销 属性模板
    ///</summary>

    [SugarTable("ZJ_MembershipMarketingAttributeTemplate_del","Zhongjiu")]
    public partial class MembershipMarketingAttributeTemplate_del
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:上级Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:属性名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AttributeName {get;set;}

           /// <summary>
           /// Desc:属性值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AttributeValue {get;set;}

           /// <summary>
           /// Desc:属性描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AttributeDescribe {get;set;}

           /// <summary>
           /// Desc:表单类型
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FormTypes {get;set;}

           /// <summary>
           /// Desc:表单类型，使用描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FormTypesDescribe {get;set;}

           /// <summary>
           /// Desc:表单 class  名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ClassName {get;set;}

           /// <summary>
           /// Desc:空值，标值提示语
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NullErrorTips {get;set;}

    }
}
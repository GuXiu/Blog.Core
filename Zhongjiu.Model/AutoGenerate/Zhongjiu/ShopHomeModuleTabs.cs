﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///地阿奴首页模块标签表
    ///</summary>

    [SugarTable("Himall_ShopHomeModuleTabs","Zhongjiu")]
    public partial class ShopHomeModuleTabs
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:模块选项卡
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TabName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long HomeModuleId {get;set;}

    }
}
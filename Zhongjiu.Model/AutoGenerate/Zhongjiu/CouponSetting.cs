﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券设置表
    ///</summary>

    [SugarTable("Himall_CouponSetting","Zhongjiu")]
    public partial class CouponSetting
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券的发行平台
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PlatForm {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CouponID {get;set;}

           /// <summary>
           /// Desc:是否显示
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Display {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品咨询表
    ///</summary>

    [SugarTable("Himall_ProductConsultations","Zhongjiu")]
    public partial class ProductConsultations
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:用户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Email {get;set;}

           /// <summary>
           /// Desc:咨询内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ConsultationContent {get;set;}

           /// <summary>
           /// Desc:咨询时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ConsultationDate {get;set;}

           /// <summary>
           /// Desc:回复内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReplyContent {get;set;}

           /// <summary>
           /// Desc:回复日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReplyDate {get;set;}

    }
}
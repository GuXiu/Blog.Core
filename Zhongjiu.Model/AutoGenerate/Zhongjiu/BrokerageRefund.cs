﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///佣金退还记录表
    ///</summary>

    [SugarTable("Himall_BrokerageRefund","Zhongjiu")]
    public partial class BrokerageRefund
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:收入表外键
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? IncomeId {get;set;}

           /// <summary>
           /// Desc:退还佣金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Brokerage {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? RefundAmount {get;set;}

           /// <summary>
           /// Desc:退款时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? RefundTime {get;set;}

           /// <summary>
           /// Desc:退款Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long RefundId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///换购活动商品详情
    ///</summary>

    [SugarTable("ZJ_RepurchaseDetail","Zhongjiu")]
    public partial class RepurchaseDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:换购Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long RepurchaseId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:换购价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:换购数量，预留字段
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Quantity {get;set;}

           /// <summary>
           /// Desc:商品类型：0换购商品，1适用商品，2不适用商品
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

    }
}
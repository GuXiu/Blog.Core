﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///第三方平台对接，配置信息
    ///</summary>

    [SugarTable("zj_ChannelPlatConfig","Zhongjiu")]
    public partial class ChannelPlatConfig
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:中酒网店铺Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:渠道Id,[14:京东到家,15:美团闪购,16:饿了么]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ChannelId {get;set;}

           /// <summary>
           /// Desc:渠道名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChannelName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Appkey {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Appsecret {get;set;}

           /// <summary>
           /// Desc:接口域名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HostUrl {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Token {get;set;}

           /// <summary>
           /// Desc:Token 过期时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? TokenTimeOut {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RefreshToken {get;set;}

           /// <summary>
           /// Desc:平台商铺ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantName {get;set;}

           /// <summary>
           /// Desc:是否可用[1:可用,0:不可用]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Enable {get;set;}

           /// <summary>
           /// Desc:平台-门店编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantStationId {get;set;}

           /// <summary>
           /// Desc:平台-商家门店编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantStationNumber {get;set;}

           /// <summary>
           /// Desc:同步商品分类的店铺Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? SyncShopId {get;set;}

    }
}
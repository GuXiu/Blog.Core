﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销 人群Id关系表
    ///</summary>

    [SugarTable("ZJ_MembershipMarketingThrongIds","Zhongjiu")]
    public partial class MembershipMarketingThrongIds
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员营销 活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MarketingId {get;set;}

           /// <summary>
           /// Desc:会员人群Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MembersThrongId {get;set;}

    }
}
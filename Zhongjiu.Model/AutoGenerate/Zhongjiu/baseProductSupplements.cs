﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品扩展表
    ///</summary>

    [SugarTable("ZJ_baseProductSupplements","Zhongjiu")]
    public partial class baseProductSupplements
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:会员价一
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice1 {get;set;}

           /// <summary>
           /// Desc:会员价二
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice2 {get;set;}

           /// <summary>
           /// Desc:会员价三
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice3 {get;set;}

           /// <summary>
           /// Desc:团购价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal GroupPrice {get;set;}

           /// <summary>
           /// Desc:批发价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal WholesalePrice {get;set;}

           /// <summary>
           /// Desc:底价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MinPrice {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:调价最低价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? TJMinPrice {get;set;}

           /// <summary>
           /// Desc:调价最高价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? TJMaxPrice {get;set;}

           /// <summary>
           /// Desc:卡券Id 或 优惠券组合Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TicketCode {get;set;}

           /// <summary>
           /// Desc:同品就近跳转 0 否；1 是
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int JumpNearShop {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///云码分配表,此表暂时不用
    ///</summary>

    [SugarTable("Himall_CloudCodeDistribution","Zhongjiu")]
    public partial class CloudCodeDistribution
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系ID（DepartmentId=0 && ShopId!=0分配到店铺  DepartmentId！=0 && ShopId=0体系内分配）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DepartmentId {get;set;}

           /// <summary>
           /// Desc:云码ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CloudId {get;set;}

           /// <summary>
           /// Desc:分配时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreteDate {get;set;}

           /// <summary>
           /// Desc:分配人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:店铺ID（DepartmentId=0 && ShopId!=0分配到店铺  DepartmentId！=0 && ShopId=0体系内分配）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题表
    ///</summary>

    [SugarTable("ZJ_CouponShareProblem","Zhongjiu")]
    public partial class CouponShareProblem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:组合优惠券ID
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? CsbgId {get;set;}

           /// <summary>
           /// Desc:问题（题目）
           /// Default:null
           /// Nullable:True
           /// </summary>           
           public string Problem {get;set;}

           /// <summary>
           /// Desc:问题类型[1单选 2问答]
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ProblemType {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统会员联系方式表
    ///</summary>

    [SugarTable("Himall_MemberContacts","Zhongjiu")]
    public partial class MemberContacts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:用户类型(0 Email  1 SMS)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int UserType {get;set;}

           /// <summary>
           /// Desc:插件名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ServiceProvider {get;set;}

           /// <summary>
           /// Desc:联系号码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Contact {get;set;}

    }
}
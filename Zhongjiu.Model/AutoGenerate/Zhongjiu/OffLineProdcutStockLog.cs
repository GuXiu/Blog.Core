﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品库存操作日志表
    ///</summary>

    [SugarTable("Himall_OffLineProdcutStockLog","Zhongjiu")]
    public partial class OffLineProdcutStockLog
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProdcutId {get;set;}

           /// <summary>
           /// Desc:当前数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal BeforeNumber {get;set;}

           /// <summary>
           /// Desc:减少/增加数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:类型[1:可用库存][2:总库存]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int NumberType {get;set;}

           /// <summary>
           /// Desc:更改后的数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal AfterNumber {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Describe {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(6)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:商品合并表备份原来商品ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? BackProductId {get;set;}

    }
}
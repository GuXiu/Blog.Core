﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///满赠活动阶梯表
    ///</summary>

    [SugarTable("himall_FullGiftLadder","Zhongjiu")]
    public partial class FullGiftLadder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:满赠ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FullGiftId {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public float Amount {get;set;}

    }
}
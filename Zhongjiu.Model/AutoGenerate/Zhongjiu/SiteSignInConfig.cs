﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///签到配置表
    ///</summary>

    [SugarTable("Himall_SiteSignInConfig","Zhongjiu")]
    public partial class SiteSignInConfig
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:开启签到
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsEnable {get;set;}

           /// <summary>
           /// Desc:签到获得积分
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DayIntegral {get;set;}

           /// <summary>
           /// Desc:持续周期
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DurationCycle {get;set;}

           /// <summary>
           /// Desc:周期额外奖励积分
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DurationReward {get;set;}

    }
}
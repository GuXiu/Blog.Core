﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///表单主键Id与流程实例Id对应关系
    ///</summary>

    [SugarTable("Himall_FormProcessInstance","Zhongjiu")]
    public partial class FormProcessInstance
    {
           /// <summary>
           /// Desc:流程ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string ProcessId {get;set;}

           /// <summary>
           /// Desc:表单数据主键Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long FormId {get;set;}

           /// <summary>
           /// Desc:表单类型
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int FormType {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
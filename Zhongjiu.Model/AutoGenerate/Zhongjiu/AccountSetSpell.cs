﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系对应的汉语拼音
    ///</summary>

    [SugarTable("Himall_AccountSetSpell","Zhongjiu")]
    public partial class AccountSetSpell
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long ID {get;set;}

           /// <summary>
           /// Desc:体系ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountSetId {get;set;}

           /// <summary>
           /// Desc:体系对应的汉语拼音
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Spell {get;set;}

           /// <summary>
           /// Desc:体系对应的汉字
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SpellName {get;set;}

    }
}
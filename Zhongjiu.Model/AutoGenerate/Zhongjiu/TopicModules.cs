﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统模块的专题表
    ///</summary>

    [SugarTable("Himall_TopicModules","Zhongjiu")]
    public partial class TopicModules
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:专题ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TopicId {get;set;}

           /// <summary>
           /// Desc:专题名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:标题位置 0、left；1、center ；2、right
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TitleAlign {get;set;}

    }
}
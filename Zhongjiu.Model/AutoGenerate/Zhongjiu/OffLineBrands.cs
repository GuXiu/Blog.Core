﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品品牌表（弃用）
    ///</summary>

    [SugarTable("Himall_OffLineBrands","Zhongjiu")]
    public partial class OffLineBrands
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:品牌名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsRecommend {get;set;}

           /// <summary>
           /// Desc:是否显示
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsShow {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Sort {get;set;}

           /// <summary>
           /// Desc:是否删除[默认0:未删除][1:已删除]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsDel {get;set;}

    }
}
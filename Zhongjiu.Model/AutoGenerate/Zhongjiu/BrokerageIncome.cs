﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销佣金记录表
    ///</summary>

    [SugarTable("Himall_BrokerageIncome","Zhongjiu")]
    public partial class BrokerageIncome
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:订单条目编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderItemId {get;set;}

           /// <summary>
           /// Desc:SKUID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SkuID {get;set;}

           /// <summary>
           /// Desc:产品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductID {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:SKU信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SkuInfo {get;set;}

           /// <summary>
           /// Desc:获得佣金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Brokerage {get;set;}

           /// <summary>
           /// Desc:分销比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:订单创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderTime {get;set;}

           /// <summary>
           /// Desc:消费者ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BuyerUserId {get;set;}

           /// <summary>
           /// Desc:结算状态 -1不可结算 0未结算，1已结算,2审核中
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:结算时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SettlementTime {get;set;}

           /// <summary>
           /// Desc:推广会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:1 平台结算；2 商家手动结算
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int SettlementType {get;set;}

           /// <summary>
           /// Desc:0 分销订单 1 拼团订单
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? OrderType {get;set;}

           /// <summary>
           /// Desc:自行结算备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Note {get;set;}

           /// <summary>
           /// Desc:税金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Taxes {get;set;}

           /// <summary>
           /// Desc:佣金税额承担 0 店铺 1推广员
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? CommissionTaxLiabilityType {get;set;}

           /// <summary>
           /// Desc:佣金税率
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? PerformanceCommissionRate {get;set;}

    }
}
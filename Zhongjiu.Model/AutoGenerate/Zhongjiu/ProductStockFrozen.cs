﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品库存冻结表
    ///</summary>

    [SugarTable("ZJ_ProductStockFrozen","Zhongjiu")]
    public partial class ProductStockFrozen
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:来源单据类型：1订单，2调拨单
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:来源单据Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long SourceId {get;set;}

           /// <summary>
           /// Desc:来源单据明细Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long SourceItemId {get;set;}

           /// <summary>
           /// Desc:状态：1冻结，2取消冻结
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Quantity {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP(3)
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyTime {get;set;}

           /// <summary>
           /// Desc:店铺Id，冗余字段
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

    }
}
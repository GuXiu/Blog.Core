﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品会员等级价表
    ///</summary>

    [SugarTable("Zj_SystemPrice","Zhongjiu")]
    public partial class SystemPrice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:账套
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:体系会员等级Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? MGradeId {get;set;}

           /// <summary>
           /// Desc:会员等级价格
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MemberPrice {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:门店Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

    }
}
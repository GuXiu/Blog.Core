﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///产品调价单
    ///</summary>

    [SugarTable("ZJ_ProductModifyPrice","Zhongjiu")]
    public partial class ProductModifyPrice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:调价单据号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TJNo {get;set;}

           /// <summary>
           /// Desc:调价机构
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TJShopId {get;set;}

           /// <summary>
           /// Desc:供应商
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SupplierId {get;set;}

           /// <summary>
           /// Desc:生效日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EffectiveDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Operator {get;set;}

           /// <summary>
           /// Desc:审核状态 0 未审核；1 已审核；-1 已终止；2 已生效
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:审核日期
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AuditDate {get;set;}

           /// <summary>
           /// Desc:是否生效 0 未生效；1 已生效
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? TakeEffectStatus {get;set;}

           /// <summary>
           /// Desc:操作人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Operater {get;set;}

    }
}
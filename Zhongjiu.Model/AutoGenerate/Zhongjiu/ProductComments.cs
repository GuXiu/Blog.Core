﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品评价表
    ///</summary>

    [SugarTable("Himall_ProductComments","Zhongjiu")]
    public partial class ProductComments
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? SubOrderId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:用户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UserName {get;set;}

           /// <summary>
           /// Desc:Email
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Email {get;set;}

           /// <summary>
           /// Desc:评价内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReviewContent {get;set;}

           /// <summary>
           /// Desc:评价日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ReviewDate {get;set;}

           /// <summary>
           /// Desc:评价说明
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ReviewMark {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReplyContent {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReplyDate {get;set;}

           /// <summary>
           /// Desc:追加内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppendContent {get;set;}

           /// <summary>
           /// Desc:追加时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AppendDate {get;set;}

           /// <summary>
           /// Desc:追加评论回复
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReplyAppendContent {get;set;}

           /// <summary>
           /// Desc:追加评论回复时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReplyAppendDate {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsHidden {get;set;}

           /// <summary>
           /// Desc:第三方订单ID，只有从第三方平台导入的数才有
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Oid {get;set;}

           /// <summary>
           /// Desc:导入评论的渠道
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ChannelPlat {get;set;}

           /// <summary>
           /// Desc:第三方评论ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CommentId {get;set;}

    }
}
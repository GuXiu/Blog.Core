﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员卡充值记录表ChargeDetailId
    ///</summary>

    [SugarTable("Himall_OfflineRechargeHistory","Zhongjiu")]
    public partial class OfflineRechargeHistory
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:充值店铺
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CardNumber {get;set;}

           /// <summary>
           /// Desc:充值金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Recharge {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Creater {get;set;}

           /// <summary>
           /// Desc:充值日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreatTime {get;set;}

           /// <summary>
           /// Desc:充值记录表id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ChargeDetailId {get;set;}

    }
}
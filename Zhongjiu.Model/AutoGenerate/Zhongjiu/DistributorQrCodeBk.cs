﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销员在所属门店下的二维码，以备门店变换公众号，分销员二维码直接从此表读取二维码，无需再调用微信链接生成数量有限的永久的带参数的二维码
    ///</summary>

    [SugarTable("ZJ_DistributorQrCodeBk","Zhongjiu")]
    public partial class DistributorQrCodeBk
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:微信公众号原始Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OriginalId {get;set;}

           /// <summary>
           /// Desc:分销员所在门店二维码链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QrCode {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime AddDate {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信小程序用户FormId信息表
    ///</summary>

    [SugarTable("ZJ_WeChatFormId","Zhongjiu")]
    public partial class WeChatFormId
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:微信小程序 FormId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FormId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:使用状态[0:未使用][1:已使用]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Status {get;set;}

           /// <summary>
           /// Desc:渠道来源[1:中酒小程序][2:扫码购小程序][3:中酒云柜小程序]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte Channel {get;set;}

    }
}
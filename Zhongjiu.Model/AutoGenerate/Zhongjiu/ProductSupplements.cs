﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///产品补充表
    ///</summary>

    [SugarTable("ZJ_ProductSupplements","Zhongjiu")]
    public partial class ProductSupplements
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:会员价一
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice1 {get;set;}

           /// <summary>
           /// Desc:会员价二
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice2 {get;set;}

           /// <summary>
           /// Desc:会员价三
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipPrice3 {get;set;}

           /// <summary>
           /// Desc:团购价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal GroupPrice {get;set;}

           /// <summary>
           /// Desc:批发价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal WholesalePrice {get;set;}

           /// <summary>
           /// Desc:底价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? MinPrice {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:采购数量
           /// Default:0.000
           /// Nullable:True
           /// </summary>           
           public decimal? PurchaseQuantity {get;set;}

           /// <summary>
           /// Desc:采购金额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? PurchasePrice {get;set;}

           /// <summary>
           /// Desc:卡券Id 或 优惠券组合Id
           /// Default:NULL
           /// Nullable:True
           /// </summary>           
           public string CardCoupon {get;set;}

           /// <summary>
           /// Desc:固定销售价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal FixedSalePrice {get;set;}

           /// <summary>
           /// Desc:商品线上销量，定时任务每天统计
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal OnlineSaleCount {get;set;}

           /// <summary>
           /// Desc:商品线下销量，定时任务每天统计
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal OfflineSaleCount {get;set;}

           /// <summary>
           /// Desc:开启商品体系积分抵扣 0 不开启，1 开启
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte EnableSystemIntegralReduce {get;set;}

           /// <summary>
           /// Desc:体系积分抵现类型 0 百分比，1 积分数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SystemIntegralReduceType {get;set;}

           /// <summary>
           /// Desc:最多可以使用的积分的百分比
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SystemIntegralReducePercentage {get;set;}

           /// <summary>
           /// Desc:最多可以使用的积分数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SystemIntegralReduceCount {get;set;}

           /// <summary>
           /// Desc:同品就近跳转 0 否；1 是
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int JumpNearShop {get;set;}

    }
}
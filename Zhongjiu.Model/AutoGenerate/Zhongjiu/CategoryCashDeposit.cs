﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分类保证金设置表
    ///</summary>

    [SugarTable("Himall_CategoryCashDeposit","Zhongjiu")]
    public partial class CategoryCashDeposit
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Id {get;set;}

           /// <summary>
           /// Desc:分类Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:需要缴纳保证金
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? NeedPayCashDeposit {get;set;}

           /// <summary>
           /// Desc:允许七天无理由退货
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? EnableNoReasonReturn {get;set;}

    }
}
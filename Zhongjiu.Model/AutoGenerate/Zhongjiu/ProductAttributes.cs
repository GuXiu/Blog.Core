﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品和属性的中间表
    ///</summary>

    [SugarTable("Himall_ProductAttributes","Zhongjiu")]
    public partial class ProductAttributes
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:属性ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AttributeId {get;set;}

           /// <summary>
           /// Desc:属性值ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ValueId {get;set;}

    }
}
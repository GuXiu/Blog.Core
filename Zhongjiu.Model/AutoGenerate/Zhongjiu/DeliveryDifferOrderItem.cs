﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配送差异单明细
    ///</summary>

    [SugarTable("ZJ_DeliveryDifferOrderItem","Zhongjiu")]
    public partial class DeliveryDifferOrderItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:差异单ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DeliveryDifferId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:配送机构ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:要货门店ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:差异入库机构（Type=1,门店  Type=2,配送机构）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long InOrgId {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:（Type=1时InOrgId为门店Id  Type=2时InOrgId为配送机构）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:差异数量处理人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Operater {get;set;}

           /// <summary>
           /// Desc:差异单处理时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? OperaterTime {get;set;}

    }
}
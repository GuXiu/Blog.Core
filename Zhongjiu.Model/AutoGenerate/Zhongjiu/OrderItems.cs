﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单明细表
    ///</summary>

    [SugarTable("Himall_OrderItems","Zhongjiu")]
    public partial class OrderItems
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:SKUId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SkuId {get;set;}

           /// <summary>
           /// Desc:未使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKU {get;set;}

           /// <summary>
           /// Desc:购买数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Quantity {get;set;}

           /// <summary>
           /// Desc:退货数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ReturnQuantity {get;set;}

           /// <summary>
           /// Desc:成本价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CostPrice {get;set;}

           /// <summary>
           /// Desc:销售价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SalePrice {get;set;}

           /// <summary>
           /// Desc:优惠金额 商家改价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal DiscountAmount {get;set;}

           /// <summary>
           /// Desc:实际应付金额 （暂时知道 不包含体系积分抵扣金额 AccountSetIntegralReduceAmountAvg ）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RealTotalPrice {get;set;}

           /// <summary>
           /// Desc:退款价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundPrice {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:SKU颜色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Color {get;set;}

           /// <summary>
           /// Desc:SKU尺寸
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Size {get;set;}

           /// <summary>
           /// Desc:SKU版本
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Version {get;set;}

           /// <summary>
           /// Desc:缩略图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ThumbnailsUrl {get;set;}

           /// <summary>
           /// Desc:分佣比例
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommisRate {get;set;}

           /// <summary>
           /// Desc:可退金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? EnabledRefundAmount {get;set;}

           /// <summary>
           /// Desc:是否为限时购商品
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsLimitBuy {get;set;}

           /// <summary>
           /// Desc:分销比例
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DistributionRate {get;set;}

           /// <summary>
           /// Desc:返现金额
           /// Default:0.0000
           /// Nullable:False
           /// </summary>           
           public decimal DistributionValue {get;set;}

           /// <summary>
           /// Desc:可退积分抵扣金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? EnabledRefundIntegral {get;set;}

           /// <summary>
           /// Desc:优惠券抵扣金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CouponDiscount {get;set;}

           /// <summary>
           /// Desc:营销均摊金额（商品满减、订单满减）
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? PromotionFee {get;set;}

           /// <summary>
           /// Desc:商品类型：1正品、6赠品、7兑换
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Activity {get;set;}

           /// <summary>
           /// Desc:可退积分抵扣数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int EnabledRefundIntegralNumber {get;set;}

           /// <summary>
           /// Desc:加减单价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DisPrice {get;set;}

           /// <summary>
           /// Desc:移动平均价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AvgPrice {get;set;}

           /// <summary>
           /// Desc:会员优惠金额（会员卡打折的均摊金额）
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VipCardFavorableAverage {get;set;}

           /// <summary>
           /// Desc:中酒币均摊金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal CoinFavorableAverage {get;set;}

           /// <summary>
           /// Desc:改价均摊金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DisPriceAverage {get;set;}

           /// <summary>
           /// Desc:营销活动ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActivityId {get;set;}

           /// <summary>
           /// Desc:营销活动类型：满赠、买赠
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActivityType {get;set;}

           /// <summary>
           /// Desc:积分抵扣均摊金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AccountSetIntegralReduceAmountAvg {get;set;}

           /// <summary>
           /// Desc:积分抵扣均摊数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AccountSetIntegralReduceCountAvg {get;set;}

           /// <summary>
           /// Desc:积分兑换金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal AccountSetIntegralExchangeAmount {get;set;}

           /// <summary>
           /// Desc:积分兑换数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AccountSetIntegralExchangeCount {get;set;}

    }
}
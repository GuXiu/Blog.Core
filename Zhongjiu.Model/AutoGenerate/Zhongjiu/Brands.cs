﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统品牌表（店铺商品、基库商品共用）
    ///</summary>

    [SugarTable("Himall_Brands","Zhongjiu")]
    public partial class Brands
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:品牌名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:顺序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:LOGO
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Logo {get;set;}

           /// <summary>
           /// Desc:未使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RewriteName {get;set;}

           /// <summary>
           /// Desc:说明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:SEO
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Description {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Meta_Keywords {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsRecommend {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Sort {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int MonthSaleCount {get;set;}

           /// <summary>
           /// Desc:品牌Banner
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Banner {get;set;}

           /// <summary>
           /// Desc:是否显示
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsShow {get;set;}

           /// <summary>
           /// Desc:最后一次修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastAsyncDateTime {get;set;}

           /// <summary>
           /// Desc:广告图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AdversementImg {get;set;}

    }
}
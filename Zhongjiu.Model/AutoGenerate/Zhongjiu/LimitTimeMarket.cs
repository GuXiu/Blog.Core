﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///限时购表
    ///</summary>

    [SugarTable("Himall_LimitTimeMarket","Zhongjiu")]
    public partial class LimitTimeMarket
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:分类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryName {get;set;}

           /// <summary>
           /// Desc:审核状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public short AuditStatus {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AuditTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:最近一个月的价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RecentMonthPrice {get;set;}

           /// <summary>
           /// Desc:开始日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:结束日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:库存
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Stock {get;set;}

           /// <summary>
           /// Desc:销售数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SaleCount {get;set;}

           /// <summary>
           /// Desc:取消原因
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CancelReson {get;set;}

           /// <summary>
           /// Desc:限量购买
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int MaxSaleCount {get;set;}

           /// <summary>
           /// Desc:商品广告
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductAd {get;set;}

           /// <summary>
           /// Desc:最小价格
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal MinPrice {get;set;}

           /// <summary>
           /// Desc:图片Path
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ImagePath {get;set;}

    }
}
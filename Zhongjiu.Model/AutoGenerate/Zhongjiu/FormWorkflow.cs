﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///表单引用工作流关系
    ///</summary>

    [SugarTable("Himall_FormWorkflow","Zhongjiu")]
    public partial class FormWorkflow
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Id {get;set;}

           /// <summary>
           /// Desc:表单Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string FormId {get;set;}

           /// <summary>
           /// Desc:表单名称，仅用作备注
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FormName {get;set;}

           /// <summary>
           /// Desc:部门ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long DepartmentId {get;set;}

           /// <summary>
           /// Desc:部门类型
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DepartmentType {get;set;}

           /// <summary>
           /// Desc:流程设计Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SchemeId {get;set;}

           /// <summary>
           /// Desc:工作流名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SchemeName {get;set;}

           /// <summary>
           /// Desc:添加人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Editer {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime EditTime {get;set;}

    }
}
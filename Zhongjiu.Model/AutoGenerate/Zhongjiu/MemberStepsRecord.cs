﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户一步直邀步数记录
    ///</summary>

    [SugarTable("ZJ_MemberStepsRecord","Zhongjiu")]
    public partial class MemberStepsRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? WeChatUserId {get;set;}

           /// <summary>
           /// Desc:微信记录步数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? WeChatSteps {get;set;}

           /// <summary>
           /// Desc:记录日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? RecordDate {get;set;}

           /// <summary>
           /// Desc:已兑换步数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ExchangeSteps {get;set;}

           /// <summary>
           /// Desc:兑换状态0不可兑换1 可兑换
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ExchangeSatus {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单营销记录表
    ///</summary>

    [SugarTable("himall_MarketingOrder","Zhongjiu")]
    public partial class MarketingOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MarketingId {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrdersId {get;set;}

           /// <summary>
           /// Desc:订单明细ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderItemsId {get;set;}

           /// <summary>
           /// Desc:活动类型（1=买赠，2=满赠，3=满减，4=限购）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int MarketingType {get;set;}

           /// <summary>
           /// Desc:活动JSON
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MarketingJson {get;set;}

    }
}
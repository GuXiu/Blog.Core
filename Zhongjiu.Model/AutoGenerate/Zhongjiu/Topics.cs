﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///专题表
    ///</summary>

    [SugarTable("Himall_Topics","Zhongjiu")]
    public partial class Topics
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:专题名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:封面图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FrontCoverImage {get;set;}

           /// <summary>
           /// Desc:主图
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TopImage {get;set;}

           /// <summary>
           /// Desc:背景图片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BackgroundImage {get;set;}

           /// <summary>
           /// Desc:使用终端
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int PlatForm {get;set;}

           /// <summary>
           /// Desc:标签
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Tags {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:是否推荐
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsRecommend {get;set;}

           /// <summary>
           /// Desc:自定义热点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SelfDefineText {get;set;}

    }
}
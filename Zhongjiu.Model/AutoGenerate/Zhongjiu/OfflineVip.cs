﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员卡表
    ///</summary>

    [SugarTable("Himall_OfflineVip","Zhongjiu")]
    public partial class OfflineVip
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:所属店铺，在哪里
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:会员姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string VipName {get;set;}

           /// <summary>
           /// Desc:会员密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string PassWord {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CardNumber {get;set;}

           /// <summary>
           /// Desc:手机号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TelPhone {get;set;}

           /// <summary>
           /// Desc:余额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Balance {get;set;}

           /// <summary>
           /// Desc:中酒币
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Coin {get;set;}

           /// <summary>
           /// Desc:办卡时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:是否解绑[0:未解绑][1:解绑]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsRelation {get;set;}

           /// <summary>
           /// Desc:营业员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TradeAssistantId {get;set;}

    }
}
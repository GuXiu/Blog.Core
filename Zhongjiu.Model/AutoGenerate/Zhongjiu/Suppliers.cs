﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///供应商表
    ///</summary>

    [SugarTable("ZJ_Suppliers","Zhongjiu")]
    public partial class Suppliers
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:供应商据号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierNo {get;set;}

           /// <summary>
           /// Desc:供应商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierName {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:采购员
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Buyer {get;set;}

           /// <summary>
           /// Desc:地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:邮箱
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Email {get;set;}

           /// <summary>
           /// Desc:联系人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LinkName {get;set;}

           /// <summary>
           /// Desc:联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LinkTel {get;set;}

           /// <summary>
           /// Desc:邮编
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PostCode {get;set;}

           /// <summary>
           /// Desc:状态 0 正常； -1 淘汰
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

           /// <summary>
           /// Desc:经营方式 0 购销； 1 代销
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ManagementMode {get;set;}

           /// <summary>
           /// Desc:结算方式 0 临时指定 ；1 指定账期；2 指定日期；3 货到付款
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? SettlementMode {get;set;}

           /// <summary>
           /// Desc:开户行
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankOfAccounts {get;set;}

           /// <summary>
           /// Desc:营业执照号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessLicenseNo {get;set;}

           /// <summary>
           /// Desc:营业执照照片
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessLicensePic {get;set;}

           /// <summary>
           /// Desc:账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankNo {get;set;}

           /// <summary>
           /// Desc:账户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Operator {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Modifier {get;set;}

           /// <summary>
           /// Desc:所属机构(顶级体系)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

    }
}
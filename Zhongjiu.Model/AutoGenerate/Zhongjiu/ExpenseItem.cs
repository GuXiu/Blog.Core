﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///费用明细
    ///</summary>

    [SugarTable("ZJ_ExpenseItem","Zhongjiu")]
    public partial class ExpenseItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:费用表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ExpenseId {get;set;}

           /// <summary>
           /// Desc:费用设置ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ExpenseSetId {get;set;}

           /// <summary>
           /// Desc:费用金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Expense {get;set;}

    }
}
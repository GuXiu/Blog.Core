﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细(提成商品)
    ///</summary>

    [SugarTable("ZJ_SalesCommissionForFinishDate","Zhongjiu")]
    public partial class SalesCommissionForFinishDate
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:销售单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MeasureUnit {get;set;}

           /// <summary>
           /// Desc:商品分类
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryPath {get;set;}

           /// <summary>
           /// Desc:SKU
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Sku {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:订单来源
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderChannel {get;set;}

           /// <summary>
           /// Desc:订单明细Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderItemId {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:商品类型：1正品、6赠品、7兑换
           /// Default:99999
           /// Nullable:False
           /// </summary>           
           public int Activity {get;set;}

           /// <summary>
           /// Desc:购买数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Quantity {get;set;}

           /// <summary>
           /// Desc:退货数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ReturnQuantity {get;set;}

           /// <summary>
           /// Desc:实际应付金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RealTotalPrice {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RefundPrice {get;set;}

           /// <summary>
           /// Desc:订单完成日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime FinishDate {get;set;}

           /// <summary>
           /// Desc:订单状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OrderStatus {get;set;}

    }
}
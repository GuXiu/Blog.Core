﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///补品卡批次明细
    ///</summary>

    [SugarTable("ZJ_GiftCardBatchItems","Zhongjiu")]
    public partial class GiftCardBatchItems
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:批次Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BatchId {get;set;}

           /// <summary>
           /// Desc:批次Simple Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BatchSimpleId {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CardNo {get;set;}

           /// <summary>
           /// Desc:面值
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Pwd {get;set;}

           /// <summary>
           /// Desc:密码验证次数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CheckCount {get;set;}

           /// <summary>
           /// Desc:充值会员
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:会员手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MemberPhone {get;set;}

           /// <summary>
           /// Desc:充值店铺
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:充值时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UsedTime {get;set;}

           /// <summary>
           /// Desc:状态：0未使用；1已使用；2已冻结；3已作废；4已退款
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:冻结状态：0未冻结，1五次密码错误，2，批次异常
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int FreezeStatus {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///直营门店盈亏明细
    ///</summary>

    [SugarTable("ZJ_DirectSaleStoreGainAndLossItem","Zhongjiu")]
    public partial class DirectSaleStoreGainAndLossItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:直营门店盈亏表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long GainAndLossId {get;set;}

           /// <summary>
           /// Desc:业务单号ID,默认为0代表时间段内销售成本或收入
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderNo {get;set;}

           /// <summary>
           /// Desc:单据类型（1：直营门店费用单，2：直营门店其他收入:3：直营门店营业成本:4：直营门店营业收入，5：销售成本，6：销售收入）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:单据金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

    }
}
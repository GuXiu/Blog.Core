﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信用户信息
    ///</summary>

    [SugarTable("ZJ_WeChatUser","Zhongjiu")]
    public partial class WeChatUser
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:微信用户的唯一标识
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:微信用户昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NickName {get;set;}

           /// <summary>
           /// Desc:微信用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sex {get;set;}

           /// <summary>
           /// Desc:微信用户个人资料填写的省份
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Province {get;set;}

           /// <summary>
           /// Desc:普通用户个人资料填写的城市
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string City {get;set;}

           /// <summary>
           /// Desc:国家，如中国为CN
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Country {get;set;}

           /// <summary>
           /// Desc:用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640 * 640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HeadImgUrl {get;set;}

           /// <summary>
           /// Desc:用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Privilege {get;set;}

           /// <summary>
           /// Desc:只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UnionId {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:用户Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:用户新设置的手机号，下次进来生效，生效后清空此字段值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PhoneNew {get;set;}

    }
}
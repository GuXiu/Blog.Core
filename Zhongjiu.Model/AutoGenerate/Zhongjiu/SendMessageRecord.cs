﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录表
    ///</summary>

    [SugarTable("Himall_SendMessageRecord","Zhongjiu")]
    public partial class SendMessageRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:消息类别
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int MessageType {get;set;}

           /// <summary>
           /// Desc:内容类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ContentType {get;set;}

           /// <summary>
           /// Desc:发送内容
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SendContent {get;set;}

           /// <summary>
           /// Desc:发送对象
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ToUserLabel {get;set;}

           /// <summary>
           /// Desc:发送状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SendState {get;set;}

           /// <summary>
           /// Desc:发送时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? SendTime {get;set;}

           /// <summary>
           /// Desc:部门ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

    }
}
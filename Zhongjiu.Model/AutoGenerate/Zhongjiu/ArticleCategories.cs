﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///系统文章分类表
    ///</summary>

    [SugarTable("Himall_ArticleCategories","Zhongjiu")]
    public partial class ArticleCategories
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ParentCategoryId {get;set;}

           /// <summary>
           /// Desc:文章类型名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:显示顺序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long DisplaySequence {get;set;}

           /// <summary>
           /// Desc:是否为默认
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsDefault {get;set;}

    }
}
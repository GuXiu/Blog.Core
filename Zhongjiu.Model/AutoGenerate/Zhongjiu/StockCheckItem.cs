﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///库存盘点明细
    ///</summary>

    [SugarTable("ZJ_StockCheckItem","Zhongjiu")]
    public partial class StockCheckItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:库存盘点主表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CheckId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:库存
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:库存盘点时移动平均价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal AvgPrice {get;set;}

           /// <summary>
           /// Desc:盘点数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CheckNumber {get;set;}

           /// <summary>
           /// Desc:差异数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal DifferNumber {get;set;}

    }
}
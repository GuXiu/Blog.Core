﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///模板添加设置表
    ///</summary>

    [SugarTable("Himall_TemplateAdditionSetting","Zhongjiu")]
    public partial class TemplateAdditionSetting
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:有效性
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsValid {get;set;}

           /// <summary>
           /// Desc:标题、描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:链接
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Link {get;set;}

           /// <summary>
           /// Desc:链接显示名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LinkDisplayName {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:图片地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ImgUrl {get;set;}

           /// <summary>
           /// Desc:是圆形图片
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsCircleImg {get;set;}

           /// <summary>
           /// Desc:类型
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:高度
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public float Height {get;set;}

           /// <summary>
           /// Desc:宽度
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public float Width {get;set;}

           /// <summary>
           /// Desc:自动宽度
           /// Default:b'1'
           /// Nullable:False
           /// </summary>           
           public bool IsAutoWidth {get;set;}

           /// <summary>
           /// Desc:右边距
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public float MarginRight {get;set;}

           /// <summary>
           /// Desc:下边距
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public float MarginBottom {get;set;}

           /// <summary>
           /// Desc:是否启用
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsActive {get;set;}

    }
}
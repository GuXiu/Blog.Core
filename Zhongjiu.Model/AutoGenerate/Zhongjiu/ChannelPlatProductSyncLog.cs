﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>

    [SugarTable("ZJ_ChannelPlatProductSyncLog","Zhongjiu")]
    public partial class ChannelPlatProductSyncLog
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:渠道Id,[14:京东到家]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ChannelId {get;set;}

           /// <summary>
           /// Desc:中酒网店铺Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:中酒网店铺 商品Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:中酒店铺SKU
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SKU {get;set;}

           /// <summary>
           /// Desc:同步状态[0:未知][1:成功][2:失败][3:处理成功]
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:提示信息
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Message {get;set;}

           /// <summary>
           /// Desc:时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateDate {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ModifyDate {get;set;}

    }
}
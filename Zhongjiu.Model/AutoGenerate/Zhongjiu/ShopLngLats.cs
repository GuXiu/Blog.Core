﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺配送范围坐标表
    ///</summary>

    [SugarTable("Himall_ShopLngLats","Zhongjiu")]
    public partial class ShopLngLats
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺配送范围
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string LngLat {get;set;}

           /// <summary>
           /// Desc:新增时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AddTime {get;set;}

           /// <summary>
           /// Desc:是否启用
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public byte? Enable {get;set;}

           /// <summary>
           /// Desc:配送范围类型 1：圆形 2：多边形
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? PeiSongType {get;set;}

           /// <summary>
           /// Desc:圆形半径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Radius {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员充值流水表
    ///</summary>

    [SugarTable("Himall_ChargeDetail","Zhongjiu")]
    public partial class ChargeDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemId {get;set;}

           /// <summary>
           /// Desc:充值时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ChargeTime {get;set;}

           /// <summary>
           /// Desc:充值金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal ChargeAmount {get;set;}

           /// <summary>
           /// Desc:充值方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ChargeWay {get;set;}

           /// <summary>
           /// Desc:充值状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ChargeStatus {get;set;}

           /// <summary>
           /// Desc:提交充值时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:店铺
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:充值来源：1卖家中心，2
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Platform {get;set;}

           /// <summary>
           /// Desc:支付方式:1礼品卡，2支付宝，3微信
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ChargeWayEnum {get;set;}

    }
}
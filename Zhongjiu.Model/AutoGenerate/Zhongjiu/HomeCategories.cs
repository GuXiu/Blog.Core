﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页分类表
    ///</summary>

    [SugarTable("Himall_HomeCategories","Zhongjiu")]
    public partial class HomeCategories
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分类所属行数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RowId {get;set;}

           /// <summary>
           /// Desc:分类ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:分类深度(最深3）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Depth {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///营业员信息表
    ///</summary>

    [SugarTable("ZJ_TradeAssistant","Zhongjiu")]
    public partial class TradeAssistant
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:营业员Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ManagerId {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Phone {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间，默认和创建时间相同
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///移动平均价记录
    ///</summary>

    [SugarTable("ZJ_AvgPriceHistory_Back","Zhongjiu")]
    public partial class AvgPriceHistory_Back
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:入库前的移动平均价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal BeforeAvgPrice {get;set;}

           /// <summary>
           /// Desc:入库后的移动平均价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal AfterAvgPrice {get;set;}

           /// <summary>
           /// Desc:移动平均价来源单号ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:单据类型（1：采购入库 ，2： 配送入库，3：调拨入库）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
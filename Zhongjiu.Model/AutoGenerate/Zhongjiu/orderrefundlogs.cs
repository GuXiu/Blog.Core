﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单售后日志表
    ///</summary>

    [SugarTable("himall_orderrefundlogs","Zhongjiu")]
    public partial class orderrefundlogs
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:售后编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long RefundId {get;set;}

           /// <summary>
           /// Desc:操作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Operator {get;set;}

           /// <summary>
           /// Desc:操作日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OperateDate {get;set;}

           /// <summary>
           /// Desc:操作内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OperateContent {get;set;}

           /// <summary>
           /// Desc:申请次数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ApplyNumber {get;set;}

    }
}
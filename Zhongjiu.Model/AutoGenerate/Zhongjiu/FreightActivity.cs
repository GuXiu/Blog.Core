﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///免运费活动
    ///</summary>

    [SugarTable("ZJ_FreightActivity","Zhongjiu")]
    public partial class FreightActivity
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:活动标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:运费金额(免运费为0)
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal FreightAmount {get;set;}

           /// <summary>
           /// Desc:计算方式[0:按金额][1:按件数]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CalculationType {get;set;}

           /// <summary>
           /// Desc:计算方式配额(根据计算方式而定)[CalculationType=0:订单额度][CalculationType=1:商品件数]
           /// Default:0.000
           /// Nullable:True
           /// </summary>           
           public decimal? CalculationQuota {get;set;}

           /// <summary>
           /// Desc:活动开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BeginTime {get;set;}

           /// <summary>
           /// Desc:活动结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:不参加活动地区(偏远地区)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RemoteArea {get;set;}

           /// <summary>
           /// Desc:是否指定商品[0:不指定][1:指定商品]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsDesignationGoods {get;set;}

           /// <summary>
           /// Desc:指定商品Id(逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:是否删除[0:默认正常][1:已删除]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsDel {get;set;}

           /// <summary>
           /// Desc:[1:待审核][2:(默认)进行中][3:未通过][4:已结束][5:已取消]
           /// Default:2
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

    }
}
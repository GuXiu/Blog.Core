﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入表
    ///</summary>

    [SugarTable("ZJ_DirectSaleStoreIncome","Zhongjiu")]
    public partial class DirectSaleStoreIncome
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:门店或配送中心ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:审核状态（0：未审核 1：已审核 2：已生效 3：已终止 4：已完成）
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? AuditState {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:创建人ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
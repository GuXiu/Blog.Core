﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品分类表
    ///</summary>

    [SugarTable("Himall_OffLineCategories","Zhongjiu")]
    public partial class OffLineCategories
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:上级Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:分类名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:分类的深度
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Depth {get;set;}

           /// <summary>
           /// Desc:分类的路径（以|分离）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Path {get;set;}

           /// <summary>
           /// Desc:是否推荐 0 不推荐 1 推荐
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsRecommend {get;set;}

           /// <summary>
           /// Desc:显示顺序，排序
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Sort {get;set;}

           /// <summary>
           /// Desc:是否删除[默认0:未删除][1:已删除]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsDel {get;set;}

    }
}
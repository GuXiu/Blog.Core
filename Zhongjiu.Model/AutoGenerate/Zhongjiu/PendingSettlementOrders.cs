﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///待结算订单表
    ///</summary>

    [SugarTable("Himall_PendingSettlementOrders","Zhongjiu")]
    public partial class PendingSettlementOrders
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:订单号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:订单金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderAmount {get;set;}

           /// <summary>
           /// Desc:商品实付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ProductsAmount {get;set;}

           /// <summary>
           /// Desc:运费
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal FreightAmount {get;set;}

           /// <summary>
           /// Desc:平台佣金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PlatCommission {get;set;}

           /// <summary>
           /// Desc:分销佣金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DistributorCommission {get;set;}

           /// <summary>
           /// Desc:退款金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal RefundAmount {get;set;}

           /// <summary>
           /// Desc:平台佣金退还
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PlatCommissionReturn {get;set;}

           /// <summary>
           /// Desc:分销佣金退还
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DistributorCommissionReturn {get;set;}

           /// <summary>
           /// Desc:结算金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SettlementAmount {get;set;}

           /// <summary>
           /// Desc:订单完成时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderFinshTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PaymentTypeName {get;set;}

           /// <summary>
           /// Desc:订单来源 [1:线上订单,2:线下订单]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte OrderSource {get;set;}

           /// <summary>
           /// Desc:推送订单 [0:默认,1:是,2:否]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte OrderPush {get;set;}

           /// <summary>
           /// Desc:订单类型[DF=0,PC=1,微信=2,安卓APP=3,苹果APP=4,移动端wap=5,门店=6,系统推送=7]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte OrderSourceType {get;set;}

           /// <summary>
           /// Desc:商家改价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ChangePrices {get;set;}

           /// <summary>
           /// Desc:订单实付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OrderTotalAmount {get;set;}

           /// <summary>
           /// Desc:平台优惠券
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PlatformCoupon {get;set;}

           /// <summary>
           /// Desc:商家优惠券
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal MerchantCoupon {get;set;}

           /// <summary>
           /// Desc:商家会员优惠 (线下：会员折扣 和 中酒币 优惠的总额)
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal BusinessMembershipDiscount {get;set;}

           /// <summary>
           /// Desc:会员卡支付
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal MembershipCardPayment {get;set;}

           /// <summary>
           /// Desc:积分代支付
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IntegralGenerationPayment {get;set;}

           /// <summary>
           /// Desc:配送费
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal DeliveryFee {get;set;}

           /// <summary>
           /// Desc:积分成本
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal IntegralCost {get;set;}

           /// <summary>
           /// Desc:实际结算金额 = 结算金额-积分成本-平台佣金(扣点)	
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SettlementTotalAmount {get;set;}

           /// <summary>
           /// Desc:商品总价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ProductTotalAmount {get;set;}

    }
}
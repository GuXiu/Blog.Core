﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///提成计算记录表
    ///</summary>

    [SugarTable("ZJ_SalesCommissionCalculation","Zhongjiu")]
    public partial class SalesCommissionCalculation
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:销售员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TradeAssistantId {get;set;}

           /// <summary>
           /// Desc:订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:提成方式[1:商品提成][2:毛利提成][3:按拓展会员数量提成]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte ModeType {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:计入人员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreateId {get;set;}

           /// <summary>
           /// Desc:订单时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OrderDate {get;set;}

           /// <summary>
           /// Desc:计入时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:销售数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal SalesVolumes {get;set;}

           /// <summary>
           /// Desc:销售金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? SalesAmount {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///满赠活动详情表
    ///</summary>

    [SugarTable("himall_FullGiftDetail","Zhongjiu")]
    public partial class FullGiftDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:满赠ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FullGiftId {get;set;}

           /// <summary>
           /// Desc:满赠阶梯表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FullGiftLadderId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:类型（0=买；1=赠）
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Type {get;set;}

    }
}
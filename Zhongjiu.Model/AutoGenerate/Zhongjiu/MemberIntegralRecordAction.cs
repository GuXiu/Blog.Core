﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///积分兑换虚拟物记录表
    ///</summary>

    [SugarTable("Himall_MemberIntegralRecordAction","Zhongjiu")]
    public partial class MemberIntegralRecordAction
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:积分兑换ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long IntegralRecordId {get;set;}

           /// <summary>
           /// Desc:兑换虚拟物l类型ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? VirtualItemTypeId {get;set;}

           /// <summary>
           /// Desc:虚拟物ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long VirtualItemId {get;set;}

    }
}
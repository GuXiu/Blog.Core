﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页推荐图片
    ///</summary>

    [SugarTable("Himall_HomeRecommendImgs","Zhongjiu")]
    public partial class HomeRecommendImgs
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long TabId {get;set;}

           /// <summary>
           /// Desc:图片的存放URL
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ImageUrl {get;set;}

           /// <summary>
           /// Desc:图片跳转地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Sort {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员人群
    ///</summary>

    [SugarTable("ZJ_MembersThrong","Zhongjiu")]
    public partial class MembersThrong
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:人群名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:人群特征
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Describe {get;set;}

           /// <summary>
           /// Desc:筛选方式[1:规则筛选,2:导入人群]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ScreeningMethod {get;set;}

           /// <summary>
           /// Desc:筛选范围[0:全部,1:线上,2:线下]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ScreeningChannel {get;set;}

           /// <summary>
           /// Desc:会员人数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Number {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Creater {get;set;}

           /// <summary>
           /// Desc:状态[0:正常,1:弃用]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

    }
}
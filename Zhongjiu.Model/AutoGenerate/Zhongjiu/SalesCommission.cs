﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///提成设置，销售佣金
    ///</summary>

    [SugarTable("ZJ_SalesCommission","Zhongjiu")]
    public partial class SalesCommission
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:提成方式[0:默认未设置][1:按商品提成][2:按毛利提成][3:按拓展会员数量提成]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte ModeType {get;set;}

           /// <summary>
           /// Desc:提成比例 以%结尾
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ModeValue {get;set;}

           /// <summary>
           /// Desc:特殊设置[0:默认未设置][1:积分商品不计提成]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte SpecialModeType {get;set;}

           /// <summary>
           /// Desc:适用订单[订单渠道:0线上、1线下]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte OrderChanel {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员营销 属性值
    ///</summary>

    [SugarTable("ZJ_MembershipMarketingAttribute_del","Zhongjiu")]
    public partial class MembershipMarketingAttribute_del
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员营销 活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MarketingId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long AttributeTemplateId {get;set;}

           /// <summary>
           /// Desc:属性值
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AttributeValue {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///菜单、功能表
    ///</summary>

    [SugarTable("Himall_Privilege","Zhongjiu")]
    public partial class Privilege
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string Id {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:父节点
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ParentId {get;set;}

           /// <summary>
           /// Desc:层级
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CascadeId {get;set;}

           /// <summary>
           /// Desc:路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:控制器
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Controller {get;set;}

           /// <summary>
           /// Desc:action:多个用逗号分隔;空表示控制器下所有权限
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Action {get;set;}

           /// <summary>
           /// Desc:可用性
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:平台：0平台中心，1卖家中心
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SiteType {get;set;}

           /// <summary>
           /// Desc:类型：1菜单、2按钮...
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:元素Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DomId {get;set;}

           /// <summary>
           /// Desc:类型:input/span/a/...
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string DomType {get;set;}

           /// <summary>
           /// Desc:元素附加属性
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string Attr {get;set;}

           /// <summary>
           /// Desc:onclick事件调用脚本
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string Script {get;set;}

           /// <summary>
           /// Desc:元素图标
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string Icon {get;set;}

           /// <summary>
           /// Desc:元素类样式
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string Class {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default: 
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:正序排序号
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Sort {get;set;}

           /// <summary>
           /// Desc:打开方式：iframe/_blank...
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Target {get;set;}

           /// <summary>
           /// Desc:描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Descript {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creator {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Editer {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EditTime {get;set;}

    }
}
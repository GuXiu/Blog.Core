﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///要货申请单明细
    ///</summary>

    [SugarTable("ZJ_ProductApplyOrderItem","Zhongjiu")]
    public partial class ProductApplyOrderItem
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:要货申请单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductApplyId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:要货数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:已经处理的数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal DealNumber {get;set;}

           /// <summary>
           /// Desc:即时库存数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Stock {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///红包领取记录表
    ///</summary>

    [SugarTable("Himall_BonusReceive","Zhongjiu")]
    public partial class BonusReceive
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:红包Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BonusId {get;set;}

           /// <summary>
           /// Desc:领取人微信Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:领取日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ReceiveTime {get;set;}

           /// <summary>
           /// Desc:领取金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public byte? IsShare {get;set;}

           /// <summary>
           /// Desc:红包金额是否已经转入了预存款
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsTransformedDeposit {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

    }
}
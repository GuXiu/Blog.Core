﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下管理员表
    ///</summary>

    [SugarTable("Himall_OfflineManager","Zhongjiu")]
    public partial class OfflineManager
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:区域Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int RegionId {get;set;}

           /// <summary>
           /// Desc:管理员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ManagerId {get;set;}

           /// <summary>
           /// Desc:真实姓名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string RealName {get;set;}

           /// <summary>
           /// Desc:营业执照图片地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BusinessLicenceNumber {get;set;}

           /// <summary>
           /// Desc:身份证号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string IDCard {get;set;}

           /// <summary>
           /// Desc:身份证正面图片地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string IDCardPhoto {get;set;}

           /// <summary>
           /// Desc:身份证背面图片地址
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string IDCardBackPhoto {get;set;}

           /// <summary>
           /// Desc:店铺状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
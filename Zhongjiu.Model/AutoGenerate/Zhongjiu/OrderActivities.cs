﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单触发的活动记录
    ///</summary>

    [SugarTable("Himall_OrderActivities","Zhongjiu")]
    public partial class OrderActivities
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:活动ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ActivityId {get;set;}

           /// <summary>
           /// Desc:活动类型:0默认；1特价；2满赠;3满减;4买赠;9会员折扣;10会员价;11限时购;12组合购;13团购价;14批发价;15积分兑换;
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActivityType {get;set;}

           /// <summary>
           /// Desc:活动值：会员价时保存商品原价，会员折扣保存折扣值；
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SalePrice {get;set;}

           /// <summary>
           /// Desc:活动选中的活动方案
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ActivityTagNumber {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
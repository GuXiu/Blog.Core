﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///营销消息表
    ///</summary>

    [SugarTable("ZJ_MarketMsgMessage","Zhongjiu")]
    public partial class MarketMsgMessage
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? MemberId {get;set;}

           /// <summary>
           /// Desc:推送时间，初始化为当天实际推送后修改为推送时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? MessageTime {get;set;}

           /// <summary>
           /// Desc:备注类型，多个类型滚动推送
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ContentType {get;set;}

           /// <summary>
           /// Desc:最终内容json
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContentJson {get;set;}

           /// <summary>
           /// Desc:推送状态0待推送
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? SendStatus {get;set;}

           /// <summary>
           /// Desc:信息链接地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string URL {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MiniProgramUrl {get;set;}

           /// <summary>
           /// Desc:消息类型 0积分月报营销 公众号消息类型枚举值。小程序订阅201
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? MsgType {get;set;}

    }
}
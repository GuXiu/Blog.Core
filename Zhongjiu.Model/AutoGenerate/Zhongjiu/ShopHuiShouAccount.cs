﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///每个店铺，关联的小精灵支付账户信息
    ///</summary>

    [SugarTable("ZJ_ShopHuiShouAccount","Zhongjiu")]
    public partial class ShopHuiShouAccount
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:合作商户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PartnerId {get;set;}

           /// <summary>
           /// Desc:联富通后台核心商户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CoreMerchantNo {get;set;}

           /// <summary>
           /// Desc:门店商户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantNo {get;set;}

           /// <summary>
           /// Desc:秘钥
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PartnerKey {get;set;}

           /// <summary>
           /// Desc:商户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MerchantName {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:微信公众号的 Appid
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WechatSubAppid {get;set;}

           /// <summary>
           /// Desc:微信小程序 Appid
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MiniProgramSubAppid {get;set;}

           /// <summary>
           /// Desc:是否开启微信小程序的支付[0:关闭][1:开启]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SwitchMiniapp {get;set;}

           /// <summary>
           /// Desc:是否开启网页端的支付[0:关闭][1:开启]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SwitchHtml {get;set;}

           /// <summary>
           /// Desc:是否开启app的支付[0:关闭][1:开启]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SwitchApp {get;set;}

    }
}
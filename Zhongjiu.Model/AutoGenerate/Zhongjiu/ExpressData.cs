﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///快递面单数据
    ///</summary>

    [SugarTable("ZJ_ExpressData","Zhongjiu")]
    public partial class ExpressData
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:城市区号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string postCode {get;set;}

           /// <summary>
           /// Desc:走货方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipmentWay {get;set;}

           /// <summary>
           /// Desc:分拣编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string unitCode {get;set;}

           /// <summary>
           /// Desc:收货人省市区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string reginAdrees {get;set;}

           /// <summary>
           /// Desc:收货人-省
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipProvice {get;set;}

           /// <summary>
           /// Desc:收货人-市
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipCity {get;set;}

           /// <summary>
           /// Desc:收货人-区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipArea {get;set;}

           /// <summary>
           /// Desc:地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipAddress {get;set;}

           /// <summary>
           /// Desc:收货人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipTo {get;set;}

           /// <summary>
           /// Desc:收货人手机
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shipPhone {get;set;}

           /// <summary>
           /// Desc:发货人单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string shopName {get;set;}

           /// <summary>
           /// Desc:快递单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string mailNo {get;set;}

           /// <summary>
           /// Desc:支付方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string payWay {get;set;}

           /// <summary>
           /// Desc:宅急送代收款
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string agencyFund {get;set;}

           /// <summary>
           /// Desc:订单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string orderNo {get;set;}

           /// <summary>
           /// Desc:货到付款标记
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string destinationPay {get;set;}

           /// <summary>
           /// Desc:createTime
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string createTime {get;set;}

           /// <summary>
           /// Desc:商品数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string quantity {get;set;}

           /// <summary>
           /// Desc:打印尺寸
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string printSize {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///推荐商品表
    ///</summary>

    [SugarTable("Himall_RecommendProduct","Zhongjiu")]
    public partial class RecommendProduct
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:产品Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:产品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:虚拟销量
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public decimal? VirtualSales {get;set;}

           /// <summary>
           /// Desc:是否可用
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte Enabled {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Creator {get;set;}

           /// <summary>
           /// Desc:推荐分类(1:购物车推荐)(2:热卖单品)(3:购买了此类商品的用户还浏览)
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int RecommendCategory {get;set;}

           /// <summary>
           /// Desc:商品分类Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CategoryId {get;set;}

           /// <summary>
           /// Desc:品牌Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

    }
}
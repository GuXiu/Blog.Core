﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///入驻设置
    ///</summary>

    [SugarTable("Himall_Settled","Zhongjiu")]
    public partial class Settled
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long ID {get;set;}

           /// <summary>
           /// Desc:商家类型 0、仅企业可入驻；1、仅个人可入驻；2、企业和个人均可
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int BusinessType {get;set;}

           /// <summary>
           /// Desc:商家结算类型 0、仅银行账户；1、仅微信账户；2、银行账户及微信账户均可
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SettlementAccountType {get;set;}

           /// <summary>
           /// Desc:试用天数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TrialDays {get;set;}

           /// <summary>
           /// Desc:地址必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsCity {get;set;}

           /// <summary>
           /// Desc:人数必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsPeopleNumber {get;set;}

           /// <summary>
           /// Desc:详细地址必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsAddress {get;set;}

           /// <summary>
           /// Desc:营业执照号必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsBusinessLicenseCode {get;set;}

           /// <summary>
           /// Desc:经营范围必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsBusinessScope {get;set;}

           /// <summary>
           /// Desc:营业执照必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsBusinessLicense {get;set;}

           /// <summary>
           /// Desc:机构代码必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsAgencyCode {get;set;}

           /// <summary>
           /// Desc:机构代码证必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsAgencyCodeLicense {get;set;}

           /// <summary>
           /// Desc:纳税人证明必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsTaxpayerToProve {get;set;}

           /// <summary>
           /// Desc:验证类型 0、验证手机；1、验证邮箱；2、均需验证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CompanyVerificationType {get;set;}

           /// <summary>
           /// Desc:个人姓名必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsSName {get;set;}

           /// <summary>
           /// Desc:个人地址必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsSCity {get;set;}

           /// <summary>
           /// Desc:个人详细地址必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsSAddress {get;set;}

           /// <summary>
           /// Desc:个人身份证必填 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsSIDCard {get;set;}

           /// <summary>
           /// Desc:个人身份证上传 0、非必填；1、必填
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsSIdCardUrl {get;set;}

           /// <summary>
           /// Desc:个人验证类型 0、验证手机；1、验证邮箱；2、均需验证
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SelfVerificationType {get;set;}

           /// <summary>
           /// Desc:法人姓名是否必填（0：非必填 1：必填）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsLegalPerson {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsLegalIDCard {get;set;}

           /// <summary>
           /// Desc:法人身份证照片是否必填（0：非必填 1：必填）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsLegalIDCardUrl {get;set;}

           /// <summary>
           /// Desc:酒水流通许可证是否必填（0：非必填 1：必填）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int IsWineCert {get;set;}

    }
}
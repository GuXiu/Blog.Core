﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///金蝶ERP推送销退记录表
    ///</summary>

    [SugarTable("ZJ_ShopEasKingdeeSaleReturn","Zhongjiu")]
    public partial class ShopEasKingdeeSaleReturn
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:商品Id(多个逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductId {get;set;}

           /// <summary>
           /// Desc:商品条码(多个逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Sku {get;set;}

           /// <summary>
           /// Desc:已推送的，ERP 订单编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EasKingdeeOrderNumber {get;set;}

           /// <summary>
           /// Desc:物料[必填]基础资料(多个逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MaterialNumber {get;set;}

           /// <summary>
           /// Desc:退货数量(多个逗号分隔)
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MaterialNum {get;set;}

           /// <summary>
           /// Desc:退货推送 的 ERP Json 数据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleReturnsJson {get;set;}

           /// <summary>
           /// Desc:处理状态[0:未推送][1:推送成功][2:推送失败]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:操作日志
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OperationRecord {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:更新时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否已删除[0:正常][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
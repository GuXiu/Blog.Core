﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信会员关注公众号状态表
    ///</summary>

    [SugarTable("Himall_OpenIds","Zhongjiu")]
    public partial class OpenIds
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UnionId {get;set;}

           /// <summary>
           /// Desc:关注时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime SubscribeTime {get;set;}

           /// <summary>
           /// Desc:是否关注
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsSubscribe {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
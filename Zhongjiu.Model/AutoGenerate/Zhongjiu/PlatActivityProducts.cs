﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///平台活动商品信息
    ///</summary>

    [SugarTable("ZJ_PlatActivityProducts","Zhongjiu")]
    public partial class PlatActivityProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:平台活动Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PlatActivityId {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:原价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SalePrice {get;set;}

           /// <summary>
           /// Desc:活动价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ActivityPrice {get;set;}

           /// <summary>
           /// Desc:活动库存
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal ActivityStock {get;set;}

           /// <summary>
           /// Desc:已售库存
           /// Default:0.000
           /// Nullable:True
           /// </summary>           
           public decimal? SaleCount {get;set;}

           /// <summary>
           /// Desc:0 待提交；1 待审核；2 已通过；3 已驳回；
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AuditTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AddTime {get;set;}

           /// <summary>
           /// Desc:驳回原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RejectReason {get;set;}

    }
}
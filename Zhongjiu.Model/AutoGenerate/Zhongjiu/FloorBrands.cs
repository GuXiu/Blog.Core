﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页楼层品牌表
    ///</summary>

    [SugarTable("Himall_FloorBrands","Zhongjiu")]
    public partial class FloorBrands
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:楼层ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FloorId {get;set;}

           /// <summary>
           /// Desc:品牌ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long BrandId {get;set;}

    }
}
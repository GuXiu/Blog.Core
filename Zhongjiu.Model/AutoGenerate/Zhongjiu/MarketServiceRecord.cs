﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///营销服务购买记录表
    ///</summary>

    [SugarTable("Himall_MarketServiceRecord","Zhongjiu")]
    public partial class MarketServiceRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MarketServiceId {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:购买时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BuyTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int SettlementFlag {get;set;}

           /// <summary>
           /// Desc:服务购买价格
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

    }
}
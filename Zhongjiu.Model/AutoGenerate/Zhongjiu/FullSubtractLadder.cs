﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///满减活动阶梯表
    ///</summary>

    [SugarTable("himall_FullSubtractLadder","Zhongjiu")]
    public partial class FullSubtractLadder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:满减ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FullSubtractId {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public float Amount {get;set;}

           /// <summary>
           /// Desc:减去的金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public float SubtractAmount {get;set;}

    }
}
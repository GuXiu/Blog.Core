﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单联合支付信息
    ///</summary>

    [SugarTable("Himall_OfflineOrderUnionPay","Zhongjiu")]
    public partial class OfflineOrderUnionPay
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:联合支付订单ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:现金
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Cash {get;set;}

           /// <summary>
           /// Desc:微信支付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal WxPay {get;set;}

           /// <summary>
           /// Desc:支付宝支付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ZfbPay {get;set;}

           /// <summary>
           /// Desc:POS支付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PosPay {get;set;}

           /// <summary>
           /// Desc:Pos云闪付支付金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal PosYsfPay {get;set;}

           /// <summary>
           /// Desc:会员余额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Balance {get;set;}

    }
}
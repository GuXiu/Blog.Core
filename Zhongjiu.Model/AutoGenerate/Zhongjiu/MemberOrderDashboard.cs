﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户订单数据统计 按用户统计每天订单,没有则不录入新数据
    ///</summary>

    [SugarTable("ZJ_MemberOrderDashboard","Zhongjiu")]
    public partial class MemberOrderDashboard
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? MemberId {get;set;}

           /// <summary>
           /// Desc:店铺id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:统计时间 统计日期按yyyy-MM-dd 00:00:00记录
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CountingTime {get;set;}

           /// <summary>
           /// Desc:订单笔数
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? OrderCounts {get;set;}

           /// <summary>
           /// Desc:订单总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? OrderValue {get;set;}

    }
}
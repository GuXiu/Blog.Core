﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///关联表
    ///</summary>

    [SugarTable("Himall_Relevance","Zhongjiu")]
    public partial class Relevance
    {
           /// <summary>
           /// Desc:ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string Id {get;set;}

           /// <summary>
           /// Desc:First ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string FId {get;set;}

           /// <summary>
           /// Desc:Second ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SId {get;set;}

           /// <summary>
           /// Desc:Third ID
           /// Default:-1
           /// Nullable:False
           /// </summary>           
           public long TId {get;set;}

           /// <summary>
           /// Desc:可用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte Enable {get;set;}

           /// <summary>
           /// Desc:类型:1平台角色-菜单,2平台用户-菜单,3:fid体系关联显示的附近店铺sid,4:体系关联显示的20公里外默认显示门店；5卖家角色-菜单;6会员-完善过信息
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

    }
}
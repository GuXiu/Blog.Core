﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员设置表
    ///</summary>

    [SugarTable("Himall_OfflineVipSet","Zhongjiu")]
    public partial class OfflineVipSet
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:消费金额满
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Monetary {get;set;}

           /// <summary>
           /// Desc:赠送中酒币数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Coin {get;set;}

           /// <summary>
           /// Desc:每多少金币低
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ConsumeCoin {get;set;}

           /// <summary>
           /// Desc:冲抵金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ChargeAgainst {get;set;}

           /// <summary>
           /// Desc:会员打几折
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? VipDiscount {get;set;}

    }
}
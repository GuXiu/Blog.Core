﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销市场分享设置表
    ///</summary>

    [SugarTable("Himall_DistributionShareSetting","Zhongjiu")]
    public partial class DistributionShareSetting
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProShareLogo {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopShareLogo {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProShareTitle {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopShareTitle {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProShareDesc {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopShareDesc {get;set;}

           /// <summary>
           /// Desc:分销市场分享Logo
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DisShareLogo {get;set;}

           /// <summary>
           /// Desc:招募分享Logo
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RecruitShareLogo {get;set;}

           /// <summary>
           /// Desc:分销市场分享标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DisShareTitle {get;set;}

           /// <summary>
           /// Desc:招募分享标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RecruitShareTitle {get;set;}

           /// <summary>
           /// Desc:分销市场分享描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DisShareDesc {get;set;}

           /// <summary>
           /// Desc:招募分享描述
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RecruitShareDesc {get;set;}

    }
}
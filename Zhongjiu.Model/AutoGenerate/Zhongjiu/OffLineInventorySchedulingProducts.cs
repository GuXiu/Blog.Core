﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下商品调拨明细表
    ///</summary>

    [SugarTable("Himall_OffLineInventorySchedulingProducts","Zhongjiu")]
    public partial class OffLineInventorySchedulingProducts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:库存调拨Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SchedulingId {get;set;}

           /// <summary>
           /// Desc:线下商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:入库/出库价格
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

           /// <summary>
           /// Desc:入库/出库数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Number {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remarks {get;set;}

           /// <summary>
           /// Desc:成本价
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal InventoryLossOutPrice {get;set;}

           /// <summary>
           /// Desc:出入库金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal InventoryLossPrice {get;set;}

    }
}
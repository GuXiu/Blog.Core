﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺表
    ///</summary>

    [SugarTable("Himall_Shops","Zhongjiu")]
    public partial class Shops
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺等级
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long GradeId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:店铺LOGO路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Logo {get;set;}

           /// <summary>
           /// Desc:预留子域名，未使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SubDomains {get;set;}

           /// <summary>
           /// Desc:预留主题，未使用
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Theme {get;set;}

           /// <summary>
           /// Desc:是否是官方自营店
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsSelf {get;set;}

           /// <summary>
           /// Desc:店铺状态(7 启用)
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ShopStatus {get;set;}

           /// <summary>
           /// Desc:审核拒绝原因
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RefuseReason {get;set;}

           /// <summary>
           /// Desc:店铺创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateDate {get;set;}

           /// <summary>
           /// Desc:店铺过期日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EndDate {get;set;}

           /// <summary>
           /// Desc:公司名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyName {get;set;}

           /// <summary>
           /// Desc:公司省市区
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CompanyRegionId {get;set;}

           /// <summary>
           /// Desc:公司地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyAddress {get;set;}

           /// <summary>
           /// Desc:公司电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CompanyPhone {get;set;}

           /// <summary>
           /// Desc:公司员工数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CompanyEmployeeCount {get;set;}

           /// <summary>
           /// Desc:公司注册资金
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CompanyRegisteredCapital {get;set;}

           /// <summary>
           /// Desc:联系人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContactsName {get;set;}

           /// <summary>
           /// Desc:联系电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContactsPhone {get;set;}

           /// <summary>
           /// Desc:联系Email
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ContactsEmail {get;set;}

           /// <summary>
           /// Desc:营业执照号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessLicenceNumber {get;set;}

           /// <summary>
           /// Desc:营业执照
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string BusinessLicenceNumberPhoto {get;set;}

           /// <summary>
           /// Desc:营业执照所在地
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int BusinessLicenceRegionId {get;set;}

           /// <summary>
           /// Desc:营业执照有效期开始
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? BusinessLicenceStart {get;set;}

           /// <summary>
           /// Desc:营业执照有效期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? BusinessLicenceEnd {get;set;}

           /// <summary>
           /// Desc:法定经营范围
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessSphere {get;set;}

           /// <summary>
           /// Desc:组织机构代码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrganizationCode {get;set;}

           /// <summary>
           /// Desc:组织机构执照
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrganizationCodePhoto {get;set;}

           /// <summary>
           /// Desc:一般纳税人证明
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GeneralTaxpayerPhot {get;set;}

           /// <summary>
           /// Desc:银行开户名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankAccountName {get;set;}

           /// <summary>
           /// Desc:公司银行账号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankAccountNumber {get;set;}

           /// <summary>
           /// Desc:开户银行支行名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankName {get;set;}

           /// <summary>
           /// Desc:支行联行号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankCode {get;set;}

           /// <summary>
           /// Desc:开户银行所在地
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int BankRegionId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BankPhoto {get;set;}

           /// <summary>
           /// Desc:税务登记证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TaxRegistrationCertificate {get;set;}

           /// <summary>
           /// Desc:税务登记证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TaxpayerId {get;set;}

           /// <summary>
           /// Desc:纳税人识别号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TaxRegistrationCertificatePhoto {get;set;}

           /// <summary>
           /// Desc:支付凭证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayPhoto {get;set;}

           /// <summary>
           /// Desc:支付注释
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayRemark {get;set;}

           /// <summary>
           /// Desc:商家发货人名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SenderName {get;set;}

           /// <summary>
           /// Desc:商家发货人地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SenderAddress {get;set;}

           /// <summary>
           /// Desc:商家发货人电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SenderPhone {get;set;}

           /// <summary>
           /// Desc:运费
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Freight {get;set;}

           /// <summary>
           /// Desc:多少钱开始免运费
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal FreeFreight {get;set;}

           /// <summary>
           /// Desc:注册步骤
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Stage {get;set;}

           /// <summary>
           /// Desc:商家发货人省市区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SenderRegionId {get;set;}

           /// <summary>
           /// Desc:营业执照证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessLicenseCert {get;set;}

           /// <summary>
           /// Desc:商品证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductCert {get;set;}

           /// <summary>
           /// Desc:其他证书
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OtherCert {get;set;}

           /// <summary>
           /// Desc:法人代表
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string legalPerson {get;set;}

           /// <summary>
           /// Desc:公司成立日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CompanyFoundingDate {get;set;}

           /// <summary>
           /// Desc:0、企业；1、个人
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? BusinessType {get;set;}

           /// <summary>
           /// Desc:身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IDCard {get;set;}

           /// <summary>
           /// Desc:身份证URL
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IDCardUrl {get;set;}

           /// <summary>
           /// Desc:微信昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinNickName {get;set;}

           /// <summary>
           /// Desc:微信性别;0、男；1、女
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? WeiXinSex {get;set;}

           /// <summary>
           /// Desc:微信地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinAddress {get;set;}

           /// <summary>
           /// Desc:微信真实姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinTrueName {get;set;}

           /// <summary>
           /// Desc:微信标识符
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinOpenId {get;set;}

           /// <summary>
           /// Desc:微信头像
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinImg {get;set;}

           /// <summary>
           /// Desc:偏远地区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string RemoteArea {get;set;}

           /// <summary>
           /// Desc:法人身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LegalIDCard {get;set;}

           /// <summary>
           /// Desc:法人身份证照片地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string LegalIDCardUrl {get;set;}

           /// <summary>
           /// Desc:酒水流通许可证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WineCert {get;set;}

           /// <summary>
           /// Desc:省
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopProvinceId {get;set;}

           /// <summary>
           /// Desc:市
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopCityId {get;set;}

           /// <summary>
           /// Desc:区
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopDistrictId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopProvinceName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopCityName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopDistrictName {get;set;}

           /// <summary>
           /// Desc:商家介绍
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopInfos {get;set;}

           /// <summary>
           /// Desc:商家类型 1:个体经营 2:连锁企业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ShopMode {get;set;}

           /// <summary>
           /// Desc:支付方式 1：微信支付 2:支付宝 3:现金 4:POS/其它设备 5:购物卡/会员卡
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayMode {get;set;}

           /// <summary>
           /// Desc:发票类型 1：纸质普通发票 2：电子普通发票 3：纸质专用发票
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BillMode {get;set;}

           /// <summary>
           /// Desc:配送方式 1：快递配送 2：自配送 3：自提
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShippingMode {get;set;}

           /// <summary>
           /// Desc:商家电话
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopTel {get;set;}

           /// <summary>
           /// Desc:营业开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessStartTime {get;set;}

           /// <summary>
           /// Desc:营业结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BusinessEndTime {get;set;}

           /// <summary>
           /// Desc:商家地址经纬度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopLnglat {get;set;}

           /// <summary>
           /// Desc:商家定位地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopAddress {get;set;}

           /// <summary>
           /// Desc:商家详细地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopAddressDetails {get;set;}

           /// <summary>
           /// Desc:店铺门脸图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopFrontPhoto {get;set;}

           /// <summary>
           /// Desc:店铺店内环境图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopEnvironmentPhoto {get;set;}

           /// <summary>
           /// Desc:店铺店内环境详细图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopEnvironmentDetailsPhoto {get;set;}

           /// <summary>
           /// Desc:周六日节假日是否营业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public byte? BusinessTimeLimit {get;set;}

           /// <summary>
           /// Desc:手机号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopPhone {get;set;}

           /// <summary>
           /// Desc:小票打印宽度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ReceiptWidth {get;set;}

           /// <summary>
           /// Desc:小精灵账户名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HuiShouYinLoginName {get;set;}

           /// <summary>
           /// Desc:小精灵账户密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HuiShouYinPassword {get;set;}

           /// <summary>
           /// Desc:是否支持双屏显示
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public byte? DoubleScreen {get;set;}

           /// <summary>
           /// Desc:[0:普通店铺][1:配送中心][2:移动酒柜][3:其他]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte SourceCategory {get;set;}

           /// <summary>
           /// Desc:配送等级
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Level {get;set;}

           /// <summary>
           /// Desc:结算比例
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? SettlementProportion {get;set;}

           /// <summary>
           /// Desc:区号
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public string ShopCode {get;set;}

           /// <summary>
           /// Desc:店铺显示顺序
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? ShopSort {get;set;}

           /// <summary>
           /// Desc:店铺类型[1:普通店铺][2:酒柜]
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? ShopType {get;set;}

           /// <summary>
           /// Desc:是否显示店铺 1显示 0不显示
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? IsShowShop {get;set;}

           /// <summary>
           /// Desc:订单金额小数位
           /// Default:2
           /// Nullable:False
           /// </summary>           
           public int AmountFractional {get;set;}

           /// <summary>
           /// Desc:订单金额保留方式：0四舍五入，1向下取整，2向上取整
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AmountFractionalType {get;set;}

           /// <summary>
           /// Desc:从云码获得店铺注册二维码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopRegisteredCode {get;set;}

           /// <summary>
           /// Desc:默认配送方式（1：快递 2：自配送 3：自提）
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? DefaultShippingMode {get;set;}

           /// <summary>
           /// Desc:自配送满金额免运费
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? SinceFreight {get;set;}

           /// <summary>
           /// Desc:店铺自配送运费
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? ShopSinceFreight {get;set;}

           /// <summary>
           /// Desc:返佣方式(1 自动结算 2自行结算)
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? ReturnCommissionWay {get;set;}

           /// <summary>
           /// Desc:PC端是否显示店铺(0不显示 1显示)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int IsPcShowShop {get;set;}

           /// <summary>
           /// Desc:APP端是否显示店铺(0不显示 1显示)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int IsAppShowShop {get;set;}

           /// <summary>
           /// Desc:M站是否显示店铺(0不显示 1显示)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int IsH5ShowShop {get;set;}

           /// <summary>
           /// Desc:小程序是否显示店铺(0不显示 1显示)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int IsWeChatShowShop {get;set;}

    }
}
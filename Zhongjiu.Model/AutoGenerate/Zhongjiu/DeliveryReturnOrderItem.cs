﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配退单明细
    ///</summary>

    [SugarTable("ZJ_DeliveryReturnOrderItem","Zhongjiu")]
    public partial class DeliveryReturnOrderItem
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:配退单ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DeliveryReturnId {get;set;}

           /// <summary>
           /// Desc:商品ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:退货数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Number {get;set;}

           /// <summary>
           /// Desc:含税配送价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal VATinclusive {get;set;}

           /// <summary>
           /// Desc:不含税配送价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NVATinclusive {get;set;}

           /// <summary>
           /// Desc:配送税额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Tax {get;set;}

    }
}
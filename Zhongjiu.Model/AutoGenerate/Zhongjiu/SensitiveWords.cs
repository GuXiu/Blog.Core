﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///敏感关键词表
    ///</summary>

    [SugarTable("Himall_SensitiveWords","Zhongjiu")]
    public partial class SensitiveWords
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:敏感词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SensitiveWord {get;set;}

           /// <summary>
           /// Desc:敏感词类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CategoryName {get;set;}

    }
}
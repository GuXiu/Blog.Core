﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员签到记录表
    ///</summary>

    [SugarTable("Himall_MemberSignIn","Zhongjiu")]
    public partial class MemberSignIn
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:用户ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long UserId {get;set;}

           /// <summary>
           /// Desc:最近签到时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? LastSignTime {get;set;}

           /// <summary>
           /// Desc:持续签到天数 每周期后清零
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DurationDay {get;set;}

           /// <summary>
           /// Desc:持续签到天数总数 非连续周期清零
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DurationDaySum {get;set;}

           /// <summary>
           /// Desc:签到总天数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SignDaySum {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺红包表
    ///</summary>

    [SugarTable("Himall_ShopBonus","Zhongjiu")]
    public partial class ShopBonus
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:红包数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Count {get;set;}

           /// <summary>
           /// Desc:随机范围Start
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RandomAmountStart {get;set;}

           /// <summary>
           /// Desc:随机范围End
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal RandomAmountEnd {get;set;}

           /// <summary>
           /// Desc:1:满X元使用  2：没有限制
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int UseState {get;set;}

           /// <summary>
           /// Desc:满多少元
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal UsrStatePrice {get;set;}

           /// <summary>
           /// Desc:满多少元才发放红包
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal GrantPrice {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime DateStart {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime DateEnd {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BonusDateStart {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BonusDateEnd {get;set;}

           /// <summary>
           /// Desc:分享
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShareTitle {get;set;}

           /// <summary>
           /// Desc:分享
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShareDetail {get;set;}

           /// <summary>
           /// Desc:分享
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShareImg {get;set;}

           /// <summary>
           /// Desc:是否同步到微信卡包，是的话才出现微信卡卷相关UI
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte SynchronizeCard {get;set;}

           /// <summary>
           /// Desc:微信卡卷相关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardTitle {get;set;}

           /// <summary>
           /// Desc:微信卡卷相关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardColor {get;set;}

           /// <summary>
           /// Desc:微信卡卷相关
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardSubtitle {get;set;}

           /// <summary>
           /// Desc:是否失效
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsInvalid {get;set;}

           /// <summary>
           /// Desc:领取数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ReceiveCount {get;set;}

           /// <summary>
           /// Desc:二维码路径
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string QRPath {get;set;}

           /// <summary>
           /// Desc:微信卡卷审核状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int WXCardState {get;set;}

           /// <summary>
           /// Desc:创建人级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:省市管理员发布活动，关联店铺Id集合，逗号分隔字符串
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

    }
}
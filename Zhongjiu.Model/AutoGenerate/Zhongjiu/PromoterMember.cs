﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///分销员与被邀请会员关系表
    ///</summary>

    [SugarTable("Himall_PromoterMember","Zhongjiu")]
    public partial class PromoterMember
    {
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:分销员用户ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? PromoterMemberId {get;set;}

           /// <summary>
           /// Desc:被邀请会员Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? UserId {get;set;}

           /// <summary>
           /// Desc:被邀请UNIONId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string UnionId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateDate {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:数据来源（0： 1：云码，2：后台分配 3：分销员分享优惠券 4:订单补充 ）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SourceType {get;set;}

           /// <summary>
           /// Desc:分销员表ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PromoterId {get;set;}

           /// <summary>
           /// Desc:有效期截至
           /// Default:9999-09-09 00:00:00
           /// Nullable:True
           /// </summary>           
           public DateTime? EndDate {get;set;}

    }
}
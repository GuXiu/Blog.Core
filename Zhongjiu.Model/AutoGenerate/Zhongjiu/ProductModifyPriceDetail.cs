﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///产品调价明细单
    ///</summary>

    [SugarTable("ZJ_ProductModifyPriceDetail","Zhongjiu")]
    public partial class ProductModifyPriceDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:调价单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TJId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:原零售价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldSalePrice {get;set;}

           /// <summary>
           /// Desc:新零售价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewSalePrice {get;set;}

           /// <summary>
           /// Desc:原会员价一
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldVipPrice1 {get;set;}

           /// <summary>
           /// Desc:新会员价一
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewVipPrice1 {get;set;}

           /// <summary>
           /// Desc:原会员价二
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldVipPrice2 {get;set;}

           /// <summary>
           /// Desc:新会员价二
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewVipPrice2 {get;set;}

           /// <summary>
           /// Desc:原会员价三
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldVipPrice3 {get;set;}

           /// <summary>
           /// Desc:新会员价三
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewVipPrice3 {get;set;}

           /// <summary>
           /// Desc:原团购价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldGroupPrice {get;set;}

           /// <summary>
           /// Desc:新团购价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewGroupPrice {get;set;}

           /// <summary>
           /// Desc:原批发价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldWholesalePrice {get;set;}

           /// <summary>
           /// Desc:新批发价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewWholesalePrice {get;set;}

           /// <summary>
           /// Desc:原含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:新含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:原不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldNotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:新不含税进价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewNotIncludeTaxPurchasePrice {get;set;}

           /// <summary>
           /// Desc:原进价税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldPurchaseTaxPrice {get;set;}

           /// <summary>
           /// Desc:新进价税额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewPurchaseTaxPrice {get;set;}

           /// <summary>
           /// Desc:原底价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal OldMinPrice {get;set;}

           /// <summary>
           /// Desc:新底价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal NewMinPrice {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

    }
}
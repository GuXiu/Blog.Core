﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///优惠券 商品适用，多商品关联表
    ///</summary>

    [SugarTable("Himall_CouponProduct","Zhongjiu")]
    public partial class CouponProduct
    {
           /// <summary>
           /// Desc:主键Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:优惠券活动Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CouponId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:商品数量限制，指定数量内(包含该数量)可享受 面值、折扣、制定价格，超出数量 按原价走
           /// Default:1.000
           /// Nullable:False
           /// </summary>           
           public decimal LimitNumber {get;set;}

    }
}
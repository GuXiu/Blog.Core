﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配退单
    ///</summary>

    [SugarTable("ZJ_DeliveryReturnOrder","Zhongjiu")]
    public partial class DeliveryReturnOrder
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:配退单编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:配送入库单编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string DeliveryEnterCode {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:配送中心ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

           /// <summary>
           /// Desc:配退总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:审核状态（0：未审核 1：已审核  2：已生效  3 ：已终止 4：已完成）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AuditState {get;set;}

           /// <summary>
           /// Desc:是否已结算（0：未结算  1：已结算）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int IsSettlement {get;set;}

           /// <summary>
           /// Desc:收货状态（0：未发货，1：已发货，2：已收货）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReceivingState {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreaterTime {get;set;}

           /// <summary>
           /// Desc:修改时间，默认和创建时间相同
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime UpdateTime {get;set;}

           /// <summary>
           /// Desc:是否删除[0:未删除][1:已删除]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员店铺关系表
    ///</summary>

    [SugarTable("Himall_MemberShops","Zhongjiu")]
    public partial class MemberShops
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

           /// <summary>
           /// Desc:会员所属店铺ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:营业员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long TradeAssistantId {get;set;}

           /// <summary>
           /// Desc:用户来源 0 下单，1 收银台注册，2 门店二维码注册，3平台中心注册，4：卖家中心注册，5：APP收银注册，6：云码，7:小票扫码，8:天猫平台订单交易 101：导入会员关系（为 3 时，并且creator为0 也视为导入会员关系）107 一步直邀注册  112门店新增收银员
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? RegisterSource {get;set;}

           /// <summary>
           /// Desc:会员类型：（0：零售，1：团购，2：批发）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:创建人：当前登录人manager表ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long Creator {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///用户信息扩展表
    ///</summary>

    [SugarTable("Himall_MoreMembers","Zhongjiu")]
    public partial class MoreMembers
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long id {get;set;}

           /// <summary>
           /// Desc:婚姻状况
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Marrage {get;set;}

           /// <summary>
           /// Desc:常饮用的酒品
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FrequenSprit {get;set;}

           /// <summary>
           /// Desc:可接受的酒品价位
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? AccepterPrice {get;set;}

           /// <summary>
           /// Desc:教育程度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EducatiionStatus {get;set;}

           /// <summary>
           /// Desc:从事职业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PursueOccupation {get;set;}

           /// <summary>
           /// Desc:所属行业
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IndustryInvolved {get;set;}

           /// <summary>
           /// Desc:月收入
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MonthIncome {get;set;}

           /// <summary>
           /// Desc:饮酒年限
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? DrinkingYears {get;set;}

           /// <summary>
           /// Desc:爱好
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Hobby {get;set;}

    }
}
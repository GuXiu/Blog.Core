﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///特价
    ///</summary>

    [SugarTable("Himall_BuySpecialOffer","Zhongjiu")]
    public partial class BuySpecialOffer
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:审核状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AuditStatus {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Auditor {get;set;}

           /// <summary>
           /// Desc:审核时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime AuditTime {get;set;}

           /// <summary>
           /// Desc:取消原因
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CancelReson {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime StartTime {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Creator {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:创建人级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:省市管理员发布活动，关联店铺Id集合，逗号分隔字符串
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

    }
}
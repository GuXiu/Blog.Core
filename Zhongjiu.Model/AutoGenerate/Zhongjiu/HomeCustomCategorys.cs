﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页主页自定义类别表
    ///</summary>

    [SugarTable("Himall_HomeCustomCategorys","Zhongjiu")]
    public partial class HomeCustomCategorys
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Image {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Sort {get;set;}

           /// <summary>
           /// Desc:是否启用；0：不启用，1：启用
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsEnable {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Operator {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:层级,深度
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Depth {get;set;}

           /// <summary>
           /// Desc:只针对三级菜单，优先显示到最前面；0：不优先显示，1：优先显示
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte TopShow {get;set;}

           /// <summary>
           /// Desc:只针对三级菜单，高亮显示，前提是优先显示为1,；0：不高亮显示，1：高亮显示
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte HighLightTopShow {get;set;}

    }
}
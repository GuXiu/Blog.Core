﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///一品多商体系设置
    ///</summary>

    [SugarTable("ZJ_YPDSAccountSet","Zhongjiu")]
    public partial class YPDSAccountSet
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:所属体系
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:是否显示 1 显示；0 不显示
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsShow {get;set;}

           /// <summary>
           /// Desc:体系范围 1 所有体系；2 本体系
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int AccountSetArea {get;set;}

           /// <summary>
           /// Desc:距离范围 1 不限；2 同省；3 同城；4 距离
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int DistanceArea {get;set;}

           /// <summary>
           /// Desc:距离
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Distances {get;set;}

    }
}
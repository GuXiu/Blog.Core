﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商品库存阈值表
    ///</summary>

    [SugarTable("ZJ_ProductStockThreshold","Zhongjiu")]
    public partial class ProductStockThreshold
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ShopId {get;set;}

           /// <summary>
           /// Desc:商品Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:Skuid
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SkuId {get;set;}

           /// <summary>
           /// Desc:库存不足阈值
           /// Default:0.0000
           /// Nullable:True
           /// </summary>           
           public decimal? MinimumThreshold {get;set;}

           /// <summary>
           /// Desc:库存溢出阈值
           /// Default:0.0000
           /// Nullable:True
           /// </summary>           
           public decimal? MaximumThreshold {get;set;}

           /// <summary>
           /// Desc:类型0动态设置预警1手动设置预警
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Type {get;set;}

           /// <summary>
           /// Desc:设置状态(1正常启用0关闭)
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Status {get;set;}

    }
}
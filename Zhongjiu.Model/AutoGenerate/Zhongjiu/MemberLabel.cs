﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员标签表
    ///</summary>

    [SugarTable("Himall_MemberLabel","Zhongjiu")]
    public partial class MemberLabel
    {
           /// <summary>
           /// Desc:Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemId {get;set;}

           /// <summary>
           /// Desc:标签Id
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long LabelId {get;set;}

    }
}
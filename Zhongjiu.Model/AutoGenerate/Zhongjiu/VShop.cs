﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微店表
    ///</summary>

    [SugarTable("Himall_VShop","Zhongjiu")]
    public partial class VShop
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:历览次数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int VisitNum {get;set;}

           /// <summary>
           /// Desc:购买数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int buyNum {get;set;}

           /// <summary>
           /// Desc:状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int State {get;set;}

           /// <summary>
           /// Desc:LOGO
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Logo {get;set;}

           /// <summary>
           /// Desc:背景图
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BackgroundImage {get;set;}

           /// <summary>
           /// Desc:详情
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Description {get;set;}

           /// <summary>
           /// Desc:标签
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Tags {get;set;}

           /// <summary>
           /// Desc:微信首页显示的标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HomePageTitle {get;set;}

           /// <summary>
           /// Desc:微信Logo
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WXLogo {get;set;}

    }
}
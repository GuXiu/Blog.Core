﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///主题设置表
    ///</summary>

    [SugarTable("Himall_Theme","Zhongjiu")]
    public partial class Theme
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long ThemeId {get;set;}

           /// <summary>
           /// Desc:0、默认主题；1、自定义主题
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TypeId {get;set;}

           /// <summary>
           /// Desc:商城主色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MainColor {get;set;}

           /// <summary>
           /// Desc:商城辅色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SecondaryColor {get;set;}

           /// <summary>
           /// Desc:字体颜色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WritingColor {get;set;}

           /// <summary>
           /// Desc:边框颜色
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string FrameColor {get;set;}

           /// <summary>
           /// Desc:商品分类栏
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ClassifiedsColor {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>

    [SugarTable("ZJ_MemberEventTracking","Zhongjiu")]
    public partial class MemberEventTracking
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:发生时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? EventTime {get;set;}

           /// <summary>
           /// Desc:设备
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TerminalDevice {get;set;}

           /// <summary>
           /// Desc:数据来源
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DataSorce {get;set;}

           /// <summary>
           /// Desc:地址
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Address {get;set;}

           /// <summary>
           /// Desc:事件名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EventName {get;set;}

           /// <summary>
           /// Desc:事件详情
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string EverntDetail {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemberId {get;set;}

    }
}
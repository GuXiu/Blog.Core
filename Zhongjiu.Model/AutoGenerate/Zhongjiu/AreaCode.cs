﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///区域编号表（删除）
    ///</summary>

    [SugarTable("Himall_AreaCode","Zhongjiu")]
    public partial class AreaCode
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long id {get;set;}

           /// <summary>
           /// Desc:省
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Province {get;set;}

           /// <summary>
           /// Desc:市
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string City {get;set;}

           /// <summary>
           /// Desc:区号
           /// Default:
           /// Nullable:True
           /// </summary>           
           [SugarColumn(ColumnName="AreaCode")]
           public long? AreaCode1 {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///库存单位表
    ///</summary>

    [SugarTable("ZJ_StockUnit","Zhongjiu")]
    public partial class StockUnit
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:最小单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MinUnit {get;set;}

           /// <summary>
           /// Desc:最大单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MaxUnit {get;set;}

           /// <summary>
           /// Desc:最大单位和最小单位换算比例
           /// Default:1
           /// Nullable:True
           /// </summary>           
           public int? Ratio {get;set;}

    }
}
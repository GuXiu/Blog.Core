﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺资金流水表
    ///</summary>

    [SugarTable("Himall_ShopAccountItem","Zhongjiu")]
    public partial class ShopAccountItem
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:店铺名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:交易流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountNo {get;set;}

           /// <summary>
           /// Desc:关联资金编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long AccoutID {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:帐户剩余
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Balance {get;set;}

           /// <summary>
           /// Desc:交易类型
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int TradeType {get;set;}

           /// <summary>
           /// Desc:是否收入
           /// Default:
           /// Nullable:False
           /// </summary>           
           public bool IsIncome {get;set;}

           /// <summary>
           /// Desc:交易备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReMark {get;set;}

           /// <summary>
           /// Desc:详情ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DetailId {get;set;}

           /// <summary>
           /// Desc:订单来源 [1:线上订单,2:线下订单]
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte OrderSource {get;set;}

    }
}
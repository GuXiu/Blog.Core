﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系公众号二维码
    ///</summary>

    [SugarTable("ZJ_PublicQrCode","Zhongjiu")]
    public partial class PublicQrCode
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SystemId {get;set;}

           /// <summary>
           /// Desc:二维码图片路径
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QrCodePath {get;set;}

    }
}
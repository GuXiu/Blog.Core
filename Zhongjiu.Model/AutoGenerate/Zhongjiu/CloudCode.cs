﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///中酒云码库
    ///</summary>

    [SugarTable("Himall_CloudCode","Zhongjiu")]
    public partial class CloudCode
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:云码Url
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url {get;set;}

           /// <summary>
           /// Desc:是否已分配 （该字段已不用）
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDistribution {get;set;}

           /// <summary>
           /// Desc:是否已使用  （该字段已不用）
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsUsed {get;set;}

           /// <summary>
           /// Desc:状态 （0：未分配 1：已分配 2: 已使用）
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:店铺ID 分配时更新该字段
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreteDate {get;set;}

           /// <summary>
           /// Desc:云码分配时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? DistributionDate {get;set;}

           /// <summary>
           /// Desc:云码使用时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? UsedDate {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:使用的订单ID
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:云码
           /// Default:
           /// Nullable:True
           /// </summary>           
           [SugarColumn(ColumnName="CloudCode")]
           public string CloudCode1 {get;set;}

           /// <summary>
           /// Desc:批次号
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public string Batch {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///拼团订单
    ///</summary>

    [SugarTable("Himall_FightGroupOrder","Zhongjiu")]
    public partial class FightGroupOrder
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:对应活动
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ActiveId {get;set;}

           /// <summary>
           /// Desc:对应商品
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ProductId {get;set;}

           /// <summary>
           /// Desc:商品SKU
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SkuId {get;set;}

           /// <summary>
           /// Desc:对应活动项
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? ActiveItemId {get;set;}

           /// <summary>
           /// Desc:所属拼团
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? GroupId {get;set;}

           /// <summary>
           /// Desc:订单时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderId {get;set;}

           /// <summary>
           /// Desc:订单用户编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? OrderUserId {get;set;}

           /// <summary>
           /// Desc:是否团首订单
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? IsFirstOrder {get;set;}

           /// <summary>
           /// Desc:参团时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? JoinTime {get;set;}

           /// <summary>
           /// Desc:参团状态 参团中  成功  失败(-10开团中(团长订单未付款) -1参团失败 0正在参与 1组团中(等待其他团友，不可发货) 2拼团失败(己付款，但拼团超时人数未满) 4组团成功(可以发货))
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? JoinStatus {get;set;}

           /// <summary>
           /// Desc:结束时间 成功或失败的时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? OverTime {get;set;}

           /// <summary>
           /// Desc:购买数量
           /// Default:0.000
           /// Nullable:False
           /// </summary>           
           public decimal Quantity {get;set;}

           /// <summary>
           /// Desc:销售价
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal SalePrice {get;set;}

           /// <summary>
           /// Desc:拼团订单数据来源标识；0 默认用户下单创建；1 定时任务自动创建的拼团订单
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int SourceTag {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///结算单表
    ///</summary>

    [SugarTable("ZJ_Statements","Zhongjiu")]
    public partial class Statements
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:单据编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string OrderCode {get;set;}

           /// <summary>
           /// Desc:供应商ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SupplierId {get;set;}

           /// <summary>
           /// Desc:门店或配送中心ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:区域中心ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

           /// <summary>
           /// Desc:审核状态（0：未审核 1：已审核 2：已生效 3：已终止 4：已完成）
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? AuditState {get;set;}

           /// <summary>
           /// Desc:结算类型（1：供应商结算，2：加盟店结算，3：直营门店盈亏）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Type {get;set;}

           /// <summary>
           /// Desc:付款方式（1：现金，2：银行卡，3：信用卡，4：支票，5：银行转账，6：托收承付，7：电汇，8：承兑汇票，9：代收，10：挂账，11：整单让价）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int PayType {get;set;}

           /// <summary>
           /// Desc:结算起始日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BeginTime {get;set;}

           /// <summary>
           /// Desc:结算结束时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndTime {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:创建人ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Creater {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsDel {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///商城首页楼层专题表
    ///</summary>

    [SugarTable("Himall_FloorTopics","Zhongjiu")]
    public partial class FloorTopics
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:楼层ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FloorId {get;set;}

           /// <summary>
           /// Desc:专题类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TopicType {get;set;}

           /// <summary>
           /// Desc:专题封面图片
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TopicImage {get;set;}

           /// <summary>
           /// Desc:专题名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string TopicName {get;set;}

           /// <summary>
           /// Desc:专题跳转URL
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Url {get;set;}

    }
}
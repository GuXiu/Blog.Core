﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///奖品明细表
    ///</summary>

    [SugarTable("ZJ_LuckDrawAwardsItem","Zhongjiu")]
    public partial class LuckDrawAwardsItem
    {
           /// <summary>
           /// Desc:主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:奖项ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long PrizeId {get;set;}

           /// <summary>
           /// Desc:奖品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AwardsName {get;set;}

           /// <summary>
           /// Desc:奖品类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int AwardsType {get;set;}

           /// <summary>
           /// Desc:数据ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DataId {get;set;}

           /// <summary>
           /// Desc:状态(1正常 0删除)
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:添加人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Cerator {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long Modified {get;set;}

           /// <summary>
           /// Desc:添加时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime CreateTime {get;set;}

           /// <summary>
           /// Desc:修改时间
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:False
           /// </summary>           
           public DateTime ModifyTime {get;set;}

    }
}
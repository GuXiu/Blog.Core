﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///体系微信公众号设置表
    ///</summary>

    [SugarTable("ZJ_AccountSetWeiXin","Zhongjiu")]
    public partial class AccountSetWeiXin
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:体系名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSetName {get;set;}

           /// <summary>
           /// Desc:体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:公众号名字
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinName {get;set;}

           /// <summary>
           /// Desc:微信公众号AppId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinAppId {get;set;}

           /// <summary>
           /// Desc:微信公众号原始Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string WeiXinOriginalId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddDate {get;set;}

           /// <summary>
           /// Desc:是否默认
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public byte IsDefault {get;set;}

           /// <summary>
           /// Desc:推送不分体系的信息优先顺序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Priority {get;set;}

           /// <summary>
           /// Desc:微信类型：0普通微信，1企业微信
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int WxType {get;set;}

    }
}
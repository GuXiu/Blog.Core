﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///订单评价统计表
    ///</summary>

    [SugarTable("Himall_StatisticOrderComments","Zhongjiu")]
    public partial class StatisticOrderComments
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:评价的枚举（宝贝与描述相符 商家得分）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CommentKey {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CommentValue {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺提现表
    ///</summary>

    [SugarTable("Himall_ShopWithDraw","Zhongjiu")]
    public partial class ShopWithDraw
    {
           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:提现流水号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CashNo {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ApplyTime {get;set;}

           /// <summary>
           /// Desc:提现状态
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:提现方式
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int CashType {get;set;}

           /// <summary>
           /// Desc:提现金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal CashAmount {get;set;}

           /// <summary>
           /// Desc:提现帐号
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Account {get;set;}

           /// <summary>
           /// Desc:提现人
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccountName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? SellerId {get;set;}

           /// <summary>
           /// Desc:申请商家用户名
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SellerName {get;set;}

           /// <summary>
           /// Desc:处理时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? DealTime {get;set;}

           /// <summary>
           /// Desc:商家备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopRemark {get;set;}

           /// <summary>
           /// Desc:平台备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PlatRemark {get;set;}

           /// <summary>
           /// Desc:商店名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopName {get;set;}

           /// <summary>
           /// Desc:支付商流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SerialNo {get;set;}

    }
}
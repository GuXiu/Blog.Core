﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///限时购表
    ///</summary>

    [SugarTable("Himall_FlashSale","Zhongjiu")]
    public partial class FlashSale
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Title {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:待审核,进行中,已结束,审核未通过,管理员取消
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Status {get;set;}

           /// <summary>
           /// Desc:活动开始日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime BeginDate {get;set;}

           /// <summary>
           /// Desc:活动结束日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime EndDate {get;set;}

           /// <summary>
           /// Desc:限制每人购买的数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal LimitCountOfThePeople {get;set;}

           /// <summary>
           /// Desc:仅仅只计算在限时购里的销售数
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal SaleCount {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string CategoryName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ImagePath {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal MinPrice {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public long? Sort {get;set;}

           /// <summary>
           /// Desc:创建人级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long ParentId {get;set;}

           /// <summary>
           /// Desc:创建级别，[0:店铺级别][1:市级管理员级别][2:省级管理员级别][3:最高级管理员级别]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CreatorLevel {get;set;}

           /// <summary>
           /// Desc:创建人 管理员Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long CreatorManagerId {get;set;}

           /// <summary>
           /// Desc:省市管理员发布活动，关联店铺Id集合，逗号分隔字符串
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ShopIds {get;set;}

           /// <summary>
           /// Desc:活动范围[0:全部][1:线上活动][2:线下活动]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int AllowType {get;set;}

           /// <summary>
           /// Desc:权限系统[部门Id]
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:限时购总数量
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? StockLimit {get;set;}

           /// <summary>
           /// Desc:配送方式[0:未选择配送方式,1:送货,2自提,3现取]
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int DistributionType {get;set;}

           /// <summary>
           /// Desc:是否在微店店铺首页显示
           /// Default:1
           /// Nullable:False
           /// </summary>           
           public int ShowType {get;set;}

           /// <summary>
           /// Desc:是否显示倒计时
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte EnableCountDown {get;set;}

           /// <summary>
           /// Desc:倒计时时间 单位：分钟
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int CountDownMins {get;set;}

           /// <summary>
           /// Desc:是否加入群聊
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte EnableJoinWXGroupChat {get;set;}

           /// <summary>
           /// Desc:是否开启下单验证
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte PlaceOrderValidation {get;set;}

           /// <summary>
           /// Desc:是否设置活动后立即显示倒计时
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public byte IsNowEnableCountDown {get;set;}

           /// <summary>
           /// Desc:参与条件：-1不限制，0加入群聊，1添加客服
           /// Default:-1
           /// Nullable:False
           /// </summary>           
           public int JoinWxGroupChatType {get;set;}

           /// <summary>
           /// Desc:是否设备限购0-不限，1-限购
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int LimitDevice {get;set;}

           /// <summary>
           /// Desc:是否账号限购0-不限，1-限购
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int LimitPin {get;set;}

           /// <summary>
           /// Desc:限购件数 0-不限，如账号限购、设备限购有一个为1，则限购件数必须大于0的整数
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int LimitCount {get;set;}

           /// <summary>
           /// Desc:-9、不限购， 1、按日限购 0、活动期限购， 2 每单限购
           /// Default:-9
           /// Nullable:False
           /// </summary>           
           public int LimitDaily {get;set;}

           /// <summary>
           /// Desc:外部系统活动Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OutActivityId {get;set;}

           /// <summary>
           /// Desc:广告词
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Advertising {get;set;}

    }
}
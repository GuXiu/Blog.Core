﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///营销补充表
    ///</summary>

    [SugarTable("Himall_MarketSetting","Zhongjiu")]
    public partial class MarketSetting
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int Id {get;set;}

           /// <summary>
           /// Desc:营销类型ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int TypeId {get;set;}

           /// <summary>
           /// Desc:营销使用价格（/月）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Price {get;set;}

    }
}
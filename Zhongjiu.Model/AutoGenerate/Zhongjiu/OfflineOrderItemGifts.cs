﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///线下订单赠品表
    ///</summary>

    [SugarTable("Himall_OfflineOrderItemGifts","Zhongjiu")]
    public partial class OfflineOrderItemGifts
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:订单明细主键ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ItemId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:赠品名称
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string ProductName {get;set;}

           /// <summary>
           /// Desc:赠品数量
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Quantity {get;set;}

           /// <summary>
           /// Desc:赠品来源（1：商品买赠 2：订单满增）
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int GiftType {get;set;}

           /// <summary>
           /// Desc:是否退货[默认0:未退货][1:退货]
           /// Default:b'0'
           /// Nullable:False
           /// </summary>           
           public bool IsReturn {get;set;}

           /// <summary>
           /// Desc:订单ID
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long OrderId {get;set;}

           /// <summary>
           /// Desc:已经退货的数量
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public int ReturnNumber {get;set;}

    }
}
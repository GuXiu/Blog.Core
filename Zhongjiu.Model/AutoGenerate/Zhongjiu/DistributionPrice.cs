﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///配送价表
    ///</summary>

    [SugarTable("ZJ_DistributionPrice","Zhongjiu")]
    public partial class DistributionPrice
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:商品条码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ProductSku {get;set;}

           /// <summary>
           /// Desc:配送等级 1 一级配送；2 二级配送； 3 三级配送
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? Level {get;set;}

           /// <summary>
           /// Desc:含税配送价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? IncludeTaxDistributionPrice {get;set;}

           /// <summary>
           /// Desc:不税配送价
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? NotIncludeTaxDistributionPrice {get;set;}

           /// <summary>
           /// Desc:配送价税额
           /// Default:0.00
           /// Nullable:True
           /// </summary>           
           public decimal? DistributionPriceTax {get;set;}

           /// <summary>
           /// Desc:
           /// Default:CURRENT_TIMESTAMP
           /// Nullable:True
           /// </summary>           
           public DateTime? AddTime {get;set;}

           /// <summary>
           /// Desc:
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public long? Operator {get;set;}

           /// <summary>
           /// Desc:顶级体系Id
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AccountSet {get;set;}

           /// <summary>
           /// Desc:直属上级部门
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DepartmentId {get;set;}

           /// <summary>
           /// Desc:配送中心id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long DistributionId {get;set;}

    }
}
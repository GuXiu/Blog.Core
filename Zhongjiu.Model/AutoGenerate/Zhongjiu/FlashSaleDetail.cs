﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///限时购详细表
    ///</summary>

    [SugarTable("Himall_FlashSaleDetail","Zhongjiu")]
    public partial class FlashSaleDetail
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ProductId {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string SkuId {get;set;}

           /// <summary>
           /// Desc:限时购时金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Price {get;set;}

           /// <summary>
           /// Desc:对应FlashSale表主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long FlashSaleId {get;set;}

    }
}
﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///会员提现申请表
    ///</summary>

    [SugarTable("Himall_ApplyWithDraw","Zhongjiu")]
    public partial class ApplyWithDraw
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:会员ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long MemId {get;set;}

           /// <summary>
           /// Desc:微信昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string NickName {get;set;}

           /// <summary>
           /// Desc:OpenId
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenId {get;set;}

           /// <summary>
           /// Desc:申请状态[1:待处理][2:付款失败][3:提现成功][4:已拒绝]
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int ApplyStatus {get;set;}

           /// <summary>
           /// Desc:提现金额
           /// Default:0.00
           /// Nullable:False
           /// </summary>           
           public decimal ApplyAmount {get;set;}

           /// <summary>
           /// Desc:申请时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime ApplyTime {get;set;}

           /// <summary>
           /// Desc:处理时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? ConfirmTime {get;set;}

           /// <summary>
           /// Desc:付款时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PayTime {get;set;}

           /// <summary>
           /// Desc:付款流水号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayNo {get;set;}

           /// <summary>
           /// Desc:操作人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpUser {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:提现类型 0 用户余额，1 分销佣金 2 拼团返现
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public int? ApplyWithDrawType {get;set;}

           /// <summary>
           /// Desc:来源订单Id
           /// Default:0
           /// Nullable:False
           /// </summary>           
           public long SourceData {get;set;}

           /// <summary>
           /// Desc:推送的订单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PushOrder {get;set;}

    }
}
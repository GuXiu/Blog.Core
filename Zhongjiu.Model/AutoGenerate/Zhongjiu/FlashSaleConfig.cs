﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///限时购预热时间设置表
    ///</summary>

    [SugarTable("Himall_FlashSaleConfig","Zhongjiu")]
    public partial class FlashSaleConfig
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:预热时间
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int Preheat {get;set;}

           /// <summary>
           /// Desc:是否允许正常购买
           /// Default:
           /// Nullable:False
           /// </summary>           
           public byte IsNormalPurchase {get;set;}

    }
}
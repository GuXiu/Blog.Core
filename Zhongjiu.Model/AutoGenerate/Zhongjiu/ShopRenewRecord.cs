﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///店铺续费记录表
    ///</summary>

    [SugarTable("Himall_ShopRenewRecord","Zhongjiu")]
    public partial class ShopRenewRecord
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:店铺ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           public long ShopId {get;set;}

           /// <summary>
           /// Desc:操作者
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string Operator {get;set;}

           /// <summary>
           /// Desc:操作日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime OperateDate {get;set;}

           /// <summary>
           /// Desc:操作明细
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OperateContent {get;set;}

           /// <summary>
           /// Desc:类型
           /// Default:
           /// Nullable:False
           /// </summary>           
           public int OperateType {get;set;}

           /// <summary>
           /// Desc:支付金额
           /// Default:
           /// Nullable:False
           /// </summary>           
           public decimal Amount {get;set;}

    }
}
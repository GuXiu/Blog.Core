﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Zhongjiu.Model.Zhongjiu
{
    ///<summary>
    ///微信TOKEN表
    ///</summary>

    [SugarTable("Himall_WXAccToken","Zhongjiu")]
    public partial class WXAccToken
    {
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public long Id {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string AppId {get;set;}

           /// <summary>
           /// Desc:微信访问令牌
           /// Default:
           /// Nullable:False
           /// </summary>           
           public string AccessToken {get;set;}

           /// <summary>
           /// Desc:微信令牌过期日期
           /// Default:
           /// Nullable:False
           /// </summary>           
           public DateTime TokenOutTime {get;set;}

    }
}
using SqlSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Zhongjiu.Model.Seed
{
  [Obsolete("作废，使用Zhongjiu.Common.DbFirst.SqlSugarDbFirst")]
  public class FrameSeed
  {

    /// <summary>
    /// 生成Controller层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <param name="isMuti"></param>
    /// <returns></returns>
    public static bool CreateControllers(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_Controller_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.Api", tableNames, "", isMuti);
      return true;
    }

    /// <summary>
    /// 生成Model层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="path">生成路径</param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <param name="isMuti"></param>
    /// <returns></returns>
    public static bool CreateModels(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_Model_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.Model", tableNames, "", isMuti);
      return true;
    }

    /// <summary>
    /// 生成IRepository层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="path">生成路径</param>
    /// <param name="isMuti"></param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <returns></returns>
    public static bool CreateIRepositorys(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_IRepository_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.IRepository", tableNames, "", isMuti);
      return true;
    }



    /// <summary>
    /// 生成 IService 层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="isMuti"></param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <returns></returns>
    public static bool CreateIServices(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_IServices_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.IServices", tableNames, "", isMuti);
      return true;
    }



    /// <summary>
    /// 生成 Repository 层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="path">生成路径</param>
    /// <param name="isMuti"></param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <returns></returns>
    public static bool CreateRepository(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_Repository_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.Repository", tableNames, "", isMuti);
      return true;
    }



    /// <summary>
    /// 生成 Service 层
    /// </summary>
    /// <param name="sqlSugarClient">sqlsugar实例</param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="isMuti"></param>
    /// <param name="tableNames">数据库表名数组，默认空，生成所有表</param>
    /// <returns></returns>
    public static bool CreateServices(SqlSugarScope sqlSugarClient, string ConnId, string path, bool isMuti = false, string[] tableNames = null)
    {
      Create_Services_ClassFileByDBTalbe(sqlSugarClient, ConnId, path, "Zhongjiu.Services", tableNames, "", isMuti);
      return true;
    }


    #region 根据数据库表生产Controller层

    /// <summary>
    /// 功能描述:根据数据库表生产Controller层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    /// <param name="blnSerializable">是否序列化</param>
    private static void Create_Controller_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false,
      bool blnSerializable = false)
    {
      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);/*实体名大写*/
        sqlSugarClient.MappingTables.Add(entityName, item.Name);
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))
        {
          if (col.DbColumnName == entityName)
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
          //sqlSugarClient.MappingColumns.Add(col.DbColumnName.ToUpper() /*类的属性大写*/, col.DbColumnName, entityName);
        }
      }
      var ls = IDbFirst.IsCreateDefaultValue().IsCreateAttribute()

           .SettingClassTemplate(p => p =
@"using Zhongjiu.IServices;
using Zhongjiu.Model;using Zhongjiu.Model.Models;using Zhongjiu.Model.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace " + strNameSpace + @"
{
	[Route(""api/[controller]/[action]"")]
	[ApiController]
    [Authorize(Permissions.Name)]
     public class {ClassName}Controller : ControllerBase
    {
            /// <summary>
            /// 服务器接口，因为是模板生成，所以首字母是大写的，自己可以重构下
            /// </summary>
            private readonly I{ClassName}Services _{ClassName}Services;
    
            public {ClassName}Controller(I{ClassName}Services {ClassName}Services)
            {
                _{ClassName}Services = {ClassName}Services;
            }
    
            [HttpGet]
            public async Task<MessageModel<PageModel<{ClassName}>>> Get(int page = 1, string key = """",int intPageSize = 50)
            {
                if (string.IsNullOrEmpty(key) || string.IsNullOrWhiteSpace(key))
                {
                    key = """";
                }
    
                Expression<Func<{ClassName}, bool>> whereExpression = a => true;
    
                return new MessageModel<PageModel<{ClassName}>>()
                {
                    msg = ""获取成功"",
                    success = true,
                    response = await _{ClassName}Services.QueryPage(whereExpression, page, intPageSize)
                };

            }

            [HttpGet(""{id}"")]
            public async Task<MessageModel<{ClassName}>> Get(string id)
            {
                return new MessageModel<{ClassName}>()
                {
                    msg = ""获取成功"",
                    success = true,
                    response = await _{ClassName}Services.QueryById(id)
                };
            }

            [HttpPost]
            public async Task<MessageModel<string>> Post([FromBody] {ClassName} request)
            {
                var data = new MessageModel<string>();

                var id = await _{ClassName}Services.Add(request);
                data.success = id > 0;
                if (data.success)
                {
                    data.response = id.ToString();
                    data.msg = ""添加成功"";
                } 

                return data;
            }

            [HttpPut]
            public async Task<MessageModel<string>> Put([FromBody] {ClassName} request)
            {
                var data = new MessageModel<string>();
                data.success = await _{ClassName}Services.Update(request);
                if (data.success)
                {
                    data.msg = ""更新成功"";
                    data.response = request?.id.ToString();
                }

                return data;
            }

            [HttpDelete]
            public async Task<MessageModel<string>> Delete(int id)
            {
                var data = new MessageModel<string>();
                var model = await _{ClassName}Services.QueryById(id);
                model.IsDeleted = true;
                data.success = await _departmentServices.Update(model);
                if (data.success)
                {
                    data.msg = ""删除成功"";
                    data.response = model?.Id.ToString();
                }

                return data;
            }
    }
}")

            .ToClassStringList(strNameSpace);

      Dictionary<string, string> newdic = new Dictionary<string, string>();
      //循环处理 首字母小写 并插入新的 Dictionary
      foreach (KeyValuePair<string, string> item in ls)
      {
        string newkey = "_" + item.Key.First().ToString().ToLower() + item.Key.Substring(1);
        string newvalue = item.Value.Replace("_" + item.Key, newkey);
        newdic.Add(item.Key, newvalue);
      }
      CreateFilesByClassStringList(newdic, strPath, "{0}Controller");
    }
    #endregion


    #region 根据数据库表生产Model层

    /// <summary>
    /// 功能描述:根据数据库表生产Model层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    /// <param name="blnSerializable">是否序列化</param>
    private static void Create_Model_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false,
      bool blnSerializable = false)
    {
        strPath = strPath + @"\" + ConnId;
        strNameSpace = strNameSpace + "." + ConnId;

      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase)||!w.StartsWith("view_"));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())//循环表
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);
        sqlSugarClient.MappingTables.Add(entityName, $"`{item.Name}`");//表名、对象名映射关系
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))//循环表字段
        {
          if (col.DbColumnName == entityName)//表名与字段名相同时修改映射
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
        }
      }
      //var defaultT=RazorFirst.DefaultRazorClassTemplate;
      var ls = IDbFirst.IsCreateAttribute()/*.IsCreateDefaultValue()*/
            //.SettingNamespaceTemplate(p =>
            //{
            //    return "using System;\r\nusing System.Linq;\r\nusing System.Text;\r\n";
            //})

            .SettingClassTemplate(p =>
            {
              return @"{using}
namespace " + strNameSpace + @"
{
{ClassDescription}
{SugarTable}{TableName}" + (blnSerializable ? "\r\n    [Serializable]" : "") + @"
    public partial class {ClassName}" + (string.IsNullOrEmpty(strInterface) ? "" : (" : " + strInterface)) + @"
    {
{PropertyName}
    }
}";
            })
            //.SettingPropertyDescriptionTemplate(p => p)
            .SettingPropertyTemplate((columns,temp,type) => {
              return temp;// @"\r\n{SugarColumn}\r\n public {PropertyType} {PropertyName} { get; set; }\r\n";
            })

             //.SettingConstructorTemplate(p => p = "              this._{PropertyName} ={DefaultValue};")

             .ToClassStringList(strNameSpace);
      CreateFilesByClassStringList(ls, strPath, "{0}");
    }
    #endregion


    #region 根据数据库表生产IRepository层

    /// <summary>
    /// 功能描述:根据数据库表生产IRepository层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    private static void Create_IRepository_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false
        )
    {
      //多库文件分离
      //if (isMuti)
      //{
        strPath = strPath + @"\" + ConnId;
        strNameSpace = strNameSpace + "." + ConnId;
      //}

      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase) || !w.StartsWith("view_"));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);/*实体名大写*/
        sqlSugarClient.MappingTables.Add(entityName, item.Name);
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))
        {
          if (col.DbColumnName == entityName)
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
          //sqlSugarClient.MappingColumns.Add(col.DbColumnName.ToUpper() /*类的属性大写*/, col.DbColumnName, entityName);
        }
      }
      var ls = IDbFirst.IsCreateAttribute()//.IsCreateDefaultValue()

           .SettingClassTemplate(p => p =
@$"using Zhongjiu.Model.{ConnId};
namespace " + strNameSpace + @"
{
{ClassDescription}
  public partial interface I{ClassName}Repository : IBaseRepository<{ClassName}>" + (string.IsNullOrEmpty(strInterface) ? "" : (" , " + strInterface)) + @"
  {
  }
}")

            .ToClassStringList(strNameSpace);
      CreateFilesByClassStringList(ls, strPath, "I{0}Repository");
    }
    #endregion



    #region 根据数据库表生产 Repository 层

    /// <summary>
    /// 功能描述:根据数据库表生产 Repository 层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    private static void Create_Repository_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false)
    {
      //多库文件分离
      //if (isMuti)
      //{
        strPath = strPath + @"\" + ConnId;
        strNameSpace = strNameSpace + "." + ConnId;
      //}

      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase) || !w.StartsWith("view_"));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);/*实体名大写*/
        sqlSugarClient.MappingTables.Add(entityName, item.Name);
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))
        {
          if (col.DbColumnName == entityName)
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
          //sqlSugarClient.MappingColumns.Add(col.DbColumnName.ToUpper() /*类的属性大写*/, col.DbColumnName, entityName);
        }
      }
      var ls = IDbFirst.IsCreateAttribute()//.IsCreateDefaultValue()//

            .SettingClassTemplate(p => p =
@$"using Zhongjiu.Model.{ConnId};
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.{ConnId};

namespace " + strNameSpace + @"
{
{ClassDescription}
  public partial class {ClassName}Repository : BaseRepository<{ClassName}>, I{ClassName}Repository" + (string.IsNullOrEmpty(strInterface) ? "" : (" , " + strInterface)) + @"
  {
    public {ClassName}Repository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}")
            .ToClassStringList(strNameSpace);


      CreateFilesByClassStringList(ls, strPath, "{0}Repository");
    }
    #endregion


    #region 根据数据库表生产IServices层

    /// <summary>
    /// 功能描述:根据数据库表生产IServices层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    private static void Create_IServices_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false)
    {
      //多库文件分离
      //if (isMuti)
      //{
        strPath = strPath + @"\" + ConnId;
        strNameSpace = strNameSpace + "." + ConnId;
      //}

      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase) || !w.StartsWith("view_"));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);/*实体名大写*/
        sqlSugarClient.MappingTables.Add(entityName, item.Name);
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))
        {
          if (col.DbColumnName == entityName)
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
          //sqlSugarClient.MappingColumns.Add(col.DbColumnName.ToUpper() /*类的属性大写*/, col.DbColumnName, entityName);
        }
      }
      var ls = IDbFirst.IsCreateDefaultValue().IsCreateAttribute()

            .SettingClassTemplate(p => p =
@$"using Zhongjiu.Model;
using Zhongjiu.Model.{ConnId};

namespace " + strNameSpace + @"
{
{ClassDescription}
  public partial interface I{ClassName}Services :IBaseServices<{ClassName}>" + (string.IsNullOrEmpty(strInterface) ? "" : (" , " + strInterface)) + @"
	  {
    }
}")

             .ToClassStringList(strNameSpace);
      CreateFilesByClassStringList(ls, strPath, "I{0}Services");
    }
    #endregion


    #region 根据数据库表生产 Services 层

    /// <summary>
    /// 功能描述:根据数据库表生产 Services 层
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <param name="sqlSugarClient"></param>
    /// <param name="ConnId">数据库链接ID</param>
    /// <param name="strPath">实体类存放路径</param>
    /// <param name="strNameSpace">命名空间</param>
    /// <param name="lstTableNames">生产指定的表</param>
    /// <param name="strInterface">实现接口</param>
    /// <param name="isMuti"></param>
    private static void Create_Services_ClassFileByDBTalbe(
      SqlSugarScope sqlSugarClient,
      string ConnId,
      string strPath,
      string strNameSpace,
      string[] lstTableNames,
      string strInterface,
      bool isMuti = false)
    {
      //多库文件分离
      //if (isMuti)
      //{
        strPath = strPath + @"\" + ConnId;
        strNameSpace = strNameSpace + "." + ConnId;
      //}

      var IDbFirst = sqlSugarClient.DbFirst;
      if (lstTableNames != null && lstTableNames.Length > 0)
      {
        IDbFirst = IDbFirst.Where(lstTableNames);
      }
      IDbFirst = IDbFirst.Where(w => w.StartsWith("ZJ_", StringComparison.OrdinalIgnoreCase) || w.StartsWith("Himall_", StringComparison.OrdinalIgnoreCase) || !w.StartsWith("view_"));
      foreach (var item in sqlSugarClient.DbMaintenance.GetTableInfoList())
      {
        string entityName = item.Name.Replace("Himall_", "", true, null).Replace("ZJ_", "", true, null);/*实体名大写*/
        sqlSugarClient.MappingTables.Add(entityName, item.Name);
        foreach (var col in sqlSugarClient.DbMaintenance.GetColumnInfosByTableName(item.Name))
        {
          if (col.DbColumnName == entityName)
          {
            sqlSugarClient.MappingColumns.Add(col.DbColumnName + "1" /*属性名+1*/, col.DbColumnName, entityName);
          }
          //sqlSugarClient.MappingColumns.Add(col.DbColumnName.ToUpper() /*类的属性大写*/, col.DbColumnName, entityName);
        }
      }
      var ls = IDbFirst.IsCreateAttribute()//.IsCreateDefaultValue()

            .SettingClassTemplate(p => p =
@$"
using Zhongjiu.Model;
using Zhongjiu.Model.{ConnId};
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.{ConnId};
using Zhongjiu.IServices;
using Zhongjiu.IServices.{ConnId};

namespace " + strNameSpace + @"
{
  {ClassDescription}
  public partial class {ClassName}Services : BaseServices<{ClassName}>, I{ClassName}Services" + (string.IsNullOrEmpty(strInterface) ? "" : (" , " + strInterface)) + @"
  {        
    I{ClassName}Repository Rep;
    public {ClassName}Services(I{ClassName}Repository repository)
    {
        Rep=repository;
    }
  }
}")
            .ToClassStringList(strNameSpace);

      CreateFilesByClassStringList(ls, strPath, "{0}Services");
    }
    #endregion


    #region 根据模板内容批量生成文件
    /// <summary>
    /// 根据模板内容批量生成文件
    /// </summary>
    /// <param name="ls">类文件字符串list</param>
    /// <param name="strPath">生成路径</param>
    /// <param name="fileNameTp">文件名格式模板</param>
    private static void CreateFilesByClassStringList(Dictionary<string, string> ls, string strPath, string fileNameTp)
    {

      foreach (var item in ls)
      {
        var fileName = $"{string.Format(fileNameTp, item.Key)}.cs";
        var fileFullPath = Path.Combine(strPath, fileName);
        if (!Directory.Exists(strPath))
        {
          Directory.CreateDirectory(strPath);
        }
        File.WriteAllText(fileFullPath, item.Value, Encoding.UTF8);
      }
    }
    #endregion
  }
}

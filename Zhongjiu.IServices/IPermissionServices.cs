﻿using Zhongjiu.Model;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;

namespace Zhongjiu.IServices
{
    public partial interface IPermissionService : IBaseServices<Permission>
    {
    }
}
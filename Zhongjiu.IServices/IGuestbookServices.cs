﻿using System.Threading.Tasks;
using Zhongjiu.Model;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.IServices
{
  public partial interface IGuestbookService : IBaseServices<Guestbook>
    {
        Task<MessageModel<string>> TestTranInRepository();
        Task<bool> TestTranInRepositoryAOP();
    }
}

using System.Threading.Tasks;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.IServices
{
  /// <summary>
  /// UserRoleServices
  /// </summary>	
  public partial interface IUserRoleServices :IBaseServices<UserRole>
	{

        Task<UserRole> SaveUserRole(int uid, int rid);
        Task<int> GetRoleIdByUid(int uid);
    }
}


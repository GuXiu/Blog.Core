﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Model;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;

namespace Zhongjiu.IServices
{
    public partial interface IBlogArticleServices :IBaseServices<BlogArticle>
    {
        Task<List<BlogArticle>> GetBlogs();
        Task<BlogViewModels> GetBlogDetails(int id);

    }

}

using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;

namespace Zhongjiu.IServices
{
  /// <summary>
  /// RoleModulePermissionServices
  /// </summary>	
  public partial interface IRoleModulePermissionService :IBaseServices<RoleModulePermission>
	{

        Task<List<RoleModulePermission>> GetRoleModule();
        Task<List<TestMuchTableResult>> QueryMuchTable();
        Task<List<RoleModulePermission>> RoleModuleMaps();
        Task<List<RoleModulePermission>> GetRMPMaps();
        /// <summary>
        /// �������²˵���ӿڵĹ�ϵ
        /// </summary>
        /// <param name="permissionId">�˵�����</param>
        /// <param name="moduleId">�ӿ�����</param>
        /// <returns></returns>
        Task UpdateModuleId(int permissionId, int moduleId);
    }
}

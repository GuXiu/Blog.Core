﻿using Zhongjiu.Model.Models;

namespace Zhongjiu.IServices
{
    /// <summary>
    /// IDepartmentServices
    /// </summary>	
    public partial interface IDepartmentServices : IBaseServices<Department>
    {
    }
}
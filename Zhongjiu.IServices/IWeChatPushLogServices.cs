using Zhongjiu.Model.Models;

namespace Zhongjiu.IServices
{
  /// <summary>
  /// IWeChatPushLogServices
  /// </summary>	
  public interface IWeChatPushLogServices : IBaseServices<WeChatPushLog>
	{
		
	}
}
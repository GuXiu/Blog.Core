using System.Threading.Tasks;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.IServices
{
  /// <summary>
  /// SysUserInfoServices
  /// </summary>	
  public partial interface ISysUserInfoServices :IBaseServices<SysUserInfo>
	{
        Task<SysUserInfo> SaveUserInfo(string loginName, string loginPwd);
        Task<string> GetUserRoleNameStr(string loginName, string loginPwd);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.IServices
{
  public partial interface ITopicService : IBaseServices<Topic>
    {
        Task<List<Topic>> GetTopics();
    }
}

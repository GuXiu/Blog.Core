﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.IServices
{
  public partial interface ITopicDetailService : IBaseServices<TopicDetail>
    {
        Task<List<TopicDetail>> GetTopicDetails();
    }
}

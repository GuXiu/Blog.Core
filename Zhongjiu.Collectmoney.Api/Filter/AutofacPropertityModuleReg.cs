﻿using Autofac;
using Microsoft.AspNetCore.Mvc;
using Zhongjiu.Services;

namespace Zhongjiu.CollectMoney.Api
{
  public class AutofacPropertityModuleReg : Autofac.Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      var controllerBaseType = typeof(ControllerBase);
      builder.RegisterAssemblyTypes(typeof(Program).Assembly)
          .Where(t => controllerBaseType.IsAssignableFrom(t) && t != controllerBaseType)
          .PropertiesAutowired()//
                                //.Where(w => w.Name.EndsWith("Services"))
                                //.PropertiesAutowired();
                                //.WithProperty("Rep",);
                ;//&&w.BaseType.Equals(typeof(BaseServices<>))


    }
  }
}

﻿
// 以下为asp.net 6.0的写法，如果用5.0，请看Program.five.cs文件
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Text;
using Zhongjiu;
using Zhongjiu.Common;
using Zhongjiu.Extensions;
using Zhongjiu.Filter;
using Zhongjiu.Hubs;
using Zhongjiu.IServices;
using Zhongjiu.Model.Seed;
using Zhongjiu.Tasks;

var builder = WebApplication.CreateBuilder(args);

// 1、配置host与容器
builder.Host
.UseServiceProviderFactory(new AutofacServiceProviderFactory())
.ConfigureContainer<ContainerBuilder>((context, cb) =>
{
  cb.RegisterModule(new AutofacModuleRegister(builder.Services.BuildServiceProvider().GetRequiredService<ILoggerFactory>()));
  cb.RegisterModule<Zhongjiu.CollectMoney.Api.AutofacPropertityModuleReg>();
})
.ConfigureAppConfiguration((hostingContext, config) =>
{
  //config.Sources.Clear();
  //config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: false);
  config.AddConfigurationApollo("appsettings.apollo.json");
  HostBuilderExtension.ConfigureAppConfiguration(hostingContext, config);
})
.ConfigureLogging((hostingContext, builder) =>
{
  builder.AddFilter("System", LogLevel.Error);
  builder.AddFilter("Microsoft", LogLevel.Error);
  builder.SetMinimumLevel(LogLevel.Error);
  //builder.AddLog4Net(Path.Combine(Directory.GetCurrentDirectory(), "Log4net.config"));
  //builder.AddSeq("http://8.141.61.134:8888", "dFYPJqKNyk0HoXATUUqS");
  builder.Services.AddSerilogSetup();//通过serilog保存到seq，可以添加属性；必要时方便切换到其它途径保存，eg：文件,数据库...
});


//// 2、配置服务
////builder.Services.AddSingleton(new Appsettings(builder.Configuration));
builder.Services.AddSingleton(new LogLock(builder.Environment.ContentRootPath));
builder.Services.AddUiFilesZipSetup(builder.Environment);

Permissions.IsUseIds4 = Appsettings.app(new string[] { "Startup", "IdentityServer4", "Enabled" }).ToBool();
RoutePrefix.Name = Appsettings.app(new string[] { "AppSettings", "SvcName" }).ToString();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

builder.Services.AddMemoryCacheSetup();
builder.Services.AddRedisCacheSetup();
builder.Services.AddSqlsugarSetup();
builder.Services.AddDbSetup();
builder.Services.AddAutoMapperSetup();
builder.Services.AddCorsSetup();
builder.Services.AddMiniProfilerSetup();
builder.Services.AddSwaggerSetup();
builder.Services.AddJobSetup();
builder.Services.AddHttpContextSetup();
builder.Services.AddAppTableConfigSetup(builder.Environment);
builder.Services.AddHttpApi();
builder.Services.AddRedisInitMqSetup();
builder.Services.AddRabbitMQSetup();
builder.Services.AddKafkaSetup();
builder.Services.AddEventBusSetup();

builder.Services.AddHostedService<NacosListenConfigurationTask>();
builder.Services.AddNacosSetup();

builder.Services.AddAuthorizationSetup();
if (Permissions.IsUseIds4)
{
  builder.Services.AddAuthentication_Ids4Setup();
}
else
{
  builder.Services.AddAuthentication_JWTSetup();
}

builder.Services.AddIpPolicyRateLimitSetup();
builder.Services.AddSignalR().AddNewtonsoftJsonProtocol();
builder.Services.AddScoped<UseServiceDIAttribute>();
builder.Services.Configure<KestrelServerOptions>(x => x.AllowSynchronousIO = true)
        .Configure<IISServerOptions>(x => x.AllowSynchronousIO = true);

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession();
builder.Services.AddHttpPollySetup();
builder.Services.AddControllers(o =>
{
  o.Filters.Add(typeof(GlobalExceptionsFilter));
  //o.Conventions.Insert(0, new GlobalRouteAuthorizeConvention());
  o.Conventions.Insert(0, new GlobalRoutePrefixFilter(new RouteAttribute(RoutePrefix.Name)));
})
.AddNewtonsoftJson(options =>
{
  options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
  options.SerializerSettings.ContractResolver = new DefaultContractResolver();
  options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
  //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
  options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
  options.SerializerSettings.Converters.Add(new StringEnumConverter());
});
builder.Services.AddEndpointsApiExplorer();

builder.Services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

var url = $"http://{Appsettings.app("host")}:{Appsettings.app("port")}";
builder.WebHost.UseUrls(url);

// 3、配置中间件
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
  app.UseDeveloperExceptionPage();
}
else
{
  app.UseExceptionHandler("/Error").UseExceptionHandlerMiddle();
  //app.UseHsts();
}

app.UseIpLimitMiddle(app.Services.GetService<ILoggerFactory>());
app.UseRequestResponseLogMiddle();
app.UseRecordAccessLogsMiddle();
app.UseSignalRSendMiddle();
app.UseIpLogMiddle();
app.UseAllServicesMiddle(builder.Services);

app.UseSession();
app.UseSwaggerAuthorized();
app.UseSwaggerMiddle(() => Assembly.GetExecutingAssembly().GetManifestResourceStream("Zhongjiu.CollectMoney.Api.index.html"));

app.UseCors(Appsettings.app(new string[] { "Startup", "Cors", "PolicyName" }));
DefaultFilesOptions defaultFilesOptions = new DefaultFilesOptions();
defaultFilesOptions.DefaultFileNames.Clear();
defaultFilesOptions.DefaultFileNames.Add("index.html");
app.UseDefaultFiles(defaultFilesOptions);
app.UseStaticFiles();
app.UseCookiePolicy();
app.UseStatusCodePages();
app.UseRouting();

if (builder.Configuration.GetValue<bool>("AppSettings:UseLoadTest"))
{
  app.UseMiddleware<ByPassAuthMiddleware>();
}
app.UseAuthentication();
app.UseAuthorization();
app.UseMiniProfilerMiddleware();
//app.UseExceptionHandlerMidd();

app.UseEndpoints(endpoints =>
{
  endpoints.MapControllerRoute(
      name: "default",
      pattern: "{controller=Home}/{action=Index}/{id?}");

  endpoints.MapHub<ChatHub>("/api2/chatHub");
});


var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();
var myContext = scope.ServiceProvider.GetRequiredService<MyContext>();
var tasksQzServices = scope.ServiceProvider.GetRequiredService<ITasksQzServices>();
var schedulerCenter = scope.ServiceProvider.GetRequiredService<ISchedulerCenter>();
var lifetime = scope.ServiceProvider.GetRequiredService<IHostApplicationLifetime>();
app.UseSeedDataMiddle(myContext, builder.Environment.WebRootPath);
app.UseQuartzJobMiddleware(tasksQzServices, schedulerCenter);
app.UseConsulRegistMiddle(lifetime);
app.ConfigureEventBus();

// 4、运行
app.Run();

﻿using Microsoft.AspNetCore.Mvc;
using Zhongjiu.Model;

namespace Zhongjiu.CollectMoney.Api
{
  public class BaseApiController : Controller
    {
        [NonAction]
        public MessageModel<T> Success<T>(T data, string msg = "成功")
        {
            return new MessageModel<T>()
            {
                IsSuccess = true,
                Msg = msg,
                Data = data,
            };
        }
        // [NonAction]
        //public MessageModel<T> Success<T>(T data, string msg = "成功",bool success = true)
        //{
        //    return new MessageModel<T>()
        //    {
        //        success = success,
        //        msg = msg,
        //        response = data,
        //    };
        //}
        [NonAction]
        public MessageModel<object> Success(string msg = "成功")
        {
            return new MessageModel<object>()
            {
                IsSuccess = true,
                Msg = msg,
                Data = null,
            };
        }
        [NonAction]
        public MessageModel<string> Failed(string msg = "失败", int status = 500)
        {
            return new MessageModel<string>()
            {
                IsSuccess = false,
                Status = status,
                Msg = msg,
                Data = null,
            };
        }
        [NonAction]
        public MessageModel<T> Failed<T>(string msg = "失败", int status = 500)
        {
            return new MessageModel<T>()
            {
                IsSuccess = false,
                Status = status,
                Msg = msg,
                Data = default,
            };
        }
        [NonAction]
        public MessageModel<PageModel<T>> SuccessPage<T>(int page, int dataCount, int pageSize, List<T> data, int pageCount, string msg = "获取成功")
        {

            return new MessageModel<PageModel<T>>()
            {
                IsSuccess = true,
                Msg = msg,
                Data = new PageModel<T>(page, dataCount, pageSize, data)

            };
        }
        [NonAction]
        public MessageModel<PageModel<T>> SuccessPage<T>(PageModel<T> pageModel, string msg = "获取成功")
        {

            return new MessageModel<PageModel<T>>()
            {
                IsSuccess = true,
                Msg = msg,
                Data = pageModel
            };
        }
    }
}

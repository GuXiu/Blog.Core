﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.CollectMoney.Api
{
  [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Permissions.Name)]
    public class MigrateController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRoleModulePermissionService _roleModulePermissionServices;
        private readonly IUserRoleServices _userRoleServices;
        private readonly IRoleServices _roleServices;
        private readonly IPermissionService _permissionServices;
        private readonly IModuleServices _ModuleServices;
        private readonly IWebHostEnvironment _env;

        public MigrateController(IUnitOfWork unitOfWork,
            IRoleModulePermissionService roleModulePermissionServices,
            IUserRoleServices userRoleServices,
            IRoleServices roleServices,
            IPermissionService permissionServices,
            IModuleServices ModuleServices,
            IWebHostEnvironment env)
        {
            _unitOfWork = unitOfWork;
            _roleModulePermissionServices = roleModulePermissionServices;
            _userRoleServices = userRoleServices;
            _roleServices = roleServices;
            _permissionServices = permissionServices;
            _ModuleServices = ModuleServices;
            _env = env;
        }


        /// <summary>
        /// 获取权限部分Map数据（从库）
        /// 迁移到新库（主库）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<MessageModel<string>> DataMigrateFromOld2New()
        {
            var data = new MessageModel<string>() { IsSuccess = true, Msg = "" };
            var filterPermissionId = 122;
            if (_env.IsDevelopment())
            {
                try
                {
                    var apiList = await _ModuleServices.Query(d => d.IsDeleted == false);
                    var permissionsAllList = await _permissionServices.Query(d => d.IsDeleted == false);
                    var permissions = permissionsAllList.Where(d => d.Pid == 0).ToList();
                    var rmps = await _roleModulePermissionServices.GetRMPMaps();
                    List<PM> pms = new();

                    // 当然，你可以做个where查询
                    rmps = rmps.Where(d => d.PermissionId >= filterPermissionId).ToList();

                    InitPermissionTree(permissions, permissionsAllList, apiList);

                    var actionPermissionIds = permissionsAllList.Where(d => d.Id >= filterPermissionId).Select(d => d.Id).ToList();
                    List<int> filterPermissionIds = new();
                    FilterPermissionTree(permissionsAllList, actionPermissionIds, filterPermissionIds);
                    permissions = permissions.Where(d => filterPermissionIds.Contains(d.Id)).ToList();

                    // 开启事务，保证数据一致性
                    _unitOfWork.BeginTran();

                    // 注意信息的完整性，不要重复添加，确保主库没有要添加的数据

                    // 1、保持菜单和接口
                    await SavePermissionTreeAsync(permissions, pms);

                    var rid = 0;
                    var pid = 0;
                    var mid = 0;
                    var rpmid = 0;

                    // 2、保存关系表
                    foreach (var item in rmps)
                    {
                        // 角色信息，防止重复添加，做了判断
                        if (item.Role != null)
                        {
                            var isExit = (await _roleServices.Query(d => d.Name == item.Role.Name && d.IsDeleted == false)).FirstOrDefault();
                            if (isExit == null)
                            {
                                rid = await _roleServices.Add(item.Role);
                                Console.WriteLine($"Role Added:{item.Role.Name}");
                            }
                            else
                            {
                                rid = isExit.Id;
                            }
                        }

                        pid = (pms.FirstOrDefault(d => d.PidOld == item.PermissionId)?.PidNew).ToInt();
                        mid = (pms.FirstOrDefault(d => d.MidOld == item.ModuleId)?.MidNew).ToInt();
                        // 关系
                        if (rid > 0 && pid > 0)
                        {
                            rpmid = await _roleModulePermissionServices.Add(new RoleModulePermission()
                            {
                                IsDeleted = false,
                                CreateTime = DateTime.Now,
                                ModifyTime = DateTime.Now,
                                ModuleId = mid,
                                PermissionId = pid,
                                RoleId = rid,
                            });
                            Console.WriteLine($"RMP Added:{rpmid}");
                        }

                    }


                    _unitOfWork.CommitTran();

                    data.IsSuccess = true;
                    data.Msg = "导入成功！";
                }
                catch (Exception)
                {
                    _unitOfWork.RollbackTran();

                }
            }
            else
            {
                data.IsSuccess = false;
                data.Msg = "当前不处于开发模式，代码生成不可用！";
            }

            return data;
        }


        /// <summary>
        /// 权限数据库导出tsv
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<MessageModel<string>> SaveData2TsvAsync()
        {
            var data = new MessageModel<string>() { IsSuccess = true, Msg = "" };
            if (_env.IsDevelopment())
            {

                JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };

                // 取出数据，序列化，自己可以处理判空
                var rolesJson = (await _roleServices.Query(d => d.IsDeleted == false)).ToJson();
                FileHelper.WriteFile(Path.Combine(_env.WebRootPath, "BlogCore.Data.json", "Role_New.tsv"), rolesJson, Encoding.UTF8);


                var permissionsJson = (await _permissionServices.Query(d => d.IsDeleted == false)).ToJson();
                FileHelper.WriteFile(Path.Combine(_env.WebRootPath, "BlogCore.Data.json", "Permission_New.tsv"), permissionsJson, Encoding.UTF8);


                var modulesJson = (await _ModuleServices.Query(d => d.IsDeleted == false)).ToJson();
                FileHelper.WriteFile(Path.Combine(_env.WebRootPath, "BlogCore.Data.json", "Modules_New.tsv"), modulesJson, Encoding.UTF8);


                var rmpsJson = (await _roleModulePermissionServices.Query(d => d.IsDeleted == false)).ToJson();
                FileHelper.WriteFile(Path.Combine(_env.WebRootPath, "BlogCore.Data.json", "RoleModulePermission_New.tsv"), rmpsJson, Encoding.UTF8);



                data.IsSuccess = true;
                data.Msg = "生成成功！";
            }
            else
            {
                data.IsSuccess = false;
                data.Msg = "当前不处于开发模式，代码生成不可用！";
            }

            return data;
        }

        private void InitPermissionTree(List<Permission> permissionsTree, List<Permission> all, List<Modules> apis)
        {
            foreach (var item in permissionsTree)
            {
                item.Children = all.Where(d => d.Pid == item.Id).ToList();
                item.Module = apis.FirstOrDefault(d => d.Id == item.Mid);
                InitPermissionTree(item.Children, all, apis);
            }
        }

        private void FilterPermissionTree(List<Permission> permissionsAll, List<int> actionPermissionId, List<int> filterPermissionIds)
        {
            actionPermissionId = actionPermissionId.Distinct().ToList();
            var doneIds = permissionsAll.Where(d => actionPermissionId.Contains(d.Id) && d.Pid == 0).Select(d => d.Id).ToList();
            filterPermissionIds.AddRange(doneIds);

            var hasDoIds = permissionsAll.Where(d => actionPermissionId.Contains(d.Id) && d.Pid != 0).Select(d => d.Pid).ToList();
            if (hasDoIds.Any())
            {
                FilterPermissionTree(permissionsAll, hasDoIds, filterPermissionIds);
            }
        }

        private async Task SavePermissionTreeAsync(List<Permission> permissionsTree, List<PM> pms, int permissionId = 0)
        {
            var parendId = permissionId;

            foreach (var item in permissionsTree)
            {
                PM pm = new PM();
                // 保留原始主键id
                pm.PidOld = item.Id;
                pm.MidOld = (item.Module?.Id).ToInt();

                var mid = 0;
                // 接口
                if (item.Module != null)
                {
                    var moduleModel = (await _ModuleServices.Query(d => d.LinkUrl == item.Module.LinkUrl)).FirstOrDefault();
                    if (moduleModel != null)
                    {
                        mid = moduleModel.Id;
                    }
                    else
                    {
                        mid = await _ModuleServices.Add(item.Module);
                    }
                    pm.MidNew = mid;
                    Console.WriteLine($"Moudle Added:{item.Module.Name}");
                }
                // 菜单
                if (item != null)
                {
                    var permissionModel = (await _permissionServices.Query(d => d.Name == item.Name && d.Pid == item.Pid && d.Mid == item.Mid)).FirstOrDefault();
                    item.Pid = parendId;
                    item.Mid = mid;
                    if (permissionModel != null)
                    {
                        permissionId = permissionModel.Id;
                    }
                    else
                    {
                        permissionId = await _permissionServices.Add(item);
                    }

                    pm.PidNew = permissionId;
                    Console.WriteLine($"Permission Added:{item.Name}");
                }
                pms.Add(pm);

                await SavePermissionTreeAsync(item.Children, pms, permissionId);
            }
        }


    }

    public class PM
    {
        public int PidOld { get; set; }
        public int MidOld { get; set; }
        public int PidNew { get; set; }
        public int MidNew { get; set; }
    }
}
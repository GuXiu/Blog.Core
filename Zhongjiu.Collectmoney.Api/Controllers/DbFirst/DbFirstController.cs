using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using Zhongjiu.Common;
using Zhongjiu.Model;
using Zhongjiu.Model.Seed;

namespace Zhongjiu.CollectMoney.Api
{
  [Route("api/[controller]/[action]")]
  [ApiController]
  //[Authorize(Permissions.Name)]
  public class DbFirstController : ControllerBase
  {
    private readonly SqlSugarScope _sqlSugarClient;
    private readonly IWebHostEnvironment Env;

    /// <summary>
    /// 构造函数
    /// </summary>
    public DbFirstController(ISqlSugarClient sqlSugarClient, IWebHostEnvironment env)
    {
      _sqlSugarClient = sqlSugarClient as SqlSugarScope;
      Env = env;
    }

    /// <summary>
    /// 获取 整体框架 文件
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public MessageModel<string> GetFrameFiles()
    {
      true.TrueThrow("生成期间会报错，使用专门控制台生成代码");
      var data = new MessageModel<string>() { IsSuccess = true, Msg = "" };
      data.Data += @"file path is:C:\my-file\}";
      var isMuti = Appsettings.app(new string[] { "MutiDBEnabled" }).ToBool();

      if (Env.IsDevelopment())
      {
        //data.response += $"Controller层生成：{FrameSeed.CreateControllers(_sqlSugarClient)} || ";
        var rootPath = Env.ContentRootPath.Substring(0, Env.ContentRootPath.LastIndexOf("Zhongjiu.Api"));// Env.ContentRootPath.Substring(0, Env.ContentRootPath.LastIndexOf("\\"));
        BaseDBConfig.MutiConnectionString.allDbs.ToList().ForEach(m =>
        {
          _sqlSugarClient.ChangeDatabase(m.ConnId.ToLower());
          data.Data += $"库{m.ConnId}-Model层生成：{FrameSeed.CreateModels(_sqlSugarClient, m.ConnId, rootPath + "Zhongjiu.Model\\AutoGenerate", isMuti)} || ";
          data.Data += $"库{m.ConnId}-IRepositorys层生成：{FrameSeed.CreateIRepositorys(_sqlSugarClient, m.ConnId, rootPath + "Zhongjiu.IRepository\\AutoGenerate", isMuti)} || ";
          data.Data += $"库{m.ConnId}-Repository层生成：{FrameSeed.CreateRepository(_sqlSugarClient, m.ConnId, rootPath + "Zhongjiu.Repository\\AutoGenerate", isMuti)} || ";
                  //不是每个表都要对应有service；
          //data.response += $"库{m.ConnId}-IServices层生成：{FrameSeed.CreateIServices(_sqlSugarClient, m.ConnId, rootPath + "Zhongjiu.IServices\\AutoGenerate", isMuti)} || ";
          //data.response += $"库{m.ConnId}-Services层生成：{FrameSeed.CreateServices(_sqlSugarClient, m.ConnId, rootPath + "Zhongjiu.Services\\AutoGenerate", isMuti)} || ";
        });

        // 切回主库
        _sqlSugarClient.ChangeDatabase(MainDb.CurrentDbConnId.ToLower());
      }
      else
      {
        data.IsSuccess = false;
        data.Msg = "当前不处于开发模式，代码生成不可用！";
      }

      return data;
    }

    /// <summary>
    /// 根据数据库表名 生成整体框架
    /// 仅针对通过CodeFirst生成表的情况
    /// </summary>
    /// <param name="ConnID">数据库链接名称</param>
    /// <param name="tableNames">需要生成的表名</param>
    /// <returns></returns>
    [HttpPost]
    public MessageModel<string> GetFrameFilesByTableNames([FromBody] string[] tableNames, [FromQuery] string ConnID = null)
    {
      ConnID = ConnID == null ? MainDb.CurrentDbConnId.ToLower() : ConnID;

      var isMuti = Appsettings.app(new string[] { "MutiDBEnabled" }).ToBool();
      var data = new MessageModel<string>() { IsSuccess = true, Msg = "" };
      if (Env.IsDevelopment())
      {
        var rootPath = Env.ContentRootPath.Substring(0, Env.ContentRootPath.LastIndexOf("\\"));
        data.Data += $"库{ConnID}-IRepositorys层生成：{FrameSeed.CreateIRepositorys(_sqlSugarClient, ConnID, rootPath + "\\Zhongjiu.IRepository\\AutoGenerate", isMuti, tableNames)} || ";
        data.Data += $"库{ConnID}-IServices层生成：{FrameSeed.CreateIServices(_sqlSugarClient, ConnID, rootPath + "\\Zhongjiu.IServices\\AutoGenerate", isMuti, tableNames)} || ";
        data.Data += $"库{ConnID}-Repository层生成：{FrameSeed.CreateRepository(_sqlSugarClient, ConnID, rootPath + "\\Zhongjiu.Repository\\AutoGenerate", isMuti, tableNames)} || ";
        data.Data += $"库{ConnID}-Services层生成：{FrameSeed.CreateServices(_sqlSugarClient, ConnID, rootPath + "\\Zhongjiu.Services\\AutoGenerate", isMuti, tableNames)} || ";
      }
      else
      {
        data.IsSuccess = false;
        data.Msg = "当前不处于开发模式，代码生成不可用！";
      }

      return data;
    }

    /// <summary>
    /// DbFrist 根据数据库表名 生成整体框架,包含Model层
    /// </summary>
    /// <param name="ConnID">数据库链接名称</param>
    /// <param name="tableNames">需要生成的表名</param>
    /// <returns></returns>
    [HttpPost]
    public MessageModel<string> GetAllFrameFilesByTableNames([FromBody] string[] tableNames, [FromQuery] string ConnID = null)
    {
      ConnID = ConnID == null ? MainDb.CurrentDbConnId.ToLower() : ConnID;

      var isMuti = Appsettings.app(new string[] { "MutiDBEnabled" }).ToBool();
      var data = new MessageModel<string>() { IsSuccess = true, Msg = "" };
      if (Env.IsDevelopment())
      {
        var rootPath = Env.ContentRootPath.Substring(0, Env.ContentRootPath.LastIndexOf("\\"));
        _sqlSugarClient.ChangeDatabase(ConnID.ToLower());
        //data.response += $"Controller层生成：{FrameSeed.CreateControllers(_sqlSugarClient, ConnID, rootPath+ "\\Zhongjiu.Model", isMuti, tableNames)} || ";
        data.Data += $"库{ConnID}-Model层生成：{FrameSeed.CreateModels(_sqlSugarClient, ConnID, rootPath + "\\Zhongjiu.Model\\AutoGenerate", isMuti, tableNames)} || ";
        //data.response += $"库{ConnID}-IRepositorys层生成：{FrameSeed.CreateIRepositorys(_sqlSugarClient, ConnID, isMuti, tableNames)} || ";
        //data.response += $"库{ConnID}-IServices层生成：{FrameSeed.CreateIServices(_sqlSugarClient, ConnID, isMuti, tableNames)} || ";
        //data.response += $"库{ConnID}-Repository层生成：{FrameSeed.CreateRepository(_sqlSugarClient, ConnID, isMuti, tableNames)} || ";
        //data.response += $"库{ConnID}-Services层生成：{FrameSeed.CreateServices(_sqlSugarClient, ConnID, isMuti, tableNames)} || ";
        // 切回主库
        _sqlSugarClient.ChangeDatabase(MainDb.CurrentDbConnId.ToLower());
      }
      else
      {
        data.IsSuccess = false;
        data.Data = "当前不处于开发模式，代码生成不可用！";
      }

      return data;
    }


  }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;

namespace Zhongjiu.IRepository.Blog
{	
	/// <summary>
	/// IRoleModulePermissionRepository
	/// </summary>	
	public partial interface IRoleModulePermissionRepository : IBaseRepository<RoleModulePermission>
    {
        Task<List<TestMuchTableResult>> QueryMuchTable();
        Task<List<RoleModulePermission>> RoleModuleMaps();
        Task<List<RoleModulePermission>> GetRMPMaps();
        /// <summary>
        /// 批量更新菜单与接口的关系
        /// </summary>
        /// <param name="permissionId">菜单主键</param>
        /// <param name="moduleId">接口主键</param>
        /// <returns></returns>
        Task UpdateModuleId(int permissionId, int moduleId);
    }
}
	
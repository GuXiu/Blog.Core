﻿using Zhongjiu.Model.Blog;
namespace Zhongjiu.IRepository.Blog
{
    ///<summary>
    ///WMBLOG_MSSQL_2
    ///</summary>
  public partial interface IPasswordLibRepository : IBaseRepository<PasswordLib>
  {
  }
}
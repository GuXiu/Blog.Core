﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员卡充值记录表ChargeDetailId
    ///</summary>
  public partial interface IOfflineRechargeHistoryRepository : IBaseRepository<OfflineRechargeHistory>
  {
  }
}
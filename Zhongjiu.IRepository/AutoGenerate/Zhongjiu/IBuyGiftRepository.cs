﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///买赠
    ///</summary>
  public partial interface IBuyGiftRepository : IBaseRepository<BuyGift>
  {
  }
}
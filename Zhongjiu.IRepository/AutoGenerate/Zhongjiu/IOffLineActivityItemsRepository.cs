﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下活动明细表（弃用）
    ///</summary>
  public partial interface IOffLineActivityItemsRepository : IBaseRepository<OffLineActivityItems>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单物流信息表
    ///</summary>
  public partial interface IOrderExpressDataRepository : IBaseRepository<OrderExpressData>
  {
  }
}
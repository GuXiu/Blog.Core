﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///积分规则表
    ///</summary>
  public partial interface IMemberIntegralRuleRepository : IBaseRepository<MemberIntegralRule>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员资金余额表
    ///</summary>
  public partial interface ICapitalRepository : IBaseRepository<Capital>
  {
  }
}
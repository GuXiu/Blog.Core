﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///营销补充表
    ///</summary>
  public partial interface IMarketSettingRepository : IBaseRepository<MarketSetting>
  {
  }
}
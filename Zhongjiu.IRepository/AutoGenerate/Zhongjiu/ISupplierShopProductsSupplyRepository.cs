﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///供应商门店商品供货关系表
    ///</summary>
  public partial interface ISupplierShopProductsSupplyRepository : IBaseRepository<SupplierShopProductsSupply>
  {
  }
}
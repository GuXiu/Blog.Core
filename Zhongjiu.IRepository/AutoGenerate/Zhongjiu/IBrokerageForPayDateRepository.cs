﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///按订单支付时间汇总订单明细(分销商品)
    ///</summary>
  public partial interface IBrokerageForPayDateRepository : IBaseRepository<BrokerageForPayDate>
  {
  }
}
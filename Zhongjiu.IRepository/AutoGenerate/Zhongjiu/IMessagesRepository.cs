﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系短信模板表
    ///</summary>
  public partial interface IMessagesRepository : IBaseRepository<Messages>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录表
    ///</summary>
  public partial interface ISendMessageRecordRepository : IBaseRepository<SendMessageRecord>
  {
  }
}
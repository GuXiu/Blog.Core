﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销表
    ///</summary>
  public partial interface IMembershipMarketingRepository : IBaseRepository<MembershipMarketing>
  {
  }
}
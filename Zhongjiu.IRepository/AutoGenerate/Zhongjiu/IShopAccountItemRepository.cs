﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺资金流水表
    ///</summary>
  public partial interface IShopAccountItemRepository : IBaseRepository<ShopAccountItem>
  {
  }
}
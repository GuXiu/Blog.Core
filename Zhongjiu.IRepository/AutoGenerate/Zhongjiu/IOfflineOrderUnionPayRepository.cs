﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单联合支付信息
    ///</summary>
  public partial interface IOfflineOrderUnionPayRepository : IBaseRepository<OfflineOrderUnionPay>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///自动回复设置表
    ///</summary>
  public partial interface IAutoReplyRepository : IBaseRepository<AutoReply>
  {
  }
}
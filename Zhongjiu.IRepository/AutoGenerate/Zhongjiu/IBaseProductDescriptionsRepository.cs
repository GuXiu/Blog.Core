﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///基库商品详情表
    ///</summary>
  public partial interface IBaseProductDescriptionsRepository : IBaseRepository<BaseProductDescriptions>
  {
  }
}
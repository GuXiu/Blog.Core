﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分类品牌关联文章表
    ///</summary>
  public partial interface IBrandArticleClassifyRepository : IBaseRepository<BrandArticleClassify>
  {
  }
}
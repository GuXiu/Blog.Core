﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送价表
    ///</summary>
  public partial interface IDistributionPriceRepository : IBaseRepository<DistributionPrice>
  {
  }
}
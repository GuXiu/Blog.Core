﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///第三方平台与商城用户绑定关系表
    ///</summary>
  public partial interface IMemberBindRepository : IBaseRepository<MemberBind>
  {
  }
}
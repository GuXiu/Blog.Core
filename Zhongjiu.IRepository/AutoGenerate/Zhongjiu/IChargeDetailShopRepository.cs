﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺充值流水表
    ///</summary>
  public partial interface IChargeDetailShopRepository : IBaseRepository<ChargeDetailShop>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购预采订单明细
    ///</summary>
  public partial interface IPurchaseBeforehandOrderItemRepository : IBaseRepository<PurchaseBeforehandOrderItem>
  {
  }
}
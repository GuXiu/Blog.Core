﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///营销服务购买记录表
    ///</summary>
  public partial interface IMarketServiceRecordRepository : IBaseRepository<MarketServiceRecord>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺经营类目（弃用）
    ///</summary>
  public partial interface IBusinessCategoriesRepository : IBaseRepository<BusinessCategories>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员OpenID表
    ///</summary>
  public partial interface IMemberOpenIdsRepository : IBaseRepository<MemberOpenIds>
  {
  }
}
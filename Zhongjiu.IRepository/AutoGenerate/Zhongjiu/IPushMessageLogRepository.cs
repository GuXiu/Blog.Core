﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///App消息推送记录表
    ///</summary>
  public partial interface IPushMessageLogRepository : IBaseRepository<PushMessageLog>
  {
  }
}
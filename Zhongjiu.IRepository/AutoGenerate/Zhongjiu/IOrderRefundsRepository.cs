﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单退款表
    ///</summary>
  public partial interface IOrderRefundsRepository : IBaseRepository<OrderRefunds>
  {
  }
}
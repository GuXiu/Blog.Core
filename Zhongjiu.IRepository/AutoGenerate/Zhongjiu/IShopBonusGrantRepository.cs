﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员订单发放红包表
    ///</summary>
  public partial interface IShopBonusGrantRepository : IBaseRepository<ShopBonusGrant>
  {
  }
}
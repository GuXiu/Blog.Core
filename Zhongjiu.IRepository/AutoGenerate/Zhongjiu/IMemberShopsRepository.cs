﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员店铺关系表
    ///</summary>
  public partial interface IMemberShopsRepository : IBaseRepository<MemberShops>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///群发分享 领取记录
    ///</summary>
  public partial interface ICouponShareBatchRecordRepository : IBaseRepository<CouponShareBatchRecord>
  {
  }
}
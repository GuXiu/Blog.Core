﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品表
    ///</summary>
  public partial interface IOffLineProductsRepository : IBaseRepository<OffLineProducts>
  {
  }
}
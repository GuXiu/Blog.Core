﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///营销服务费结算明细表
    ///</summary>
  public partial interface IAccountMetaRepository : IBaseRepository<AccountMeta>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///小精灵支付，按天统计(订单记录信息)
    ///</summary>
  public partial interface IOrdersHuiShouYinGroupDayRepository : IBaseRepository<OrdersHuiShouYinGroupDay>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///活动奖项
    ///</summary>
  public partial interface ILuckDrawActivityPrizeRepository : IBaseRepository<LuckDrawActivityPrize>
  {
  }
}
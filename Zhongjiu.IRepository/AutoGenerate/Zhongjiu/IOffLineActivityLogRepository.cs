﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下活动修改信息表（弃用）
    ///</summary>
  public partial interface IOffLineActivityLogRepository : IBaseRepository<OffLineActivityLog>
  {
  }
}
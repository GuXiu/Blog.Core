﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层分类表
    ///</summary>
  public partial interface IFloorCategoriesRepository : IBaseRepository<FloorCategories>
  {
  }
}
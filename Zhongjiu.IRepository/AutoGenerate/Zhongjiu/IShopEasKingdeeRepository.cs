﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置店铺信息
    ///</summary>
  public partial interface IShopEasKingdeeRepository : IBaseRepository<ShopEasKingdee>
  {
  }
}
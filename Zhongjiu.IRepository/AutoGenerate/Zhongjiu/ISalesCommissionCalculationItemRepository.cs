﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///提成计算记录 商品详情表
    ///</summary>
  public partial interface ISalesCommissionCalculationItemRepository : IBaseRepository<SalesCommissionCalculationItem>
  {
  }
}
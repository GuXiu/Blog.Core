﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///奖品明细表
    ///</summary>
  public partial interface ILuckDrawAwardsItemRepository : IBaseRepository<LuckDrawAwardsItem>
  {
  }
}
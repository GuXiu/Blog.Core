﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///群聊设置选择的企业群
    ///</summary>
  public partial interface IWxGroupChatItemsRepository : IBaseRepository<WxGroupChatItems>
  {
  }
}
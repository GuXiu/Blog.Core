﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品和店铺分类的关联表
    ///</summary>
  public partial interface IProductShopCategoriesRepository : IBaseRepository<ProductShopCategories>
  {
  }
}
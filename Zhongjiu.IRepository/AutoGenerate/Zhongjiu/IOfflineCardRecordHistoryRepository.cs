﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单使用会员卡信息表
    ///</summary>
  public partial interface IOfflineCardRecordHistoryRepository : IBaseRepository<OfflineCardRecordHistory>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///结算单明细
    ///</summary>
  public partial interface IStatementsItemRepository : IBaseRepository<StatementsItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品索引表
    ///</summary>
  public partial interface IIndexProductRepository : IBaseRepository<IndexProduct>
  {
  }
}
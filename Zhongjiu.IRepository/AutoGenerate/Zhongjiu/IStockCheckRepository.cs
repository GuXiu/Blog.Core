﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///库存盘点
    ///</summary>
  public partial interface IStockCheckRepository : IBaseRepository<StockCheck>
  {
  }
}
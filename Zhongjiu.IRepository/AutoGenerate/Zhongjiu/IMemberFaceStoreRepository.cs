﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///人脸信息库
    ///</summary>
  public partial interface IMemberFaceStoreRepository : IBaseRepository<MemberFaceStore>
  {
  }
}
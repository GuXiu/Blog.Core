﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台资金流水表
    ///</summary>
  public partial interface IPlatAccountItemRepository : IBaseRepository<PlatAccountItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销首页显示商品
    ///</summary>
  public partial interface IDistributionProductsRepository : IBaseRepository<DistributionProducts>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///注册送优惠券关联优惠券
    ///</summary>
  public partial interface ICouponSendByRegisterDetailedRepository : IBaseRepository<CouponSendByRegisterDetailed>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单退货表
    ///</summary>
  public partial interface IOffLineOrderRefundsRepository : IBaseRepository<OffLineOrderRefunds>
  {
  }
}
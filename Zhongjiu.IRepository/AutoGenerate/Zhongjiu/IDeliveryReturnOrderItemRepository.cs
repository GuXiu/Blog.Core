﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配退单明细
    ///</summary>
  public partial interface IDeliveryReturnOrderItemRepository : IBaseRepository<DeliveryReturnOrderItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拼团活动
    ///</summary>
  public partial interface IFightGroupActiveRepository : IBaseRepository<FightGroupActive>
  {
  }
}
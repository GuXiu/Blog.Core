﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商家品牌申请表
    ///</summary>
  public partial interface IShopBrandApplysRepository : IBaseRepository<ShopBrandApplys>
  {
  }
}
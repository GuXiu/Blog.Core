﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台资金表
    ///</summary>
  public partial interface IPlatAccountRepository : IBaseRepository<PlatAccount>
  {
  }
}
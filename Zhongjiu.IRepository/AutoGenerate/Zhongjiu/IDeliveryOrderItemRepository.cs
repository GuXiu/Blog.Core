﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送单明细
    ///</summary>
  public partial interface IDeliveryOrderItemRepository : IBaseRepository<DeliveryOrderItem>
  {
  }
}
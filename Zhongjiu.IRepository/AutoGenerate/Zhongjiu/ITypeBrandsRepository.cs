﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///类型和品牌的关联表
    ///</summary>
  public partial interface ITypeBrandsRepository : IBaseRepository<TypeBrands>
  {
  }
}
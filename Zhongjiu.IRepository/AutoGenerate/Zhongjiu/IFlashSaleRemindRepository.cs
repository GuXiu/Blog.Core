﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限时购提现记录表
    ///</summary>
  public partial interface IFlashSaleRemindRepository : IBaseRepository<FlashSaleRemind>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页推荐表
    ///</summary>
  public partial interface IHomeRecommendsRepository : IBaseRepository<HomeRecommends>
  {
  }
}
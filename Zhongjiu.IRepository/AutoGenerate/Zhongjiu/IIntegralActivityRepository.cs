﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///积分活动表
    ///</summary>
  public partial interface IIntegralActivityRepository : IBaseRepository<IntegralActivity>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统优惠券使用记录表
    ///</summary>
  public partial interface ICouponRecordRepository : IBaseRepository<CouponRecord>
  {
  }
}
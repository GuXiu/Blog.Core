﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品目录分类索引表
    ///</summary>
  public partial interface IIndexCategoryRepository : IBaseRepository<IndexCategory>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///移动平均价记录
    ///</summary>
  public partial interface IAvgPriceHistoryRepository : IBaseRepository<AvgPriceHistory>
  {
  }
}
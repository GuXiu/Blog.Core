﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拓展会员提成表
    ///</summary>
  public partial interface ISalesCommissionExtendedMembershipRepository : IBaseRepository<SalesCommissionExtendedMembership>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系对应的汉语拼音
    ///</summary>
  public partial interface IAccountSetSpellRepository : IBaseRepository<AccountSetSpell>
  {
  }
}
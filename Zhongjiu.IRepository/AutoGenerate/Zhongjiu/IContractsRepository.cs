﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///合同信息表
    ///</summary>
  public partial interface IContractsRepository : IBaseRepository<Contracts>
  {
  }
}
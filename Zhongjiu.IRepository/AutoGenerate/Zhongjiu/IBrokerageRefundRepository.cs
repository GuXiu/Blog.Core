﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///佣金退还记录表
    ///</summary>
  public partial interface IBrokerageRefundRepository : IBaseRepository<BrokerageRefund>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品分组表
    ///</summary>
  public partial interface IProductsGroupRepository : IBaseRepository<ProductsGroup>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品基库副属性值
    ///</summary>
  public partial interface IBaseProattachedSpecValueRepository : IBaseRepository<BaseProattachedSpecValue>
  {
  }
}
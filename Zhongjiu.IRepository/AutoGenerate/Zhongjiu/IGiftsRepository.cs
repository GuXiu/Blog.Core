﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品活动表
    ///</summary>
  public partial interface IGiftsRepository : IBaseRepository<Gifts>
  {
  }
}
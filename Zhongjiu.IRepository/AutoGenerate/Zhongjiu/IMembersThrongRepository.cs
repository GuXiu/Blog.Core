﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员人群
    ///</summary>
  public partial interface IMembersThrongRepository : IBaseRepository<MembersThrong>
  {
  }
}
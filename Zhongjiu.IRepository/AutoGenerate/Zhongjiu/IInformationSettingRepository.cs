﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系消息发送设置表
    ///</summary>
  public partial interface IInformationSettingRepository : IBaseRepository<InformationSetting>
  {
  }
}
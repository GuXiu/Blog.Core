﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信用户信息
    ///</summary>
  public partial interface IWeChatUserRepository : IBaseRepository<WeChatUser>
  {
  }
}
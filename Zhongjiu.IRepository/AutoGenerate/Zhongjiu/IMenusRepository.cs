﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///移动端菜单表
    ///</summary>
  public partial interface IMenusRepository : IBaseRepository<Menus>
  {
  }
}
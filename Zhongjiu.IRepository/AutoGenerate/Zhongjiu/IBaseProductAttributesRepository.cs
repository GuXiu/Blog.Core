﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///基库商品和商品属性值关系表
    ///</summary>
  public partial interface IBaseProductAttributesRepository : IBaseRepository<BaseProductAttributes>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///经营类别申请类目详细表（弃用）
    ///</summary>
  public partial interface IBusinessCategoriesApplyDetailRepository : IBaseRepository<BusinessCategoriesApplyDetail>
  {
  }
}
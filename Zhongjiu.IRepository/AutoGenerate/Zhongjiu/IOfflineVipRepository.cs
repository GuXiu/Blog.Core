﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员卡表
    ///</summary>
  public partial interface IOfflineVipRepository : IBaseRepository<OfflineVip>
  {
  }
}
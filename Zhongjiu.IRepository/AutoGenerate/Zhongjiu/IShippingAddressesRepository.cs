﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员的收货地址表
    ///</summary>
  public partial interface IShippingAddressesRepository : IBaseRepository<ShippingAddresses>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单评价表
    ///</summary>
  public partial interface IOrderCommentsRepository : IBaseRepository<OrderComments>
  {
  }
}
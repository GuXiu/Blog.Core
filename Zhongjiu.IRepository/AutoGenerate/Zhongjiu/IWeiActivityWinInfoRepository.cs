﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡获奖详情表
    ///</summary>
  public partial interface IWeiActivityWinInfoRepository : IBaseRepository<WeiActivityWinInfo>
  {
  }
}
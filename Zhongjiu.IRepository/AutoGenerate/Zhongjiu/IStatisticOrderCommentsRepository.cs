﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单评价统计表
    ///</summary>
  public partial interface IStatisticOrderCommentsRepository : IBaseRepository<StatisticOrderComments>
  {
  }
}
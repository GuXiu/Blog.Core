﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品分类品牌关联表
    ///</summary>
  public partial interface IOffLineCategoriesBrandsRepository : IBaseRepository<OffLineCategoriesBrands>
  {
  }
}
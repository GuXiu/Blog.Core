﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///菜单、功能表
    ///</summary>
  public partial interface IPrivilegeRepository : IBaseRepository<Privilege>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信消息模板表
    ///</summary>
  public partial interface IWeiXinMsgTemplateRepository : IBaseRepository<WeiXinMsgTemplate>
  {
  }
}
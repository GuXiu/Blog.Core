﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///结算详细表
    ///</summary>
  public partial interface IAccountDetailsRepository : IBaseRepository<AccountDetails>
  {
  }
}
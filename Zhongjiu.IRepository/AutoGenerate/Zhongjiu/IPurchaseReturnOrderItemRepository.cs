﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购退货订单明细
    ///</summary>
  public partial interface IPurchaseReturnOrderItemRepository : IBaseRepository<PurchaseReturnOrderItem>
  {
  }
}
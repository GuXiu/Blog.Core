﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品基库表
    ///</summary>
  public partial interface IBaseProductsRepository : IBaseRepository<BaseProducts>
  {
  }
}
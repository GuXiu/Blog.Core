﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信会员关注公众号状态表
    ///</summary>
  public partial interface IOpenIdsRepository : IBaseRepository<OpenIds>
  {
  }
}
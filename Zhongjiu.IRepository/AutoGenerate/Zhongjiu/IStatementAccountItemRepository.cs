﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///对账单明细
    ///</summary>
  public partial interface IStatementAccountItemRepository : IBaseRepository<StatementAccountItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品订单表
    ///</summary>
  public partial interface IGiftsOrderRepository : IBaseRepository<GiftsOrder>
  {
  }
}
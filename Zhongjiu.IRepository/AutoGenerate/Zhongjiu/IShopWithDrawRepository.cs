﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺提现表
    ///</summary>
  public partial interface IShopWithDrawRepository : IBaseRepository<ShopWithDraw>
  {
  }
}
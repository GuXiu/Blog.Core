﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户订单数据统计 按用户统计每天订单,没有则不录入新数据
    ///</summary>
  public partial interface IMemberOrderDashboardRepository : IBaseRepository<MemberOrderDashboard>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品冻结表
    ///</summary>
  public partial interface IOffLineInventoryFrozenRepository : IBaseRepository<OffLineInventoryFrozen>
  {
  }
}
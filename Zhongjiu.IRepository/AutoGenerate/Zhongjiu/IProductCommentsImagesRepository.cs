﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品评论图片表
    ///</summary>
  public partial interface IProductCommentsImagesRepository : IBaseRepository<ProductCommentsImages>
  {
  }
}
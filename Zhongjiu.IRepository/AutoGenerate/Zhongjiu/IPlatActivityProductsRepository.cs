﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台活动商品信息
    ///</summary>
  public partial interface IPlatActivityProductsRepository : IBaseRepository<PlatActivityProducts>
  {
  }
}
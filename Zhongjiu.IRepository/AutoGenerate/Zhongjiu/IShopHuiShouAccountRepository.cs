﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///每个店铺，关联的小精灵支付账户信息
    ///</summary>
  public partial interface IShopHuiShouAccountRepository : IBaseRepository<ShopHuiShouAccount>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///同步数据记录表
    ///</summary>
  public partial interface IsynchronousdatarecordingRepository : IBaseRepository<synchronousdatarecording>
  {
  }
}
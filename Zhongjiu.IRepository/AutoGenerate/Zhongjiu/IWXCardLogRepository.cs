﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///卡卷领取表
    ///</summary>
  public partial interface IWXCardLogRepository : IBaseRepository<WXCardLog>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页、店铺首页广告图片表
    ///</summary>
  public partial interface IImageAdsRepository : IBaseRepository<ImageAds>
  {
  }
}
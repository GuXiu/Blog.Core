﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///表单主键Id与流程实例Id对应关系
    ///</summary>
  public partial interface IFormProcessInstanceRepository : IBaseRepository<FormProcessInstance>
  {
  }
}
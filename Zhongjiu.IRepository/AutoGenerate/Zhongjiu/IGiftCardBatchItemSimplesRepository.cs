﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品卡简易批次明细
    ///</summary>
  public partial interface IGiftCardBatchItemSimplesRepository : IBaseRepository<GiftCardBatchItemSimples>
  {
  }
}
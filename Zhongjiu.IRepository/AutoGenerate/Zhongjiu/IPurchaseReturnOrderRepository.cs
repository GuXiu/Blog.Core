﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购退货订单
    ///</summary>
  public partial interface IPurchaseReturnOrderRepository : IBaseRepository<PurchaseReturnOrder>
  {
  }
}
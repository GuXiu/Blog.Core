﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///区域编号表（删除）
    ///</summary>
  public partial interface IAreaCodeRepository : IBaseRepository<AreaCode>
  {
  }
}
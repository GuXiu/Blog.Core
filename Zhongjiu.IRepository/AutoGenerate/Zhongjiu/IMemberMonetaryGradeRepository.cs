﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员消费等级表
    ///</summary>
  public partial interface IMemberMonetaryGradeRepository : IBaseRepository<MemberMonetaryGrade>
  {
  }
}
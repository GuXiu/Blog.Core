﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///抽奖活动问题表
    ///</summary>
  public partial interface ILuckDrawActivityCodeRepository : IBaseRepository<LuckDrawActivityCode>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单操作日志表
    ///</summary>
  public partial interface IOrderOperationLogsRepository : IBaseRepository<OrderOperationLogs>
  {
  }
}
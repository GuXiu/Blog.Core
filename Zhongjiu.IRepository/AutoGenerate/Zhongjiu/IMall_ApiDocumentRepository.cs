﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///小程序API 接口文档
    ///</summary>
  public partial interface IMall_ApiDocumentRepository : IBaseRepository<Mall_ApiDocument>
  {
  }
}
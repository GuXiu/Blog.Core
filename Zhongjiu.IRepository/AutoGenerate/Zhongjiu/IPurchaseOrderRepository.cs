﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购订单
    ///</summary>
  public partial interface IPurchaseOrderRepository : IBaseRepository<PurchaseOrder>
  {
  }
}
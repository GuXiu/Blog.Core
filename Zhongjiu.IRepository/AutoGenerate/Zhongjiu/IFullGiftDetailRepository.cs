﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满赠活动详情表
    ///</summary>
  public partial interface IFullGiftDetailRepository : IBaseRepository<FullGiftDetail>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///部门下店铺
    ///</summary>
  public partial interface IDepartmentShopsRepository : IBaseRepository<DepartmentShops>
  {
  }
}
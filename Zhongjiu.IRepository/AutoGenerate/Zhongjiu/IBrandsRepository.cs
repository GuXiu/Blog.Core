﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统品牌表（店铺商品、基库商品共用）
    ///</summary>
  public partial interface IBrandsRepository : IBaseRepository<Brands>
  {
  }
}
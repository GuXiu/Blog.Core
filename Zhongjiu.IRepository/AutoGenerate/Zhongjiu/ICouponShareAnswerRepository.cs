﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题答案表
    ///</summary>
  public partial interface ICouponShareAnswerRepository : IBaseRepository<CouponShareAnswer>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品副属性（店铺商品、基库商品共用）
    ///</summary>
  public partial interface IProattachedSpecRepository : IBaseRepository<ProattachedSpec>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销板块开关设置
    ///</summary>
  public partial interface IDistributorSettingRepository : IBaseRepository<DistributorSetting>
  {
  }
}
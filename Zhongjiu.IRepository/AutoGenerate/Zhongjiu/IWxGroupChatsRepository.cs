﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信小程序群聊配置
    ///</summary>
  public partial interface IWxGroupChatsRepository : IBaseRepository<WxGroupChats>
  {
  }
}
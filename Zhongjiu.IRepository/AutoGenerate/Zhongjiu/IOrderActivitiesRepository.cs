﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单触发的活动记录
    ///</summary>
  public partial interface IOrderActivitiesRepository : IBaseRepository<OrderActivities>
  {
  }
}
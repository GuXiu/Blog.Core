﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户注册数统计表 统计逻辑将每日新增的会员按店铺分组录入，为0则不录入
    ///</summary>
  public partial interface IMemberRegisterDashboardRepository : IBaseRepository<MemberRegisterDashboard>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺自动获取批次
    ///</summary>
  public partial interface IShopBatchRepository : IBaseRepository<ShopBatch>
  {
  }
}
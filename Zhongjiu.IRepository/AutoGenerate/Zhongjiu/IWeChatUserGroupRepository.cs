﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信用户群组表
    ///</summary>
  public partial interface IWeChatUserGroupRepository : IBaseRepository<WeChatUserGroup>
  {
  }
}
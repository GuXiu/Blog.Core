﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺对应的快递网点配置
    ///</summary>
  public partial interface IExpressPointRepository : IBaseRepository<ExpressPoint>
  {
  }
}
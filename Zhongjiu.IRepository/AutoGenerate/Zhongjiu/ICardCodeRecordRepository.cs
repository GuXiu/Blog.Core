﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///卡券识别记录表
    ///</summary>
  public partial interface ICardCodeRecordRepository : IBaseRepository<CardCodeRecord>
  {
  }
}
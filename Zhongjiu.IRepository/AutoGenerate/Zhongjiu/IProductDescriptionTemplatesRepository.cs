﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///关联版式表
    ///</summary>
  public partial interface IProductDescriptionTemplatesRepository : IBaseRepository<ProductDescriptionTemplates>
  {
  }
}
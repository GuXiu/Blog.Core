﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单明细表
    ///</summary>
  public partial interface IOrderItemsRepository : IBaseRepository<OrderItems>
  {
  }
}
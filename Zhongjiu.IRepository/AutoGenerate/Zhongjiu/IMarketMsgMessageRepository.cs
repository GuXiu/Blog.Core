﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///营销消息表
    ///</summary>
  public partial interface IMarketMsgMessageRepository : IBaseRepository<MarketMsgMessage>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户积分兑换规则表
    ///</summary>
  public partial interface IMemberIntegralExchangeRulesRepository : IBaseRepository<MemberIntegralExchangeRules>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层Tab表
    ///</summary>
  public partial interface IFloorTablDetailsRepository : IBaseRepository<FloorTablDetails>
  {
  }
}
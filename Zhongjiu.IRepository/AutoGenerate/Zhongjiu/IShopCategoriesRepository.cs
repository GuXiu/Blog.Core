﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺分类表
    ///</summary>
  public partial interface IShopCategoriesRepository : IBaseRepository<ShopCategories>
  {
  }
}
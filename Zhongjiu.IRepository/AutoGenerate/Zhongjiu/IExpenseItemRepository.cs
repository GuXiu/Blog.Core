﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///费用明细
    ///</summary>
  public partial interface IExpenseItemRepository : IBaseRepository<ExpenseItem>
  {
  }
}
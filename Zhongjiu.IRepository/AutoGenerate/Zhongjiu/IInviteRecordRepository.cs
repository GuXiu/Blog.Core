﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///我要推广邀请注册记录表
    ///</summary>
  public partial interface IInviteRecordRepository : IBaseRepository<InviteRecord>
  {
  }
}
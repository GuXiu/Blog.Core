﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品调拨明细表
    ///</summary>
  public partial interface IOffLineInventorySchedulingProductsRepository : IBaseRepository<OffLineInventorySchedulingProducts>
  {
  }
}
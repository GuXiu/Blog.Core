﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///App首页模块表
    ///</summary>
  public partial interface IAPPModuleRepository : IBaseRepository<APPModule>
  {
  }
}
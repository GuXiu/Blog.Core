﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///发送优惠券详细
    ///</summary>
  public partial interface ISendmessagerecordCouponRepository : IBaseRepository<SendmessagerecordCoupon>
  {
  }
}
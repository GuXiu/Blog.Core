﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置物料信息
    ///</summary>
  public partial interface IShopEasKingdeeProductMaterialRepository : IBaseRepository<ShopEasKingdeeProductMaterial>
  {
  }
}
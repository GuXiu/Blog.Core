﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单营销记录表
    ///</summary>
  public partial interface IMarketingOrderRepository : IBaseRepository<MarketingOrder>
  {
  }
}
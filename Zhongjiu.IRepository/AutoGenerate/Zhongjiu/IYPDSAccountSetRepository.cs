﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///一品多商体系设置
    ///</summary>
  public partial interface IYPDSAccountSetRepository : IBaseRepository<YPDSAccountSet>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///App版本更新
    ///</summary>
  public partial interface IAppUpdateRepository : IBaseRepository<AppUpdate>
  {
  }
}
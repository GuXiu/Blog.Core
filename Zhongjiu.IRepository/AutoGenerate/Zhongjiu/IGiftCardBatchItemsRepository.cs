﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///补品卡批次明细
    ///</summary>
  public partial interface IGiftCardBatchItemsRepository : IBaseRepository<GiftCardBatchItems>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单明细表
    ///</summary>
  public partial interface IOffLineOrderItemsRepository : IBaseRepository<OffLineOrderItems>
  {
  }
}
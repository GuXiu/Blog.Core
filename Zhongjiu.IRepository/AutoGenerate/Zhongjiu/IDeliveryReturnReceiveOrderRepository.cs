﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配退收货单
    ///</summary>
  public partial interface IDeliveryReturnReceiveOrderRepository : IBaseRepository<DeliveryReturnReceiveOrder>
  {
  }
}
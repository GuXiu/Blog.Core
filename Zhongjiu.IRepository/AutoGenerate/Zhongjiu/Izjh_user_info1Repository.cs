﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///云码关注用户表
    ///</summary>
  public partial interface Izjh_user_info1Repository : IBaseRepository<zjh_user_info1>
  {
  }
}
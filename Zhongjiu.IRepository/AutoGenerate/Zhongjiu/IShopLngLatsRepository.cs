﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺配送范围坐标表
    ///</summary>
  public partial interface IShopLngLatsRepository : IBaseRepository<ShopLngLats>
  {
  }
}
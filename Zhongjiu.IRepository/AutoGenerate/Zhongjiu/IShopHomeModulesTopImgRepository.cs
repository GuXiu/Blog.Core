﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺首页楼层轮播图
    ///</summary>
  public partial interface IShopHomeModulesTopImgRepository : IBaseRepository<ShopHomeModulesTopImg>
  {
  }
}
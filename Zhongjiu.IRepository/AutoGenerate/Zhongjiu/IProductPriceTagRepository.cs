﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///电子价签商品信息推送记录表
    ///</summary>
  public partial interface IProductPriceTagRepository : IBaseRepository<ProductPriceTag>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///短信发送限制记录表
    ///</summary>
  public partial interface IMessageLimitLogRepository : IBaseRepository<MessageLimitLog>
  {
  }
}
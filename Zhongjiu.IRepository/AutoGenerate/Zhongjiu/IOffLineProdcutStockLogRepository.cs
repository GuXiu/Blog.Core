﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品库存操作日志表
    ///</summary>
  public partial interface IOffLineProdcutStockLogRepository : IBaseRepository<OffLineProdcutStockLog>
  {
  }
}
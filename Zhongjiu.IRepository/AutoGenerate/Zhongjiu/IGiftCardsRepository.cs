﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品卡表
    ///</summary>
  public partial interface IGiftCardsRepository : IBaseRepository<GiftCards>
  {
  }
}
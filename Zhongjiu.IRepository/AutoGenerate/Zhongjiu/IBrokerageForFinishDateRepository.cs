﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细(分销商品)
    ///</summary>
  public partial interface IBrokerageForFinishDateRepository : IBaseRepository<BrokerageForFinishDate>
  {
  }
}
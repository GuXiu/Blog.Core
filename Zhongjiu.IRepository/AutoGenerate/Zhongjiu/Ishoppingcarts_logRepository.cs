﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///添加购物车日志表
    ///</summary>
  public partial interface Ishoppingcarts_logRepository : IBaseRepository<shoppingcarts_log>
  {
  }
}
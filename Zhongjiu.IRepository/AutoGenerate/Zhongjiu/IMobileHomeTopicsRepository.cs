﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信端首页的专题表
    ///</summary>
  public partial interface IMobileHomeTopicsRepository : IBaseRepository<MobileHomeTopics>
  {
  }
}
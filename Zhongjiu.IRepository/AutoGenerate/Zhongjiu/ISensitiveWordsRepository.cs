﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///敏感关键词表
    ///</summary>
  public partial interface ISensitiveWordsRepository : IBaseRepository<SensitiveWords>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员人群 属性值
    ///</summary>
  public partial interface IMembersThrongAttributeRepository : IBaseRepository<MembersThrongAttribute>
  {
  }
}
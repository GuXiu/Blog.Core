﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销用户与店铺关联表
    ///</summary>
  public partial interface IDistributionUserLinkRepository : IBaseRepository<DistributionUserLink>
  {
  }
}
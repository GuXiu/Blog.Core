﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购入库订单
    ///</summary>
  public partial interface IPurchaseEnterOrderRepository : IBaseRepository<PurchaseEnterOrder>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺续费记录表
    ///</summary>
  public partial interface IShopRenewRecordRepository : IBaseRepository<ShopRenewRecord>
  {
  }
}
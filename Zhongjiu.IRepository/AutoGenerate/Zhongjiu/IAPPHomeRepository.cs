﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///App首页信息表
    ///</summary>
  public partial interface IAPPHomeRepository : IBaseRepository<APPHome>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///保证金详情表
    ///</summary>
  public partial interface ICashDepositDetailRepository : IBaseRepository<CashDepositDetail>
  {
  }
}
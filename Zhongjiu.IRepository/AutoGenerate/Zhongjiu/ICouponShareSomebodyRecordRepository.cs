﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分享给某人记录表
    ///</summary>
  public partial interface ICouponShareSomebodyRecordRepository : IBaseRepository<CouponShareSomebodyRecord>
  {
  }
}
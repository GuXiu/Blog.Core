﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///快递面单数据
    ///</summary>
  public partial interface IExpressDataRepository : IBaseRepository<ExpressData>
  {
  }
}
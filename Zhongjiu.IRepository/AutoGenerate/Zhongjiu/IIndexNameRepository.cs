﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品名索引表
    ///</summary>
  public partial interface IIndexNameRepository : IBaseRepository<IndexName>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品库存阈值表
    ///</summary>
  public partial interface IProductStockThresholdRepository : IBaseRepository<ProductStockThreshold>
  {
  }
}
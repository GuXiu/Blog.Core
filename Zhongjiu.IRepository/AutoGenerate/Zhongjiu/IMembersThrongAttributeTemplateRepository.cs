﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员人群 属性模板
    ///</summary>
  public partial interface IMembersThrongAttributeTemplateRepository : IBaseRepository<MembersThrongAttributeTemplate>
  {
  }
}
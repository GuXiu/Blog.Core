﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺红包领取记录表
    ///</summary>
  public partial interface IShopBonusReceiveRepository : IBaseRepository<ShopBonusReceive>
  {
  }
}
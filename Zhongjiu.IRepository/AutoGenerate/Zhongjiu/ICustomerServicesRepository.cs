﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统客服表
    ///</summary>
  public partial interface ICustomerServicesRepository : IBaseRepository<CustomerServices>
  {
  }
}
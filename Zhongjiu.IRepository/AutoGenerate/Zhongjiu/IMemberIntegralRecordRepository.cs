﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户积分兑换记录表
    ///</summary>
  public partial interface IMemberIntegralRecordRepository : IBaseRepository<MemberIntegralRecord>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单App扫码支付表
    ///</summary>
  public partial interface IOffLine_PaymentRecordRepository : IBaseRepository<OffLine_PaymentRecord>
  {
  }
}
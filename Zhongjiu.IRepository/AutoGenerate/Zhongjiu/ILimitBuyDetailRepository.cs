﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限购活动详情表
    ///</summary>
  public partial interface ILimitBuyDetailRepository : IBaseRepository<LimitBuyDetail>
  {
  }
}
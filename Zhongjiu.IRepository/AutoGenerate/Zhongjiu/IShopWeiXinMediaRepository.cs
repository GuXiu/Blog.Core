﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺微信素材表
    ///</summary>
  public partial interface IShopWeiXinMediaRepository : IBaseRepository<ShopWeiXinMedia>
  {
  }
}
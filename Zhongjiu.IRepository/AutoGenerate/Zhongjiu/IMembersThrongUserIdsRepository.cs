﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员人群 会员Id关系表
    ///</summary>
  public partial interface IMembersThrongUserIdsRepository : IBaseRepository<MembersThrongUserIds>
  {
  }
}
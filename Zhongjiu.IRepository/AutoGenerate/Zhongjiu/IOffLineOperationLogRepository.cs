﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下操作日志表
    ///</summary>
  public partial interface IOffLineOperationLogRepository : IBaseRepository<OffLineOperationLog>
  {
  }
}
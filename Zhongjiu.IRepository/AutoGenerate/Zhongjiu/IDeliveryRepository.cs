﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系配送员表
    ///</summary>
  public partial interface IDeliveryRepository : IBaseRepository<Delivery>
  {
  }
}
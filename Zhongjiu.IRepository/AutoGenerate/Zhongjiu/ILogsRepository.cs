﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///操作日志表
    ///</summary>
  public partial interface ILogsRepository : IBaseRepository<Logs>
  {
  }
}
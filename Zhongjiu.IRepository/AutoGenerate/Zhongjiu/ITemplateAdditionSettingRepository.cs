﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///模板添加设置表
    ///</summary>
  public partial interface ITemplateAdditionSettingRepository : IBaseRepository<TemplateAdditionSetting>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台活动适用店铺
    ///</summary>
  public partial interface IPlatActivityShopRepository : IBaseRepository<PlatActivityShop>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品订单明细
    ///</summary>
  public partial interface IGiftOrderItemRepository : IBaseRepository<GiftOrderItem>
  {
  }
}
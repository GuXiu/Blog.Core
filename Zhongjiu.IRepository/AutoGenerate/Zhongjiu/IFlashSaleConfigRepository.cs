﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限时购预热时间设置表
    ///</summary>
  public partial interface IFlashSaleConfigRepository : IBaseRepository<FlashSaleConfig>
  {
  }
}
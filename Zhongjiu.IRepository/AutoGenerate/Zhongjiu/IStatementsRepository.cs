﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///结算单表
    ///</summary>
  public partial interface IStatementsRepository : IBaseRepository<Statements>
  {
  }
}
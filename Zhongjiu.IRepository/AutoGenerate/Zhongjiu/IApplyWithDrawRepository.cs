﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员提现申请表
    ///</summary>
  public partial interface IApplyWithDrawRepository : IBaseRepository<ApplyWithDraw>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券组合，群发分享，红包群发形式 领取
    ///</summary>
  public partial interface ICouponShareBatchGroupRepository : IBaseRepository<CouponShareBatchGroup>
  {
  }
}
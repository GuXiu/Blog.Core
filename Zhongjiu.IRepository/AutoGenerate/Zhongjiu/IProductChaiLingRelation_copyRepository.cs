﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拆零商品关系表
    ///</summary>
  public partial interface IProductChaiLingRelation_copyRepository : IBaseRepository<ProductChaiLingRelation_copy>
  {
  }
}
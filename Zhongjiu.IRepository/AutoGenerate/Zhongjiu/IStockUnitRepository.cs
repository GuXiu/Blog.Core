﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///库存单位表
    ///</summary>
  public partial interface IStockUnitRepository : IBaseRepository<StockUnit>
  {
  }
}
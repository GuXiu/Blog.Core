﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满减活动表
    ///</summary>
  public partial interface IFullSubtractRepository : IBaseRepository<FullSubtract>
  {
  }
}
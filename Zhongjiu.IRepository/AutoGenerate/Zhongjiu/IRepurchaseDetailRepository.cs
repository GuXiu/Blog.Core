﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///换购活动商品详情
    ///</summary>
  public partial interface IRepurchaseDetailRepository : IBaseRepository<RepurchaseDetail>
  {
  }
}
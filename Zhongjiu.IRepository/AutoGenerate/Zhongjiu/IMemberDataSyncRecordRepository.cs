﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员数据同步记录表
    ///</summary>
  public partial interface IMemberDataSyncRecordRepository : IBaseRepository<MemberDataSyncRecord>
  {
  }
}
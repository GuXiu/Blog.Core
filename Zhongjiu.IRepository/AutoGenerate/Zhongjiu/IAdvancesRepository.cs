﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///预付款表
    ///</summary>
  public partial interface IAdvancesRepository : IBaseRepository<Advances>
  {
  }
}
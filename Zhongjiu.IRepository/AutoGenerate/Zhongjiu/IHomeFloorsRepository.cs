﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层表
    ///</summary>
  public partial interface IHomeFloorsRepository : IBaseRepository<HomeFloors>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下管理员关联店铺表
    ///</summary>
  public partial interface IOfflineManagerShopsRepository : IBaseRepository<OfflineManagerShops>
  {
  }
}
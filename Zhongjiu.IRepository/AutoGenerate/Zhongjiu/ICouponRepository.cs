﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城优惠券表
    ///</summary>
  public partial interface ICouponRepository : IBaseRepository<Coupon>
  {
  }
}
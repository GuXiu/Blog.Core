﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员标签表
    ///</summary>
  public partial interface ILabelRepository : IBaseRepository<Label>
  {
  }
}
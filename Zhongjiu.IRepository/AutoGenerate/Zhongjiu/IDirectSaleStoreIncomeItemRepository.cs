﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入明细
    ///</summary>
  public partial interface IDirectSaleStoreIncomeItemRepository : IBaseRepository<DirectSaleStoreIncomeItem>
  {
  }
}
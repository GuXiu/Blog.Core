﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺客显广告表
    ///</summary>
  public partial interface IAdvertisementRepository : IBaseRepository<Advertisement>
  {
  }
}
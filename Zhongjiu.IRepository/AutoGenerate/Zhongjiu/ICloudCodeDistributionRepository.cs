﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///云码分配表,此表暂时不用
    ///</summary>
  public partial interface ICloudCodeDistributionRepository : IBaseRepository<CloudCodeDistribution>
  {
  }
}
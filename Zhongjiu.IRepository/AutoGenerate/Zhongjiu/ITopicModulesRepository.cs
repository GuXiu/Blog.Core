﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统模块的专题表
    ///</summary>
  public partial interface ITopicModulesRepository : IBaseRepository<TopicModules>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///产品调价明细单
    ///</summary>
  public partial interface IProductModifyPriceDetailRepository : IBaseRepository<ProductModifyPriceDetail>
  {
  }
}
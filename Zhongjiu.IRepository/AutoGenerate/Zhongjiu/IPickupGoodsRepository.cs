﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下取单记录表
    ///</summary>
  public partial interface IPickupGoodsRepository : IBaseRepository<PickupGoods>
  {
  }
}
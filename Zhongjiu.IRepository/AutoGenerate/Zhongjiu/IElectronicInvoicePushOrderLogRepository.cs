﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单佣金推送天津电票记录
    ///</summary>
  public partial interface IElectronicInvoicePushOrderLogRepository : IBaseRepository<ElectronicInvoicePushOrderLog>
  {
  }
}
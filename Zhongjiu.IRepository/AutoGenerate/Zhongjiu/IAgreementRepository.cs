﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///协议表
    ///</summary>
  public partial interface IAgreementRepository : IBaseRepository<Agreement>
  {
  }
}
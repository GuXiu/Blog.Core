﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系公众号二维码
    ///</summary>
  public partial interface IPublicQrCodeRepository : IBaseRepository<PublicQrCode>
  {
  }
}
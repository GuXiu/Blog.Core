﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///直营门店盈亏明细
    ///</summary>
  public partial interface IDirectSaleStoreGainAndLossItemRepository : IBaseRepository<DirectSaleStoreGainAndLossItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺浏览量记录表
    ///</summary>
  public partial interface IShopVistisRepository : IBaseRepository<ShopVistis>
  {
  }
}
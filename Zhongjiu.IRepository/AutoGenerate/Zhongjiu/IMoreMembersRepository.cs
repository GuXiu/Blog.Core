﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户信息扩展表
    ///</summary>
  public partial interface IMoreMembersRepository : IBaseRepository<MoreMembers>
  {
  }
}
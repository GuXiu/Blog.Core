﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品模块表
    ///</summary>
  public partial interface IModuleProductsRepository : IBaseRepository<ModuleProducts>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///公众号消息发送记录表
    ///</summary>
  public partial interface IMsgSendLogRepository : IBaseRepository<MsgSendLog>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺OpenAPI秘钥
    ///</summary>
  public partial interface IShopOpenApiSettingRepository : IBaseRepository<ShopOpenApiSetting>
  {
  }
}
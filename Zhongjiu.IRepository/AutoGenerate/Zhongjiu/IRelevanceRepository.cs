﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///关联表
    ///</summary>
  public partial interface IRelevanceRepository : IBaseRepository<Relevance>
  {
  }
}
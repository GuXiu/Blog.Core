﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///表单引用工作流关系
    ///</summary>
  public partial interface IFormWorkflowRepository : IBaseRepository<FormWorkflow>
  {
  }
}
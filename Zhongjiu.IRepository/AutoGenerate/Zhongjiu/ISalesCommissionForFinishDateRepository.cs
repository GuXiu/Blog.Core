﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细(提成商品)
    ///</summary>
  public partial interface ISalesCommissionForFinishDateRepository : IBaseRepository<SalesCommissionForFinishDate>
  {
  }
}
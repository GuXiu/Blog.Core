﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系微信公众号设置表
    ///</summary>
  public partial interface IAccountSetWeiXinRepository : IBaseRepository<AccountSetWeiXin>
  {
  }
}
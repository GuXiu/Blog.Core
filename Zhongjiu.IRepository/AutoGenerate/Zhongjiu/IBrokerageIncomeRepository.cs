﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销佣金记录表
    ///</summary>
  public partial interface IBrokerageIncomeRepository : IBaseRepository<BrokerageIncome>
  {
  }
}
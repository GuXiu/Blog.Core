﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券分享单问题表
    ///</summary>
  public partial interface ICouponShareBatchGroupCodeRepository : IBaseRepository<CouponShareBatchGroupCode>
  {
  }
}
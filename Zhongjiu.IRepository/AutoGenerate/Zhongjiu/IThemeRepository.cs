﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///主题设置表
    ///</summary>
  public partial interface IThemeRepository : IBaseRepository<Theme>
  {
  }
}
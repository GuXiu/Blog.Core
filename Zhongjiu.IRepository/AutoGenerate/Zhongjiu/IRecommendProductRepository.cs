﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///推荐商品表
    ///</summary>
  public partial interface IRecommendProductRepository : IBaseRepository<RecommendProduct>
  {
  }
}
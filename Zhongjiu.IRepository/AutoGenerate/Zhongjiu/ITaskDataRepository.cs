﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///任务数据
    ///</summary>
  public partial interface ITaskDataRepository : IBaseRepository<TaskData>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///对账单表
    ///</summary>
  public partial interface IStatementAccountRepository : IBaseRepository<StatementAccount>
  {
  }
}
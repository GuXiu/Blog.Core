﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///库存不足提醒记录表
    ///</summary>
  public partial interface IStockNotEnoughMsgLogRepository : IBaseRepository<StockNotEnoughMsgLog>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///接收店铺有关消息的用户表
    ///</summary>
  public partial interface IManagersReceivingMessagesRepository : IBaseRepository<ManagersReceivingMessages>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品浏览记录表
    ///</summary>
  public partial interface IProductVistisRepository : IBaseRepository<ProductVistis>
  {
  }
}
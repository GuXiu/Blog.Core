﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商家规格快照
    ///</summary>
  public partial interface ISellerSpecificationValuesRepository : IBaseRepository<SellerSpecificationValues>
  {
  }
}
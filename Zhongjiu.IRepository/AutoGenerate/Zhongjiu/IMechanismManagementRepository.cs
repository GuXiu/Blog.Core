﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///机构档案表
    ///</summary>
  public partial interface IMechanismManagementRepository : IBaseRepository<MechanismManagement>
  {
  }
}
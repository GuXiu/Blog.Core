﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户步数兑换记录
    ///</summary>
  public partial interface IMemberStepsExchangeRecordRepository : IBaseRepository<MemberStepsExchangeRecord>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺　关联　支持跨店使用　的优惠券　
    ///</summary>
  public partial interface ICouponShopRepository : IBaseRepository<CouponShop>
  {
  }
}
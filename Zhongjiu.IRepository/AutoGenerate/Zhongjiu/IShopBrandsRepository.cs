﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺品牌表
    ///</summary>
  public partial interface IShopBrandsRepository : IBaseRepository<ShopBrands>
  {
  }
}
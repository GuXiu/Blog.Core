﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系分类分佣比例表
    ///</summary>
  public partial interface ICategoryDerpartmentRepository : IBaseRepository<CategoryDerpartment>
  {
  }
}
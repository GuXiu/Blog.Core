﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页分类所属行表
    ///</summary>
  public partial interface IHomeCategoryRowsRepository : IBaseRepository<HomeCategoryRows>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台活动
    ///</summary>
  public partial interface IPlatActivityRepository : IBaseRepository<PlatActivity>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///收藏店铺表
    ///</summary>
  public partial interface IFavoriteShopsRepository : IBaseRepository<FavoriteShops>
  {
  }
}
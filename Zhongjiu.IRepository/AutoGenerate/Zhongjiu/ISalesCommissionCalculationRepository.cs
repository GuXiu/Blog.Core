﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///提成计算记录表
    ///</summary>
  public partial interface ISalesCommissionCalculationRepository : IBaseRepository<SalesCommissionCalculation>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///注册赠送优惠券
    ///</summary>
  public partial interface ICouponSendByRegisterRepository : IBaseRepository<CouponSendByRegister>
  {
  }
}
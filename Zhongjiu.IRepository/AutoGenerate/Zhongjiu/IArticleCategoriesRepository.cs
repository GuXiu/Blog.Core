﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统文章分类表
    ///</summary>
  public partial interface IArticleCategoriesRepository : IBaseRepository<ArticleCategories>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满减活动阶梯表
    ///</summary>
  public partial interface IFullSubtractLadderRepository : IBaseRepository<FullSubtractLadder>
  {
  }
}
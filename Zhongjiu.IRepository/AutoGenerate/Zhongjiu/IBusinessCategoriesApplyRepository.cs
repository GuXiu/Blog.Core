﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///经营类目申请表（弃用）
    ///</summary>
  public partial interface IBusinessCategoriesApplyRepository : IBaseRepository<BusinessCategoriesApply>
  {
  }
}
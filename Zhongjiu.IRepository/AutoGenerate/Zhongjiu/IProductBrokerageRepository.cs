﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销商品表
    ///</summary>
  public partial interface IProductBrokerageRepository : IBaseRepository<ProductBrokerage>
  {
  }
}
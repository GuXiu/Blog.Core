﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///图库类别表
    ///</summary>
  public partial interface IPhotoSpaceCategoryRepository : IBaseRepository<PhotoSpaceCategory>
  {
  }
}
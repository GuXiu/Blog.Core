﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///抽奖记录表
    ///</summary>
  public partial interface ILuckDrawConsumingRecordRepository : IBaseRepository<LuckDrawConsumingRecord>
  {
  }
}
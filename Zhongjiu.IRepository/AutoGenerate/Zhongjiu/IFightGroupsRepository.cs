﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拼团组团详情
    ///</summary>
  public partial interface IFightGroupsRepository : IBaseRepository<FightGroups>
  {
  }
}
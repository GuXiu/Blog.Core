﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺热卖商品表
    ///</summary>
  public partial interface IShopHotSaleProductsRepository : IBaseRepository<ShopHotSaleProducts>
  {
  }
}
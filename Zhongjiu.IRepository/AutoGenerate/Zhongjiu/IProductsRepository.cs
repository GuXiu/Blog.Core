﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品主表
    ///</summary>
  public partial interface IProductsRepository : IBaseRepository<Products>
  {
  }
}
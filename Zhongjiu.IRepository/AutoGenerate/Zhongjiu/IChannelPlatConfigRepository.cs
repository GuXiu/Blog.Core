﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///第三方平台对接，配置信息
    ///</summary>
  public partial interface IChannelPlatConfigRepository : IBaseRepository<ChannelPlatConfig>
  {
  }
}
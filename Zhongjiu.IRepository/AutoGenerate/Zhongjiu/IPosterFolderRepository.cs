﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///海报素材文件夹
    ///</summary>
  public partial interface IPosterFolderRepository : IBaseRepository<PosterFolder>
  {
  }
}
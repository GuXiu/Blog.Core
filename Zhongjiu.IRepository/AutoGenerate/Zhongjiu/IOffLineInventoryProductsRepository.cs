﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品出入库明细表
    ///</summary>
  public partial interface IOffLineInventoryProductsRepository : IBaseRepository<OffLineInventoryProducts>
  {
  }
}
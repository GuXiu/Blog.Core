﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺首页模板设置表
    ///</summary>
  public partial interface ITemplateVisualizationSettingsRepository : IBaseRepository<TemplateVisualizationSettings>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页分类表
    ///</summary>
  public partial interface IHomeCategoriesRepository : IBaseRepository<HomeCategories>
  {
  }
}
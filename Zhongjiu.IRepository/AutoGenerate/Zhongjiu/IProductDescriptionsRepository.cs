﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品详情表
    ///</summary>
  public partial interface IProductDescriptionsRepository : IBaseRepository<ProductDescriptions>
  {
  }
}
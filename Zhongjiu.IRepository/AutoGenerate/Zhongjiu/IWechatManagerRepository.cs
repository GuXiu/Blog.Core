﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信公众号资料表
    ///</summary>
  public partial interface IWechatManagerRepository : IBaseRepository<WechatManager>
  {
  }
}
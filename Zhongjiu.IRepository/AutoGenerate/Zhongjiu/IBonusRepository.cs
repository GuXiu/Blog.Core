﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///红包表
    ///</summary>
  public partial interface IBonusRepository : IBaseRepository<Bonus>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///推广员海报
    ///</summary>
  public partial interface IPromoterPosterRepository : IBaseRepository<PromoterPoster>
  {
  }
}
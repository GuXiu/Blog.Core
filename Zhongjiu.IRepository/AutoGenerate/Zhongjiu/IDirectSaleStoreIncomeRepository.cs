﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入表
    ///</summary>
  public partial interface IDirectSaleStoreIncomeRepository : IBaseRepository<DirectSaleStoreIncome>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///直营门店盈亏表
    ///</summary>
  public partial interface IDirectSaleStoreGainAndLossRepository : IBaseRepository<DirectSaleStoreGainAndLoss>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品分类表
    ///</summary>
  public partial interface IOffLineCategoriesRepository : IBaseRepository<OffLineCategories>
  {
  }
}
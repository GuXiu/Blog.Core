﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员充值流水表
    ///</summary>
  public partial interface IChargeDetailRepository : IBaseRepository<ChargeDetail>
  {
  }
}
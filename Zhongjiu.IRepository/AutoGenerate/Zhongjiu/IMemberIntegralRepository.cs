﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员积分表
    ///</summary>
  public partial interface IMemberIntegralRepository : IBaseRepository<MemberIntegral>
  {
  }
}
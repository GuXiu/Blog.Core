﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限购活动表
    ///</summary>
  public partial interface ILimitBuyRepository : IBaseRepository<LimitBuy>
  {
  }
}
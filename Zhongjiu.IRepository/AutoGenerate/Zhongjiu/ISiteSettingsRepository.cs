﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统设置表
    ///</summary>
  public partial interface ISiteSettingsRepository : IBaseRepository<SiteSettings>
  {
  }
}
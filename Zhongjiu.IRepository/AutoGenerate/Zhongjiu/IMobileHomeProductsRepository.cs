﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信端首页的商品配置表
    ///</summary>
  public partial interface IMobileHomeProductsRepository : IBaseRepository<MobileHomeProducts>
  {
  }
}
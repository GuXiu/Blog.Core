﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微店扩展表
    ///</summary>
  public partial interface IVShopExtendRepository : IBaseRepository<VShopExtend>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销员与被邀请会员关系表
    ///</summary>
  public partial interface IPromoterMemberRepository : IBaseRepository<PromoterMember>
  {
  }
}
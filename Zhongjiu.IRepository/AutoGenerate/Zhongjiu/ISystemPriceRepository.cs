﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品会员等级价表
    ///</summary>
  public partial interface ISystemPriceRepository : IBaseRepository<SystemPrice>
  {
  }
}
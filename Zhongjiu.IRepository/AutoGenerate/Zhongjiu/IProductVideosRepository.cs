﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品视频表
    ///</summary>
  public partial interface IProductVideosRepository : IBaseRepository<ProductVideos>
  {
  }
}
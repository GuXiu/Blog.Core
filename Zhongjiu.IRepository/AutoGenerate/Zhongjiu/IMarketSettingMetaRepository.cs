﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///可扩充营销服务表
    ///</summary>
  public partial interface IMarketSettingMetaRepository : IBaseRepository<MarketSettingMeta>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///产品补充表
    ///</summary>
  public partial interface IProductSupplementsRepository : IBaseRepository<ProductSupplements>
  {
  }
}
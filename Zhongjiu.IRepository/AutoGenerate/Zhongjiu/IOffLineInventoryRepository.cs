﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品出入库表库存表
    ///</summary>
  public partial interface IOffLineInventoryRepository : IBaseRepository<OffLineInventory>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户App设备ID绑定关系表
    ///</summary>
  public partial interface IPushUserByDeviceRepository : IBaseRepository<PushUserByDevice>
  {
  }
}
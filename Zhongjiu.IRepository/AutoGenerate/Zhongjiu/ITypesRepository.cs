﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统类型表
    ///</summary>
  public partial interface ITypesRepository : IBaseRepository<Types>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送差异单明细
    ///</summary>
  public partial interface IDeliveryDifferOrderItemRepository : IBaseRepository<DeliveryDifferOrderItem>
  {
  }
}
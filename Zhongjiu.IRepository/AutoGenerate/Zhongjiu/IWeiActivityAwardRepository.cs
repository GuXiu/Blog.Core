﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡奖品表
    ///</summary>
  public partial interface IWeiActivityAwardRepository : IBaseRepository<WeiActivityAward>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页手动轮播图片
    ///</summary>
  public partial interface IHandSlideAdsRepository : IBaseRepository<HandSlideAds>
  {
  }
}
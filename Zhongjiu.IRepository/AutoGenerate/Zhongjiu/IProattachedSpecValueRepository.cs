﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品副属性值表
    ///</summary>
  public partial interface IProattachedSpecValueRepository : IBaseRepository<ProattachedSpecValue>
  {
  }
}
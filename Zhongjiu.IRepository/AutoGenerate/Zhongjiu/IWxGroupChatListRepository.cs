﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///企业微信群列表
    ///</summary>
  public partial interface IWxGroupChatListRepository : IBaseRepository<WxGroupChatList>
  {
  }
}
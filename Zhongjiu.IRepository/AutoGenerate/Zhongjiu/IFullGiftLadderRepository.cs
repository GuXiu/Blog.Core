﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满赠活动阶梯表
    ///</summary>
  public partial interface IFullGiftLadderRepository : IBaseRepository<FullGiftLadder>
  {
  }
}
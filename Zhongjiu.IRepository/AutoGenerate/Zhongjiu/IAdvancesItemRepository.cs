﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///预付款明细
    ///</summary>
  public partial interface IAdvancesItemRepository : IBaseRepository<AdvancesItem>
  {
  }
}
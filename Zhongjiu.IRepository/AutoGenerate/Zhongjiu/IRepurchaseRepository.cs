﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///换购活动
    ///</summary>
  public partial interface IRepurchaseRepository : IBaseRepository<Repurchase>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///官网个人中心绑定购物卡表
    ///</summary>
  public partial interface IMemberOfflineVipRepository : IBaseRepository<MemberOfflineVip>
  {
  }
}
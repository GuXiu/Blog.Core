﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下管理员表
    ///</summary>
  public partial interface IOfflineManagerRepository : IBaseRepository<OfflineManager>
  {
  }
}
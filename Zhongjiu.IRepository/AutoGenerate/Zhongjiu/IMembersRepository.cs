﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员表
    ///</summary>
  public partial interface IMembersRepository : IBaseRepository<Members>
  {
  }
}
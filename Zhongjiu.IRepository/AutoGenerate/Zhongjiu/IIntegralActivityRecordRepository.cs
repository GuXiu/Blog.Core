﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///积分翻倍活动Id
    ///</summary>
  public partial interface IIntegralActivityRecordRepository : IBaseRepository<IntegralActivityRecord>
  {
  }
}
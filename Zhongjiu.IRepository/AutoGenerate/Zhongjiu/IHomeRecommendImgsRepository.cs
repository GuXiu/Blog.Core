﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页推荐图片
    ///</summary>
  public partial interface IHomeRecommendImgsRepository : IBaseRepository<HomeRecommendImgs>
  {
  }
}
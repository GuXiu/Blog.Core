﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统会员联系方式表
    ///</summary>
  public partial interface IMemberContactsRepository : IBaseRepository<MemberContacts>
  {
  }
}
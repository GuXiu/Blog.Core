﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///积分活动商品表
    ///</summary>
  public partial interface IIntegraActivityDetailRepository : IBaseRepository<IntegraActivityDetail>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单金额小数操作记录（删除）
    ///</summary>
  public partial interface IOrderAmountFractionalRepository : IBaseRepository<OrderAmountFractional>
  {
  }
}
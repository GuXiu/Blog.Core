﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微店配置表
    ///</summary>
  public partial interface IWXshopRepository : IBaseRepository<WXshop>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///抽奖活动
    ///</summary>
  public partial interface ILuckDrawActivityRepository : IBaseRepository<LuckDrawActivity>
  {
  }
}
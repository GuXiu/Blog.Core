﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///单据审核记录表
    ///</summary>
  public partial interface IAuditHistoryRepository : IBaseRepository<AuditHistory>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///体系公众号自动回复消息
    ///</summary>
  public partial interface IShopAutomaticReplyMessagesRepository : IBaseRepository<ShopAutomaticReplyMessages>
  {
  }
}
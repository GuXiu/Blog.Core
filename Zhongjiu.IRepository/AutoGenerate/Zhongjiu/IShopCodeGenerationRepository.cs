﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺编号生成表
    ///</summary>
  public partial interface IShopCodeGenerationRepository : IBaseRepository<ShopCodeGeneration>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///海报素材
    ///</summary>
  public partial interface IShopsPosterRepository : IBaseRepository<ShopsPoster>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///角色表
    ///</summary>
  public partial interface IRolesRepository : IBaseRepository<Roles>
  {
  }
}
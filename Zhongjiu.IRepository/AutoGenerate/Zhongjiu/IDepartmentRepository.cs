﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///部门表
    ///</summary>
  public partial interface IDepartmentRepository : IBaseRepository<Department>
  {
  }
}
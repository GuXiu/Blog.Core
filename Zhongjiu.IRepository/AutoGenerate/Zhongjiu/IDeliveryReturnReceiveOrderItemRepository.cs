﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配退收货单明细
    ///</summary>
  public partial interface IDeliveryReturnReceiveOrderItemRepository : IBaseRepository<DeliveryReturnReceiveOrderItem>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层商品表
    ///</summary>
  public partial interface IFloorProductsRepository : IBaseRepository<FloorProducts>
  {
  }
}
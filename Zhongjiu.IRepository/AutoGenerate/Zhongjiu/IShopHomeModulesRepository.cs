﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺首页模块表
    ///</summary>
  public partial interface IShopHomeModulesRepository : IBaseRepository<ShopHomeModules>
  {
  }
}
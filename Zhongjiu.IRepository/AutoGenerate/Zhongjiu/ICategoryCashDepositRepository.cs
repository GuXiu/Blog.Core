﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分类保证金设置表
    ///</summary>
  public partial interface ICategoryCashDepositRepository : IBaseRepository<CategoryCashDeposit>
  {
  }
}
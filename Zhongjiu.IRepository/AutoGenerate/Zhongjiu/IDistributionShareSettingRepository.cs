﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销市场分享设置表
    ///</summary>
  public partial interface IDistributionShareSettingRepository : IBaseRepository<DistributionShareSetting>
  {
  }
}
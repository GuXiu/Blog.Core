﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单锁定设置
    ///</summary>
  public partial interface ILockOrdersRepository : IBaseRepository<LockOrders>
  {
  }
}
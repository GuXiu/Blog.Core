﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员设置表
    ///</summary>
  public partial interface IOfflineVipSetRepository : IBaseRepository<OfflineVipSet>
  {
  }
}
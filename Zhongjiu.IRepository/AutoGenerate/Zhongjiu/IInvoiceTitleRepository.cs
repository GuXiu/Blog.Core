﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///发票抬头表
    ///</summary>
  public partial interface IInvoiceTitleRepository : IBaseRepository<InvoiceTitle>
  {
  }
}
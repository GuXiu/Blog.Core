﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拼团活动项
    ///</summary>
  public partial interface IFightGroupActiveItemRepository : IBaseRepository<FightGroupActiveItem>
  {
  }
}
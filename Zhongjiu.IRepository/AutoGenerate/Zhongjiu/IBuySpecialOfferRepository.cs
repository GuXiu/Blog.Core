﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///特价
    ///</summary>
  public partial interface IBuySpecialOfferRepository : IBaseRepository<BuySpecialOffer>
  {
  }
}
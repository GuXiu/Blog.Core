﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单CPS记录表
    ///</summary>
  public partial interface ICpsCookiesListRepository : IBaseRepository<CpsCookiesList>
  {
  }
}
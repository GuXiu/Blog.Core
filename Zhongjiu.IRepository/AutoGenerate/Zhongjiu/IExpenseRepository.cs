﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///费用表
    ///</summary>
  public partial interface IExpenseRepository : IBaseRepository<Expense>
  {
  }
}
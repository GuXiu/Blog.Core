﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///中酒云码库
    ///</summary>
  public partial interface ICloudCodeRepository : IBaseRepository<CloudCode>
  {
  }
}
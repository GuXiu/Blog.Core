﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///(线下订单支付表)暂时不用
    ///</summary>
  public partial interface IOffLineOrderPayRepository : IBaseRepository<OffLineOrderPay>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///区域支付方式配置
    ///</summary>
  public partial interface IReceivingAddressConfigRepository : IBaseRepository<ReceivingAddressConfig>
  {
  }
}
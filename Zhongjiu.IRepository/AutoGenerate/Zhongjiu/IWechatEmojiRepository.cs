﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信表情
    ///</summary>
  public partial interface IWechatEmojiRepository : IBaseRepository<WechatEmoji>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员等级表
    ///</summary>
  public partial interface IMemberGradeRepository : IBaseRepository<MemberGrade>
  {
  }
}
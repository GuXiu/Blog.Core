﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///入驻设置
    ///</summary>
  public partial interface ISettledRepository : IBaseRepository<Settled>
  {
  }
}
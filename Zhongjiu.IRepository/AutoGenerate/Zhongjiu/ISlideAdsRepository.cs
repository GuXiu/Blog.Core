﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城、店铺首页
    ///</summary>
  public partial interface ISlideAdsRepository : IBaseRepository<SlideAds>
  {
  }
}
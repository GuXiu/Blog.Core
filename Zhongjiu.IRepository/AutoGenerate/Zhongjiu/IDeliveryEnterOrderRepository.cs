﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送入库单
    ///</summary>
  public partial interface IDeliveryEnterOrderRepository : IBaseRepository<DeliveryEnterOrder>
  {
  }
}
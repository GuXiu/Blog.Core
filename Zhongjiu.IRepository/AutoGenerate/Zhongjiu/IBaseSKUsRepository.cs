﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统基库SKU表
    ///</summary>
  public partial interface IBaseSKUsRepository : IBaseRepository<BaseSKUs>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单投诉表
    ///</summary>
  public partial interface IOrderComplaintsRepository : IBaseRepository<OrderComplaints>
  {
  }
}
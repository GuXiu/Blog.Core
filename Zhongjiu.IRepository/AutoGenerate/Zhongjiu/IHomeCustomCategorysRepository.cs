﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页主页自定义类别表
    ///</summary>
  public partial interface IHomeCustomCategorysRepository : IBaseRepository<HomeCustomCategorys>
  {
  }
}
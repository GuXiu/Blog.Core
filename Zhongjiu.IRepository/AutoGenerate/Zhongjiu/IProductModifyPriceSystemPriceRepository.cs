﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品调价单会员等级价调价明细
    ///</summary>
  public partial interface IProductModifyPriceSystemPriceRepository : IBaseRepository<ProductModifyPriceSystemPrice>
  {
  }
}
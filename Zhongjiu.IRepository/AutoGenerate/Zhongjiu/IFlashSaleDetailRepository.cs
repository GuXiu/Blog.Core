﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限时购详细表
    ///</summary>
  public partial interface IFlashSaleDetailRepository : IBaseRepository<FlashSaleDetail>
  {
  }
}
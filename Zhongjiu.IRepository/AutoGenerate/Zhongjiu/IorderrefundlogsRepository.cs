﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单售后日志表
    ///</summary>
  public partial interface IorderrefundlogsRepository : IBaseRepository<orderrefundlogs>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录
    ///</summary>
  public partial interface IMembershipMarketingRecordRepository : IBaseRepository<MembershipMarketingRecord>
  {
  }
}
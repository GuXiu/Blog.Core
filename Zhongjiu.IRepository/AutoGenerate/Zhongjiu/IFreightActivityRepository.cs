﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///免运费活动
    ///</summary>
  public partial interface IFreightActivityRepository : IBaseRepository<FreightActivity>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券设置表
    ///</summary>
  public partial interface ICouponSettingRepository : IBaseRepository<CouponSetting>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层品牌表
    ///</summary>
  public partial interface IFloorBrandsRepository : IBaseRepository<FloorBrands>
  {
  }
}
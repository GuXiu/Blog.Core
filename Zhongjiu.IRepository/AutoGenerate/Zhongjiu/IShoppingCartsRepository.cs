﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///购物车表
    ///</summary>
  public partial interface IShoppingCartsRepository : IBaseRepository<ShoppingCarts>
  {
  }
}
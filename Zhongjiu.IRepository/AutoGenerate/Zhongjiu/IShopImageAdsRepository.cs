﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺轮播图表
    ///</summary>
  public partial interface IShopImageAdsRepository : IBaseRepository<ShopImageAds>
  {
  }
}
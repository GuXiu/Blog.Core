﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品卡批次
    ///</summary>
  public partial interface IGiftCardBatchesRepository : IBaseRepository<GiftCardBatches>
  {
  }
}
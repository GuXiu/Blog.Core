﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///供应商对店铺商品供货资格主表
    ///</summary>
  public partial interface ISupplierProductsRepository : IBaseRepository<SupplierProducts>
  {
  }
}
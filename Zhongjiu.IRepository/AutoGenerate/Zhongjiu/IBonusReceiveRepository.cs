﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///红包领取记录表
    ///</summary>
  public partial interface IBonusReceiveRepository : IBaseRepository<BonusReceive>
  {
  }
}
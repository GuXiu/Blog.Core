﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品提成设置
    ///</summary>
  public partial interface ISalesCommissionProductRepository : IBaseRepository<SalesCommissionProduct>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///平台分类表
    ///</summary>
  public partial interface ICategoriesRepository : IBaseRepository<Categories>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信小程序用户FormId信息表
    ///</summary>
  public partial interface IWeChatFormIdRepository : IBaseRepository<WeChatFormId>
  {
  }
}
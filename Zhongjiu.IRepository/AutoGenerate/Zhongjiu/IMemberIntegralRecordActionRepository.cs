﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///积分兑换虚拟物记录表
    ///</summary>
  public partial interface IMemberIntegralRecordActionRepository : IBaseRepository<MemberIntegralRecordAction>
  {
  }
}
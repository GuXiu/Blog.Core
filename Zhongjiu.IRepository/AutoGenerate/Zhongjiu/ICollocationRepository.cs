﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///组合购表
    ///</summary>
  public partial interface ICollocationRepository : IBaseRepository<Collocation>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///门店注册二维码、接收消息二维码按照微信公众号原始Id备份，以备门店变换绑定公众号从此表中直接取，无需再调用微信接口生成数量有限的永久带参数的二维码
    ///</summary>
  public partial interface IShopQrCodeBkRepository : IBaseRepository<ShopQrCodeBk>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信基础信息表
    ///</summary>
  public partial interface IWeiXinBasicRepository : IBaseRepository<WeiXinBasic>
  {
  }
}
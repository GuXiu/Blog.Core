﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销招募计划表
    ///</summary>
  public partial interface IRecruitPlanRepository : IBaseRepository<RecruitPlan>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///群发分享 优惠券活动关联表
    ///</summary>
  public partial interface ICouponShareBatchDetailRepository : IBaseRepository<CouponShareBatchDetail>
  {
  }
}
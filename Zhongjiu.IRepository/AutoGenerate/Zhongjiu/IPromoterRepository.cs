﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销员表
    ///</summary>
  public partial interface IPromoterRepository : IBaseRepository<Promoter>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///金蝶ERP推送销退记录表
    ///</summary>
  public partial interface IShopEasKingdeeSaleReturnRepository : IBaseRepository<ShopEasKingdeeSaleReturn>
  {
  }
}
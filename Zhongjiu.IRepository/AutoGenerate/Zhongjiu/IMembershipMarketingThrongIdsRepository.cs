﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销 人群Id关系表
    ///</summary>
  public partial interface IMembershipMarketingThrongIdsRepository : IBaseRepository<MembershipMarketingThrongIds>
  {
  }
}
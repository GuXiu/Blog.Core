﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单支付记录表
    ///</summary>
  public partial interface IOrderPayRepository : IBaseRepository<OrderPay>
  {
  }
}
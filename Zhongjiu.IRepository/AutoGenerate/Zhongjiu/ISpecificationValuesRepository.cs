﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///规格值表
    ///</summary>
  public partial interface ISpecificationValuesRepository : IBaseRepository<SpecificationValues>
  {
  }
}
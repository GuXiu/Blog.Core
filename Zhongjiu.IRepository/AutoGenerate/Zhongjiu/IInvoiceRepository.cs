﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///发票表
    ///</summary>
  public partial interface IInvoiceRepository : IBaseRepository<Invoice>
  {
  }
}
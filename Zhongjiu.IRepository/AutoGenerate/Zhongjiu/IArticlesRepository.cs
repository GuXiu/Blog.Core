﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统文章表
    ///</summary>
  public partial interface IArticlesRepository : IBaseRepository<Articles>
  {
  }
}
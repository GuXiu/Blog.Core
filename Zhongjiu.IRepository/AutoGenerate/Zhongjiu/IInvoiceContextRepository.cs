﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///发票内容表
    ///</summary>
  public partial interface IInvoiceContextRepository : IBaseRepository<InvoiceContext>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺表
    ///</summary>
  public partial interface IShopsRepository : IBaseRepository<Shops>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///App消息推送模板表
    ///</summary>
  public partial interface IPushMessageTemplateRepository : IBaseRepository<PushMessageTemplate>
  {
  }
}
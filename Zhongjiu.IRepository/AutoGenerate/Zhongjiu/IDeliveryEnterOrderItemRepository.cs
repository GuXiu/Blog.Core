﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送入库单明细
    ///</summary>
  public partial interface IDeliveryEnterOrderItemRepository : IBaseRepository<DeliveryEnterOrderItem>
  {
  }
}
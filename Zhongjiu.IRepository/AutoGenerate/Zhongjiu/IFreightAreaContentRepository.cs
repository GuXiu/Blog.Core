﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///运费模板详细表
    ///</summary>
  public partial interface IFreightAreaContentRepository : IBaseRepository<FreightAreaContent>
  {
  }
}
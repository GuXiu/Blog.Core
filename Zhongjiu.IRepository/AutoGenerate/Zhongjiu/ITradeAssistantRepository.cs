﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///营业员信息表
    ///</summary>
  public partial interface ITradeAssistantRepository : IBaseRepository<TradeAssistant>
  {
  }
}
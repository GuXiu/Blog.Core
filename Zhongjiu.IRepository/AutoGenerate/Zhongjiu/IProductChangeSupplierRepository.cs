﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品供应商变更表
    ///</summary>
  public partial interface IProductChangeSupplierRepository : IBaseRepository<ProductChangeSupplier>
  {
  }
}
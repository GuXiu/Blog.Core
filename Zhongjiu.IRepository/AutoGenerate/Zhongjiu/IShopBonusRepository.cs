﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺红包表
    ///</summary>
  public partial interface IShopBonusRepository : IBaseRepository<ShopBonus>
  {
  }
}
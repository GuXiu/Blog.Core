﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品调拨表
    ///</summary>
  public partial interface IOffLineInventorySchedulingRepository : IBaseRepository<OffLineInventoryScheduling>
  {
  }
}
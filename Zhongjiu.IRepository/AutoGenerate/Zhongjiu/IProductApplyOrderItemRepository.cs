﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///要货申请单明细
    ///</summary>
  public partial interface IProductApplyOrderItemRepository : IBaseRepository<ProductApplyOrderItem>
  {
  }
}
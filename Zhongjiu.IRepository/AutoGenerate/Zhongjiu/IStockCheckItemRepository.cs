﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///库存盘点明细
    ///</summary>
  public partial interface IStockCheckItemRepository : IBaseRepository<StockCheckItem>
  {
  }
}
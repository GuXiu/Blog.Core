﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品调移动平均价表
    ///</summary>
  public partial interface IOffLineMovingAverageRepository : IBaseRepository<OffLineMovingAverage>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送差异单
    ///</summary>
  public partial interface IDeliveryDifferOrderRepository : IBaseRepository<DeliveryDifferOrder>
  {
  }
}
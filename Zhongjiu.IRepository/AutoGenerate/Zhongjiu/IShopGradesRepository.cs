﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺等级表
    ///</summary>
  public partial interface IShopGradesRepository : IBaseRepository<ShopGrades>
  {
  }
}
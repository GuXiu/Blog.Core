﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分仓发货
    ///</summary>
  public partial interface IWarehouseDeliveryRepository : IBaseRepository<WarehouseDelivery>
  {
  }
}
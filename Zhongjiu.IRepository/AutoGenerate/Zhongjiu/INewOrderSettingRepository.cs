﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺新订单提醒设置
    ///</summary>
  public partial interface INewOrderSettingRepository : IBaseRepository<NewOrderSetting>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购订单明细
    ///</summary>
  public partial interface IPurchaseOrderItemRepository : IBaseRepository<PurchaseOrderItem>
  {
  }
}
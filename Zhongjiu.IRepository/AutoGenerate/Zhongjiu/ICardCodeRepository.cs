﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单卡券记录表
    ///</summary>
  public partial interface ICardCodeRepository : IBaseRepository<CardCode>
  {
  }
}
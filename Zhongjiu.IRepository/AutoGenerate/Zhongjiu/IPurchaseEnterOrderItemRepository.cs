﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购入库订单明细
    ///</summary>
  public partial interface IPurchaseEnterOrderItemRepository : IBaseRepository<PurchaseEnterOrderItem>
  {
  }
}
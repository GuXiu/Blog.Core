﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺购买的营销服务表(暂时不用，目前店铺营销不需要购买)
    ///</summary>
  public partial interface IActiveMarketServiceRepository : IBaseRepository<ActiveMarketService>
  {
  }
}
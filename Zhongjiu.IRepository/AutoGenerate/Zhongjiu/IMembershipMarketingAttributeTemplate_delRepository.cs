﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销 属性模板
    ///</summary>
  public partial interface IMembershipMarketingAttributeTemplate_delRepository : IBaseRepository<MembershipMarketingAttributeTemplate_del>
  {
  }
}
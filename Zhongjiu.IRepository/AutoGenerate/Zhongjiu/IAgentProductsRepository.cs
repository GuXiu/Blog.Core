﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销商品代理记录表
    ///</summary>
  public partial interface IAgentProductsRepository : IBaseRepository<AgentProducts>
  {
  }
}
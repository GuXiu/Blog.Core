﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///结算预付款表
    ///</summary>
  public partial interface IAccountPurchaseAgreementRepository : IBaseRepository<AccountPurchaseAgreement>
  {
  }
}
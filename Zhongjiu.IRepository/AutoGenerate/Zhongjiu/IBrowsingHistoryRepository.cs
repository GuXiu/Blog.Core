﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///浏览历史记录表
    ///</summary>
  public partial interface IBrowsingHistoryRepository : IBaseRepository<BrowsingHistory>
  {
  }
}
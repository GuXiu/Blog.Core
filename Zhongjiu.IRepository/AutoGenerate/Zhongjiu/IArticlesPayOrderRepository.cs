﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///文章支付表
    ///</summary>
  public partial interface IArticlesPayOrderRepository : IBaseRepository<ArticlesPayOrder>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品供应商变更详情表
    ///</summary>
  public partial interface IProductChangeSupplierDetailRepository : IBaseRepository<ProductChangeSupplierDetail>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单表
    ///</summary>
  public partial interface IOffLineOrdersRepository : IBaseRepository<OffLineOrders>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///要货申请单
    ///</summary>
  public partial interface IProductApplyOrderRepository : IBaseRepository<ProductApplyOrder>
  {
  }
}
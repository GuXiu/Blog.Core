﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拆零商品关系表
    ///</summary>
  public partial interface IProductChaiLingRelationRepository : IBaseRepository<ProductChaiLingRelation>
  {
  }
}
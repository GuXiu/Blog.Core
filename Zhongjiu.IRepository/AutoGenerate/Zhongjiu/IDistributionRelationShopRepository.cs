﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送中心店铺关联表
    ///</summary>
  public partial interface IDistributionRelationShopRepository : IBaseRepository<DistributionRelationShop>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///收藏商品表
    ///</summary>
  public partial interface IFavoritesRepository : IBaseRepository<Favorites>
  {
  }
}
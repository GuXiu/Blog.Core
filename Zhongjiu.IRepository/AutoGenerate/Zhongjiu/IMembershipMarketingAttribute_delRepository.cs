﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员营销 属性值
    ///</summary>
  public partial interface IMembershipMarketingAttribute_delRepository : IBaseRepository<MembershipMarketingAttribute_del>
  {
  }
}
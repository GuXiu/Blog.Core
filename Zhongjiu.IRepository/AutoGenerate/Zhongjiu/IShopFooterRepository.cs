﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺底部设置表
    ///</summary>
  public partial interface IShopFooterRepository : IBaseRepository<ShopFooter>
  {
  }
}
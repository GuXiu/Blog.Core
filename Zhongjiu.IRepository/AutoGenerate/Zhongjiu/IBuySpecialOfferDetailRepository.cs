﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///特价商品详情表
    ///</summary>
  public partial interface IBuySpecialOfferDetailRepository : IBaseRepository<BuySpecialOfferDetail>
  {
  }
}
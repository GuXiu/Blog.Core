﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员人群 店铺Id关系表
    ///</summary>
  public partial interface IMembersThrongShopIdsRepository : IBaseRepository<MembersThrongShopIds>
  {
  }
}
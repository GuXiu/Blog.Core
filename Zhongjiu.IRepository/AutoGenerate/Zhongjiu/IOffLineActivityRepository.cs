﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下活动表（弃用）
    ///</summary>
  public partial interface IOffLineActivityRepository : IBaseRepository<OffLineActivity>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户微信UnionId和店铺关系表
    ///</summary>
  public partial interface IMemberUnionIdsShopIdsRepository : IBaseRepository<MemberUnionIdsShopIds>
  {
  }
}
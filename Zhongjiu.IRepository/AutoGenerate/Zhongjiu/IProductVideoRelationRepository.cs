﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品视频关联商品表
    ///</summary>
  public partial interface IProductVideoRelationRepository : IBaseRepository<ProductVideoRelation>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下商品品牌表（弃用）
    ///</summary>
  public partial interface IOffLineBrandsRepository : IBaseRepository<OffLineBrands>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///管理员表
    ///</summary>
  public partial interface IManagersRepository : IBaseRepository<Managers>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///图库表
    ///</summary>
  public partial interface IPhotoSpaceRepository : IBaseRepository<PhotoSpace>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺投诉表
    ///</summary>
  public partial interface IShopReportsRepository : IBaseRepository<ShopReports>
  {
  }
}
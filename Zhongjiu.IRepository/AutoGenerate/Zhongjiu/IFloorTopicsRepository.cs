﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层专题表
    ///</summary>
  public partial interface IFloorTopicsRepository : IBaseRepository<FloorTopics>
  {
  }
}
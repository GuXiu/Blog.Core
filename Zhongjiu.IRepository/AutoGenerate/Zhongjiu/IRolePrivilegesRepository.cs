﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统权限和角色的关联表
    ///</summary>
  public partial interface IRolePrivilegesRepository : IBaseRepository<RolePrivileges>
  {
  }
}
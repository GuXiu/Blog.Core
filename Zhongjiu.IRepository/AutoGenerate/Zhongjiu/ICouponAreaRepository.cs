﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券领取区域
    ///</summary>
  public partial interface ICouponAreaRepository : IBaseRepository<CouponArea>
  {
  }
}
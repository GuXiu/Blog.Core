﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员OpenID备份表
    ///</summary>
  public partial interface IMemberOpenIds_qqbakRepository : IBaseRepository<MemberOpenIds_qqbak>
  {
  }
}
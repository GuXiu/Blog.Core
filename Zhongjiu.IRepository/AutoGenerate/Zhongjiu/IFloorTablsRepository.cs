﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层Table表
    ///</summary>
  public partial interface IFloorTablsRepository : IBaseRepository<FloorTabls>
  {
  }
}
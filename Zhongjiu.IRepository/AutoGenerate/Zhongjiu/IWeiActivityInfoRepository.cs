﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡表
    ///</summary>
  public partial interface IWeiActivityInfoRepository : IBaseRepository<WeiActivityInfo>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员资金流水表
    ///</summary>
  public partial interface ICapitalDetailRepository : IBaseRepository<CapitalDetail>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销申请设置表
    ///</summary>
  public partial interface IRecruitSettingRepository : IBaseRepository<RecruitSetting>
  {
  }
}
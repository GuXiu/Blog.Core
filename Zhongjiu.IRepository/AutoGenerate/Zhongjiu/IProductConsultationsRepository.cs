﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品咨询表
    ///</summary>
  public partial interface IProductConsultationsRepository : IBaseRepository<ProductConsultations>
  {
  }
}
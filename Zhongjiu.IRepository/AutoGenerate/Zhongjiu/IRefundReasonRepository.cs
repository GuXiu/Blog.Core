﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///售后原因
    ///</summary>
  public partial interface IRefundReasonRepository : IBaseRepository<RefundReason>
  {
  }
}
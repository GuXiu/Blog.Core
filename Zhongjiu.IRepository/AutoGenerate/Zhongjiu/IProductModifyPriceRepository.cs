﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///产品调价单
    ///</summary>
  public partial interface IProductModifyPriceRepository : IBaseRepository<ProductModifyPrice>
  {
  }
}
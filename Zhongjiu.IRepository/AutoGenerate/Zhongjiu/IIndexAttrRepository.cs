﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品属性索引表
    ///</summary>
  public partial interface IIndexAttrRepository : IBaseRepository<IndexAttr>
  {
  }
}
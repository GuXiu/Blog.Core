﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///结算表
    ///</summary>
  public partial interface IAccountsRepository : IBaseRepository<Accounts>
  {
  }
}
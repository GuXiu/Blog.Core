﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///签到配置表
    ///</summary>
  public partial interface ISiteSignInConfigRepository : IBaseRepository<SiteSignInConfig>
  {
  }
}
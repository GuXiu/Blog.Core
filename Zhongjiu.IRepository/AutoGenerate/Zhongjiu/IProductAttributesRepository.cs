﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品和属性的中间表
    ///</summary>
  public partial interface IProductAttributesRepository : IBaseRepository<ProductAttributes>
  {
  }
}
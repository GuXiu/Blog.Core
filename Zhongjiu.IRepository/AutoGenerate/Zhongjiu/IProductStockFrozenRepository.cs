﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///商品库存冻结表
    ///</summary>
  public partial interface IProductStockFrozenRepository : IBaseRepository<ProductStockFrozen>
  {
  }
}
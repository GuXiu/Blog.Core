﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统商城首页导航表
    ///</summary>
  public partial interface IBannersRepository : IBaseRepository<Banners>
  {
  }
}
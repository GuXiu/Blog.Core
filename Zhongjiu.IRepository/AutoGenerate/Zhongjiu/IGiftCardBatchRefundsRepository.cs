﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///礼品卡退款
    ///</summary>
  public partial interface IGiftCardBatchRefundsRepository : IBaseRepository<GiftCardBatchRefunds>
  {
  }
}
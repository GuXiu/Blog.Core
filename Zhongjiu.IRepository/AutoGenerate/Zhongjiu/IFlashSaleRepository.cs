﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///限时购表
    ///</summary>
  public partial interface IFlashSaleRepository : IBaseRepository<FlashSale>
  {
  }
}
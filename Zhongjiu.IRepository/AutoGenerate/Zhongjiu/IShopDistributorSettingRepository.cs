﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺分销设置表
    ///</summary>
  public partial interface IShopDistributorSettingRepository : IBaseRepository<ShopDistributorSetting>
  {
  }
}
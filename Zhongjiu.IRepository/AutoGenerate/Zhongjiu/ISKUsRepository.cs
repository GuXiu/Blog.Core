﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///系统SKU表
    ///</summary>
  public partial interface ISKUsRepository : IBaseRepository<SKUs>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配退单
    ///</summary>
  public partial interface IDeliveryReturnOrderRepository : IBaseRepository<DeliveryReturnOrder>
  {
  }
}
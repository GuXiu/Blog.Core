﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///否支持货到付款设置表
    ///</summary>
  public partial interface IPaymentConfigRepository : IBaseRepository<PaymentConfig>
  {
  }
}
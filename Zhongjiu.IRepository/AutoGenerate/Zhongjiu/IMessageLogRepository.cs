﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///短信发送记录表
    ///</summary>
  public partial interface IMessageLogRepository : IBaseRepository<MessageLog>
  {
  }
}
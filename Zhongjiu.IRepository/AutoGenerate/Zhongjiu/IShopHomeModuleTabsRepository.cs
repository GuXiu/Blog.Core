﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///地阿奴首页模块标签表
    ///</summary>
  public partial interface IShopHomeModuleTabsRepository : IBaseRepository<ShopHomeModuleTabs>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///采购预采订单
    ///</summary>
  public partial interface IPurchaseBeforehandOrderRepository : IBaseRepository<PurchaseBeforehandOrder>
  {
  }
}
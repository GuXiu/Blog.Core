﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///运费模板表
    ///</summary>
  public partial interface IFreightTemplateRepository : IBaseRepository<FreightTemplate>
  {
  }
}
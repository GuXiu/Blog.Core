﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///买赠商品详情表
    ///</summary>
  public partial interface IBuyGiftDetailRepository : IBaseRepository<BuyGiftDetail>
  {
  }
}
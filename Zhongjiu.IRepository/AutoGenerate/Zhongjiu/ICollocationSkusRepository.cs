﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///组合购SKU表
    ///</summary>
  public partial interface ICollocationSkusRepository : IBaseRepository<CollocationSkus>
  {
  }
}
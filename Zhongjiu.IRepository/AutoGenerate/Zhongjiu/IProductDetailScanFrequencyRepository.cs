﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///电子价签商品二维码扫码记录表
    ///</summary>
  public partial interface IProductDetailScanFrequencyRepository : IBaseRepository<ProductDetailScanFrequency>
  {
  }
}
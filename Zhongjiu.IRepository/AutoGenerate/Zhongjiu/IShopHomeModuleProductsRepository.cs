﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺首页的楼层模板商品表
    ///</summary>
  public partial interface IShopHomeModuleProductsRepository : IBaseRepository<ShopHomeModuleProducts>
  {
  }
}
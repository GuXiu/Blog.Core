﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///费用设置表
    ///</summary>
  public partial interface IExpenseSetRepository : IBaseRepository<ExpenseSet>
  {
  }
}
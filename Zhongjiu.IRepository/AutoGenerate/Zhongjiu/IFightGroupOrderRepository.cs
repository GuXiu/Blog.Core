﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///拼团订单
    ///</summary>
  public partial interface IFightGroupOrderRepository : IBaseRepository<FightGroupOrder>
  {
  }
}
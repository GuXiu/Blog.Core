﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///我要推广设置表
    ///</summary>
  public partial interface IInviteRuleRepository : IBaseRepository<InviteRule>
  {
  }
}
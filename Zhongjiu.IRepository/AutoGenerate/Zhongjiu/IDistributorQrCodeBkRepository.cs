﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///分销员在所属门店下的二维码，以备门店变换公众号，分销员二维码直接从此表读取二维码，无需再调用微信链接生成数量有限的永久的带参数的二维码
    ///</summary>
  public partial interface IDistributorQrCodeBkRepository : IBaseRepository<DistributorQrCodeBk>
  {
  }
}
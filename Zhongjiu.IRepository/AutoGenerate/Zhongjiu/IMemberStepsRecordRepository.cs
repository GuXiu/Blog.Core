﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///用户一步直邀步数记录
    ///</summary>
  public partial interface IMemberStepsRecordRepository : IBaseRepository<MemberStepsRecord>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///订单表
    ///</summary>
  public partial interface IOrdersRepository : IBaseRepository<Orders>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题表
    ///</summary>
  public partial interface ICouponShareProblemRepository : IBaseRepository<CouponShareProblem>
  {
  }
}
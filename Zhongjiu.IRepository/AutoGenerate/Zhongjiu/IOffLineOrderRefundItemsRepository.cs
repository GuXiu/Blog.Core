﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单退货明细表
    ///</summary>
  public partial interface IOffLineOrderRefundItemsRepository : IBaseRepository<OffLineOrderRefundItems>
  {
  }
}
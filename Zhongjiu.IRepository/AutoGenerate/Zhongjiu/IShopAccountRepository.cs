﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺资金表
    ///</summary>
  public partial interface IShopAccountRepository : IBaseRepository<ShopAccount>
  {
  }
}
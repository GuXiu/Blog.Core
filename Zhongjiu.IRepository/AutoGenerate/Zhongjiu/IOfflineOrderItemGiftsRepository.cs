﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///线下订单赠品表
    ///</summary>
  public partial interface IOfflineOrderItemGiftsRepository : IBaseRepository<OfflineOrderItemGifts>
  {
  }
}
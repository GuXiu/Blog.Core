﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满减活动详情表
    ///</summary>
  public partial interface IFullSubtractDetailRepository : IBaseRepository<FullSubtractDetail>
  {
  }
}
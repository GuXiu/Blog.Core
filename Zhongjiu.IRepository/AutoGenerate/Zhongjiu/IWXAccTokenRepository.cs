﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///微信TOKEN表
    ///</summary>
  public partial interface IWXAccTokenRepository : IBaseRepository<WXAccToken>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///提成设置，销售佣金
    ///</summary>
  public partial interface ISalesCommissionRepository : IBaseRepository<SalesCommission>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///会员签到记录表
    ///</summary>
  public partial interface IMemberSignInRepository : IBaseRepository<MemberSignIn>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///满赠活动表
    ///</summary>
  public partial interface IFullGiftRepository : IBaseRepository<FullGift>
  {
  }
}
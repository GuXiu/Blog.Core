﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///待结算订单表
    ///</summary>
  public partial interface IPendingSettlementOrdersRepository : IBaseRepository<PendingSettlementOrders>
  {
  }
}
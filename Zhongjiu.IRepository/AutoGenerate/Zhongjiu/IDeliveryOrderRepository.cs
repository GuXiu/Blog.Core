﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///配送单
    ///</summary>
  public partial interface IDeliveryOrderRepository : IBaseRepository<DeliveryOrder>
  {
  }
}
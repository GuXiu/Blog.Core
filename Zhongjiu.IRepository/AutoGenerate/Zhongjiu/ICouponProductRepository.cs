﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///优惠券 商品适用，多商品关联表
    ///</summary>
  public partial interface ICouponProductRepository : IBaseRepository<CouponProduct>
  {
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///店铺扩展表，存放一些不常用信息
    ///</summary>
  public partial interface IShopsExpansionRepository : IBaseRepository<ShopsExpansion>
  {
  }
}
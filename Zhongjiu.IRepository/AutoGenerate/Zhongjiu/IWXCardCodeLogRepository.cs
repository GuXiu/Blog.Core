﻿using Zhongjiu.Model.Zhongjiu;
namespace Zhongjiu.IRepository.Zhongjiu
{
    ///<summary>
    ///卡卷表
    ///</summary>
  public partial interface IWXCardCodeLogRepository : IBaseRepository<WXCardCodeLog>
  {
  }
}
﻿

namespace Zhongjiu.Common
{
    public static class StaticPayInfo
    {
        /// <summary>
        /// 商户号
        /// </summary>
        public readonly static string MERCHANTID = Appsettings.app(new string[] { "PayInfo", "MERCHANTID" }).ToString();
        /// <summary>
        /// 柜台号
        /// </summary>
        public readonly static string POSID = Appsettings.app(new string[] { "PayInfo", "POSID" }).ToString();
        /// <summary>
        /// 分行号
        /// </summary>
        public readonly static string BRANCHID = Appsettings.app(new string[] { "PayInfo", "BRANCHID" }).ToString();
        /// <summary>
        /// 公钥
        /// </summary>
        public readonly static string pubKey = Appsettings.app(new string[] { "PayInfo", "pubKey" }).ToString(); 
        /// <summary>
        /// 操作员号
        /// </summary>
        public readonly static string USER_ID = Appsettings.app(new string[] { "PayInfo", "USER_ID" }).ToString();
        /// <summary>
        /// 密码
        /// </summary>
        public readonly static string PASSWORD = Appsettings.app(new string[] { "PayInfo", "PASSWORD" }).ToString();
        /// <summary>
        /// 外联平台通讯地址
        /// </summary>
        public readonly static string OutAddress = Appsettings.app(new string[] { "PayInfo", "OutAddress" }).ToString();
        
    }
}

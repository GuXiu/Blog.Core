﻿using System;

namespace Zhongjiu.Common
{
    public static class ExceptionExtensions
    {
        public static void TrueThrow(this bool expression, string msgFormatter, params object[] param)
        {
            if (expression)
            {
                throw new TipException(string.Format(msgFormatter, param));
            }
        }

        public static Exception GetInnerException(this Exception ex)
        {
            if (ex.InnerException != null)
            {
                return ex.InnerException.GetInnerException();
            }
            return ex;
        }
    }
}

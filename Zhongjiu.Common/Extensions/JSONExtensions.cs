﻿using Newtonsoft.Json;
using System;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace Zhongjiu.Common
{
    public static class JSONExtensions
    {
        public static string ToJson(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return json;
        }
        public static T ToObj<T>(this string str,JsonSerializerSettings setting=null)
        {
            var obj = JsonConvert.DeserializeObject<T>(str, setting);
            return obj;
        }
        public static object ToObj(this string str,Type t)
        {
            var result=JsonConvert.DeserializeObject(str, t);
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zhongjiu.Common
{
    public class TipException : ApplicationException
    {
        public TipException()
        {

        }
        public TipException(string message) : base(message)
        {

        }
        public TipException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zhongjiu.Common
{
  /// <summary>
  /// appsettings.json操作类
  /// </summary>
  public class Appsettings
  {
    public static IConfiguration Configuration { get; private set; }
    //static string contentPath { get; set; }

    /// <summary>
    /// private 禁止实例化
    /// </summary>
    /// <param name="contentPath"></param>
    private Appsettings()//string contentPath
    {
      //string Path = "appsettings.json";

      ////如果你把配置文件 是 根据环境变量来分开了，可以这样写
      ////Path = $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json";

      //Configuration = new ConfigurationBuilder()
      //   .SetBasePath(contentPath)
      //   .Add(new JsonConfigurationSource { Path = Path, Optional = false, ReloadOnChange = true })//这样的话，可以直接读目录里的json文件，而不是 bin 文件夹下的，所以不用修改复制属性
      //   .Build();
    }
    public static void Init(string contentPath)
    {
      string Path = "appsettings.json";

      //如果你把配置文件 是 根据环境变量来分开了，可以这样写
      //Path = $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json";

      Configuration = new ConfigurationBuilder()
         .SetBasePath(contentPath)
         .Add(new JsonConfigurationSource { Path = Path, Optional = false, ReloadOnChange = true })//这样的话，可以直接读目录里的json文件，而不是 bin 文件夹下的，所以不用修改复制属性
         .Build();
    }

    //public Appsettings(IConfiguration configuration)
    //{
    //    Configuration = configuration;
    //}
    public static void Init(IConfiguration configuration)
    {
      //ConfigurationBuilder builder = new ConfigurationBuilder();
      //if (Configuration != null) builder.AddConfiguration(Configuration);
      //if (configuration != null) builder.AddConfiguration(configuration);
      //Configuration = builder.Build();
      Configuration = configuration;
    }

    /// <summary>
    /// 获取配置项
    /// app("a:b:c"),app("a","b","c")
    /// </summary>
    /// <param name="sections">节点配置</param>
    /// <returns></returns>
    public static string app(params string[] sections)
    {
      try
      {
        if (sections.Any())
        {
          return Configuration[string.Join(":", sections)];
        }
      }
      catch (Exception) { }
      return "";
    }

    /// <summary>
    /// 递归获取配置信息数组
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sections"></param>
    /// <returns></returns>
    public static List<T> app<T>(params string[] sections)
    {
      return GetSection<List<T>>(string.Join(":", sections));
      //List<T> list = new List<T>();
      //// 引用 Microsoft.Extensions.Configuration.Binder 包
      //Configuration.Bind(string.Join(":", sections), list);
      //return list;
    }

    public static IConfigurationSection GetSection(string section)
    {
      var result = Configuration.GetSection(section);
      return result;
    }

    public static T GetSection<T>(string section)
    {
      var result = Configuration.GetSection(section).Get<T>();
      return result;

      //实现二：where T : new()
      //var result = new T();
      //Configuration.Bind(section, result);
      //return result;
    }
    /// <summary>
    /// 递归获取配置信息数组
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sections"></param>
    /// <returns></returns>
    //public static List<T> appList<T>(params string[] sections)
    //{
    //    List<T> list = new List<T>();
    //    // 引用 Microsoft.Extensions.Configuration.Binder 包
    //    Configuration.Bind(string.Join(":", sections), list);
    //    return list;
    //}
  }
}

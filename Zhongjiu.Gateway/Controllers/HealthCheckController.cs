﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zhongjiu.Common;

namespace Zhongjiu.Gateway.Controllers
{
  //[Authorize(AuthenticationSchemes = Permissions.GWName)]
  [Route("/[controller]/[action]")]
  public class HealthCheckController : ControllerBase
  {
    [HttpGet]
    public IActionResult Index()
    {
      return Ok("GatewayHealth");
    }
  }
}

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Zhongjiu.Common;
using Zhongjiu.Extensions;

namespace Zhongjiu.Gateway
{
  public class Program
  {
    public static void Main(string[] args)
    {
      CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
      var builder = Host.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration((hostingContext, config) =>
        {
          config.AddJsonFile("ocelot.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"ocelot.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true);
          HostBuilderExtension.ConfigureAppConfiguration(hostingContext, config);
        })
        .ConfigureLogging((hostingContext, builder) =>
        {
          builder.Services.AddSerilogSetup();
        })
        .ConfigureWebHostDefaults(webBuilder =>
        {
          //var url = $"http://{Appsettings.app("host")}:{Appsettings.app("port")}";//还未走config赋值方法
          webBuilder.UseStartup<Startup>();//.UseUrls(url);
        });
      return builder;
    }
  }
}

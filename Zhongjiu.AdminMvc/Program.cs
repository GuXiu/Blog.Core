using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Zhongjiu.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Host
.ConfigureAppConfiguration((hostingContext, config) =>
          {
            config.AddJsonFile("OcelotGatewaySet.json", optional: false, reloadOnChange: true);
            HostBuilderExtension.ConfigureAppConfiguration(hostingContext, config);
          }).ConfigureLogging((hostingContext, builder) =>
          {
            builder.AddFilter("System", LogLevel.Error);
            builder.AddFilter("Microsoft", LogLevel.Error);
            builder.SetMinimumLevel(LogLevel.Information);
            builder.Services.AddSerilogSetup();
          });
builder.Services.AddOcelot();


var app = builder.Build();
app.UseOcelot().Wait();

app.Run();

#region old-net5
//namespace Zhongjiu.AdminMvc
//{
//public class Program
//{
//    public static void Main(string[] args)
//    {
//        CreateHostBuilder(args).Build().Run();
//    }

//    public static IHostBuilder CreateHostBuilder(string[] args) =>
//        Host.CreateDefaultBuilder(args)
//            .ConfigureAppConfiguration((hostingContext, config) =>
//            {
//                config.AddJsonFile("OcelotGatewaySet.json", optional: false, reloadOnChange: true);
//            })
//            .ConfigureWebHostDefaults(webBuilder =>
//            {
//                webBuilder.UseStartup<Startup>().UseUrls("http://*:9000");
//            });
//}
//}
#endregion

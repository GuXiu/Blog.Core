﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///可扩充营销服务表
    ///</summary>
  public partial class MarketSettingMetaRepository : BaseRepository<MarketSettingMeta>, IMarketSettingMetaRepository
  {
    public MarketSettingMetaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
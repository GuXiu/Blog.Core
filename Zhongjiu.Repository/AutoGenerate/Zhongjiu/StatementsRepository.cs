﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///结算单表
    ///</summary>
  public partial class StatementsRepository : BaseRepository<Statements>, IStatementsRepository
  {
    public StatementsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
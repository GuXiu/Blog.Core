﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统品牌表（店铺商品、基库商品共用）
    ///</summary>
  public partial class BrandsRepository : BaseRepository<Brands>, IBrandsRepository
  {
    public BrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///菜单、功能表
    ///</summary>
  public partial class PrivilegeRepository : BaseRepository<Privilege>, IPrivilegeRepository
  {
    public PrivilegeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
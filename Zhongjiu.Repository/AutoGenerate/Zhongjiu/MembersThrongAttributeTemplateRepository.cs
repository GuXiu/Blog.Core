﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员人群 属性模板
    ///</summary>
  public partial class MembersThrongAttributeTemplateRepository : BaseRepository<MembersThrongAttributeTemplate>, IMembersThrongAttributeTemplateRepository
  {
    public MembersThrongAttributeTemplateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
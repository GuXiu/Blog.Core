﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层分类表
    ///</summary>
  public partial class FloorCategoriesRepository : BaseRepository<FloorCategories>, IFloorCategoriesRepository
  {
    public FloorCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class WechatGroupChatRepository : BaseRepository<WechatGroupChat>, IWechatGroupChatRepository
  {
    public WechatGroupChatRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
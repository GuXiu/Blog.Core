﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺配送范围坐标表
    ///</summary>
  public partial class ShopLngLatsRepository : BaseRepository<ShopLngLats>, IShopLngLatsRepository
  {
    public ShopLngLatsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
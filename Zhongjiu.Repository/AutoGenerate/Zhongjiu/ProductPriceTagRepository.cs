﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///电子价签商品信息推送记录表
    ///</summary>
  public partial class ProductPriceTagRepository : BaseRepository<ProductPriceTag>, IProductPriceTagRepository
  {
    public ProductPriceTagRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
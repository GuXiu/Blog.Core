﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置店铺信息
    ///</summary>
  public partial class ShopEasKingdeeRepository : BaseRepository<ShopEasKingdee>, IShopEasKingdeeRepository
  {
    public ShopEasKingdeeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///供应商门店商品供货关系表
    ///</summary>
  public partial class SupplierShopProductsSupplyRepository : BaseRepository<SupplierShopProductsSupply>, ISupplierShopProductsSupplyRepository
  {
    public SupplierShopProductsSupplyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
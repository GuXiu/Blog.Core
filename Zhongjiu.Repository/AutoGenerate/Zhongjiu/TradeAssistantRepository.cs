﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///营业员信息表
    ///</summary>
  public partial class TradeAssistantRepository : BaseRepository<TradeAssistant>, ITradeAssistantRepository
  {
    public TradeAssistantRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
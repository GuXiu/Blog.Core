﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层商品表
    ///</summary>
  public partial class FloorProductsRepository : BaseRepository<FloorProducts>, IFloorProductsRepository
  {
    public FloorProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
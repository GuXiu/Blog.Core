﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台分类表
    ///</summary>
  public partial class CategoriesRepository : BaseRepository<Categories>, ICategoriesRepository
  {
    public CategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
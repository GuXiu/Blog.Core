﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单物流信息表
    ///</summary>
  public partial class OrderExpressDataRepository : BaseRepository<OrderExpressData>, IOrderExpressDataRepository
  {
    public OrderExpressDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
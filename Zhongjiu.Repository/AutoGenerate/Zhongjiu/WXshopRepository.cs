﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微店配置表
    ///</summary>
  public partial class WXshopRepository : BaseRepository<WXshop>, IWXshopRepository
  {
    public WXshopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
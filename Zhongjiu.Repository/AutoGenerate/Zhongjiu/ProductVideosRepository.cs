﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品视频表
    ///</summary>
  public partial class ProductVideosRepository : BaseRepository<ProductVideos>, IProductVideosRepository
  {
    public ProductVideosRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
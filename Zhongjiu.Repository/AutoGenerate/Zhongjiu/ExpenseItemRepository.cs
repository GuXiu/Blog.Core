﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///费用明细
    ///</summary>
  public partial class ExpenseItemRepository : BaseRepository<ExpenseItem>, IExpenseItemRepository
  {
    public ExpenseItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
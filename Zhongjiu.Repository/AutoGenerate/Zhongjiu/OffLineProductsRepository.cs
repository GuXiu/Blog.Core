﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品表
    ///</summary>
  public partial class OffLineProductsRepository : BaseRepository<OffLineProducts>, IOffLineProductsRepository
  {
    public OffLineProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
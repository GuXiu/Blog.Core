﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单营销记录表
    ///</summary>
  public partial class MarketingOrderRepository : BaseRepository<MarketingOrder>, IMarketingOrderRepository
  {
    public MarketingOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
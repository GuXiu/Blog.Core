﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限时购详细表
    ///</summary>
  public partial class FlashSaleDetailRepository : BaseRepository<FlashSaleDetail>, IFlashSaleDetailRepository
  {
    public FlashSaleDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
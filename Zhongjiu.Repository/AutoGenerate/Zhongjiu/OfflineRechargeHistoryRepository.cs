﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员卡充值记录表ChargeDetailId
    ///</summary>
  public partial class OfflineRechargeHistoryRepository : BaseRepository<OfflineRechargeHistory>, IOfflineRechargeHistoryRepository
  {
    public OfflineRechargeHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
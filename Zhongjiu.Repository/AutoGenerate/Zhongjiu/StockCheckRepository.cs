﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///库存盘点
    ///</summary>
  public partial class StockCheckRepository : BaseRepository<StockCheck>, IStockCheckRepository
  {
    public StockCheckRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
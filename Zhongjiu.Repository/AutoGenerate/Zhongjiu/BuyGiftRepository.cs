﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///买赠
    ///</summary>
  public partial class BuyGiftRepository : BaseRepository<BuyGift>, IBuyGiftRepository
  {
    public BuyGiftRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///预付款明细
    ///</summary>
  public partial class AdvancesItemRepository : BaseRepository<AdvancesItem>, IAdvancesItemRepository
  {
    public AdvancesItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
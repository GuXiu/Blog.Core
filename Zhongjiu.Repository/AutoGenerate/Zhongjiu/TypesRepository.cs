﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统类型表
    ///</summary>
  public partial class TypesRepository : BaseRepository<Types>, ITypesRepository
  {
    public TypesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
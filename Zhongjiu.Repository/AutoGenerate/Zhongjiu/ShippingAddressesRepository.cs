﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员的收货地址表
    ///</summary>
  public partial class ShippingAddressesRepository : BaseRepository<ShippingAddresses>, IShippingAddressesRepository
  {
    public ShippingAddressesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
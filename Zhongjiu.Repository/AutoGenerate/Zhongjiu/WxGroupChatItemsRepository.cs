﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///群聊设置选择的企业群
    ///</summary>
  public partial class WxGroupChatItemsRepository : BaseRepository<WxGroupChatItems>, IWxGroupChatItemsRepository
  {
    public WxGroupChatItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
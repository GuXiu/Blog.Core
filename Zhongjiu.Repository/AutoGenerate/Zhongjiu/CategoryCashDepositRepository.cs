﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分类保证金设置表
    ///</summary>
  public partial class CategoryCashDepositRepository : BaseRepository<CategoryCashDeposit>, ICategoryCashDepositRepository
  {
    public CategoryCashDepositRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
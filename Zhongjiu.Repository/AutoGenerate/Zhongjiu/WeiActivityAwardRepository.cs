﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡奖品表
    ///</summary>
  public partial class WeiActivityAwardRepository : BaseRepository<WeiActivityAward>, IWeiActivityAwardRepository
  {
    public WeiActivityAwardRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品订单明细
    ///</summary>
  public partial class GiftOrderItemRepository : BaseRepository<GiftOrderItem>, IGiftOrderItemRepository
  {
    public GiftOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
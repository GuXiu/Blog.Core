﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///运费模板详细表
    ///</summary>
  public partial class FreightAreaContentRepository : BaseRepository<FreightAreaContent>, IFreightAreaContentRepository
  {
    public FreightAreaContentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
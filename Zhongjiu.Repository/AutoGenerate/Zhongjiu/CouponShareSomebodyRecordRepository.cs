﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分享给某人记录表
    ///</summary>
  public partial class CouponShareSomebodyRecordRepository : BaseRepository<CouponShareSomebodyRecord>, ICouponShareSomebodyRecordRepository
  {
    public CouponShareSomebodyRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员表
    ///</summary>
  public partial class MembersRepository : BaseRepository<Members>, IMembersRepository
  {
    public MembersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
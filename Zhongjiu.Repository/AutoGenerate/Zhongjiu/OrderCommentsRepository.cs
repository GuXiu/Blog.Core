﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单评价表
    ///</summary>
  public partial class OrderCommentsRepository : BaseRepository<OrderComments>, IOrderCommentsRepository
  {
    public OrderCommentsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
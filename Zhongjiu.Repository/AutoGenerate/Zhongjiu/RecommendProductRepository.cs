﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///推荐商品表
    ///</summary>
  public partial class RecommendProductRepository : BaseRepository<RecommendProduct>, IRecommendProductRepository
  {
    public RecommendProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员提现申请表
    ///</summary>
  public partial class ApplyWithDrawRepository : BaseRepository<ApplyWithDraw>, IApplyWithDrawRepository
  {
    public ApplyWithDrawRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
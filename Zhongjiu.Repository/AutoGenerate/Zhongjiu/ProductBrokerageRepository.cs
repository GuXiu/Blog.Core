﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销商品表
    ///</summary>
  public partial class ProductBrokerageRepository : BaseRepository<ProductBrokerage>, IProductBrokerageRepository
  {
    public ProductBrokerageRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
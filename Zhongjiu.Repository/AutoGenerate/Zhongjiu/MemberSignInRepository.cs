﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员签到记录表
    ///</summary>
  public partial class MemberSignInRepository : BaseRepository<MemberSignIn>, IMemberSignInRepository
  {
    public MemberSignInRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
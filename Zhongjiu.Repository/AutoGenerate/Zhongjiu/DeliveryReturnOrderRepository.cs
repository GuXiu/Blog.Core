﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配退单
    ///</summary>
  public partial class DeliveryReturnOrderRepository : BaseRepository<DeliveryReturnOrder>, IDeliveryReturnOrderRepository
  {
    public DeliveryReturnOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
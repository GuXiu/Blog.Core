﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满减活动阶梯表
    ///</summary>
  public partial class FullSubtractLadderRepository : BaseRepository<FullSubtractLadder>, IFullSubtractLadderRepository
  {
    public FullSubtractLadderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
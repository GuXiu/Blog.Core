﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///要货申请单
    ///</summary>
  public partial class ProductApplyOrderRepository : BaseRepository<ProductApplyOrder>, IProductApplyOrderRepository
  {
    public ProductApplyOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入明细
    ///</summary>
  public partial class DirectSaleStoreIncomeItemRepository : BaseRepository<DirectSaleStoreIncomeItem>, IDirectSaleStoreIncomeItemRepository
  {
    public DirectSaleStoreIncomeItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///机构档案表
    ///</summary>
  public partial class MechanismManagementRepository : BaseRepository<MechanismManagement>, IMechanismManagementRepository
  {
    public MechanismManagementRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
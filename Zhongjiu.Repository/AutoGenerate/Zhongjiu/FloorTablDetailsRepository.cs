﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层Tab表
    ///</summary>
  public partial class FloorTablDetailsRepository : BaseRepository<FloorTablDetails>, IFloorTablDetailsRepository
  {
    public FloorTablDetailsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
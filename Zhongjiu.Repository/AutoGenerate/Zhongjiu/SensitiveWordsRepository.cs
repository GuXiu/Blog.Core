﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///敏感关键词表
    ///</summary>
  public partial class SensitiveWordsRepository : BaseRepository<SensitiveWords>, ISensitiveWordsRepository
  {
    public SensitiveWordsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
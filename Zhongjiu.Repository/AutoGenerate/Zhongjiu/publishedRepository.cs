﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class publishedRepository : BaseRepository<published>, IpublishedRepository
  {
    public publishedRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///供应商表
    ///</summary>
  public partial class SuppliersRepository : BaseRepository<Suppliers>, ISuppliersRepository
  {
    public SuppliersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
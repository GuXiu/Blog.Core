﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拼团组团详情
    ///</summary>
  public partial class FightGroupsRepository : BaseRepository<FightGroups>, IFightGroupsRepository
  {
    public FightGroupsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下管理员表
    ///</summary>
  public partial class OfflineManagerRepository : BaseRepository<OfflineManager>, IOfflineManagerRepository
  {
    public OfflineManagerRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
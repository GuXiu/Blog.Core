﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品调移动平均价表
    ///</summary>
  public partial class OffLineMovingAverageProductsRepository : BaseRepository<OffLineMovingAverageProducts>, IOffLineMovingAverageProductsRepository
  {
    public OffLineMovingAverageProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员人群
    ///</summary>
  public partial class MembersThrongRepository : BaseRepository<MembersThrong>, IMembersThrongRepository
  {
    public MembersThrongRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
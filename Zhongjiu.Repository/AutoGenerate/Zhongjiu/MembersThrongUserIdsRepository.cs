﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员人群 会员Id关系表
    ///</summary>
  public partial class MembersThrongUserIdsRepository : BaseRepository<MembersThrongUserIds>, IMembersThrongUserIdsRepository
  {
    public MembersThrongUserIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
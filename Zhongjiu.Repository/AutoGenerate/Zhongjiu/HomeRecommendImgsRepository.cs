﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页推荐图片
    ///</summary>
  public partial class HomeRecommendImgsRepository : BaseRepository<HomeRecommendImgs>, IHomeRecommendImgsRepository
  {
    public HomeRecommendImgsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满减活动详情表
    ///</summary>
  public partial class FullSubtractDetailRepository : BaseRepository<FullSubtractDetail>, IFullSubtractDetailRepository
  {
    public FullSubtractDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///协议表
    ///</summary>
  public partial class AgreementRepository : BaseRepository<Agreement>, IAgreementRepository
  {
    public AgreementRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
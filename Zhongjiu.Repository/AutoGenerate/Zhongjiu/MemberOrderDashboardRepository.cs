﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户订单数据统计 按用户统计每天订单,没有则不录入新数据
    ///</summary>
  public partial class MemberOrderDashboardRepository : BaseRepository<MemberOrderDashboard>, IMemberOrderDashboardRepository
  {
    public MemberOrderDashboardRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送价表
    ///</summary>
  public partial class DistributionPriceRepository : BaseRepository<DistributionPrice>, IDistributionPriceRepository
  {
    public DistributionPriceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
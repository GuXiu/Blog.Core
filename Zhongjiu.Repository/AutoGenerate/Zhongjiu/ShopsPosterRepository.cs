﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///海报素材
    ///</summary>
  public partial class ShopsPosterRepository : BaseRepository<ShopsPoster>, IShopsPosterRepository
  {
    public ShopsPosterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
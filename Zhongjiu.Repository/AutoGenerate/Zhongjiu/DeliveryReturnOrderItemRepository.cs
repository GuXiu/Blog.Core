﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配退单明细
    ///</summary>
  public partial class DeliveryReturnOrderItemRepository : BaseRepository<DeliveryReturnOrderItem>, IDeliveryReturnOrderItemRepository
  {
    public DeliveryReturnOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
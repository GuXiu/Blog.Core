﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录表
    ///</summary>
  public partial class SendMessageRecordRepository : BaseRepository<SendMessageRecord>, ISendMessageRecordRepository
  {
    public SendMessageRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
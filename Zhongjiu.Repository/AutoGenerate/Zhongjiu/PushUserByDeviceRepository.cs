﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户App设备ID绑定关系表
    ///</summary>
  public partial class PushUserByDeviceRepository : BaseRepository<PushUserByDevice>, IPushUserByDeviceRepository
  {
    public PushUserByDeviceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
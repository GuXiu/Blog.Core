﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///单据审核记录表
    ///</summary>
  public partial class AuditHistoryRepository : BaseRepository<AuditHistory>, IAuditHistoryRepository
  {
    public AuditHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
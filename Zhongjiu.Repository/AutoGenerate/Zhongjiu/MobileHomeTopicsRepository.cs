﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信端首页的专题表
    ///</summary>
  public partial class MobileHomeTopicsRepository : BaseRepository<MobileHomeTopics>, IMobileHomeTopicsRepository
  {
    public MobileHomeTopicsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
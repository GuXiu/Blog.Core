﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员积分表
    ///</summary>
  public partial class MemberIntegralRepository : BaseRepository<MemberIntegral>, IMemberIntegralRepository
  {
    public MemberIntegralRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层专题表
    ///</summary>
  public partial class FloorTopicsRepository : BaseRepository<FloorTopics>, IFloorTopicsRepository
  {
    public FloorTopicsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单支付时间汇总订单明细
    ///</summary>
  public partial class OrderItemForPayDateRepository : BaseRepository<OrderItemForPayDate>, IOrderItemForPayDateRepository
  {
    public OrderItemForPayDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺续费记录表
    ///</summary>
  public partial class ShopRenewRecordRepository : BaseRepository<ShopRenewRecord>, IShopRenewRecordRepository
  {
    public ShopRenewRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
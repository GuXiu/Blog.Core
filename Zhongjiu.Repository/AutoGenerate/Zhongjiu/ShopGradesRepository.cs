﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺等级表
    ///</summary>
  public partial class ShopGradesRepository : BaseRepository<ShopGrades>, IShopGradesRepository
  {
    public ShopGradesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///移动端菜单表
    ///</summary>
  public partial class MenusRepository : BaseRepository<Menus>, IMenusRepository
  {
    public MenusRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
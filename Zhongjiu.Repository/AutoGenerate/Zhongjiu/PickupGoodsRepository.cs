﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下取单记录表
    ///</summary>
  public partial class PickupGoodsRepository : BaseRepository<PickupGoods>, IPickupGoodsRepository
  {
    public PickupGoodsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
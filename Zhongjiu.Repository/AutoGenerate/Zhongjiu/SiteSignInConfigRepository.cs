﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///签到配置表
    ///</summary>
  public partial class SiteSignInConfigRepository : BaseRepository<SiteSignInConfig>, ISiteSignInConfigRepository
  {
    public SiteSignInConfigRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///直营门店其他收入表
    ///</summary>
  public partial class DirectSaleStoreIncomeRepository : BaseRepository<DirectSaleStoreIncome>, IDirectSaleStoreIncomeRepository
  {
    public DirectSaleStoreIncomeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
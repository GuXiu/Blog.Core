﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///产品调价明细单
    ///</summary>
  public partial class ProductModifyPriceDetailRepository : BaseRepository<ProductModifyPriceDetail>, IProductModifyPriceDetailRepository
  {
    public ProductModifyPriceDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
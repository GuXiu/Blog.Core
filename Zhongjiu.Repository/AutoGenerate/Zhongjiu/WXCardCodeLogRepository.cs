﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///卡卷表
    ///</summary>
  public partial class WXCardCodeLogRepository : BaseRepository<WXCardCodeLog>, IWXCardCodeLogRepository
  {
    public WXCardCodeLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///提成设置，销售佣金
    ///</summary>
  public partial class SalesCommissionRepository : BaseRepository<SalesCommission>, ISalesCommissionRepository
  {
    public SalesCommissionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///VIEW
    ///</summary>
  public partial class veiw_offlineactivity_offlineactivityitemsRepository : BaseRepository<veiw_offlineactivity_offlineactivityitems>, Iveiw_offlineactivity_offlineactivityitemsRepository
  {
    public veiw_offlineactivity_offlineactivityitemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
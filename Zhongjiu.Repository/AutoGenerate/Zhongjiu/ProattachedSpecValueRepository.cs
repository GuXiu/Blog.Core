﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品副属性值表
    ///</summary>
  public partial class ProattachedSpecValueRepository : BaseRepository<ProattachedSpecValue>, IProattachedSpecValueRepository
  {
    public ProattachedSpecValueRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
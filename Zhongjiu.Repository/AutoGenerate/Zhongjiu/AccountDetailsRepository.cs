﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///结算详细表
    ///</summary>
  public partial class AccountDetailsRepository : BaseRepository<AccountDetails>, IAccountDetailsRepository
  {
    public AccountDetailsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
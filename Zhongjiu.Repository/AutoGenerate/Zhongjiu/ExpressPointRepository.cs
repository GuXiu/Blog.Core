﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺对应的快递网点配置
    ///</summary>
  public partial class ExpressPointRepository : BaseRepository<ExpressPoint>, IExpressPointRepository
  {
    public ExpressPointRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
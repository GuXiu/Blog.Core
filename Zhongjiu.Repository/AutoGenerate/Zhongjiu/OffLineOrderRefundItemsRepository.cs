﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单退货明细表
    ///</summary>
  public partial class OffLineOrderRefundItemsRepository : BaseRepository<OffLineOrderRefundItems>, IOffLineOrderRefundItemsRepository
  {
    public OffLineOrderRefundItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
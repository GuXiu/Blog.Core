﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送差异单
    ///</summary>
  public partial class DeliveryDifferOrderRepository : BaseRepository<DeliveryDifferOrder>, IDeliveryDifferOrderRepository
  {
    public DeliveryDifferOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品订单表
    ///</summary>
  public partial class GiftsOrderRepository : BaseRepository<GiftsOrder>, IGiftsOrderRepository
  {
    public GiftsOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
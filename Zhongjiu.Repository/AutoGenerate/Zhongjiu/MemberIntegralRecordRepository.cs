﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户积分兑换记录表
    ///</summary>
  public partial class MemberIntegralRecordRepository : BaseRepository<MemberIntegralRecord>, IMemberIntegralRecordRepository
  {
    public MemberIntegralRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
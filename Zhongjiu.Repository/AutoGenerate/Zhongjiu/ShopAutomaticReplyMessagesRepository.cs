﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系公众号自动回复消息
    ///</summary>
  public partial class ShopAutomaticReplyMessagesRepository : BaseRepository<ShopAutomaticReplyMessages>, IShopAutomaticReplyMessagesRepository
  {
    public ShopAutomaticReplyMessagesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
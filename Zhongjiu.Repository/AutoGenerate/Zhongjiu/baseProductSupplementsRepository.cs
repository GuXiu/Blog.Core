﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品扩展表
    ///</summary>
  public partial class baseProductSupplementsRepository : BaseRepository<baseProductSupplements>, IbaseProductSupplementsRepository
  {
    public baseProductSupplementsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
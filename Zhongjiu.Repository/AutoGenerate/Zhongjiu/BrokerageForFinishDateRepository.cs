﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细(分销商品)
    ///</summary>
  public partial class BrokerageForFinishDateRepository : BaseRepository<BrokerageForFinishDate>, IBrokerageForFinishDateRepository
  {
    public BrokerageForFinishDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
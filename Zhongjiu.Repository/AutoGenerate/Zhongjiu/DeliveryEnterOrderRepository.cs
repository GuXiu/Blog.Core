﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送入库单
    ///</summary>
  public partial class DeliveryEnterOrderRepository : BaseRepository<DeliveryEnterOrder>, IDeliveryEnterOrderRepository
  {
    public DeliveryEnterOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
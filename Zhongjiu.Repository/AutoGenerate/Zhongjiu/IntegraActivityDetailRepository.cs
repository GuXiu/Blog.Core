﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///积分活动商品表
    ///</summary>
  public partial class IntegraActivityDetailRepository : BaseRepository<IntegraActivityDetail>, IIntegraActivityDetailRepository
  {
    public IntegraActivityDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
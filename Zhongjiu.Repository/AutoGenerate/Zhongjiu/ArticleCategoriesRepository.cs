﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统文章分类表
    ///</summary>
  public partial class ArticleCategoriesRepository : BaseRepository<ArticleCategories>, IArticleCategoriesRepository
  {
    public ArticleCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
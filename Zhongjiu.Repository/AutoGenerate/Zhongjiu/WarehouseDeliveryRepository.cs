﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分仓发货
    ///</summary>
  public partial class WarehouseDeliveryRepository : BaseRepository<WarehouseDelivery>, IWarehouseDeliveryRepository
  {
    public WarehouseDeliveryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
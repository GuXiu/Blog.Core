﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限时购表
    ///</summary>
  public partial class FlashSaleRepository : BaseRepository<FlashSale>, IFlashSaleRepository
  {
    public FlashSaleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
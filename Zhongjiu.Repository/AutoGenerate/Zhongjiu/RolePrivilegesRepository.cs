﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统权限和角色的关联表
    ///</summary>
  public partial class RolePrivilegesRepository : BaseRepository<RolePrivileges>, IRolePrivilegesRepository
  {
    public RolePrivilegesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
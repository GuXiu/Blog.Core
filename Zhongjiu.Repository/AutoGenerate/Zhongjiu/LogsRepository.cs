﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///操作日志表
    ///</summary>
  public partial class LogsRepository : BaseRepository<Logs>, ILogsRepository
  {
    public LogsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
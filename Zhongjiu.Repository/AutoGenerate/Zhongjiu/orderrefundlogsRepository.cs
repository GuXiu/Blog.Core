﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单售后日志表
    ///</summary>
  public partial class orderrefundlogsRepository : BaseRepository<orderrefundlogs>, IorderrefundlogsRepository
  {
    public orderrefundlogsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
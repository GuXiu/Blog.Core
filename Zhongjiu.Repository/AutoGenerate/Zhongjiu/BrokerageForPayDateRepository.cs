﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单支付时间汇总订单明细(分销商品)
    ///</summary>
  public partial class BrokerageForPayDateRepository : BaseRepository<BrokerageForPayDate>, IBrokerageForPayDateRepository
  {
    public BrokerageForPayDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
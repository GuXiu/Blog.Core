﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品目录分类索引表
    ///</summary>
  public partial class IndexCategoryRepository : BaseRepository<IndexCategory>, IIndexCategoryRepository
  {
    public IndexCategoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页主页自定义类别表
    ///</summary>
  public partial class HomeCustomCategorysRepository : BaseRepository<HomeCustomCategorys>, IHomeCustomCategorysRepository
  {
    public HomeCustomCategorysRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
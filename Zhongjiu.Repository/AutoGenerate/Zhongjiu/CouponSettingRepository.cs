﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券设置表
    ///</summary>
  public partial class CouponSettingRepository : BaseRepository<CouponSetting>, ICouponSettingRepository
  {
    public CouponSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
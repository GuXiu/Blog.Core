﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///企业微信群列表
    ///</summary>
  public partial class WxGroupChatListRepository : BaseRepository<WxGroupChatList>, IWxGroupChatListRepository
  {
    public WxGroupChatListRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
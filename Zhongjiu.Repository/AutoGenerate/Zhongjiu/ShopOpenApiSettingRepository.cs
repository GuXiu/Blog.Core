﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺OpenAPI秘钥
    ///</summary>
  public partial class ShopOpenApiSettingRepository : BaseRepository<ShopOpenApiSetting>, IShopOpenApiSettingRepository
  {
    public ShopOpenApiSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
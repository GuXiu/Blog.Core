﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///模板添加设置表
    ///</summary>
  public partial class TemplateAdditionSettingRepository : BaseRepository<TemplateAdditionSetting>, ITemplateAdditionSettingRepository
  {
    public TemplateAdditionSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系短信模板表
    ///</summary>
  public partial class MessagesRepository : BaseRepository<Messages>, IMessagesRepository
  {
    public MessagesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///推广员海报
    ///</summary>
  public partial class PromoterPosterRepository : BaseRepository<PromoterPoster>, IPromoterPosterRepository
  {
    public PromoterPosterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
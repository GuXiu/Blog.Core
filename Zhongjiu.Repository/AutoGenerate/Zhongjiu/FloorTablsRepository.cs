﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层Table表
    ///</summary>
  public partial class FloorTablsRepository : BaseRepository<FloorTabls>, IFloorTablsRepository
  {
    public FloorTablsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
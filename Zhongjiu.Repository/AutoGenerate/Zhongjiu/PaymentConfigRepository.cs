﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///否支持货到付款设置表
    ///</summary>
  public partial class PaymentConfigRepository : BaseRepository<PaymentConfig>, IPaymentConfigRepository
  {
    public PaymentConfigRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拆零商品关系表
    ///</summary>
  public partial class ProductChaiLingRelation_copyRepository : BaseRepository<ProductChaiLingRelation_copy>, IProductChaiLingRelation_copyRepository
  {
    public ProductChaiLingRelation_copyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
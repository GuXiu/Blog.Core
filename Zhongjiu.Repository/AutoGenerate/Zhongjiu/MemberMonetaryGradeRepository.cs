﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员消费等级表
    ///</summary>
  public partial class MemberMonetaryGradeRepository : BaseRepository<MemberMonetaryGrade>, IMemberMonetaryGradeRepository
  {
    public MemberMonetaryGradeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
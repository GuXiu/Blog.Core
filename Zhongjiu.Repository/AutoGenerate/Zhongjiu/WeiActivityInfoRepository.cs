﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡表
    ///</summary>
  public partial class WeiActivityInfoRepository : BaseRepository<WeiActivityInfo>, IWeiActivityInfoRepository
  {
    public WeiActivityInfoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
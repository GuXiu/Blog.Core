﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///接收店铺有关消息的用户表
    ///</summary>
  public partial class ManagersReceivingMessagesRepository : BaseRepository<ManagersReceivingMessages>, IManagersReceivingMessagesRepository
  {
    public ManagersReceivingMessagesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
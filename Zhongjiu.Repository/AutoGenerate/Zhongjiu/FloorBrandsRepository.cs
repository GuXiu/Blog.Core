﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层品牌表
    ///</summary>
  public partial class FloorBrandsRepository : BaseRepository<FloorBrands>, IFloorBrandsRepository
  {
    public FloorBrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员设置表
    ///</summary>
  public partial class OfflineVipSetRepository : BaseRepository<OfflineVipSet>, IOfflineVipSetRepository
  {
    public OfflineVipSetRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信活动刮刮卡获奖详情表
    ///</summary>
  public partial class WeiActivityWinInfoRepository : BaseRepository<WeiActivityWinInfo>, IWeiActivityWinInfoRepository
  {
    public WeiActivityWinInfoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///区域编号表（删除）
    ///</summary>
  public partial class AreaCodeRepository : BaseRepository<AreaCode>, IAreaCodeRepository
  {
    public AreaCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
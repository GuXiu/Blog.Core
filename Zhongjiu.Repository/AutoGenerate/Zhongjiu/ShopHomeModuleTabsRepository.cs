﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///地阿奴首页模块标签表
    ///</summary>
  public partial class ShopHomeModuleTabsRepository : BaseRepository<ShopHomeModuleTabs>, IShopHomeModuleTabsRepository
  {
    public ShopHomeModuleTabsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
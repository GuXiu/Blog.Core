﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///文章支付表
    ///</summary>
  public partial class ArticlesPayOrderRepository : BaseRepository<ArticlesPayOrder>, IArticlesPayOrderRepository
  {
    public ArticlesPayOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
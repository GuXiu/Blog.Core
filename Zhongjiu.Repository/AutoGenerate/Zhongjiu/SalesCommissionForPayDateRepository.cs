﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单支付时间汇总订单明细(提成商品)
    ///</summary>
  public partial class SalesCommissionForPayDateRepository : BaseRepository<SalesCommissionForPayDate>, ISalesCommissionForPayDateRepository
  {
    public SalesCommissionForPayDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class receivedRepository : BaseRepository<received>, IreceivedRepository
  {
    public receivedRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
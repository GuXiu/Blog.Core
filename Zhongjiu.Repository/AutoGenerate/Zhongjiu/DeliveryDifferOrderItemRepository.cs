﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送差异单明细
    ///</summary>
  public partial class DeliveryDifferOrderItemRepository : BaseRepository<DeliveryDifferOrderItem>, IDeliveryDifferOrderItemRepository
  {
    public DeliveryDifferOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///卡券识别记录表
    ///</summary>
  public partial class CardCodeRecordRepository : BaseRepository<CardCodeRecord>, ICardCodeRecordRepository
  {
    public CardCodeRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
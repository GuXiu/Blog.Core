﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单表
    ///</summary>
  public partial class OrdersRepository : BaseRepository<Orders>, IOrdersRepository
  {
    public OrdersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
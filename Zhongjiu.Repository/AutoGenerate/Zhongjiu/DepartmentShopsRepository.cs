﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///部门下店铺
    ///</summary>
  public partial class DepartmentShopsRepository : BaseRepository<DepartmentShops>, IDepartmentShopsRepository
  {
    public DepartmentShopsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
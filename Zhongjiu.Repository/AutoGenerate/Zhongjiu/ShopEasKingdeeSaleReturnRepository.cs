﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///金蝶ERP推送销退记录表
    ///</summary>
  public partial class ShopEasKingdeeSaleReturnRepository : BaseRepository<ShopEasKingdeeSaleReturn>, IShopEasKingdeeSaleReturnRepository
  {
    public ShopEasKingdeeSaleReturnRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
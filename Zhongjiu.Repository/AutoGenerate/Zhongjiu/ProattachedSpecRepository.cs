﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品副属性（店铺商品、基库商品共用）
    ///</summary>
  public partial class ProattachedSpecRepository : BaseRepository<ProattachedSpec>, IProattachedSpecRepository
  {
    public ProattachedSpecRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
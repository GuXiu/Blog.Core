﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户积分兑换规则表
    ///</summary>
  public partial class MemberIntegralExchangeRulesRepository : BaseRepository<MemberIntegralExchangeRules>, IMemberIntegralExchangeRulesRepository
  {
    public MemberIntegralExchangeRulesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
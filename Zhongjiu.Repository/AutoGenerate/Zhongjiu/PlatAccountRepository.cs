﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台资金表
    ///</summary>
  public partial class PlatAccountRepository : BaseRepository<PlatAccount>, IPlatAccountRepository
  {
    public PlatAccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
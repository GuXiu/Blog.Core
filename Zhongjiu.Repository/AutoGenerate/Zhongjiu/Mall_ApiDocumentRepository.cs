﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///小程序API 接口文档
    ///</summary>
  public partial class Mall_ApiDocumentRepository : BaseRepository<Mall_ApiDocument>, IMall_ApiDocumentRepository
  {
    public Mall_ApiDocumentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
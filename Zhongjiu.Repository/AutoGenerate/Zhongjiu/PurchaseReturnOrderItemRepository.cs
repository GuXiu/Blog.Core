﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购退货订单明细
    ///</summary>
  public partial class PurchaseReturnOrderItemRepository : BaseRepository<PurchaseReturnOrderItem>, IPurchaseReturnOrderItemRepository
  {
    public PurchaseReturnOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///售后原因
    ///</summary>
  public partial class RefundReasonRepository : BaseRepository<RefundReason>, IRefundReasonRepository
  {
    public RefundReasonRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
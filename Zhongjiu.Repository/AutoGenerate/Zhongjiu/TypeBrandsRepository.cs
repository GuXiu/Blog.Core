﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///类型和品牌的关联表
    ///</summary>
  public partial class TypeBrandsRepository : BaseRepository<TypeBrands>, ITypeBrandsRepository
  {
    public TypeBrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
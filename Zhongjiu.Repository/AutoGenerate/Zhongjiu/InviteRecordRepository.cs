﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///我要推广邀请注册记录表
    ///</summary>
  public partial class InviteRecordRepository : BaseRepository<InviteRecord>, IInviteRecordRepository
  {
    public InviteRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
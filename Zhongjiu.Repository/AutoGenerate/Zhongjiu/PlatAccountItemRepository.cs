﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台资金流水表
    ///</summary>
  public partial class PlatAccountItemRepository : BaseRepository<PlatAccountItem>, IPlatAccountItemRepository
  {
    public PlatAccountItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///部门表
    ///</summary>
  public partial class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
  {
    public DepartmentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购入库订单
    ///</summary>
  public partial class PurchaseEnterOrderRepository : BaseRepository<PurchaseEnterOrder>, IPurchaseEnterOrderRepository
  {
    public PurchaseEnterOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///库存不足提醒记录表
    ///</summary>
  public partial class StockNotEnoughMsgLogRepository : BaseRepository<StockNotEnoughMsgLog>, IStockNotEnoughMsgLogRepository
  {
    public StockNotEnoughMsgLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销板块开关设置
    ///</summary>
  public partial class DistributorSettingRepository : BaseRepository<DistributorSetting>, IDistributorSettingRepository
  {
    public DistributorSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
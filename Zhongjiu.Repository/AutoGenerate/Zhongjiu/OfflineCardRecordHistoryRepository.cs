﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单使用会员卡信息表
    ///</summary>
  public partial class OfflineCardRecordHistoryRepository : BaseRepository<OfflineCardRecordHistory>, IOfflineCardRecordHistoryRepository
  {
    public OfflineCardRecordHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
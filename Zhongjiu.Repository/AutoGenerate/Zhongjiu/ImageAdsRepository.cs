﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页、店铺首页广告图片表
    ///</summary>
  public partial class ImageAdsRepository : BaseRepository<ImageAds>, IImageAdsRepository
  {
    public ImageAdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
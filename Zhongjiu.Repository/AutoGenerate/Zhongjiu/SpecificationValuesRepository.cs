﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///规格值表
    ///</summary>
  public partial class SpecificationValuesRepository : BaseRepository<SpecificationValues>, ISpecificationValuesRepository
  {
    public SpecificationValuesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
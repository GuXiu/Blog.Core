﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///库存单位表
    ///</summary>
  public partial class StockUnitRepository : BaseRepository<StockUnit>, IStockUnitRepository
  {
    public StockUnitRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
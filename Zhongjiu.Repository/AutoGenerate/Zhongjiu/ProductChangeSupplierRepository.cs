﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品供应商变更表
    ///</summary>
  public partial class ProductChangeSupplierRepository : BaseRepository<ProductChangeSupplier>, IProductChangeSupplierRepository
  {
    public ProductChangeSupplierRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
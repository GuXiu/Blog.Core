﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下活动表（弃用）
    ///</summary>
  public partial class OffLineActivityRepository : BaseRepository<OffLineActivity>, IOffLineActivityRepository
  {
    public OffLineActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
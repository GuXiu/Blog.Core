﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺资金流水表
    ///</summary>
  public partial class ShopAccountItemRepository : BaseRepository<ShopAccountItem>, IShopAccountItemRepository
  {
    public ShopAccountItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
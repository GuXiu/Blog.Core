﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销佣金记录表
    ///</summary>
  public partial class BrokerageIncomeRepository : BaseRepository<BrokerageIncome>, IBrokerageIncomeRepository
  {
    public BrokerageIncomeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
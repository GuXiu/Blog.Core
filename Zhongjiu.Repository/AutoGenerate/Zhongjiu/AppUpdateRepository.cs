﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///App版本更新
    ///</summary>
  public partial class AppUpdateRepository : BaseRepository<AppUpdate>, IAppUpdateRepository
  {
    public AppUpdateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
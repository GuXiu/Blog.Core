﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///产品补充表
    ///</summary>
  public partial class ProductSupplementsRepository : BaseRepository<ProductSupplements>, IProductSupplementsRepository
  {
    public ProductSupplementsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
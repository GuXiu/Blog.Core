﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///任务数据
    ///</summary>
  public partial class TaskDataRepository : BaseRepository<TaskData>, ITaskDataRepository
  {
    public TaskDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
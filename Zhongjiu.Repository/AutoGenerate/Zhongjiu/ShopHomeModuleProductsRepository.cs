﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺首页的楼层模板商品表
    ///</summary>
  public partial class ShopHomeModuleProductsRepository : BaseRepository<ShopHomeModuleProducts>, IShopHomeModuleProductsRepository
  {
    public ShopHomeModuleProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
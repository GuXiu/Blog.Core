﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满赠活动阶梯表
    ///</summary>
  public partial class FullGiftLadderRepository : BaseRepository<FullGiftLadder>, IFullGiftLadderRepository
  {
    public FullGiftLadderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
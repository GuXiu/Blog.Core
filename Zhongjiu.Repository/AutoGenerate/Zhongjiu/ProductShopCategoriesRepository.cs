﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品和店铺分类的关联表
    ///</summary>
  public partial class ProductShopCategoriesRepository : BaseRepository<ProductShopCategories>, IProductShopCategoriesRepository
  {
    public ProductShopCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
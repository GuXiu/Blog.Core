﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品视频关联商品表
    ///</summary>
  public partial class ProductVideoRelationRepository : BaseRepository<ProductVideoRelation>, IProductVideoRelationRepository
  {
    public ProductVideoRelationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
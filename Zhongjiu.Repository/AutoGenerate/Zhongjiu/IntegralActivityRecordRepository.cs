﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///积分翻倍活动Id
    ///</summary>
  public partial class IntegralActivityRecordRepository : BaseRepository<IntegralActivityRecord>, IIntegralActivityRecordRepository
  {
    public IntegralActivityRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
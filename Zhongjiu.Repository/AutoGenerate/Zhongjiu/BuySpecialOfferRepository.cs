﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///特价
    ///</summary>
  public partial class BuySpecialOfferRepository : BaseRepository<BuySpecialOffer>, IBuySpecialOfferRepository
  {
    public BuySpecialOfferRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
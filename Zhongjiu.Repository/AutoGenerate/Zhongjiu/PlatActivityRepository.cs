﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台活动
    ///</summary>
  public partial class PlatActivityRepository : BaseRepository<PlatActivity>, IPlatActivityRepository
  {
    public PlatActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
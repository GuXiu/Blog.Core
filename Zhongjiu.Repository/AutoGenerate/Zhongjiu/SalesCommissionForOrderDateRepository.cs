﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单创建时间汇总订单明细(提成商品)
    ///</summary>
  public partial class SalesCommissionForOrderDateRepository : BaseRepository<SalesCommissionForOrderDate>, ISalesCommissionForOrderDateRepository
  {
    public SalesCommissionForOrderDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
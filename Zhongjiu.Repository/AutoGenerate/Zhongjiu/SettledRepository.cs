﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///入驻设置
    ///</summary>
  public partial class SettledRepository : BaseRepository<Settled>, ISettledRepository
  {
    public SettledRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
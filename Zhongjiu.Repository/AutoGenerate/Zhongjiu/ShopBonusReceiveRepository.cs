﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺红包领取记录表
    ///</summary>
  public partial class ShopBonusReceiveRepository : BaseRepository<ShopBonusReceive>, IShopBonusReceiveRepository
  {
    public ShopBonusReceiveRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
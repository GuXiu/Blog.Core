﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///海报素材文件夹
    ///</summary>
  public partial class PosterFolderRepository : BaseRepository<PosterFolder>, IPosterFolderRepository
  {
    public PosterFolderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
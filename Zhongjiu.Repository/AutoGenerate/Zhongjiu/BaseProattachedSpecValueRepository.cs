﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品基库副属性值
    ///</summary>
  public partial class BaseProattachedSpecValueRepository : BaseRepository<BaseProattachedSpecValue>, IBaseProattachedSpecValueRepository
  {
    public BaseProattachedSpecValueRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
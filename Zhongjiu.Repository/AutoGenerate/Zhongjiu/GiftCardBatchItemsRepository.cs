﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///补品卡批次明细
    ///</summary>
  public partial class GiftCardBatchItemsRepository : BaseRepository<GiftCardBatchItems>, IGiftCardBatchItemsRepository
  {
    public GiftCardBatchItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
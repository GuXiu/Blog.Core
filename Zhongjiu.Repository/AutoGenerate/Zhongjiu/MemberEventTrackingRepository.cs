﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class MemberEventTrackingRepository : BaseRepository<MemberEventTracking>, IMemberEventTrackingRepository
  {
    public MemberEventTrackingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单退款表
    ///</summary>
  public partial class OrderRefundsRepository : BaseRepository<OrderRefunds>, IOrderRefundsRepository
  {
    public OrderRefundsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
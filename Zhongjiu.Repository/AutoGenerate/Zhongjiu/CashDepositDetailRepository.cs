﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///保证金详情表
    ///</summary>
  public partial class CashDepositDetailRepository : BaseRepository<CashDepositDetail>, ICashDepositDetailRepository
  {
    public CashDepositDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
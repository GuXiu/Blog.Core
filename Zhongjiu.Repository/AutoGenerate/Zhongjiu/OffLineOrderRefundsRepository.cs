﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单退货表
    ///</summary>
  public partial class OffLineOrderRefundsRepository : BaseRepository<OffLineOrderRefunds>, IOffLineOrderRefundsRepository
  {
    public OffLineOrderRefundsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
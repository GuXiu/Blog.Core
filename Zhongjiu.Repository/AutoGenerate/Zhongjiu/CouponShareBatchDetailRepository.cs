﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///群发分享 优惠券活动关联表
    ///</summary>
  public partial class CouponShareBatchDetailRepository : BaseRepository<CouponShareBatchDetail>, ICouponShareBatchDetailRepository
  {
    public CouponShareBatchDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
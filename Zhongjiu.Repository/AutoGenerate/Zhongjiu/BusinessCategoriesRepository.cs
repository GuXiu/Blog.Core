﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺经营类目（弃用）
    ///</summary>
  public partial class BusinessCategoriesRepository : BaseRepository<BusinessCategories>, IBusinessCategoriesRepository
  {
    public BusinessCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统文章表
    ///</summary>
  public partial class ArticlesRepository : BaseRepository<Articles>, IArticlesRepository
  {
    public ArticlesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///第三方平台与商城用户绑定关系表
    ///</summary>
  public partial class MemberBindRepository : BaseRepository<MemberBind>, IMemberBindRepository
  {
    public MemberBindRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
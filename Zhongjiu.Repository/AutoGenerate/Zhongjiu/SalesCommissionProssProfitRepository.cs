﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///毛利提成表
    ///</summary>
  public partial class SalesCommissionProssProfitRepository : BaseRepository<SalesCommissionProssProfit>, ISalesCommissionProssProfitRepository
  {
    public SalesCommissionProssProfitRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺首页模块表
    ///</summary>
  public partial class ShopHomeModulesRepository : BaseRepository<ShopHomeModules>, IShopHomeModulesRepository
  {
    public ShopHomeModulesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
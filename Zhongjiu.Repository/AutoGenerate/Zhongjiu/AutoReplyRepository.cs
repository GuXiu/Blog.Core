﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///自动回复设置表
    ///</summary>
  public partial class AutoReplyRepository : BaseRepository<AutoReply>, IAutoReplyRepository
  {
    public AutoReplyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
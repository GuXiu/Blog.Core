﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户信息扩展表
    ///</summary>
  public partial class MoreMembersRepository : BaseRepository<MoreMembers>, IMoreMembersRepository
  {
    public MoreMembersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
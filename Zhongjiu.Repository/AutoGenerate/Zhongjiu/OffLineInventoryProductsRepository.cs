﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品出入库明细表
    ///</summary>
  public partial class OffLineInventoryProductsRepository : BaseRepository<OffLineInventoryProducts>, IOffLineInventoryProductsRepository
  {
    public OffLineInventoryProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
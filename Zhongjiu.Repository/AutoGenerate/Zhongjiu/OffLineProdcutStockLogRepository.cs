﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品库存操作日志表
    ///</summary>
  public partial class OffLineProdcutStockLogRepository : BaseRepository<OffLineProdcutStockLog>, IOffLineProdcutStockLogRepository
  {
    public OffLineProdcutStockLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
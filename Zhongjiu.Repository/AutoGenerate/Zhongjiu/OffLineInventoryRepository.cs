﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品出入库表库存表
    ///</summary>
  public partial class OffLineInventoryRepository : BaseRepository<OffLineInventory>, IOffLineInventoryRepository
  {
    public OffLineInventoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
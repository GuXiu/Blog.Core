﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///移动平均价记录
    ///</summary>
  public partial class AvgPriceHistoryRepository : BaseRepository<AvgPriceHistory>, IAvgPriceHistoryRepository
  {
    public AvgPriceHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
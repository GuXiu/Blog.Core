﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送入库单明细
    ///</summary>
  public partial class DeliveryEnterOrderItemRepository : BaseRepository<DeliveryEnterOrderItem>, IDeliveryEnterOrderItemRepository
  {
    public DeliveryEnterOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
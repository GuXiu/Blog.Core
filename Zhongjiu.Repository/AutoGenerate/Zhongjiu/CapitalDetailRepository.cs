﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员资金流水表
    ///</summary>
  public partial class CapitalDetailRepository : BaseRepository<CapitalDetail>, ICapitalDetailRepository
  {
    public CapitalDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
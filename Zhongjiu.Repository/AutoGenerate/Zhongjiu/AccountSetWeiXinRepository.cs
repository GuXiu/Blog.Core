﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系微信公众号设置表
    ///</summary>
  public partial class AccountSetWeiXinRepository : BaseRepository<AccountSetWeiXin>, IAccountSetWeiXinRepository
  {
    public AccountSetWeiXinRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
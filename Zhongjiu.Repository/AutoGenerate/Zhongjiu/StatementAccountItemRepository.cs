﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///对账单明细
    ///</summary>
  public partial class StatementAccountItemRepository : BaseRepository<StatementAccountItem>, IStatementAccountItemRepository
  {
    public StatementAccountItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
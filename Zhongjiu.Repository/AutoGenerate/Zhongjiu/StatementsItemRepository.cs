﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///结算单明细
    ///</summary>
  public partial class StatementsItemRepository : BaseRepository<StatementsItem>, IStatementsItemRepository
  {
    public StatementsItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
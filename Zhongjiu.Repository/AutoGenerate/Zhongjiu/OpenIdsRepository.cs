﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信会员关注公众号状态表
    ///</summary>
  public partial class OpenIdsRepository : BaseRepository<OpenIds>, IOpenIdsRepository
  {
    public OpenIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///图库类别表
    ///</summary>
  public partial class PhotoSpaceCategoryRepository : BaseRepository<PhotoSpaceCategory>, IPhotoSpaceCategoryRepository
  {
    public PhotoSpaceCategoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///群发分享 领取记录
    ///</summary>
  public partial class CouponShareBatchRecordRepository : BaseRepository<CouponShareBatchRecord>, ICouponShareBatchRecordRepository
  {
    public CouponShareBatchRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
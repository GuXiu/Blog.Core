﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单明细表
    ///</summary>
  public partial class OffLineOrderItemsRepository : BaseRepository<OffLineOrderItems>, IOffLineOrderItemsRepository
  {
    public OffLineOrderItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
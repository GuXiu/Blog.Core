﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///App消息推送记录表
    ///</summary>
  public partial class PushMessageLogRepository : BaseRepository<PushMessageLog>, IPushMessageLogRepository
  {
    public PushMessageLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
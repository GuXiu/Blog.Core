﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配退收货单
    ///</summary>
  public partial class DeliveryReturnReceiveOrderRepository : BaseRepository<DeliveryReturnReceiveOrder>, IDeliveryReturnReceiveOrderRepository
  {
    public DeliveryReturnReceiveOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
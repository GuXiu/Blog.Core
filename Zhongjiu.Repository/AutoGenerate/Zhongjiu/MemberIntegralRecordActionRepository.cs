﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///积分兑换虚拟物记录表
    ///</summary>
  public partial class MemberIntegralRecordActionRepository : BaseRepository<MemberIntegralRecordAction>, IMemberIntegralRecordActionRepository
  {
    public MemberIntegralRecordActionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
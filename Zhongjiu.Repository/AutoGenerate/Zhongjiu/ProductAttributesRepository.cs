﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品和属性的中间表
    ///</summary>
  public partial class ProductAttributesRepository : BaseRepository<ProductAttributes>, IProductAttributesRepository
  {
    public ProductAttributesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
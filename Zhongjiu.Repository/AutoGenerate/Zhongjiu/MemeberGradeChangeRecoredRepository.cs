﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员等级变更记录表
    ///</summary>
  public partial class MemeberGradeChangeRecoredRepository : BaseRepository<MemeberGradeChangeRecored>, IMemeberGradeChangeRecoredRepository
  {
    public MemeberGradeChangeRecoredRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销首页显示商品
    ///</summary>
  public partial class DistributionProductsRepository : BaseRepository<DistributionProducts>, IDistributionProductsRepository
  {
    public DistributionProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
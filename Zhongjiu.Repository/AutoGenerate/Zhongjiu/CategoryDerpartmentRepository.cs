﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系分类分佣比例表
    ///</summary>
  public partial class CategoryDerpartmentRepository : BaseRepository<CategoryDerpartment>, ICategoryDerpartmentRepository
  {
    public CategoryDerpartmentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
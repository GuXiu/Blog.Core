﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺热卖商品表
    ///</summary>
  public partial class ShopHotSaleProductsRepository : BaseRepository<ShopHotSaleProducts>, IShopHotSaleProductsRepository
  {
    public ShopHotSaleProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
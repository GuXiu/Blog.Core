﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统商城首页导航表
    ///</summary>
  public partial class BannersRepository : BaseRepository<Banners>, IBannersRepository
  {
    public BannersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
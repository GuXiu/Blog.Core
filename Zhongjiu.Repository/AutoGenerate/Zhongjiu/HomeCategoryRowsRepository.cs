﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页分类所属行表
    ///</summary>
  public partial class HomeCategoryRowsRepository : BaseRepository<HomeCategoryRows>, IHomeCategoryRowsRepository
  {
    public HomeCategoryRowsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
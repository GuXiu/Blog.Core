﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购订单
    ///</summary>
  public partial class PurchaseOrderRepository : BaseRepository<PurchaseOrder>, IPurchaseOrderRepository
  {
    public PurchaseOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单赠品表
    ///</summary>
  public partial class OfflineOrderItemGiftsRepository : BaseRepository<OfflineOrderItemGifts>, IOfflineOrderItemGiftsRepository
  {
    public OfflineOrderItemGiftsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
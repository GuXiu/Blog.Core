﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统商品属性值表（店铺商品、基库商品共用）
    ///</summary>
  public partial class AttributeValuesRepository : BaseRepository<AttributeValues>, IAttributeValuesRepository
  {
    public AttributeValuesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
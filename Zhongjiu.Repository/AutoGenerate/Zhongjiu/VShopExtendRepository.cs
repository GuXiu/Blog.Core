﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微店扩展表
    ///</summary>
  public partial class VShopExtendRepository : BaseRepository<VShopExtend>, IVShopExtendRepository
  {
    public VShopExtendRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
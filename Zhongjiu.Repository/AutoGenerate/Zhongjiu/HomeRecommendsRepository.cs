﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页推荐表
    ///</summary>
  public partial class HomeRecommendsRepository : BaseRepository<HomeRecommends>, IHomeRecommendsRepository
  {
    public HomeRecommendsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
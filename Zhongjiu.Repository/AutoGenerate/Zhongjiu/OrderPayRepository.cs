﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单支付记录表
    ///</summary>
  public partial class OrderPayRepository : BaseRepository<OrderPay>, IOrderPayRepository
  {
    public OrderPayRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺客显广告表
    ///</summary>
  public partial class AdvertisementRepository : BaseRepository<Advertisement>, IAdvertisementRepository
  {
    public AdvertisementRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
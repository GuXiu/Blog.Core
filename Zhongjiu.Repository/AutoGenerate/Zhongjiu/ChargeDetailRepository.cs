﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员充值流水表
    ///</summary>
  public partial class ChargeDetailRepository : BaseRepository<ChargeDetail>, IChargeDetailRepository
  {
    public ChargeDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
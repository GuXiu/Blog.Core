﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销数据统计 统计逻辑记录每日分销员的关联订单数量和金额，首次成为分销员当日及有订单时记录
    ///</summary>
  public partial class DistributionDashboardRepository : BaseRepository<DistributionDashboard>, IDistributionDashboardRepository
  {
    public DistributionDashboardRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
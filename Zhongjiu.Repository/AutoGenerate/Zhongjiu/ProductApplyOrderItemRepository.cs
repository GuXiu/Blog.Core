﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///要货申请单明细
    ///</summary>
  public partial class ProductApplyOrderItemRepository : BaseRepository<ProductApplyOrderItem>, IProductApplyOrderItemRepository
  {
    public ProductApplyOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
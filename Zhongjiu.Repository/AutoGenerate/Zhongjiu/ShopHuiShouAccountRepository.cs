﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///每个店铺，关联的小精灵支付账户信息
    ///</summary>
  public partial class ShopHuiShouAccountRepository : BaseRepository<ShopHuiShouAccount>, IShopHuiShouAccountRepository
  {
    public ShopHuiShouAccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系消息发送设置表
    ///</summary>
  public partial class InformationSettingRepository : BaseRepository<InformationSetting>, IInformationSettingRepository
  {
    public InformationSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送中心店铺关联表
    ///</summary>
  public partial class DistributionRelationShopRepository : BaseRepository<DistributionRelationShop>, IDistributionRelationShopRepository
  {
    public DistributionRelationShopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
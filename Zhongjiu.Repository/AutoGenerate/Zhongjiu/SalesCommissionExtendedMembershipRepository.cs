﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拓展会员提成表
    ///</summary>
  public partial class SalesCommissionExtendedMembershipRepository : BaseRepository<SalesCommissionExtendedMembership>, ISalesCommissionExtendedMembershipRepository
  {
    public SalesCommissionExtendedMembershipRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
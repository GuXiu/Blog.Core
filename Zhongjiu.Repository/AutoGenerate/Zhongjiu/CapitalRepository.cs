﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员资金余额表
    ///</summary>
  public partial class CapitalRepository : BaseRepository<Capital>, ICapitalRepository
  {
    public CapitalRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
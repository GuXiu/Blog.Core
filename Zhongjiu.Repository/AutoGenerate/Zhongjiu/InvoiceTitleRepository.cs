﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///发票抬头表
    ///</summary>
  public partial class InvoiceTitleRepository : BaseRepository<InvoiceTitle>, IInvoiceTitleRepository
  {
    public InvoiceTitleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///App首页模块表
    ///</summary>
  public partial class APPModuleRepository : BaseRepository<APPModule>, IAPPModuleRepository
  {
    public APPModuleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限时购预热时间设置表
    ///</summary>
  public partial class FlashSaleConfigRepository : BaseRepository<FlashSaleConfig>, IFlashSaleConfigRepository
  {
    public FlashSaleConfigRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
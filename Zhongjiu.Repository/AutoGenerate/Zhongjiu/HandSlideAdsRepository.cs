﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页手动轮播图片
    ///</summary>
  public partial class HandSlideAdsRepository : BaseRepository<HandSlideAds>, IHandSlideAdsRepository
  {
    public HandSlideAdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
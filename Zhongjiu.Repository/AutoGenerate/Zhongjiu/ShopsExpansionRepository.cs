﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺扩展表，存放一些不常用信息
    ///</summary>
  public partial class ShopsExpansionRepository : BaseRepository<ShopsExpansion>, IShopsExpansionRepository
  {
    public ShopsExpansionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
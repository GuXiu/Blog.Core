﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送单明细
    ///</summary>
  public partial class DeliveryOrderItemRepository : BaseRepository<DeliveryOrderItem>, IDeliveryOrderItemRepository
  {
    public DeliveryOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
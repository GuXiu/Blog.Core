﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///主题设置表
    ///</summary>
  public partial class ThemeRepository : BaseRepository<Theme>, IThemeRepository
  {
    public ThemeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
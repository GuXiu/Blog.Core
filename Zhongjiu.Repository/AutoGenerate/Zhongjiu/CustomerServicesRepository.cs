﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统客服表
    ///</summary>
  public partial class CustomerServicesRepository : BaseRepository<CustomerServices>, ICustomerServicesRepository
  {
    public CustomerServicesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
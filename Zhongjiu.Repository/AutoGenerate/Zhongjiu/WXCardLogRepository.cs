﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///卡卷领取表
    ///</summary>
  public partial class WXCardLogRepository : BaseRepository<WXCardLog>, IWXCardLogRepository
  {
    public WXCardLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
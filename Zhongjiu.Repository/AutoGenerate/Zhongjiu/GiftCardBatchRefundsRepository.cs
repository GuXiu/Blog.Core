﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品卡退款
    ///</summary>
  public partial class GiftCardBatchRefundsRepository : BaseRepository<GiftCardBatchRefunds>, IGiftCardBatchRefundsRepository
  {
    public GiftCardBatchRefundsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券 商品适用，多商品关联表
    ///</summary>
  public partial class CouponProductRepository : BaseRepository<CouponProduct>, ICouponProductRepository
  {
    public CouponProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
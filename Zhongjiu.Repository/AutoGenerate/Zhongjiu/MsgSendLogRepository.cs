﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///公众号消息发送记录表
    ///</summary>
  public partial class MsgSendLogRepository : BaseRepository<MsgSendLog>, IMsgSendLogRepository
  {
    public MsgSendLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
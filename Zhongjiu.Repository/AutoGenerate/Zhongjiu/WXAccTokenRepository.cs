﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信TOKEN表
    ///</summary>
  public partial class WXAccTokenRepository : BaseRepository<WXAccToken>, IWXAccTokenRepository
  {
    public WXAccTokenRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
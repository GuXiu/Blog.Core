﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细
    ///</summary>
  public partial class OrderItemForFinishDateRepository : BaseRepository<OrderItemForFinishDate>, IOrderItemForFinishDateRepository
  {
    public OrderItemForFinishDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
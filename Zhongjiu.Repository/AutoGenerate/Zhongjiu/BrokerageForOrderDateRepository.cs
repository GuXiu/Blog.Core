﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单创建时间汇总订单明细(分销商品)
    ///</summary>
  public partial class BrokerageForOrderDateRepository : BaseRepository<BrokerageForOrderDate>, IBrokerageForOrderDateRepository
  {
    public BrokerageForOrderDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
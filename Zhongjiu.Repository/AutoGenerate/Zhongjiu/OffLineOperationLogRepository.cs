﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下操作日志表
    ///</summary>
  public partial class OffLineOperationLogRepository : BaseRepository<OffLineOperationLog>, IOffLineOperationLogRepository
  {
    public OffLineOperationLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品卡批次
    ///</summary>
  public partial class GiftCardBatchesRepository : BaseRepository<GiftCardBatches>, IGiftCardBatchesRepository
  {
    public GiftCardBatchesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺　关联　支持跨店使用　的优惠券　
    ///</summary>
  public partial class CouponShopRepository : BaseRepository<CouponShop>, ICouponShopRepository
  {
    public CouponShopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
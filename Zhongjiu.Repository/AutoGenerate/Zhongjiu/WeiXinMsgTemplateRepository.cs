﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信消息模板表
    ///</summary>
  public partial class WeiXinMsgTemplateRepository : BaseRepository<WeiXinMsgTemplate>, IWeiXinMsgTemplateRepository
  {
    public WeiXinMsgTemplateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
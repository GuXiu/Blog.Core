﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺浏览量记录表
    ///</summary>
  public partial class ShopVistisRepository : BaseRepository<ShopVistis>, IShopVistisRepository
  {
    public ShopVistisRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
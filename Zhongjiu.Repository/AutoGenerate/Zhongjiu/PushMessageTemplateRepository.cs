﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///App消息推送模板表
    ///</summary>
  public partial class PushMessageTemplateRepository : BaseRepository<PushMessageTemplate>, IPushMessageTemplateRepository
  {
    public PushMessageTemplateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
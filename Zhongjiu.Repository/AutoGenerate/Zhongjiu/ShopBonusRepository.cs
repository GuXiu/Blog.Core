﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺红包表
    ///</summary>
  public partial class ShopBonusRepository : BaseRepository<ShopBonus>, IShopBonusRepository
  {
    public ShopBonusRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
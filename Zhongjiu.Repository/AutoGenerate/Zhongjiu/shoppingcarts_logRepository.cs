﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///添加购物车日志表
    ///</summary>
  public partial class shoppingcarts_logRepository : BaseRepository<shoppingcarts_log>, Ishoppingcarts_logRepository
  {
    public shoppingcarts_logRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
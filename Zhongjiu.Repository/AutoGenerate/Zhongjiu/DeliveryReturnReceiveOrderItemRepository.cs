﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配退收货单明细
    ///</summary>
  public partial class DeliveryReturnReceiveOrderItemRepository : BaseRepository<DeliveryReturnReceiveOrderItem>, IDeliveryReturnReceiveOrderItemRepository
  {
    public DeliveryReturnReceiveOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品库存冻结表
    ///</summary>
  public partial class ProductStockFrozenRepository : BaseRepository<ProductStockFrozen>, IProductStockFrozenRepository
  {
    public ProductStockFrozenRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购退货订单
    ///</summary>
  public partial class PurchaseReturnOrderRepository : BaseRepository<PurchaseReturnOrder>, IPurchaseReturnOrderRepository
  {
    public PurchaseReturnOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
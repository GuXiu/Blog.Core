﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///图库表
    ///</summary>
  public partial class PhotoSpaceRepository : BaseRepository<PhotoSpace>, IPhotoSpaceRepository
  {
    public PhotoSpaceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺首页楼层轮播图
    ///</summary>
  public partial class ShopHomeModulesTopImgRepository : BaseRepository<ShopHomeModulesTopImg>, IShopHomeModulesTopImgRepository
  {
    public ShopHomeModulesTopImgRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
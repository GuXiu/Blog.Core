﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单表
    ///</summary>
  public partial class OffLineOrdersRepository : BaseRepository<OffLineOrders>, IOffLineOrdersRepository
  {
    public OffLineOrdersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
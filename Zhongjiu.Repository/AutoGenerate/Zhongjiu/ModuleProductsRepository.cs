﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品模块表
    ///</summary>
  public partial class ModuleProductsRepository : BaseRepository<ModuleProducts>, IModuleProductsRepository
  {
    public ModuleProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///红包领取记录表
    ///</summary>
  public partial class BonusReceiveRepository : BaseRepository<BonusReceive>, IBonusReceiveRepository
  {
    public BonusReceiveRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
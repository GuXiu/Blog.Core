﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销申请设置表
    ///</summary>
  public partial class RecruitSettingRepository : BaseRepository<RecruitSetting>, IRecruitSettingRepository
  {
    public RecruitSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class ChannelPlatProductSyncLogRepository : BaseRepository<ChannelPlatProductSyncLog>, IChannelPlatProductSyncLogRepository
  {
    public ChannelPlatProductSyncLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
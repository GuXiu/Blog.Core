﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///中酒云码库
    ///</summary>
  public partial class CloudCodeRepository : BaseRepository<CloudCode>, ICloudCodeRepository
  {
    public CloudCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
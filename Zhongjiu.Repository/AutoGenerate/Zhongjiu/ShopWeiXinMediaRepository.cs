﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺微信素材表
    ///</summary>
  public partial class ShopWeiXinMediaRepository : BaseRepository<ShopWeiXinMedia>, IShopWeiXinMediaRepository
  {
    public ShopWeiXinMediaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
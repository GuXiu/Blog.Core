﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系配送员表
    ///</summary>
  public partial class DeliveryRepository : BaseRepository<Delivery>, IDeliveryRepository
  {
    public DeliveryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
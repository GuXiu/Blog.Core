﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///提成计算记录 商品详情表
    ///</summary>
  public partial class SalesCommissionCalculationItemRepository : BaseRepository<SalesCommissionCalculationItem>, ISalesCommissionCalculationItemRepository
  {
    public SalesCommissionCalculationItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
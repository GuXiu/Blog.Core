﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺品牌表
    ///</summary>
  public partial class ShopBrandsRepository : BaseRepository<ShopBrands>, IShopBrandsRepository
  {
    public ShopBrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统基库SKU表
    ///</summary>
  public partial class BaseSKUsRepository : BaseRepository<BaseSKUs>, IBaseSKUsRepository
  {
    public BaseSKUsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///电子价签商品二维码扫码记录表
    ///</summary>
  public partial class ProductDetailScanFrequencyRepository : BaseRepository<ProductDetailScanFrequency>, IProductDetailScanFrequencyRepository
  {
    public ProductDetailScanFrequencyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
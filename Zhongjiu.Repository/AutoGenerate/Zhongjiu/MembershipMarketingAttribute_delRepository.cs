﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销 属性值
    ///</summary>
  public partial class MembershipMarketingAttribute_delRepository : BaseRepository<MembershipMarketingAttribute_del>, IMembershipMarketingAttribute_delRepository
  {
    public MembershipMarketingAttribute_delRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
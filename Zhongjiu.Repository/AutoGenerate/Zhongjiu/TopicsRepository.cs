﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///专题表
    ///</summary>
  public partial class TopicsRepository : BaseRepository<Topics>, ITopicsRepository
  {
    public TopicsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
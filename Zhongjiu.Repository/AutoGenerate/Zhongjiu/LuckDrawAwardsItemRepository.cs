﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///奖品明细表
    ///</summary>
  public partial class LuckDrawAwardsItemRepository : BaseRepository<LuckDrawAwardsItem>, ILuckDrawAwardsItemRepository
  {
    public LuckDrawAwardsItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
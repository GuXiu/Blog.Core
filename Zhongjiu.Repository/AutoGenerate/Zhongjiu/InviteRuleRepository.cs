﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///我要推广设置表
    ///</summary>
  public partial class InviteRuleRepository : BaseRepository<InviteRule>, IInviteRuleRepository
  {
    public InviteRuleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class WechatGroupChatAreaRepository : BaseRepository<WechatGroupChatArea>, IWechatGroupChatAreaRepository
  {
    public WechatGroupChatAreaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
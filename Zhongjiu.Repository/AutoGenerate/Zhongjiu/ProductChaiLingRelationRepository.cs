﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拆零商品关系表
    ///</summary>
  public partial class ProductChaiLingRelationRepository : BaseRepository<ProductChaiLingRelation>, IProductChaiLingRelationRepository
  {
    public ProductChaiLingRelationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
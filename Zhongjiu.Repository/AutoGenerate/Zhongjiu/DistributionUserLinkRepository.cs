﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销用户与店铺关联表
    ///</summary>
  public partial class DistributionUserLinkRepository : BaseRepository<DistributionUserLink>, IDistributionUserLinkRepository
  {
    public DistributionUserLinkRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
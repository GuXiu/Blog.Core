﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品提成设置
    ///</summary>
  public partial class SalesCommissionProductRepository : BaseRepository<SalesCommissionProduct>, ISalesCommissionProductRepository
  {
    public SalesCommissionProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
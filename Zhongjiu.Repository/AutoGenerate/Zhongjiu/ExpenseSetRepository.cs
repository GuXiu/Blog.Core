﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///费用设置表
    ///</summary>
  public partial class ExpenseSetRepository : BaseRepository<ExpenseSet>, IExpenseSetRepository
  {
    public ExpenseSetRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
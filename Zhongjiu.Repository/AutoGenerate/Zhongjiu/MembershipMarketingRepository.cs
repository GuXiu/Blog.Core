﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销表
    ///</summary>
  public partial class MembershipMarketingRepository : BaseRepository<MembershipMarketing>, IMembershipMarketingRepository
  {
    public MembershipMarketingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
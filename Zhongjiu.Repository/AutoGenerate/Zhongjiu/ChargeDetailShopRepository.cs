﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺充值流水表
    ///</summary>
  public partial class ChargeDetailShopRepository : BaseRepository<ChargeDetailShop>, IChargeDetailShopRepository
  {
    public ChargeDetailShopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
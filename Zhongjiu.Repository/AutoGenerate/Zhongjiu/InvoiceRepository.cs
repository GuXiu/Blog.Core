﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///发票表
    ///</summary>
  public partial class InvoiceRepository : BaseRepository<Invoice>, IInvoiceRepository
  {
    public InvoiceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
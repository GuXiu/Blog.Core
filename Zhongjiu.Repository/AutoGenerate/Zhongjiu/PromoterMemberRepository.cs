﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销员与被邀请会员关系表
    ///</summary>
  public partial class PromoterMemberRepository : BaseRepository<PromoterMember>, IPromoterMemberRepository
  {
    public PromoterMemberRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
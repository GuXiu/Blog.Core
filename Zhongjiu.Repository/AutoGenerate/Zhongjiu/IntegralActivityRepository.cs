﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///积分活动表
    ///</summary>
  public partial class IntegralActivityRepository : BaseRepository<IntegralActivity>, IIntegralActivityRepository
  {
    public IntegralActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
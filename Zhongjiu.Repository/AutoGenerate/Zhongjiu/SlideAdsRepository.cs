﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城、店铺首页
    ///</summary>
  public partial class SlideAdsRepository : BaseRepository<SlideAds>, ISlideAdsRepository
  {
    public SlideAdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
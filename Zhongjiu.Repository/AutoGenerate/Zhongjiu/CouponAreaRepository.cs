﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券领取区域
    ///</summary>
  public partial class CouponAreaRepository : BaseRepository<CouponArea>, ICouponAreaRepository
  {
    public CouponAreaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
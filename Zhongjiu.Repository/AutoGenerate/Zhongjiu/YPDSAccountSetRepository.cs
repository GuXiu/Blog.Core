﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///一品多商体系设置
    ///</summary>
  public partial class YPDSAccountSetRepository : BaseRepository<YPDSAccountSet>, IYPDSAccountSetRepository
  {
    public YPDSAccountSetRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
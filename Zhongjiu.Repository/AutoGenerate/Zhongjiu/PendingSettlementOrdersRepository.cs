﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///待结算订单表
    ///</summary>
  public partial class PendingSettlementOrdersRepository : BaseRepository<PendingSettlementOrders>, IPendingSettlementOrdersRepository
  {
    public PendingSettlementOrdersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
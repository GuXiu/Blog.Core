﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品供应商变更详情表
    ///</summary>
  public partial class ProductChangeSupplierDetailRepository : BaseRepository<ProductChangeSupplierDetail>, IProductChangeSupplierDetailRepository
  {
    public ProductChangeSupplierDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
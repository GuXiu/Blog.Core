﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单明细表
    ///</summary>
  public partial class OrderItemsRepository : BaseRepository<OrderItems>, IOrderItemsRepository
  {
    public OrderItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
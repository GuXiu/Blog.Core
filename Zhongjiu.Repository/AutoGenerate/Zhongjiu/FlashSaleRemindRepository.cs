﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限时购提现记录表
    ///</summary>
  public partial class FlashSaleRemindRepository : BaseRepository<FlashSaleRemind>, IFlashSaleRemindRepository
  {
    public FlashSaleRemindRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
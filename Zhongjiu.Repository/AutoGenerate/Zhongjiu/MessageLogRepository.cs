﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///短信发送记录表
    ///</summary>
  public partial class MessageLogRepository : BaseRepository<MessageLog>, IMessageLogRepository
  {
    public MessageLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
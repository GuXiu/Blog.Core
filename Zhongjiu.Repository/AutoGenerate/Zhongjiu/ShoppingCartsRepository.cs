﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///购物车表
    ///</summary>
  public partial class ShoppingCartsRepository : BaseRepository<ShoppingCarts>, IShoppingCartsRepository
  {
    public ShoppingCartsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
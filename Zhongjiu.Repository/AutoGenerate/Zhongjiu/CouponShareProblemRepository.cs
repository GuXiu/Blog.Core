﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题表
    ///</summary>
  public partial class CouponShareProblemRepository : BaseRepository<CouponShareProblem>, ICouponShareProblemRepository
  {
    public CouponShareProblemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///保证金表
    ///</summary>
  public partial class CashDepositRepository : BaseRepository<CashDeposit>, ICashDepositRepository
  {
    public CashDepositRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
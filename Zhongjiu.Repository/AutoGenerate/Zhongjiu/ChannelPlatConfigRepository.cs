﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///第三方平台对接，配置信息
    ///</summary>
  public partial class ChannelPlatConfigRepository : BaseRepository<ChannelPlatConfig>, IChannelPlatConfigRepository
  {
    public ChannelPlatConfigRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///短信发送限制记录表
    ///</summary>
  public partial class MessageLimitLogRepository : BaseRepository<MessageLimitLog>, IMessageLimitLogRepository
  {
    public MessageLimitLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///免运费活动
    ///</summary>
  public partial class FreightActivityRepository : BaseRepository<FreightActivity>, IFreightActivityRepository
  {
    public FreightActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
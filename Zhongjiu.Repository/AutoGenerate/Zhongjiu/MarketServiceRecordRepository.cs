﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///营销服务购买记录表
    ///</summary>
  public partial class MarketServiceRecordRepository : BaseRepository<MarketServiceRecord>, IMarketServiceRecordRepository
  {
    public MarketServiceRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
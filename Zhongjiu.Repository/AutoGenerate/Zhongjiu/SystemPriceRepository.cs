﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品会员等级价表
    ///</summary>
  public partial class SystemPriceRepository : BaseRepository<SystemPrice>, ISystemPriceRepository
  {
    public SystemPriceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
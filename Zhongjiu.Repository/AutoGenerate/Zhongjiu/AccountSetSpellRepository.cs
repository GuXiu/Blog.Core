﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系对应的汉语拼音
    ///</summary>
  public partial class AccountSetSpellRepository : BaseRepository<AccountSetSpell>, IAccountSetSpellRepository
  {
    public AccountSetSpellRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
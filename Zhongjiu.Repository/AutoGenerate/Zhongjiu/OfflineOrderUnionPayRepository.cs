﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单联合支付信息
    ///</summary>
  public partial class OfflineOrderUnionPayRepository : BaseRepository<OfflineOrderUnionPay>, IOfflineOrderUnionPayRepository
  {
    public OfflineOrderUnionPayRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
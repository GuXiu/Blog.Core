﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///直营门店盈亏表
    ///</summary>
  public partial class DirectSaleStoreGainAndLossRepository : BaseRepository<DirectSaleStoreGainAndLoss>, IDirectSaleStoreGainAndLossRepository
  {
    public DirectSaleStoreGainAndLossRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
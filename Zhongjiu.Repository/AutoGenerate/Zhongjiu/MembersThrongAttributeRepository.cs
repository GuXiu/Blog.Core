﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员人群 属性值
    ///</summary>
  public partial class MembersThrongAttributeRepository : BaseRepository<MembersThrongAttribute>, IMembersThrongAttributeRepository
  {
    public MembersThrongAttributeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
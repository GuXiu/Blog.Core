﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销招募计划表
    ///</summary>
  public partial class RecruitPlanRepository : BaseRepository<RecruitPlan>, IRecruitPlanRepository
  {
    public RecruitPlanRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员店铺关系表
    ///</summary>
  public partial class MemberShopsRepository : BaseRepository<MemberShops>, IMemberShopsRepository
  {
    public MemberShopsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
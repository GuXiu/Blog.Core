﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员人群 店铺Id关系表
    ///</summary>
  public partial class MembersThrongShopIdsRepository : BaseRepository<MembersThrongShopIds>, IMembersThrongShopIdsRepository
  {
    public MembersThrongShopIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///发送优惠券详细
    ///</summary>
  public partial class SendmessagerecordCouponRepository : BaseRepository<SendmessagerecordCoupon>, ISendmessagerecordCouponRepository
  {
    public SendmessagerecordCouponRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///直营门店盈亏明细
    ///</summary>
  public partial class DirectSaleStoreGainAndLossItemRepository : BaseRepository<DirectSaleStoreGainAndLossItem>, IDirectSaleStoreGainAndLossItemRepository
  {
    public DirectSaleStoreGainAndLossItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品卡简易批次明细
    ///</summary>
  public partial class GiftCardBatchItemSimplesRepository : BaseRepository<GiftCardBatchItemSimples>, IGiftCardBatchItemSimplesRepository
  {
    public GiftCardBatchItemSimplesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
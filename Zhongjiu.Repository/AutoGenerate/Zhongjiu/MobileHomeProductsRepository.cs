﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信端首页的商品配置表
    ///</summary>
  public partial class MobileHomeProductsRepository : BaseRepository<MobileHomeProducts>, IMobileHomeProductsRepository
  {
    public MobileHomeProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
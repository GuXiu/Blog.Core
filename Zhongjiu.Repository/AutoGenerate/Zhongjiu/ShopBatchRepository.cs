﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺自动获取批次
    ///</summary>
  public partial class ShopBatchRepository : BaseRepository<ShopBatch>, IShopBatchRepository
  {
    public ShopBatchRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///组合购表
    ///</summary>
  public partial class CollocationRepository : BaseRepository<Collocation>, ICollocationRepository
  {
    public CollocationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
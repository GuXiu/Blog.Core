﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满减活动表
    ///</summary>
  public partial class FullSubtractRepository : BaseRepository<FullSubtract>, IFullSubtractRepository
  {
    public FullSubtractRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
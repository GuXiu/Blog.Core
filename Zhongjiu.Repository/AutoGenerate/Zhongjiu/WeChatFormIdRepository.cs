﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信小程序用户FormId信息表
    ///</summary>
  public partial class WeChatFormIdRepository : BaseRepository<WeChatFormId>, IWeChatFormIdRepository
  {
    public WeChatFormIdRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺首页模板设置表
    ///</summary>
  public partial class TemplateVisualizationSettingsRepository : BaseRepository<TemplateVisualizationSettings>, ITemplateVisualizationSettingsRepository
  {
    public TemplateVisualizationSettingsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
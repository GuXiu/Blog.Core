﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///提成计算记录表
    ///</summary>
  public partial class SalesCommissionCalculationRepository : BaseRepository<SalesCommissionCalculation>, ISalesCommissionCalculationRepository
  {
    public SalesCommissionCalculationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
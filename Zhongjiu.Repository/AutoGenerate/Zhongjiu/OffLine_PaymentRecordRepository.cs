﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下订单App扫码支付表
    ///</summary>
  public partial class OffLine_PaymentRecordRepository : BaseRepository<OffLine_PaymentRecord>, IOffLine_PaymentRecordRepository
  {
    public OffLine_PaymentRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
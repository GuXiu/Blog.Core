﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///买赠商品详情表
    ///</summary>
  public partial class BuyGiftDetailRepository : BaseRepository<BuyGiftDetail>, IBuyGiftDetailRepository
  {
    public BuyGiftDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
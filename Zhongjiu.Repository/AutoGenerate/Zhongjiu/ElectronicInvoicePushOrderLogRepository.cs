﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单佣金推送天津电票记录
    ///</summary>
  public partial class ElectronicInvoicePushOrderLogRepository : BaseRepository<ElectronicInvoicePushOrderLog>, IElectronicInvoicePushOrderLogRepository
  {
    public ElectronicInvoicePushOrderLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
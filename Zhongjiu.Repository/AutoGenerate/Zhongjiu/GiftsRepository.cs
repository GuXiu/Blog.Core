﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品活动表
    ///</summary>
  public partial class GiftsRepository : BaseRepository<Gifts>, IGiftsRepository
  {
    public GiftsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺购买的营销服务表(暂时不用，目前店铺营销不需要购买)
    ///</summary>
  public partial class ActiveMarketServiceRepository : BaseRepository<ActiveMarketService>, IActiveMarketServiceRepository
  {
    public ActiveMarketServiceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
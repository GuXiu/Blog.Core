﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员OpenID表
    ///</summary>
  public partial class MemberOpenIdsRepository : BaseRepository<MemberOpenIds>, IMemberOpenIdsRepository
  {
    public MemberOpenIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///运费模板表
    ///</summary>
  public partial class FreightTemplateRepository : BaseRepository<FreightTemplate>, IFreightTemplateRepository
  {
    public FreightTemplateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///特价商品详情表
    ///</summary>
  public partial class BuySpecialOfferDetailRepository : BaseRepository<BuySpecialOfferDetail>, IBuySpecialOfferDetailRepository
  {
    public BuySpecialOfferDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
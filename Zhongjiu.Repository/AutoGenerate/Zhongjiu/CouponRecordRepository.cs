﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统优惠券使用记录表
    ///</summary>
  public partial class CouponRecordRepository : BaseRepository<CouponRecord>, ICouponRecordRepository
  {
    public CouponRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
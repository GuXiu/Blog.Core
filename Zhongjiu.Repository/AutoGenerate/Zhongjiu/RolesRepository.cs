﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///角色表
    ///</summary>
  public partial class RolesRepository : BaseRepository<Roles>, IRolesRepository
  {
    public RolesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
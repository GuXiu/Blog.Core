﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///供应商对店铺商品供货资格明细表
    ///</summary>
  public partial class SupplierProductsDetailRepository : BaseRepository<SupplierProductsDetail>, ISupplierProductsDetailRepository
  {
    public SupplierProductsDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
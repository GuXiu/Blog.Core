﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信基础信息表
    ///</summary>
  public partial class WeiXinBasicRepository : BaseRepository<WeiXinBasic>, IWeiXinBasicRepository
  {
    public WeiXinBasicRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
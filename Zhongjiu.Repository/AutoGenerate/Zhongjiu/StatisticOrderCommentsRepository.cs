﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单评价统计表
    ///</summary>
  public partial class StatisticOrderCommentsRepository : BaseRepository<StatisticOrderComments>, IStatisticOrderCommentsRepository
  {
    public StatisticOrderCommentsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
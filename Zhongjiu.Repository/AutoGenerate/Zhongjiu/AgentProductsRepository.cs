﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销商品代理记录表
    ///</summary>
  public partial class AgentProductsRepository : BaseRepository<AgentProducts>, IAgentProductsRepository
  {
    public AgentProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
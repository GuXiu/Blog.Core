﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///
    ///</summary>
  public partial class InformationcentreMemberInfoRepository : BaseRepository<InformationcentreMemberInfo>, IInformationcentreMemberInfoRepository
  {
    public InformationcentreMemberInfoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
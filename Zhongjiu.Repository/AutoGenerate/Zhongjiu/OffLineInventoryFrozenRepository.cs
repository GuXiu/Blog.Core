﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品冻结表
    ///</summary>
  public partial class OffLineInventoryFrozenRepository : BaseRepository<OffLineInventoryFrozen>, IOffLineInventoryFrozenRepository
  {
    public OffLineInventoryFrozenRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
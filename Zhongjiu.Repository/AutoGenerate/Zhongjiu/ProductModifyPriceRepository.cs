﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///产品调价单
    ///</summary>
  public partial class ProductModifyPriceRepository : BaseRepository<ProductModifyPrice>, IProductModifyPriceRepository
  {
    public ProductModifyPriceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
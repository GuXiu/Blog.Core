﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户一步直邀步数记录
    ///</summary>
  public partial class MemberStepsRecordRepository : BaseRepository<MemberStepsRecord>, IMemberStepsRecordRepository
  {
    public MemberStepsRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拼团活动项
    ///</summary>
  public partial class FightGroupActiveItemRepository : BaseRepository<FightGroupActiveItem>, IFightGroupActiveItemRepository
  {
    public FightGroupActiveItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
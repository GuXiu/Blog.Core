﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///App首页信息表
    ///</summary>
  public partial class APPHomeRepository : BaseRepository<APPHome>, IAPPHomeRepository
  {
    public APPHomeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺底部设置表
    ///</summary>
  public partial class ShopFooterRepository : BaseRepository<ShopFooter>, IShopFooterRepository
  {
    public ShopFooterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
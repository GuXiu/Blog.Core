﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员卡表
    ///</summary>
  public partial class OfflineVipRepository : BaseRepository<OfflineVip>, IOfflineVipRepository
  {
    public OfflineVipRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
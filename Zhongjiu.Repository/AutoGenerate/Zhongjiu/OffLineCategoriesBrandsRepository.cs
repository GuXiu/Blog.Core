﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品分类品牌关联表
    ///</summary>
  public partial class OffLineCategoriesBrandsRepository : BaseRepository<OffLineCategoriesBrands>, IOffLineCategoriesBrandsRepository
  {
    public OffLineCategoriesBrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
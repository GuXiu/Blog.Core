﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品调价单会员等级价调价明细
    ///</summary>
  public partial class ProductModifyPriceSystemPriceRepository : BaseRepository<ProductModifyPriceSystemPrice>, IProductModifyPriceSystemPriceRepository
  {
    public ProductModifyPriceSystemPriceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
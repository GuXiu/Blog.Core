﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///合同信息表
    ///</summary>
  public partial class ContractsRepository : BaseRepository<Contracts>, IContractsRepository
  {
    public ContractsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺投诉表
    ///</summary>
  public partial class ShopReportsRepository : BaseRepository<ShopReports>, IShopReportsRepository
  {
    public ShopReportsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
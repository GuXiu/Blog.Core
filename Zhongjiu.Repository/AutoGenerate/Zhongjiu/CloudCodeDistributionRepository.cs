﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///云码分配表,此表暂时不用
    ///</summary>
  public partial class CloudCodeDistributionRepository : BaseRepository<CloudCodeDistribution>, ICloudCodeDistributionRepository
  {
    public CloudCodeDistributionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
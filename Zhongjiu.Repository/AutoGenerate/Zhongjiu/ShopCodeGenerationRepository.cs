﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺编号生成表
    ///</summary>
  public partial class ShopCodeGenerationRepository : BaseRepository<ShopCodeGeneration>, IShopCodeGenerationRepository
  {
    public ShopCodeGenerationRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
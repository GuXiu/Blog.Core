﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户微信UnionId和店铺关系表
    ///</summary>
  public partial class MemberUnionIdsShopIdsRepository : BaseRepository<MemberUnionIdsShopIds>, IMemberUnionIdsShopIdsRepository
  {
    public MemberUnionIdsShopIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商家品牌申请表
    ///</summary>
  public partial class ShopBrandApplysRepository : BaseRepository<ShopBrandApplys>, IShopBrandApplysRepository
  {
    public ShopBrandApplysRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
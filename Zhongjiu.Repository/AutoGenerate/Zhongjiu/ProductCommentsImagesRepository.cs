﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品评论图片表
    ///</summary>
  public partial class ProductCommentsImagesRepository : BaseRepository<ProductCommentsImages>, IProductCommentsImagesRepository
  {
    public ProductCommentsImagesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///经营类目申请表（弃用）
    ///</summary>
  public partial class BusinessCategoriesApplyRepository : BaseRepository<BusinessCategoriesApply>, IBusinessCategoriesApplyRepository
  {
    public BusinessCategoriesApplyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
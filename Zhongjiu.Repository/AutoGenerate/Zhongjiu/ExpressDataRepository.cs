﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///快递面单数据
    ///</summary>
  public partial class ExpressDataRepository : BaseRepository<ExpressData>, IExpressDataRepository
  {
    public ExpressDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品品牌表（弃用）
    ///</summary>
  public partial class OffLineBrandsRepository : BaseRepository<OffLineBrands>, IOffLineBrandsRepository
  {
    public OffLineBrandsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///关联版式表
    ///</summary>
  public partial class ProductDescriptionTemplatesRepository : BaseRepository<ProductDescriptionTemplates>, IProductDescriptionTemplatesRepository
  {
    public ProductDescriptionTemplatesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
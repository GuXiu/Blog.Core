﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单CPS记录表
    ///</summary>
  public partial class CpsCookiesListRepository : BaseRepository<CpsCookiesList>, ICpsCookiesListRepository
  {
    public CpsCookiesListRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
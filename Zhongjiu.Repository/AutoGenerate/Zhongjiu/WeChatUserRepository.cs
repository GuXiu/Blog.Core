﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信用户信息
    ///</summary>
  public partial class WeChatUserRepository : BaseRepository<WeChatUser>, IWeChatUserRepository
  {
    public WeChatUserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
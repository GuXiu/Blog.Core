﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///基库商品和商品属性值关系表
    ///</summary>
  public partial class BaseProductAttributesRepository : BaseRepository<BaseProductAttributes>, IBaseProductAttributesRepository
  {
    public BaseProductAttributesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
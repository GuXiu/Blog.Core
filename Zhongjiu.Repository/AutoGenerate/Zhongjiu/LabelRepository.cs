﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员标签表
    ///</summary>
  public partial class LabelRepository : BaseRepository<Label>, ILabelRepository
  {
    public LabelRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///供应商对店铺商品供货资格主表
    ///</summary>
  public partial class SupplierProductsRepository : BaseRepository<SupplierProducts>, ISupplierProductsRepository
  {
    public SupplierProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
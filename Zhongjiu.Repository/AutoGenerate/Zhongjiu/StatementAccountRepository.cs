﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///对账单表
    ///</summary>
  public partial class StatementAccountRepository : BaseRepository<StatementAccount>, IStatementAccountRepository
  {
    public StatementAccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
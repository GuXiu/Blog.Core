﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///配送单
    ///</summary>
  public partial class DeliveryOrderRepository : BaseRepository<DeliveryOrder>, IDeliveryOrderRepository
  {
    public DeliveryOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
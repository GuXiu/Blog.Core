﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品调拨明细表
    ///</summary>
  public partial class OffLineInventorySchedulingProductsRepository : BaseRepository<OffLineInventorySchedulingProducts>, IOffLineInventorySchedulingProductsRepository
  {
    public OffLineInventorySchedulingProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
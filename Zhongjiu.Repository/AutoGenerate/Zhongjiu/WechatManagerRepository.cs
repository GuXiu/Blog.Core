﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信公众号资料表
    ///</summary>
  public partial class WechatManagerRepository : BaseRepository<WechatManager>, IWechatManagerRepository
  {
    public WechatManagerRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品名索引表
    ///</summary>
  public partial class IndexNameRepository : BaseRepository<IndexName>, IIndexNameRepository
  {
    public IndexNameRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员OpenID备份表
    ///</summary>
  public partial class MemberOpenIds_qqbakRepository : BaseRepository<MemberOpenIds_qqbak>, IMemberOpenIds_qqbakRepository
  {
    public MemberOpenIds_qqbakRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
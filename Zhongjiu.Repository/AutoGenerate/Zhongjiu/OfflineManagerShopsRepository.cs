﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下管理员关联店铺表
    ///</summary>
  public partial class OfflineManagerShopsRepository : BaseRepository<OfflineManagerShops>, IOfflineManagerShopsRepository
  {
    public OfflineManagerShopsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
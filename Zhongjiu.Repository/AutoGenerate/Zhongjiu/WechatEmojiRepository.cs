﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信表情
    ///</summary>
  public partial class WechatEmojiRepository : BaseRepository<WechatEmoji>, IWechatEmojiRepository
  {
    public WechatEmojiRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
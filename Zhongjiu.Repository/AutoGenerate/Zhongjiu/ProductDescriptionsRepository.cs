﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品详情表
    ///</summary>
  public partial class ProductDescriptionsRepository : BaseRepository<ProductDescriptions>, IProductDescriptionsRepository
  {
    public ProductDescriptionsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
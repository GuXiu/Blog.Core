﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///结算表
    ///</summary>
  public partial class AccountsRepository : BaseRepository<Accounts>, IAccountsRepository
  {
    public AccountsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
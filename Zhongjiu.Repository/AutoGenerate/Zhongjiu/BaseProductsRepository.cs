﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品基库表
    ///</summary>
  public partial class BaseProductsRepository : BaseRepository<BaseProducts>, IBaseProductsRepository
  {
    public BaseProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
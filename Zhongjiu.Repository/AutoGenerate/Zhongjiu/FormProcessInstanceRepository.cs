﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///表单主键Id与流程实例Id对应关系
    ///</summary>
  public partial class FormProcessInstanceRepository : BaseRepository<FormProcessInstance>, IFormProcessInstanceRepository
  {
    public FormProcessInstanceRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限购活动表
    ///</summary>
  public partial class LimitBuyRepository : BaseRepository<LimitBuy>, ILimitBuyRepository
  {
    public LimitBuyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///小精灵支付，按天统计(订单记录信息)
    ///</summary>
  public partial class OrdersHuiShouYinGroupDayRepository : BaseRepository<OrdersHuiShouYinGroupDay>, IOrdersHuiShouYinGroupDayRepository
  {
    public OrdersHuiShouYinGroupDayRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
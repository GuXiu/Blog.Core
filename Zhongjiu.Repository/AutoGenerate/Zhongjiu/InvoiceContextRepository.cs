﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///发票内容表
    ///</summary>
  public partial class InvoiceContextRepository : BaseRepository<InvoiceContext>, IInvoiceContextRepository
  {
    public InvoiceContextRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
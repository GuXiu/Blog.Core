﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销 人群Id关系表
    ///</summary>
  public partial class MembershipMarketingThrongIdsRepository : BaseRepository<MembershipMarketingThrongIds>, IMembershipMarketingThrongIdsRepository
  {
    public MembershipMarketingThrongIdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺分类表
    ///</summary>
  public partial class ShopCategoriesRepository : BaseRepository<ShopCategories>, IShopCategoriesRepository
  {
    public ShopCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
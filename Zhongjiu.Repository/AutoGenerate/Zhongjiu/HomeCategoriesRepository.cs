﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页分类表
    ///</summary>
  public partial class HomeCategoriesRepository : BaseRepository<HomeCategories>, IHomeCategoriesRepository
  {
    public HomeCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
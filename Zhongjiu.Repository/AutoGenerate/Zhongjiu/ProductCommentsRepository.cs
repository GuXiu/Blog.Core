﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品评价表
    ///</summary>
  public partial class ProductCommentsRepository : BaseRepository<ProductComments>, IProductCommentsRepository
  {
    public ProductCommentsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
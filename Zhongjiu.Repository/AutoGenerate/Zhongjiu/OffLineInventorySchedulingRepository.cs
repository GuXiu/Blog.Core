﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品调拨表
    ///</summary>
  public partial class OffLineInventorySchedulingRepository : BaseRepository<OffLineInventoryScheduling>, IOffLineInventorySchedulingRepository
  {
    public OffLineInventorySchedulingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
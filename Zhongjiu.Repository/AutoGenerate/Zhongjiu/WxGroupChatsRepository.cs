﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信小程序群聊配置
    ///</summary>
  public partial class WxGroupChatsRepository : BaseRepository<WxGroupChats>, IWxGroupChatsRepository
  {
    public WxGroupChatsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
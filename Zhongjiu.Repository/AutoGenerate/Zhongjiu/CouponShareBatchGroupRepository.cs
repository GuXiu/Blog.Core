﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券组合，群发分享，红包群发形式 领取
    ///</summary>
  public partial class CouponShareBatchGroupRepository : BaseRepository<CouponShareBatchGroup>, ICouponShareBatchGroupRepository
  {
    public CouponShareBatchGroupRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
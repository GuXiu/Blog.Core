﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品浏览记录表
    ///</summary>
  public partial class ProductVistisRepository : BaseRepository<ProductVistis>, IProductVistisRepository
  {
    public ProductVistisRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
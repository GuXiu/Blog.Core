﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///抽奖活动
    ///</summary>
  public partial class LuckDrawActivityRepository : BaseRepository<LuckDrawActivity>, ILuckDrawActivityRepository
  {
    public LuckDrawActivityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
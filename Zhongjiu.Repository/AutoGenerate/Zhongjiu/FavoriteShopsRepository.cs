﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///收藏店铺表
    ///</summary>
  public partial class FavoriteShopsRepository : BaseRepository<FavoriteShops>, IFavoriteShopsRepository
  {
    public FavoriteShopsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///换购活动
    ///</summary>
  public partial class RepurchaseRepository : BaseRepository<Repurchase>, IRepurchaseRepository
  {
    public RepurchaseRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
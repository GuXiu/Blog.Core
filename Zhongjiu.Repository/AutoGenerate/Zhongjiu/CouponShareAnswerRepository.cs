﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券分享问题答案表
    ///</summary>
  public partial class CouponShareAnswerRepository : BaseRepository<CouponShareAnswer>, ICouponShareAnswerRepository
  {
    public CouponShareAnswerRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
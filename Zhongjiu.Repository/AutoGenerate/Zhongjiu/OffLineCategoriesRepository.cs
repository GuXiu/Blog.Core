﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下商品分类表
    ///</summary>
  public partial class OffLineCategoriesRepository : BaseRepository<OffLineCategories>, IOffLineCategoriesRepository
  {
    public OffLineCategoriesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销市场分享设置表
    ///</summary>
  public partial class DistributionShareSettingRepository : BaseRepository<DistributionShareSetting>, IDistributionShareSettingRepository
  {
    public DistributionShareSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
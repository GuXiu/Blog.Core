﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///浏览历史记录表
    ///</summary>
  public partial class BrowsingHistoryRepository : BaseRepository<BrowsingHistory>, IBrowsingHistoryRepository
  {
    public BrowsingHistoryRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限购活动详情表
    ///</summary>
  public partial class LimitBuyDetailRepository : BaseRepository<LimitBuyDetail>, ILimitBuyDetailRepository
  {
    public LimitBuyDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
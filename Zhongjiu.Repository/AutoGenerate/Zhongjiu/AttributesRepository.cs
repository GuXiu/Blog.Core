﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统商品属性表（店铺商品、基库商品共用）
    ///</summary>
  public partial class AttributesRepository : BaseRepository<Attributes>, IAttributesRepository
  {
    public AttributesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
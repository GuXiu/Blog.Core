﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺资金表
    ///</summary>
  public partial class ShopAccountRepository : BaseRepository<ShopAccount>, IShopAccountRepository
  {
    public ShopAccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///小精灵支付，订单记录信息
    ///</summary>
  public partial class OrdersHuiShouYinRepository : BaseRepository<OrdersHuiShouYin>, IOrdersHuiShouYinRepository
  {
    public OrdersHuiShouYinRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
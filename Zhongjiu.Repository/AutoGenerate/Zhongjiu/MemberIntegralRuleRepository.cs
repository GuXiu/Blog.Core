﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///积分规则表
    ///</summary>
  public partial class MemberIntegralRuleRepository : BaseRepository<MemberIntegralRule>, IMemberIntegralRuleRepository
  {
    public MemberIntegralRuleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户注册数统计表 统计逻辑将每日新增的会员按店铺分组录入，为0则不录入
    ///</summary>
  public partial class MemberRegisterDashboardRepository : BaseRepository<MemberRegisterDashboard>, IMemberRegisterDashboardRepository
  {
    public MemberRegisterDashboardRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
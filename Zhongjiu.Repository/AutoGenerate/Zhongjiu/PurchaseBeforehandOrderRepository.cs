﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购预采订单
    ///</summary>
  public partial class PurchaseBeforehandOrderRepository : BaseRepository<PurchaseBeforehandOrder>, IPurchaseBeforehandOrderRepository
  {
    public PurchaseBeforehandOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
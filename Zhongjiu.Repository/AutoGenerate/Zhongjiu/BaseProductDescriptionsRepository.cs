﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///基库商品详情表
    ///</summary>
  public partial class BaseProductDescriptionsRepository : BaseRepository<BaseProductDescriptions>, IBaseProductDescriptionsRepository
  {
    public BaseProductDescriptionsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
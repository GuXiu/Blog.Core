﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拼团订单
    ///</summary>
  public partial class FightGroupOrderRepository : BaseRepository<FightGroupOrder>, IFightGroupOrderRepository
  {
    public FightGroupOrderRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///库存盘点明细
    ///</summary>
  public partial class StockCheckItemRepository : BaseRepository<StockCheckItem>, IStockCheckItemRepository
  {
    public StockCheckItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
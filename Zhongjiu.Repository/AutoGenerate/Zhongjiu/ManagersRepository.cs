﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///管理员表
    ///</summary>
  public partial class ManagersRepository : BaseRepository<Managers>, IManagersRepository
  {
    public ManagersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
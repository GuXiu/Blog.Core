﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///抽奖记录表
    ///</summary>
  public partial class LuckDrawConsumingRecordRepository : BaseRepository<LuckDrawConsumingRecord>, ILuckDrawConsumingRecordRepository
  {
    public LuckDrawConsumingRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
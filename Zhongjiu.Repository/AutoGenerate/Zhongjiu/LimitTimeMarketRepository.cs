﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///限时购表
    ///</summary>
  public partial class LimitTimeMarketRepository : BaseRepository<LimitTimeMarket>, ILimitTimeMarketRepository
  {
    public LimitTimeMarketRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
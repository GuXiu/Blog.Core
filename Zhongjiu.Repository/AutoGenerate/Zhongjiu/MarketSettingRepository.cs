﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///营销补充表
    ///</summary>
  public partial class MarketSettingRepository : BaseRepository<MarketSetting>, IMarketSettingRepository
  {
    public MarketSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
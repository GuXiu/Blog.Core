﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///表单引用工作流关系
    ///</summary>
  public partial class FormWorkflowRepository : BaseRepository<FormWorkflow>, IFormWorkflowRepository
  {
    public FormWorkflowRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台活动商品信息
    ///</summary>
  public partial class PlatActivityProductsRepository : BaseRepository<PlatActivityProducts>, IPlatActivityProductsRepository
  {
    public PlatActivityProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
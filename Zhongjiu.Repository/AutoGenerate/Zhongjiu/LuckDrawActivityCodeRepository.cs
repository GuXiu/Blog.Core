﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///抽奖活动问题表
    ///</summary>
  public partial class LuckDrawActivityCodeRepository : BaseRepository<LuckDrawActivityCode>, ILuckDrawActivityCodeRepository
  {
    public LuckDrawActivityCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
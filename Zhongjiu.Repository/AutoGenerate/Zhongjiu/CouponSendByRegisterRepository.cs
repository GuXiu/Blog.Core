﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///注册赠送优惠券
    ///</summary>
  public partial class CouponSendByRegisterRepository : BaseRepository<CouponSendByRegister>, ICouponSendByRegisterRepository
  {
    public CouponSendByRegisterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺表
    ///</summary>
  public partial class ShopsRepository : BaseRepository<Shops>, IShopsRepository
  {
    public ShopsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
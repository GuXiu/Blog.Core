﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购预采订单明细
    ///</summary>
  public partial class PurchaseBeforehandOrderItemRepository : BaseRepository<PurchaseBeforehandOrderItem>, IPurchaseBeforehandOrderItemRepository
  {
    public PurchaseBeforehandOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///换购活动商品详情
    ///</summary>
  public partial class RepurchaseDetailRepository : BaseRepository<RepurchaseDetail>, IRepurchaseDetailRepository
  {
    public RepurchaseDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
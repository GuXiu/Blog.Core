﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///注册送优惠券关联优惠券
    ///</summary>
  public partial class CouponSendByRegisterDetailedRepository : BaseRepository<CouponSendByRegisterDetailed>, ICouponSendByRegisterDetailedRepository
  {
    public CouponSendByRegisterDetailedRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
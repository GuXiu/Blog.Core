﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///优惠券分享单问题表
    ///</summary>
  public partial class CouponShareBatchGroupCodeRepository : BaseRepository<CouponShareBatchGroupCode>, ICouponShareBatchGroupCodeRepository
  {
    public CouponShareBatchGroupCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
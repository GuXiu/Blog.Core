﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///组合购SKU表
    ///</summary>
  public partial class CollocationSkusRepository : BaseRepository<CollocationSkus>, ICollocationSkusRepository
  {
    public CollocationSkusRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
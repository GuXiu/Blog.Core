﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///活动奖项
    ///</summary>
  public partial class LuckDrawActivityPrizeRepository : BaseRepository<LuckDrawActivityPrize>, ILuckDrawActivityPrizeRepository
  {
    public LuckDrawActivityPrizeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///官网个人中心绑定购物卡表
    ///</summary>
  public partial class MemberOfflineVipRepository : BaseRepository<MemberOfflineVip>, IMemberOfflineVipRepository
  {
    public MemberOfflineVipRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微信用户群组表
    ///</summary>
  public partial class WeChatUserGroupRepository : BaseRepository<WeChatUserGroup>, IWeChatUserGroupRepository
  {
    public WeChatUserGroupRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
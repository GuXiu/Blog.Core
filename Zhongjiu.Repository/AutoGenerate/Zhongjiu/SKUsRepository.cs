﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统SKU表
    ///</summary>
  public partial class SKUsRepository : BaseRepository<SKUs>, ISKUsRepository
  {
    public SKUsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
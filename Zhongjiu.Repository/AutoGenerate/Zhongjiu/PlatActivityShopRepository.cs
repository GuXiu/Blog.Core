﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///平台活动适用店铺
    ///</summary>
  public partial class PlatActivityShopRepository : BaseRepository<PlatActivityShop>, IPlatActivityShopRepository
  {
    public PlatActivityShopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺分销设置表
    ///</summary>
  public partial class ShopDistributorSettingRepository : BaseRepository<ShopDistributorSetting>, IShopDistributorSettingRepository
  {
    public ShopDistributorSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
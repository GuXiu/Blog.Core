﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员订单发放红包表
    ///</summary>
  public partial class ShopBonusGrantRepository : BaseRepository<ShopBonusGrant>, IShopBonusGrantRepository
  {
    public ShopBonusGrantRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///收藏商品表
    ///</summary>
  public partial class FavoritesRepository : BaseRepository<Favorites>, IFavoritesRepository
  {
    public FavoritesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
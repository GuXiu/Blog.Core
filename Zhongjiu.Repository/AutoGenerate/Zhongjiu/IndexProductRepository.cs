﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品索引表
    ///</summary>
  public partial class IndexProductRepository : BaseRepository<IndexProduct>, IIndexProductRepository
  {
    public IndexProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
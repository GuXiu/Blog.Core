﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///费用表
    ///</summary>
  public partial class ExpenseRepository : BaseRepository<Expense>, IExpenseRepository
  {
    public ExpenseRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
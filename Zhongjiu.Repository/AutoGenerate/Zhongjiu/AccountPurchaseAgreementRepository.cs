﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///结算预付款表
    ///</summary>
  public partial class AccountPurchaseAgreementRepository : BaseRepository<AccountPurchaseAgreement>, IAccountPurchaseAgreementRepository
  {
    public AccountPurchaseAgreementRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
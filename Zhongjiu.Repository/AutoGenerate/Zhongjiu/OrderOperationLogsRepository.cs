﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单操作日志表
    ///</summary>
  public partial class OrderOperationLogsRepository : BaseRepository<OrderOperationLogs>, IOrderOperationLogsRepository
  {
    public OrderOperationLogsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
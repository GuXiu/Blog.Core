﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分销员表
    ///</summary>
  public partial class PromoterRepository : BaseRepository<Promoter>, IPromoterRepository
  {
    public PromoterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单触发的活动记录
    ///</summary>
  public partial class OrderActivitiesRepository : BaseRepository<OrderActivities>, IOrderActivitiesRepository
  {
    public OrderActivitiesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
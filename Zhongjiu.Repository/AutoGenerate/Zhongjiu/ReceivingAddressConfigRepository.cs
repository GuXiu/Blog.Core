﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///区域支付方式配置
    ///</summary>
  public partial class ReceivingAddressConfigRepository : BaseRepository<ReceivingAddressConfig>, IReceivingAddressConfigRepository
  {
    public ReceivingAddressConfigRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
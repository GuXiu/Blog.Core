﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///分类品牌关联文章表
    ///</summary>
  public partial class BrandArticleClassifyRepository : BaseRepository<BrandArticleClassify>, IBrandArticleClassifyRepository
  {
    public BrandArticleClassifyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///(线下订单支付表)暂时不用
    ///</summary>
  public partial class OffLineOrderPayRepository : BaseRepository<OffLineOrderPay>, IOffLineOrderPayRepository
  {
    public OffLineOrderPayRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
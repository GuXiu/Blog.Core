﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城优惠券表
    ///</summary>
  public partial class CouponRepository : BaseRepository<Coupon>, ICouponRepository
  {
    public CouponRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
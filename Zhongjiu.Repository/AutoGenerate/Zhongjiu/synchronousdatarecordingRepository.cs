﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///同步数据记录表
    ///</summary>
  public partial class synchronousdatarecordingRepository : BaseRepository<synchronousdatarecording>, IsynchronousdatarecordingRepository
  {
    public synchronousdatarecordingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
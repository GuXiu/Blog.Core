﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///佣金退还记录表
    ///</summary>
  public partial class BrokerageRefundRepository : BaseRepository<BrokerageRefund>, IBrokerageRefundRepository
  {
    public BrokerageRefundRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
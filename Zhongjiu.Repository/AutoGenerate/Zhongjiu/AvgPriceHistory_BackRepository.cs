﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///移动平均价记录
    ///</summary>
  public partial class AvgPriceHistory_BackRepository : BaseRepository<AvgPriceHistory_Back>, IAvgPriceHistory_BackRepository
  {
    public AvgPriceHistory_BackRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
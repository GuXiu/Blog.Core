﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///营销服务费结算明细表
    ///</summary>
  public partial class AccountMetaRepository : BaseRepository<AccountMeta>, IAccountMetaRepository
  {
    public AccountMetaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///微店表
    ///</summary>
  public partial class VShopRepository : BaseRepository<VShop>, IVShopRepository
  {
    public VShopRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///礼品卡表
    ///</summary>
  public partial class GiftCardsRepository : BaseRepository<GiftCards>, IGiftCardsRepository
  {
    public GiftCardsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
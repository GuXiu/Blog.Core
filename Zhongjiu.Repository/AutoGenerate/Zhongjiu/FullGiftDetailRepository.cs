﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满赠活动详情表
    ///</summary>
  public partial class FullGiftDetailRepository : BaseRepository<FullGiftDetail>, IFullGiftDetailRepository
  {
    public FullGiftDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
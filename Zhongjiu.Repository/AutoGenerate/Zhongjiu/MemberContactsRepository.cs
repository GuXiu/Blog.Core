﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统会员联系方式表
    ///</summary>
  public partial class MemberContactsRepository : BaseRepository<MemberContacts>, IMemberContactsRepository
  {
    public MemberContactsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///预付款表
    ///</summary>
  public partial class AdvancesRepository : BaseRepository<Advances>, IAdvancesRepository
  {
    public AdvancesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
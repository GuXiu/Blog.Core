﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///经营类别申请类目详细表（弃用）
    ///</summary>
  public partial class BusinessCategoriesApplyDetailRepository : BaseRepository<BusinessCategoriesApplyDetail>, IBusinessCategoriesApplyDetailRepository
  {
    public BusinessCategoriesApplyDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺轮播图表
    ///</summary>
  public partial class ShopImageAdsRepository : BaseRepository<ShopImageAds>, IShopImageAdsRepository
  {
    public ShopImageAdsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///拼团活动
    ///</summary>
  public partial class FightGroupActiveRepository : BaseRepository<FightGroupActive>, IFightGroupActiveRepository
  {
    public FightGroupActiveRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
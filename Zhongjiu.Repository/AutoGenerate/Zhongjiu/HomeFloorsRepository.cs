﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商城首页楼层表
    ///</summary>
  public partial class HomeFloorsRepository : BaseRepository<HomeFloors>, IHomeFloorsRepository
  {
    public HomeFloorsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
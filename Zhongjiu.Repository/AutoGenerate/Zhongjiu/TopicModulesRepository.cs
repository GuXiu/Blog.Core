﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统模块的专题表
    ///</summary>
  public partial class TopicModulesRepository : BaseRepository<TopicModules>, ITopicModulesRepository
  {
    public TopicModulesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
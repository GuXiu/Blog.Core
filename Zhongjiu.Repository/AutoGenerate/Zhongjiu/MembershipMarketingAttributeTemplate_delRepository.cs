﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销 属性模板
    ///</summary>
  public partial class MembershipMarketingAttributeTemplate_delRepository : BaseRepository<MembershipMarketingAttributeTemplate_del>, IMembershipMarketingAttributeTemplate_delRepository
  {
    public MembershipMarketingAttributeTemplate_delRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下活动明细表（弃用）
    ///</summary>
  public partial class OffLineActivityItemsRepository : BaseRepository<OffLineActivityItems>, IOffLineActivityItemsRepository
  {
    public OffLineActivityItemsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
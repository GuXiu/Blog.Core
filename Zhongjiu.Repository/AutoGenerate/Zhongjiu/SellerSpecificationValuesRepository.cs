﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商家规格快照
    ///</summary>
  public partial class SellerSpecificationValuesRepository : BaseRepository<SellerSpecificationValues>, ISellerSpecificationValuesRepository
  {
    public SellerSpecificationValuesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员营销发送记录
    ///</summary>
  public partial class MembershipMarketingRecordRepository : BaseRepository<MembershipMarketingRecord>, IMembershipMarketingRecordRepository
  {
    public MembershipMarketingRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
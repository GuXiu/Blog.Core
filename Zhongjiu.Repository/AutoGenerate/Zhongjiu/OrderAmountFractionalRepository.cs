﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单金额小数操作记录（删除）
    ///</summary>
  public partial class OrderAmountFractionalRepository : BaseRepository<OrderAmountFractional>, IOrderAmountFractionalRepository
  {
    public OrderAmountFractionalRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
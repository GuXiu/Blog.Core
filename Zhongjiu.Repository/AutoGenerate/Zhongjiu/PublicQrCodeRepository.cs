﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///体系公众号二维码
    ///</summary>
  public partial class PublicQrCodeRepository : BaseRepository<PublicQrCode>, IPublicQrCodeRepository
  {
    public PublicQrCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
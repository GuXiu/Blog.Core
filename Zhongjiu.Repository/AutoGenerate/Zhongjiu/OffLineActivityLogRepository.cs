﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///线下活动修改信息表（弃用）
    ///</summary>
  public partial class OffLineActivityLogRepository : BaseRepository<OffLineActivityLog>, IOffLineActivityLogRepository
  {
    public OffLineActivityLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品分组表
    ///</summary>
  public partial class ProductsGroupRepository : BaseRepository<ProductsGroup>, IProductsGroupRepository
  {
    public ProductsGroupRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
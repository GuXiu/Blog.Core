﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺提现表
    ///</summary>
  public partial class ShopWithDrawRepository : BaseRepository<ShopWithDraw>, IShopWithDrawRepository
  {
    public ShopWithDrawRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///红包表
    ///</summary>
  public partial class BonusRepository : BaseRepository<Bonus>, IBonusRepository
  {
    public BonusRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
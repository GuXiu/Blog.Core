﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///云码关注用户表
    ///</summary>
  public partial class zjh_user_info1Repository : BaseRepository<zjh_user_info1>, Izjh_user_info1Repository
  {
    public zjh_user_info1Repository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
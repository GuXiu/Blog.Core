﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单完成时间汇总订单明细(提成商品)
    ///</summary>
  public partial class SalesCommissionForFinishDateRepository : BaseRepository<SalesCommissionForFinishDate>, ISalesCommissionForFinishDateRepository
  {
    public SalesCommissionForFinishDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
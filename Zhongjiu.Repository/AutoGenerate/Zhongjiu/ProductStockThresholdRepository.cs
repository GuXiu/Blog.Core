﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品库存阈值表
    ///</summary>
  public partial class ProductStockThresholdRepository : BaseRepository<ProductStockThreshold>, IProductStockThresholdRepository
  {
    public ProductStockThresholdRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///营销消息表
    ///</summary>
  public partial class MarketMsgMessageRepository : BaseRepository<MarketMsgMessage>, IMarketMsgMessageRepository
  {
    public MarketMsgMessageRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
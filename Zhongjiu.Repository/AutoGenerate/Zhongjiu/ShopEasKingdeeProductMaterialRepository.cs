﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///青稞酒，金蝶ERP 接口对接，配置物料信息
    ///</summary>
  public partial class ShopEasKingdeeProductMaterialRepository : BaseRepository<ShopEasKingdeeProductMaterial>, IShopEasKingdeeProductMaterialRepository
  {
    public ShopEasKingdeeProductMaterialRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
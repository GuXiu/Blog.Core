﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品属性索引表
    ///</summary>
  public partial class IndexAttrRepository : BaseRepository<IndexAttr>, IIndexAttrRepository
  {
    public IndexAttrRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
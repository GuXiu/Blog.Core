﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单投诉表
    ///</summary>
  public partial class OrderComplaintsRepository : BaseRepository<OrderComplaints>, IOrderComplaintsRepository
  {
    public OrderComplaintsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
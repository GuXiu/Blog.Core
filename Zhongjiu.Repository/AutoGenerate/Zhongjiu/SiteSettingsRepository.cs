﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///系统设置表
    ///</summary>
  public partial class SiteSettingsRepository : BaseRepository<SiteSettings>, ISiteSettingsRepository
  {
    public SiteSettingsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
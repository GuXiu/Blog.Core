﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购入库订单明细
    ///</summary>
  public partial class PurchaseEnterOrderItemRepository : BaseRepository<PurchaseEnterOrderItem>, IPurchaseEnterOrderItemRepository
  {
    public PurchaseEnterOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
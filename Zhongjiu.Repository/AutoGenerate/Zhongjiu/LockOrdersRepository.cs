﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单锁定设置
    ///</summary>
  public partial class LockOrdersRepository : BaseRepository<LockOrders>, ILockOrdersRepository
  {
    public LockOrdersRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
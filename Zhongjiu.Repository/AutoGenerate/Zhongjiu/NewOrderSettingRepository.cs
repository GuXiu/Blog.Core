﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///店铺新订单提醒设置
    ///</summary>
  public partial class NewOrderSettingRepository : BaseRepository<NewOrderSetting>, INewOrderSettingRepository
  {
    public NewOrderSettingRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
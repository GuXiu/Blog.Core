﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///订单卡券记录表
    ///</summary>
  public partial class CardCodeRepository : BaseRepository<CardCode>, ICardCodeRepository
  {
    public CardCodeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
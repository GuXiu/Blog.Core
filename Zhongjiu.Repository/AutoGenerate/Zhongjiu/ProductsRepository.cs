﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品主表
    ///</summary>
  public partial class ProductsRepository : BaseRepository<Products>, IProductsRepository
  {
    public ProductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
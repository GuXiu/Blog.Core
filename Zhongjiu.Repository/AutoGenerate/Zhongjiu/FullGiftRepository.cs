﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///满赠活动表
    ///</summary>
  public partial class FullGiftRepository : BaseRepository<FullGift>, IFullGiftRepository
  {
    public FullGiftRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///用户步数兑换记录
    ///</summary>
  public partial class MemberStepsExchangeRecordRepository : BaseRepository<MemberStepsExchangeRecord>, IMemberStepsExchangeRecordRepository
  {
    public MemberStepsExchangeRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
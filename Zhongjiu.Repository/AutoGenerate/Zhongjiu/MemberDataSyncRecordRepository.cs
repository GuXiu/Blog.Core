﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员数据同步记录表
    ///</summary>
  public partial class MemberDataSyncRecordRepository : BaseRepository<MemberDataSyncRecord>, IMemberDataSyncRecordRepository
  {
    public MemberDataSyncRecordRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
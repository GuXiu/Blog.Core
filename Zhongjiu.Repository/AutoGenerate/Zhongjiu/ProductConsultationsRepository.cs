﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///商品咨询表
    ///</summary>
  public partial class ProductConsultationsRepository : BaseRepository<ProductConsultations>, IProductConsultationsRepository
  {
    public ProductConsultationsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
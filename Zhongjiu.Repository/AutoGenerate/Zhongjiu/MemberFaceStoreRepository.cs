﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///人脸信息库
    ///</summary>
  public partial class MemberFaceStoreRepository : BaseRepository<MemberFaceStore>, IMemberFaceStoreRepository
  {
    public MemberFaceStoreRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
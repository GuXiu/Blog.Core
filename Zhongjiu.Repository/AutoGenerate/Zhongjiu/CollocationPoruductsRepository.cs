﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///组合购商品表
    ///</summary>
  public partial class CollocationPoruductsRepository : BaseRepository<CollocationPoruducts>, ICollocationPoruductsRepository
  {
    public CollocationPoruductsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///按订单创建时间汇总订单明细
    ///</summary>
  public partial class OrderItemForOrderDateRepository : BaseRepository<OrderItemForOrderDate>, IOrderItemForOrderDateRepository
  {
    public OrderItemForOrderDateRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
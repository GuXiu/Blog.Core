﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///会员等级表
    ///</summary>
  public partial class MemberGradeRepository : BaseRepository<MemberGrade>, IMemberGradeRepository
  {
    public MemberGradeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
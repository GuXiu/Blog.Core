﻿using Zhongjiu.Model.Zhongjiu;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Zhongjiu;

namespace Zhongjiu.Repository.Zhongjiu
{
    ///<summary>
    ///采购订单明细
    ///</summary>
  public partial class PurchaseOrderItemRepository : BaseRepository<PurchaseOrderItem>, IPurchaseOrderItemRepository
  {
    public PurchaseOrderItemRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class TestMuchTableResultRepository : BaseRepository<TestMuchTableResult>, ITestMuchTableResultRepository
  {
    public TestMuchTableResultRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class TopicRepository : BaseRepository<Topic>, ITopicRepository
  {
    public TopicRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
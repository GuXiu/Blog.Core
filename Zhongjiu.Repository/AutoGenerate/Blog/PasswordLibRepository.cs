﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///WMBLOG_MSSQL_2
    ///</summary>
  public partial class PasswordLibRepository : BaseRepository<PasswordLib>, IPasswordLibRepository
  {
    public PasswordLibRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
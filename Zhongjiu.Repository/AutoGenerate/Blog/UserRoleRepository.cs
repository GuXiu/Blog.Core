﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
  {
    public UserRoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
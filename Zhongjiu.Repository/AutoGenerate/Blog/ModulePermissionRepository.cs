﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class ModulePermissionRepository : BaseRepository<ModulePermission>, IModulePermissionRepository
  {
    public ModulePermissionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
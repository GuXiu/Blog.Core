﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class SysUserInfoRepository : BaseRepository<SysUserInfo>, ISysUserInfoRepository
  {
    public SysUserInfoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
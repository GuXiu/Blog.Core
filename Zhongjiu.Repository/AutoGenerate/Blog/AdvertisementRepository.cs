﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class AdvertisementRepository : BaseRepository<Advertisement>, IAdvertisementRepository
  {
    public AdvertisementRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class RoleRepository : BaseRepository<Role>, IRoleRepository
  {
    public RoleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
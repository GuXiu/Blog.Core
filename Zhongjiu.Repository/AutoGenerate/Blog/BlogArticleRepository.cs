﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class BlogArticleRepository : BaseRepository<BlogArticle>, IBlogArticleRepository
  {
    public BlogArticleRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
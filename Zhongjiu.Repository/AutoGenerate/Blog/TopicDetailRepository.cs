﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class TopicDetailRepository : BaseRepository<TopicDetail>, ITopicDetailRepository
  {
    public TopicDetailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
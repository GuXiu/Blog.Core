﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class OperateLogRepository : BaseRepository<OperateLog>, IOperateLogRepository
  {
    public OperateLogRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
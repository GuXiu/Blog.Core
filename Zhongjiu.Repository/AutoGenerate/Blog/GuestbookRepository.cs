﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class GuestbookRepository : BaseRepository<Guestbook>, IGuestbookRepository
  {
    public GuestbookRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
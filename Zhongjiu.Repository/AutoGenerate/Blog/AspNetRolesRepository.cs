﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class AspNetRolesRepository : BaseRepository<AspNetRoles>, IAspNetRolesRepository
  {
    public AspNetRolesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
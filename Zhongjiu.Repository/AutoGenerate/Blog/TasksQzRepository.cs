﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class TasksQzRepository : BaseRepository<TasksQz>, ITasksQzRepository
  {
    public TasksQzRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
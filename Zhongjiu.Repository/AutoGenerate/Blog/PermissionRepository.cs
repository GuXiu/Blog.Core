﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class PermissionRepository : BaseRepository<Permission>, IPermissionRepository
  {
    public PermissionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
﻿using Zhongjiu.Model.Blog;
using Zhongjiu.IRepository;
using Zhongjiu.IRepository.Blog;

namespace Zhongjiu.Repository.Blog
{
    ///<summary>
    ///
    ///</summary>
  public partial class ModulesRepository : BaseRepository<Modules>, IModulesRepository
  {
    public ModulesRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
    }
  }
}
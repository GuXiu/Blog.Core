﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model.Models;

namespace Zhongjiu.Services
{
  public partial class TopicDetailService : BaseServices<TopicDetail, IBaseRepository<TopicDetail>>, ITopicDetailService
    {
        /// <summary>
        /// 获取开Bug数据（缓存）
        /// </summary>
        /// <returns></returns>
        [Caching(AbsoluteExpiration = 10)]
        public async Task<List<TopicDetail>> GetTopicDetails()
        {
            return await base.Query(a => !a.tdIsDelete && a.tdSectendDetail == "tbug");
        }
    }
}

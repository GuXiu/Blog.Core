﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model;

namespace Zhongjiu.Services
{
  /// <summary>
  /// 
  /// </summary>
  /// <typeparam name="TEntity">泛型模型</typeparam>
  /// <typeparam name="TRep">泛型repository</typeparam>
  public partial class BaseServices<TEntity,TRep> : IBaseServices<TEntity> where TEntity : class, new() where TRep : IBaseRepository<TEntity>
  {
    /// <summary>
    /// 属性注入的Repository
    /// </summary>
    public TRep Rep { get; protected set; }

    public async Task<TEntity> QueryById(object objId)
    {
      var result = await Rep.QueryById(objId);
      return result;
    }
    /// <summary>
    /// 功能描述:根据ID查询一条数据
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="objId">id（必须指定主键特性 [SugarColumn(IsPrimaryKey=true)]），如果是联合主键，请使用Where条件</param>
    /// <param name="blnUseCache">是否使用缓存</param>
    /// <returns>数据实体</returns>
    public async Task<TEntity> QueryById(object objId, bool blnUseCache = false)
    {
      var result = await Rep.QueryById(objId, blnUseCache);
      return result;
    }

    /// <summary>
    /// 功能描述:根据ID查询数据
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="lstIds">id列表（必须指定主键特性 [SugarColumn(IsPrimaryKey=true)]），如果是联合主键，请使用Where条件</param>
    /// <returns>数据实体列表</returns>
    public async Task<List<TEntity>> QueryByIDs(object[] lstIds)
    {
      var result = await Rep.QueryByIDs(lstIds);
      return result;
    }

    /// <summary>
    /// 写入实体数据
    /// </summary>
    /// <param name="entity">博文实体类</param>
    /// <returns></returns>
    public async Task<int> Add(TEntity entity)
    {
      var result = await Rep.Add(entity);
      return result;
    }

    /// <summary>
    /// 批量插入实体(速度快)
    /// </summary>
    /// <param name="listEntity">实体集合</param>
    /// <returns>影响行数</returns>
    public async Task<int> Add(List<TEntity> listEntity)
    {
      var result = await Rep.Add(listEntity);
      return result;
    }

    /// <summary>
    /// 更新实体数据
    /// </summary>
    /// <param name="entity">博文实体类</param>
    /// <returns></returns>
    public async Task<bool> Update(TEntity entity)
    {
      var result = await Rep.Update(entity);
      return result;
    }
    public async Task<bool> Update(TEntity entity, string where)
    {
      var result = await Rep.Update(entity, where);
      return result;
    }
    public async Task<bool> Update(object operateAnonymousObjects)
    {
      var result = await Rep.Update(operateAnonymousObjects);
      return result;
    }

    public async Task<bool> Update(
     TEntity entity,
     List<string> lstColumns = null,
     List<string> lstIgnoreColumns = null,
     string where = ""
        )
    {
      var result = await Rep.Update(entity, lstColumns, lstIgnoreColumns, where);
      return result;
    }


    /// <summary>
    /// 根据实体删除一条数据
    /// </summary>
    /// <param name="entity">博文实体类</param>
    /// <returns></returns>
    public async Task<bool> Delete(TEntity entity)
    {
      var result = await Rep.Delete(entity);
      return result;
    }

    /// <summary>
    /// 删除指定ID的数据
    /// </summary>
    /// <param name="id">主键ID</param>
    /// <returns></returns>
    public async Task<bool> DeleteById(object id)
    {
      var result = await Rep.DeleteById(id);
      return result;
    }

    /// <summary>
    /// 删除指定ID集合的数据(批量删除)
    /// </summary>
    /// <param name="ids">主键ID集合</param>
    /// <returns></returns>
    public async Task<bool> DeleteByIds(object[] ids)
    {
      var result = await Rep.DeleteByIds(ids);
      return result;
    }



    /// <summary>
    /// 功能描述:查询所有数据
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query()
    {
      var result = await Rep.Query();
      return result;
    }

    /// <summary>
    /// 功能描述:查询数据列表
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="where">条件</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(string where)
    {
      var result = await Rep.Query(where);
      return result;
    }

    /// <summary>
    /// 功能描述:查询数据列表
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="whereExpression">whereExpression</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression)
    {
      var result = await Rep.Query(whereExpression);
      return result;
    }

    /// <summary>
    /// 功能描述:按照特定列查询数据列表
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="expression"></param>
    /// <returns></returns>
    public async Task<List<TResult>> Query<TResult>(Expression<Func<TEntity, TResult>> expression)
    {
      var result = await Rep.Query(expression);
      return result;
    }

    /// <summary>
    /// 功能描述:按照特定列查询数据列表带条件排序
    /// 作　　者:Zhongjiu
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="strWhere">过滤条件</param>
    /// <param name="expression">查询实体条件</param>
    /// <param name="orderByFileds">排序条件</param>
    /// <returns></returns>

    public async Task<List<TResult>> Query<TResult>(Expression<Func<TEntity, TResult>> expression, Expression<Func<TEntity, bool>> whereExpression, string orderByFileds)
    {
      var result = await Rep.Query(expression, whereExpression, orderByFileds);
      return result;
    }

    /// <summary>
    /// 功能描述:查询一个列表
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="whereExpression">条件表达式</param>
    /// <param name="strOrderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, Expression<Func<TEntity, object>> orderByExpression, bool isAsc = true)
    {
      var result = await Rep.Query(whereExpression, orderByExpression, isAsc);
      return result;
    }

    public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, string orderByFileds)
    {
      var result = await Rep.Query(whereExpression, orderByFileds);
      return result;
    }

    /// <summary>
    /// 功能描述:查询一个列表
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="where">条件</param>
    /// <param name="orderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(string where, string orderByFileds)
    {
      var result = await Rep.Query(where, orderByFileds);
      return result;
    }

    /// <summary>
    /// 根据sql语句查询
    /// </summary>
    /// <param name="sql">完整的sql语句</param>
    /// <param name="parameters">参数</param>
    /// <returns>泛型集合</returns>
    public async Task<List<TEntity>> QuerySql(string sql, SugarParameter[] parameters = null)
    {
      var result = await Rep.QuerySql(sql, parameters);
      return result;

    }

    /// <summary>
    /// 根据sql语句查询
    /// </summary>
    /// <param name="sql">完整的sql语句</param>
    /// <param name="parameters">参数</param>
    /// <returns>DataTable</returns>
    public async Task<DataTable> QueryTable(string sql, SugarParameter[] parameters = null)
    {
      var result = await Rep.QueryTable(sql, parameters);
      return result;

    }
    /// <summary>
    /// 功能描述:查询前N条数据
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="whereExpression">条件表达式</param>
    /// <param name="top">前N条</param>
    /// <param name="orderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(Expression<Func<TEntity, bool>> whereExpression, int top, string orderByFileds)
    {
      var result = await Rep.Query(whereExpression, top, orderByFileds);
      return result;
    }

    /// <summary>
    /// 功能描述:查询前N条数据
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="where">条件</param>
    /// <param name="top">前N条</param>
    /// <param name="orderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(
        string where,
        int top,
        string orderByFileds)
    {
      var result = await Rep.Query(where, top, orderByFileds);
      return result;
    }

    /// <summary>
    /// 功能描述:分页查询
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="whereExpression">条件表达式</param>
    /// <param name="pageIndex">页码（下标0）</param>
    /// <param name="pageSize">页大小</param>
    /// <param name="orderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(
        Expression<Func<TEntity, bool>> whereExpression,
        int pageIndex,
        int pageSize,
        string orderByFileds)
    {
      var result = await Rep.Query(
        whereExpression,
        pageIndex,
        pageSize,
        orderByFileds);
      return result;
    }

    /// <summary>
    /// 功能描述:分页查询
    /// 作　　者:AZLinli.Zhongjiu
    /// </summary>
    /// <param name="where">条件</param>
    /// <param name="pageIndex">页码（下标0）</param>
    /// <param name="pageSize">页大小</param>
    /// <param name="orderByFileds">排序字段，如name asc,age desc</param>
    /// <returns>数据列表</returns>
    public async Task<List<TEntity>> Query(
      string where,
      int pageIndex,
      int pageSize,
      string orderByFileds)
    {
      var result = await Rep.Query(
      where,
      pageIndex,
      pageSize,
      orderByFileds);
      return result;
    }
    //[Common.Caching]
    public async Task<PageModel<TEntity>> QueryPage(Expression<Func<TEntity, bool>> whereExpression,
    int pageIndex = 1, int pageSize = 20, string orderByFileds = null)
    {
      var result = await Rep.QueryPage(whereExpression, pageIndex, pageSize, orderByFileds);
      return result;
    }

    public async Task<List<TResult>> QueryMuch<T, T2, T3, TResult>(Expression<Func<T, T2, T3, object[]>> joinExpression, Expression<Func<T, T2, T3, TResult>> selectExpression, Expression<Func<T, T2, T3, bool>> whereLambda = null) where T : class, new()
    {
      var result = await Rep.QueryMuch(joinExpression, selectExpression, whereLambda);
      return result;
    }
    public async Task<PageModel<TEntity>> QueryPage(PaginationModel pagination)
    {
      var express = DynamicLinqFactory.CreateLambda<TEntity>(pagination.Conditions);
      var result = await QueryPage(express, pagination.PageIndex, pagination.PageSize, pagination.OrderByFileds);
      return result;
    }
  }

}

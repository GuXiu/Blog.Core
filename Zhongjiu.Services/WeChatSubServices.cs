using Microsoft.Extensions.Logging;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model.Models;

namespace Zhongjiu.Services
{
  /// <summary>
  /// WeChatSubServices
  /// </summary>
  public class WeChatSubServices : BaseServices<WeChatSub, IBaseRepository<WeChatSub>>, IWeChatSubServices
    {
        readonly IUnitOfWork _unitOfWork;
        readonly ILogger<WeChatSubServices> _logger;
        public WeChatSubServices(IUnitOfWork unitOfWork, ILogger<WeChatSubServices> logger)//:base(dal)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
        }  
        
    }
}
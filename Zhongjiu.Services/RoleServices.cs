using System.Linq;
using System.Threading.Tasks;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;

namespace Zhongjiu.Services
{
  /// <summary>
  /// RoleServices
  /// </summary>	
  public partial class RoleServices : BaseServices<Role, IBaseRepository<Role>>, IRoleServices
  {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleName"></param>
    /// <returns></returns>
    public async Task<Role> SaveRole(string roleName)
    {
      Role role = new Role(roleName);
      Role model = new Role();
      var userList = await base.Query(a => a.Name == role.Name && a.Enabled);
      if (userList.Count() > 0)
      {
        model = userList.FirstOrDefault();
      }
      else
      {
        var id = await base.Add(role);
        model = await base.QueryById(id);
      }

      return model;

    }

    [Caching(AbsoluteExpiration = 30)]
    public async Task<string> GetRoleNameByRid(int rid)
    {
      return ((await base.QueryById(rid))?.Name);
    }
  }
}

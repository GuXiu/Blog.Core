﻿using System;
using System.Threading.Tasks;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model;
using Zhongjiu.Model.Blog;
using Zhongjiu.Model.Models;
namespace Zhongjiu.Services
{
  public partial class GuestbookService : BaseServices<Guestbook, IBaseRepository<Guestbook>>, IGuestbookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBaseRepository<PasswordLib> _passwordLibRepository;
        public GuestbookService(IUnitOfWork unitOfWork, IBaseRepository<PasswordLib> passwordLibRepository)//:base(dal)
        {
            _unitOfWork = unitOfWork;
            _passwordLibRepository = passwordLibRepository;
        }

        public async Task<MessageModel<string>> TestTranInRepository()
        {
            try
            {
                Console.WriteLine($"");
                Console.WriteLine($"事务操作开始");
                _unitOfWork.BeginTran();
                Console.WriteLine($"");

                Console.WriteLine($"insert a data into the table PasswordLib now.");
                var insertPassword = await _passwordLibRepository.Add(new PasswordLib()
                {
                    IsDeleted = false,
                    plAccountName = "aaa",
                    plCreateTime = DateTime.Now
                });


                var passwords = await _passwordLibRepository.Query(d => d.IsDeleted == false);
                Console.WriteLine($"second time : the count of passwords is :{passwords.Count}");

                //......

                Console.WriteLine($"");
                var guestbooks = await Rep.Query();
                Console.WriteLine($"first time : the count of guestbooks is :{guestbooks.Count}");

                int ex = 0;
                Console.WriteLine($"\nThere's an exception!!");
                int throwEx = 1 / ex;

                Console.WriteLine($"insert a data into the table Guestbook now.");
                var insertGuestbook = await Rep.Add(new Guestbook()
                {
                    username = "bbb",
                    blogId = 1,
                    createdate = DateTime.Now,
                    isshow = true
                });

                guestbooks = await Rep.Query();
                Console.WriteLine($"second time : the count of guestbooks is :{guestbooks.Count}");


                _unitOfWork.CommitTran();

                return new MessageModel<string>()
                {
                    IsSuccess = true,
                    Msg = "操作完成"
                };
            }
            catch (Exception)
            {
                _unitOfWork.RollbackTran();
                var passwords = await _passwordLibRepository.Query();
                Console.WriteLine($"third time : the count of passwords is :{passwords.Count}");

                var guestbooks = await Rep.Query();
                Console.WriteLine($"third time : the count of guestbooks is :{guestbooks.Count}");

                return new MessageModel<string>()
                {
                    IsSuccess = false,
                    Msg = "操作异常"
                };
            }
        }

        [UseTran]
        public async Task<bool> TestTranInRepositoryAOP()
        {
            var passwords = await _passwordLibRepository.Query();
            Console.WriteLine($"first time : the count of passwords is :{passwords.Count}");


            Console.WriteLine($"insert a data into the table PasswordLib now.");
            var insertPassword = await _passwordLibRepository.Add(new PasswordLib()
            {
                IsDeleted = false,
                plAccountName = "aaa",
                plCreateTime = DateTime.Now
            });


            passwords = await _passwordLibRepository.Query(d => d.IsDeleted == false);
            Console.WriteLine($"second time : the count of passwords is :{passwords.Count}");

            //......

            Console.WriteLine($"");
            var guestbooks = await Rep.Query();
            Console.WriteLine($"first time : the count of guestbooks is :{guestbooks.Count}");

            int ex = 0;
            Console.WriteLine($"\nThere's an exception!!");
            int throwEx = 1 / ex;

            Console.WriteLine($"insert a data into the table Guestbook now.");
            var insertGuestbook = await Rep.Add(new Guestbook()
            {
                username = "bbb",
                blogId = 1,
                createdate = DateTime.Now,
                isshow = true
            });

            guestbooks = await Rep.Query();
            Console.WriteLine($"second time : the count of guestbooks is :{guestbooks.Count}");

            return true;
        }

    }
}

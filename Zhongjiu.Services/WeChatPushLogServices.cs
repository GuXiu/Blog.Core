using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model;using Zhongjiu.Model.Models;using Zhongjiu.Model.Models;
using Zhongjiu.Model;using Zhongjiu.Model.Models;using Zhongjiu.Model.Models;
using Zhongjiu.Model;
using Zhongjiu.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Zhongjiu.Services
{
    /// <summary>
	/// WeChatPushLogServices
	/// </summary>
    public class WeChatPushLogServices : BaseServices<WeChatPushLog, IBaseRepository<WeChatPushLog>>, IWeChatPushLogServices
    {
        readonly IUnitOfWork _unitOfWork;
        readonly ILogger<WeChatPushLogServices> _logger;
        public WeChatPushLogServices(IUnitOfWork unitOfWork, ILogger<WeChatPushLogServices> logger)//:base(dal)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
        }  
        
    }
}
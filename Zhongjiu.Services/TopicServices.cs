﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zhongjiu.Common;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model.Blog;

namespace Zhongjiu.Services
{
  public partial class TopicService: BaseServices<Topic, IBaseRepository<Topic>>, ITopicService
    {

        /// <summary>
        /// 获取开Bug专题分类（缓存）
        /// </summary>
        /// <returns></returns>
        [Caching(AbsoluteExpiration = 60)]
        public async Task<List<Topic>> GetTopics()
        {
            return await base.Query(a => !a.tIsDelete && a.tSectendDetail == "tbug");
        }

    }
}

using Microsoft.Extensions.Logging;
using Zhongjiu.IRepository;
using Zhongjiu.IServices;
using Zhongjiu.Model.Models;

namespace Zhongjiu.Services
{
  /// <summary>
  /// WeChatCompanyServices
  /// </summary>
  public class WeChatCompanyServices : BaseServices<WeChatCompany,IBaseRepository<WeChatCompany>>, IWeChatCompanyServices
    {
        readonly IUnitOfWork _unitOfWork;
        readonly ILogger<WeChatCompanyServices> _logger;
        public WeChatCompanyServices(IUnitOfWork unitOfWork, ILogger<WeChatCompanyServices> logger)//:base(dal)
        {
            this._unitOfWork = unitOfWork;
            this._logger = logger;
        }  
        
    }
}
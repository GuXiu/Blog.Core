﻿using Zhongjiu.IServices;
using Zhongjiu.Model.Models;
using Zhongjiu.IRepository;

namespace Zhongjiu.Services
{
  /// <summary>
  /// DepartmentServices
  /// </summary>
  public partial class DepartmentServices : BaseServices<Department, IBaseRepository<Department>>, IDepartmentServices
  {
  }
}
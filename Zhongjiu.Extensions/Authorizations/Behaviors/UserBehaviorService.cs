﻿using Zhongjiu.Common;
using Zhongjiu.IServices;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Zhongjiu.Extensions
{
    public class UserBehaviorService : IUserBehaviorService
    {
        private readonly IUser _user;
        private readonly ISysUserInfoServices _SysUserInfoServices;
        private readonly ILogger<UserBehaviorService> _logger;
        private readonly string _uid;
        private readonly string _token;

        public UserBehaviorService(IUser user
            , ISysUserInfoServices SysUserInfoServices
            , ILogger<UserBehaviorService> logger)
        {
            _user = user;
            _SysUserInfoServices = SysUserInfoServices;
            _logger = logger;
            _uid = _user.ID.ToString();
            _token = _user.GetToken();
        }


        public Task<bool> CheckTokenIsNormal()
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> CheckUserIsNormal()
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> CreateOrUpdateUserAccessByUid()
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> RemoveAllUserAccessByUid()
        {
            throw new System.NotImplementedException();
        }
    }
}

﻿using Zhongjiu.AuthHelper.Policys;
using Microsoft.AspNetCore.Http;
using Zhongjiu.Common;
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Zhongjiu.Extensions
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ExceptionHandlerMidd));
        ILogger<ExceptionHandlerMiddleware> log;
        public ExceptionHandlerMiddleware(RequestDelegate next,ILogger<ExceptionHandlerMiddleware> _log)
        {
            _next = next;
            log = _log;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception e)
        {
            if (e == null) return;

      var baseException = e.GetBaseException();
            log.LogError(e,e.Message);

            await WriteExceptionAsync(context, e).ConfigureAwait(false);
        }

        private static async Task WriteExceptionAsync(HttpContext context, Exception e)
        {
            if (e is UnauthorizedAccessException)
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            else if (e is Exception)
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(new ApiResponse(StatusCode.CODE500, e.Message).MessageModel.ToJson()).ConfigureAwait(false);
        }
    }
}

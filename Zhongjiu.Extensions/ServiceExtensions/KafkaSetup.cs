﻿using Zhongjiu.Common;
using Zhongjiu.EventBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Zhongjiu.Extensions
{
    /// <summary>
    /// 注入Kafka相关配置
    /// </summary>
   public static class KafkaSetup
    {
        public static void AddKafkaSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            if (Appsettings.app(new string[] { "Kafka", "Enabled" }).ToBool())
            {
                services.Configure<KafkaOptions>(Appsettings.GetSection("kafka"));
                services.AddSingleton<IKafkaConnectionPool,KafkaConnectionPool>();
            }
        }
    }
}

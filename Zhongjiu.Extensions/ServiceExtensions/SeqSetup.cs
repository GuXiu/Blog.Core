﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Zhongjiu.Common;

namespace Zhongjiu.Extensions
{
    //abort;作废，使用serilog功能丰富，eg：可同时保存本地、seq


    //配置文件
    //"Seq": {直接使用Seq时的Section结构
    //  "Enabled": true,
    //  "ServerUrl": "http://8.141.61.134:8888",
    //  "ApiKey": "SXdLEq7rWZCJnafHhK7I", //->"zhongjiuapi",
    //  "MinimumLevel": "Infomation",
    //  "LevelOverride": {
    //    "Microsoft": "Infomation",
    //    "LoggingService": "Infomation"
    //  }
    //}



    /// <summary>
    /// 
    /// </summary>
    //public static class SeqSetup
    //{
    //    public static void AddSeqSetup(this IServiceCollection services)
    //    {
    //        if (services == null) throw new ArgumentNullException(nameof(services));

    //        try
    //        {
    //            if (Appsettings.app("Logging:Seq:Enabled").ToBool())
    //            {
    //                services.AddLogging(loggerBuilder =>
    //                {
    //                    //loggerBuilder.AddSeq(Appsettings.app("Logging", "Seq", "ServerUrl"), Appsettings.app("Logging", "Seq", "ApiKey"));
    //                    //或者：使用Section配置文件；需要把Seq节点放到根目录；
    //                    loggerBuilder.AddSeq(Appsettings.GetSection("Logging:Seq"));
    //                });
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            //LoggerFactory.Create()
    //        }
    //    }
    //}
}

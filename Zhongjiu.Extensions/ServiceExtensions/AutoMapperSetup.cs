﻿using AutoMapper;
using Zhongjiu.AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Zhongjiu.Extensions
{
    /// <summary>
    /// Automapper 启动服务
    /// </summary>
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            //方法一：
            services.AddAutoMapper(typeof(AutoMapperConfig));
            AutoMapperConfig.RegisterMappings();

            //方法二：
            //services.AddAutoMapper(m => m.AddProfile(new CustomProfile()));

        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Zhongjiu.Common;

namespace Zhongjiu.Extensions
{
  public class HostBuilderExtension
  {
    public static void ConfigureAppConfiguration(HostBuilderContext context, IConfigurationBuilder builder)
    {
      var config = builder.Build();
      Appsettings.Init(config);
      //JsonConfigSettings.Configuration = config;
      //config.Add()

      //config.AddJsonStream()

      //Consul配置中心
      builder.AddConsulConfigCenterSetup(context);
    }
  }
}
